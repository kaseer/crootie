import 'dart:convert';
// final String tableCategories = "Categories";

List<CartData> cartResponseFromJson(String str) {
  final jsonData = json.decode(str);
  return new List<CartData>.from(
      jsonData.map((x) => CartData.fromJson(x)));
}

class CartData {
  int productId;
  int quantity;
  CartData(
    {
      required this.productId,
      required this.quantity,
  });

  factory CartData.fromJson(Map<String, dynamic> json) =>
      new CartData(
        productId: json["productId"],
        quantity: json["quantity"],
      );
}