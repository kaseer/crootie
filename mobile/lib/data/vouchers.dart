class VouchersData {
  int cardId;
  String logo;
  String description;
  int topColor;
  int bottomColor;
  double price;
  VouchersData(
      {required this.cardId,
      required this.description,
      required this.logo,
      required this.bottomColor,
      required this.topColor,
      required this.price});
}

class VouchersList {
  VouchersList._();
  static List<VouchersData> vouchers = [
    VouchersData(
        cardId: 1,
        description: "25",
        logo: "Almadar.png",
        topColor: 0xff0F7C10,
        bottomColor: 0xff21AC17,
        price: 30),
    VouchersData(
        cardId: 2,
        description: "45",
        logo: "Amazon.png",
        topColor: 0xff4D4D4D,
        bottomColor: 0xff4D4D4D,
        price: 30),
    VouchersData(
        cardId: 3,
        description: "10",
        logo: "Apple.png",
        topColor: 0xff787878,
        bottomColor: 0xffD6D6D6,
        price: 30),
    VouchersData(
        cardId: 4,
        description: "15",
        logo: "Netflix.png",
        topColor: 0xff0C0C0C,
        bottomColor: 0xff531919,
        price: 30),
    VouchersData(
        cardId: 5,
        description: "25",
        logo: "Spotify.png",
        topColor: 0xff434240,
        bottomColor: 0xff110C0C,
        price: 30),
    VouchersData(
        cardId: 6,
        description: "60",
        logo: "Xbox.png",
        topColor: 0xff0F7C10,
        bottomColor: 0xff21AC17,
        price: 30),
    VouchersData(
        cardId: 7,
        description: "100",
        logo: "Libyana.png",
        topColor: 0xff8A2686,
        bottomColor: 0xff51104F,
        price: 30),
    VouchersData(
        cardId: 8,
        description: "999",
        logo: "Apple.png",
        topColor: 0xff787878,
        bottomColor: 0xffD6D6D6,
        price: 30),
    VouchersData(
        cardId: 9,
        description: "500",
        logo: "Almadar.png",
        topColor: 0xff0F7C10,
        bottomColor: 0xff21AC17,
        price: 30),
  ];
}



// List<VouchersData> vouchers = [
//   VouchersData(
//       cardId: 1,
//       description: "25",
//       logo: "Almadar.png",
//       topColor: 0xff0F7C10,
//       bottomColor: 0xff21AC17,
//       price: 30),
//   VouchersData(
//       cardId: 2,
//       description: "45",
//       logo: "Amazon.png",
//       topColor: 0xff4D4D4D,
//       bottomColor: 0xff4D4D4D,
//       price: 30),
//   VouchersData(
//       cardId: 3,
//       description: "10",
//       logo: "Apple.png",
//       topColor: 0xff787878,
//       bottomColor: 0xffD6D6D6,
//       price: 30),
//   VouchersData(
//       cardId: 4,
//       description: "15",
//       logo: "Netflix.png",
//       topColor: 0xff0C0C0C,
//       bottomColor: 0xff531919,
//       price: 30),
//   VouchersData(
//       cardId: 5,
//       description: "25",
//       logo: "Spotify.png",
//       topColor: 0xff434240,
//       bottomColor: 0xff110C0C,
//       price: 30),
//   VouchersData(
//       cardId: 6,
//       description: "60",
//       logo: "Xbox.png",
//       topColor: 0xff0F7C10,
//       bottomColor: 0xff21AC17,
//       price: 30),
//   VouchersData(
//       cardId: 7,
//       description: "100",
//       logo: "Libyana.png",
//       topColor: 0xff8A2686,
//       bottomColor: 0xff51104F,
//       price: 30),
//   VouchersData(
//       cardId: 8,
//       description: "999",
//       logo: "Apple.png",
//       topColor: 0xff787878,
//       bottomColor: 0xffD6D6D6,
//       price: 30),
//   VouchersData(
//       cardId: 9,
//       description: "500",
//       logo: "Almadar.png",
//       topColor: 0xff0F7C10,
//       bottomColor: 0xff21AC17,
//       price: 30),
// ];


// List<VouchersData> vouchers = [
//   VouchersData(
//       cardId: 1,
//       description: "25",
//       logo: "Almadar.png",
//       topColor: 0xff0F7C10,
//       bottomColor: 0xff21AC17,
//       price: 30),
//   VouchersData(
//       cardId: 2,
//       description: "45",
//       logo: "Amazon.png",
//       topColor: 0xff4D4D4D,
//       bottomColor: 0xff4D4D4D,
//       price: 30),
//   VouchersData(
//       cardId: 3,
//       description: "10",
//       logo: "Apple.png",
//       topColor: 0xff787878,
//       bottomColor: 0xffD6D6D6,
//       price: 30),
//   VouchersData(
//       cardId: 4,
//       description: "15",
//       logo: "Netflix.png",
//       topColor: 0xff0C0C0C,
//       bottomColor: 0xff531919,
//       price: 30),
//   VouchersData(
//       cardId: 5,
//       description: "25",
//       logo: "Spotify.png",
//       topColor: 0xff434240,
//       bottomColor: 0xff110C0C,
//       price: 30),
//   VouchersData(
//       cardId: 6,
//       description: "60",
//       logo: "Xbox.png",
//       topColor: 0xff0F7C10,
//       bottomColor: 0xff21AC17,
//       price: 30),
//   VouchersData(
//       cardId: 7,
//       description: "100",
//       logo: "Libyana.png",
//       topColor: 0xff8A2686,
//       bottomColor: 0xff51104F,
//       price: 30),
//   VouchersData(
//       cardId: 8,
//       description: "999",
//       logo: "Apple.png",
//       topColor: 0xff787878,
//       bottomColor: 0xffD6D6D6,
//       price: 30),
//   VouchersData(
//       cardId: 9,
//       description: "500",
//       logo: "Almadar.png",
//       topColor: 0xff0F7C10,
//       bottomColor: 0xff21AC17,
//       price: 30),
// ];


// class VouchersData {
//   int cardId;
//   String logo;
//   String description;
//   int topColor;
//   int bottomColor;
//   double price;
//   VouchersData(
//       {required this.cardId,
//       required this.description,
//       required this.logo,
//       required this.bottomColor,
//       required this.topColor,
//       required this.price});
// }

// List<VouchersData> vouchers = [
//   VouchersData(
//       cardId: 1,
//       description: "25",
//       logo: "Almadar.png",
//       topColor: 0xff0F7C10,
//       bottomColor: 0xff21AC17,
//       price: 30),
//   VouchersData(
//       cardId: 2,
//       description: "45",
//       logo: "Amazon.png",
//       topColor: 0xff4D4D4D,
//       bottomColor: 0xff4D4D4D,
//       price: 30),
//   VouchersData(
//       cardId: 3,
//       description: "10",
//       logo: "Apple.png",
//       topColor: 0xff787878,
//       bottomColor: 0xffD6D6D6,
//       price: 30),
//   VouchersData(
//       cardId: 4,
//       description: "15",
//       logo: "Netflix.png",
//       topColor: 0xff0C0C0C,
//       bottomColor: 0xff531919,
//       price: 30),
//   VouchersData(
//       cardId: 5,
//       description: "25",
//       logo: "Spotify.png",
//       topColor: 0xff434240,
//       bottomColor: 0xff110C0C,
//       price: 30),
//   VouchersData(
//       cardId: 6,
//       description: "60",
//       logo: "Xbox.png",
//       topColor: 0xff0F7C10,
//       bottomColor: 0xff21AC17,
//       price: 30),
//   VouchersData(
//       cardId: 7,
//       description: "100",
//       logo: "Libyana.png",
//       topColor: 0xff8A2686,
//       bottomColor: 0xff51104F,
//       price: 30),
//   VouchersData(
//       cardId: 8,
//       description: "999",
//       logo: "Apple.png",
//       topColor: 0xff787878,
//       bottomColor: 0xffD6D6D6,
//       price: 30),
//   VouchersData(
//       cardId: 9,
//       description: "500",
//       logo: "Almadar.png",
//       topColor: 0xff0F7C10,
//       bottomColor: 0xff21AC17,
//       price: 30),
// ];


// class VouchersData {
//   int cardId;
//   String logo;
//   String description;
//   int topColor;
//   int bottomColor;
//   double price;
//   VouchersData(
//       {required this.cardId,
//       required this.description,
//       required this.logo,
//       required this.bottomColor,
//       required this.topColor,
//       required this.price});
// }

// List<VouchersData> vouchers = [
//   VouchersData(
//       cardId: 1,
//       description: "25",
//       logo: "Almadar.png",
//       topColor: 0xff0F7C10,
//       bottomColor: 0xff21AC17,
//       price: 30),
//   VouchersData(
//       cardId: 2,
//       description: "45",
//       logo: "Amazon.png",
//       topColor: 0xff4D4D4D,
//       bottomColor: 0xff4D4D4D,
//       price: 30),
//   VouchersData(
//       cardId: 3,
//       description: "10",
//       logo: "Apple.png",
//       topColor: 0xff787878,
//       bottomColor: 0xffD6D6D6,
//       price: 30),
//   VouchersData(
//       cardId: 4,
//       description: "15",
//       logo: "Netflix.png",
//       topColor: 0xff0C0C0C,
//       bottomColor: 0xff531919,
//       price: 30),
//   VouchersData(
//       cardId: 5,
//       description: "25",
//       logo: "Spotify.png",
//       topColor: 0xff434240,
//       bottomColor: 0xff110C0C,
//       price: 30),
//   VouchersData(
//       cardId: 6,
//       description: "60",
//       logo: "Xbox.png",
//       topColor: 0xff0F7C10,
//       bottomColor: 0xff21AC17,
//       price: 30),
//   VouchersData(
//       cardId: 7,
//       description: "100",
//       logo: "Libyana.png",
//       topColor: 0xff8A2686,
//       bottomColor: 0xff51104F,
//       price: 30),
//   VouchersData(
//       cardId: 8,
//       description: "999",
//       logo: "Apple.png",
//       topColor: 0xff787878,
//       bottomColor: 0xffD6D6D6,
//       price: 30),
//   VouchersData(
//       cardId: 9,
//       description: "500",
//       logo: "Almadar.png",
//       topColor: 0xff0F7C10,
//       bottomColor: 0xff21AC17,
//       price: 30),
// ];

// class VouchersData {
//   int cardId;
//   String logo;
//   String description;
//   int topColor;
//   int bottomColor;
//   double price;
//   VouchersData(
//       {required this.cardId,
//       required this.description,
//       required this.logo,
//       required this.bottomColor,
//       required this.topColor,
//       required this.price});
// }

// List<VouchersData> vouchers = [
//   VouchersData(
//       cardId: 1,
//       description: "25",
//       logo: "Almadar.png",
//       topColor: 0xff0F7C10,
//       bottomColor: 0xff21AC17,
//       price: 30),
//   VouchersData(
//       cardId: 2,
//       description: "45",
//       logo: "Amazon.png",
//       topColor: 0xff4D4D4D,
//       bottomColor: 0xff4D4D4D,
//       price: 30),
//   VouchersData(
//       cardId: 3,
//       description: "10",
//       logo: "Apple.png",
//       topColor: 0xff787878,
//       bottomColor: 0xffD6D6D6,
//       price: 30),
//   VouchersData(
//       cardId: 4,
//       description: "15",
//       logo: "Netflix.png",
//       topColor: 0xff0C0C0C,
//       bottomColor: 0xff531919,
//       price: 30),
//   VouchersData(
//       cardId: 5,
//       description: "25",
//       logo: "Spotify.png",
//       topColor: 0xff434240,
//       bottomColor: 0xff110C0C,
//       price: 30),
//   VouchersData(
//       cardId: 6,
//       description: "60",
//       logo: "Xbox.png",
//       topColor: 0xff0F7C10,
//       bottomColor: 0xff21AC17,
//       price: 30),
//   VouchersData(
//       cardId: 7,
//       description: "100",
//       logo: "Libyana.png",
//       topColor: 0xff8A2686,
//       bottomColor: 0xff51104F,
//       price: 30),
//   VouchersData(
//       cardId: 8,
//       description: "999",
//       logo: "Apple.png",
//       topColor: 0xff787878,
//       bottomColor: 0xffD6D6D6,
//       price: 30),
//   VouchersData(
//       cardId: 9,
//       description: "500",
//       logo: "Almadar.png",
//       topColor: 0xff0F7C10,
//       bottomColor: 0xff21AC17,
//       price: 30),
// ];


// class VouchersData {
//   int cardId;
//   String logo;
//   String description;
//   int topColor;
//   int bottomColor;
//   double price;
//   VouchersData(
//       {required this.cardId,
//       required this.description,
//       required this.logo,
//       required this.bottomColor,
//       required this.topColor,
//       required this.price});
// }

// List<VouchersData> vouchers = [
//   VouchersData(
//       cardId: 1,
//       description: "25",
//       logo: "Almadar.png",
//       topColor: 0xff0F7C10,
//       bottomColor: 0xff21AC17,
//       price: 30),
//   VouchersData(
//       cardId: 2,
//       description: "45",
//       logo: "Amazon.png",
//       topColor: 0xff4D4D4D,
//       bottomColor: 0xff4D4D4D,
//       price: 30),
//   VouchersData(
//       cardId: 3,
//       description: "10",
//       logo: "Apple.png",
//       topColor: 0xff787878,
//       bottomColor: 0xffD6D6D6,
//       price: 30),
//   VouchersData(
//       cardId: 4,
//       description: "15",
//       logo: "Netflix.png",
//       topColor: 0xff0C0C0C,
//       bottomColor: 0xff531919,
//       price: 30),
//   VouchersData(
//       cardId: 5,
//       description: "25",
//       logo: "Spotify.png",
//       topColor: 0xff434240,
//       bottomColor: 0xff110C0C,
//       price: 30),
//   VouchersData(
//       cardId: 6,
//       description: "60",
//       logo: "Xbox.png",
//       topColor: 0xff0F7C10,
//       bottomColor: 0xff21AC17,
//       price: 30),
//   VouchersData(
//       cardId: 7,
//       description: "100",
//       logo: "Libyana.png",
//       topColor: 0xff8A2686,
//       bottomColor: 0xff51104F,
//       price: 30),
//   VouchersData(
//       cardId: 8,
//       description: "999",
//       logo: "Apple.png",
//       topColor: 0xff787878,
//       bottomColor: 0xffD6D6D6,
//       price: 30),
//   VouchersData(
//       cardId: 9,
//       description: "500",
//       logo: "Almadar.png",
//       topColor: 0xff0F7C10,
//       bottomColor: 0xff21AC17,
//       price: 30),
// ];


// class VouchersData {
//   int cardId;
//   String logo;
//   String description;
//   int topColor;
//   int bottomColor;
//   VouchersData(
//       {required this.cardId,
//       required this.description,
//       required this.logo,
//       required this.bottomColor,
//       required this.topColor});
// }

// List<VouchersData> vouchers = [
//   VouchersData(
//     cardId: 1,
//     description: "25",
//     logo: "Almadar.png",
//     topColor: 0xff0F7C10,
//     bottomColor: 0xff21AC17,
//   ),
//   VouchersData(
//     cardId: 2,
//     description: "45",
//     logo: "Amazon.png",
//     topColor: 0xff4D4D4D,
//     bottomColor: 0xff4D4D4D,
//   ),
//   VouchersData(
//     cardId: 3,
//     description: "10",
//     logo: "Apple.png",
//     topColor: 0xff787878,
//     bottomColor: 0xffD6D6D6,
//   ),
//   VouchersData(
//     cardId: 4,
//     description: "15",
//     logo: "Netflix.png",
//     topColor: 0xff0C0C0C,
//     bottomColor: 0xff531919,
//   ),
//   VouchersData(
//     cardId: 5,
//     description: "25",
//     logo: "Spotify.png",
//     topColor: 0xff434240,
//     bottomColor: 0xff110C0C,
//   ),
//   VouchersData(
//     cardId: 6,
//     description: "60",
//     logo: "Xbox.png",
//     topColor: 0xff0F7C10,
//     bottomColor: 0xff21AC17,
//   ),
//   VouchersData(
//     cardId: 7,
//     description: "100",
//     logo: "Libyana.png",
//     topColor: 0xff8A2686,
//     bottomColor: 0xff51104F,
//   ),
//   VouchersData(
//     cardId: 8,
//     description: "999",
//     logo: "Apple.png",
//     topColor: 0xff787878,
//     bottomColor: 0xffD6D6D6,
//   ),
//   VouchersData(
//     cardId: 9,
//     description: "500",
//     logo: "Almadar.png",
//     topColor: 0xff0F7C10,
//     bottomColor: 0xff21AC17,
//   ),
// ];


// class Cat {
//   int categoryId;
//   String categoryName;
//   int topColor;
//   int bottomColor;
//   String illistration;
//   Cat(
//       {required this.categoryId,
//       required this.categoryName,
//       required this.topColor,
//       required this.bottomColor,
//       required this.illistration});
// }

// List<Cat> cat = [
//   Cat(
//       categoryId: 1,
//       categoryName: "Games",
//       topColor: 0xff434240,
//       bottomColor: 0xff110C0C,
//       illistration: "Gaming.png"),
//   Cat(
//       categoryId: 2,
//       categoryName: "INTERNET",
//       topColor: 0xffF58523,
//       bottomColor: 0xffFFCB9C,
//       illistration: "Internet.png"),
//   Cat(
//       categoryId: 3,
//       categoryName: "Telecom",
//       topColor: 0xff8A2686,
//       bottomColor: 0xff51104F,
//       illistration: "Telelcom.png"),
//   Cat(
//       categoryId: 4,
//       categoryName: "OnlineCard",
//       topColor: 0xff0C0C0C,
//       bottomColor: 0xff531919,
//       illistration: "Online_Card.png"),
//   Cat(
//       categoryId: 5,
//       categoryName: "OnlineCard",
//       topColor: 0xff0C0C0C,
//       bottomColor: 0xff531919,
//       illistration: "Online_Card.png"),
//   Cat(
//       categoryId: 6,
//       categoryName: "OnlineCard",
//       topColor: 0xff0C0C0C,
//       bottomColor: 0xff531919,
//       illistration: "Online_Card.png"),
//   Cat(
//       categoryId: 7,
//       categoryName: "OnlineCard",
//       topColor: 0xff0C0C0C,
//       bottomColor: 0xff531919,
//       illistration: "Online_Card.png"),
// ];
