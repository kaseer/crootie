class VouchersData {
  int? cardId;
  String? logo;
  String? description;
  int? topColor;
  int? bottomColor;
  double? price;
  VouchersData(
    {
      this.cardId,
      this.description,
      this.logo,
      this.bottomColor,
      this.topColor,
      this.price
  });
}