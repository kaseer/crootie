import 'package:Crootie/services/db/Database.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:Crootie/providers/ProviderList.dart';
import 'package:Crootie/screens/AppPage.dart';
import 'package:Crootie/shared/Routes.dart';
import 'package:Crootie/style/Style.dart';
import 'package:provider/provider.dart';
import 'locale/codegen_loader.g.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

//*********************************** ***********************************/
String? token;
Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  await Firebase.initializeApp();
  print('Handling a background message ${message.messageId}');
  print(message.data);
  flutterLocalNotificationsPlugin.show(
      message.data.hashCode,
      message.data['title'],
      message.data['body'],
      NotificationDetails(
        android: AndroidNotificationDetails(
          channel.id,
          channel.name,
          channel.description,
        ),
      ));
}
//*********************************** ***********************************/

const AndroidNotificationChannel channel = AndroidNotificationChannel(
  'high_importance_channel', // id
  'High Importance Notifications', // title
  'This channel is used for important notifications.', // description
  importance: Importance.high,
);
//*********************************** ***********************************/

final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();

//*********************************** ***********************************/

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  Firebase.initializeApp();
  await EasyLocalization.ensureInitialized();
  // await Firebase.initializeApp();
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
  await flutterLocalNotificationsPlugin
      .resolvePlatformSpecificImplementation<
          AndroidFlutterLocalNotificationsPlugin>()
      ?.createNotificationChannel(channel);

  await FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(
    alert: true,
    badge: true,
    sound: true,
  );

  runApp(
    EasyLocalization(
        supportedLocales: [
          Locale('en'),
          Locale('ar'),
        ],
        path: 'assets/locale', // <-- change the path of the translation files
        fallbackLocale: Locale('en'),
        assetLoader: CodegenLoader(),
        child: MyApp()),
  );
}
//***********************************Main WIDGET***********************************/

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  //*********************************** ***********************************/
  void initState() {
    // WidgetsFlutterBinding.ensureInitialized();
    // Firebase.initializeApp();
    super.initState();
    var initialzationSettingsAndroid =
        AndroidInitializationSettings('@mipmap/launch_logo');
    var initializationSettings =
        InitializationSettings(android: initialzationSettingsAndroid);

    flutterLocalNotificationsPlugin.initialize(initializationSettings);
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      RemoteNotification notification = message.notification!;
      AndroidNotification? android = message.notification?.android!;
      // ignore: unnecessary_null_comparison
      if (notification != null && android != null) {
        flutterLocalNotificationsPlugin.show(
            notification.hashCode,
            notification.title,
            notification.body,
            NotificationDetails(
              android: AndroidNotificationDetails(
                channel.id,
                channel.name,
                channel.description,
                icon: "launch_background",
              ),
            ));
      }
    });
    getToken();
    checkDatabase();
  }

  //*********************************** ***********************************/
  Future<void> checkDatabase() async {
    await CrootieDatabase().initDB('crootie.db');
    //print(c);
  }

  //*********************************** ***********************************/
  @override
  Widget build(BuildContext context) {
    // LocalStorge.reset();
    return MultiProvider(
      providers: ProviderList.providers,
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        localizationsDelegates: context.localizationDelegates,
        supportedLocales: context.supportedLocales,
        locale: context.locale,
        title: 'CROOTIE',
        theme: AppTheme.buildLightTheme(context.locale.toString()),
        home: AppPage(),
        routes: Routes.routes,
      ),
    );
  }

  //*********************************** ***********************************/
  getToken() async {
    token = await FirebaseMessaging.instance.getToken();
    setState(() {
      token = token;
    });
    print(token);
  }
}
// https://www.youtube.com/watch?v=PEUUYOQ2Ixo