import 'package:http/http.dart' as http;
import 'package:http/http.dart';

class Api {
  // Api._();
  String url = "192.168.0.204:6001";
  // String url = "192.168.99.2:6001";
  // String url = "192.168.149.203:6001";

  String getHeader(Map<String, String> fromQuery) {
    String query = "";
    int count = 0;
    fromQuery.entries.toList().forEach((element) {
      if (count == 0)
        query = query + "?";
      else
        query = query = query + "&";
      query = query + element.key.toString() + "=" + element.value.toString();
      count++;
      // element.key.toString().
    });
    return query;
  }

//**************************************** Post ********************************************* */
  Future<Response> post(String controller, Object? body) async {
    return http.post(Uri.http(this.url, controller),
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: body);
  }

  Future<Response> authorizationPost(String controller, Object? body) async {
    // var token = await getApplicationSavedInformation('token') ?? '';
    return http.post(Uri.http(this.url, controller), headers: {
      "Accept": "application/json",
      "content-type": "application/json",
      // 'Authorization': 'Bearer $token'
    }, body: {});
  }

//**************************************** Get ********************************************* */
  Future<Response> get(String controller, Map<String, String> fromQuery) async {
    String query = this.getHeader(fromQuery);

    return http.get(
      Uri.http(this.url, controller + query),
      headers: {
        "Accept": "application/json",
      },
    );
  }

  Future<Response> authorizationGet(
      String controller, Map<String, String> fromQuery) async {
    String query = this.getHeader(fromQuery);
    // var token = await getApplicationSavedInformation('token') ?? '';
    return http.get(
      Uri.http(this.url, controller + query),
      headers: {
        "Accept": "application/json",
        // 'Authorization': 'Bearer $token'
      },
    );
  }

//**************************************** Put ********************************************* */
  Future<Response> put(String controller, Object? body) async {
    return http.put(Uri.http(this.url, controller),
        headers: {
          "Accept": "application/json",
          "content-type": "application/json",
        },
        body: body);
  }

  Future<Response> authorizationPut(String controller, Object? body) async {
    // var token = await getApplicationSavedInformation('token') ?? '';
    return http.put(Uri.http(this.url, controller),
        headers: {
          "Accept": "application/json",
          "content-type": "application/json",
          // 'Authorization': 'Bearer $token'
        },
        body: body);
  }

//**************************************** Delete ********************************************* */
  Future<Response> delete(String controller, int id) async {
    // var token = await getApplicationSavedInformation('token') ?? '';
    return http.delete(
      Uri.http(this.url, controller + "/" + id.toString()),
      headers: {
        "Accept": "application/json",
        // 'Authorization': 'Bearer $token'
      },
    );
  }
}
