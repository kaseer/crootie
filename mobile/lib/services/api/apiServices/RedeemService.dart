import 'dart:async';
import 'dart:convert';

import 'package:Crootie/models/responseModels/redeem/RedeemResponse.dart';
import 'package:Crootie/services/api/Api.dart';
import 'package:Crootie/shared/GlobalFunctions.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';

String api = "/api/redeem";
Api instance = Api();

List<RedeemResponse> vouchers = [
  RedeemResponse(
      productId: 1,
      priceDescription: "25",
      logo: "Almadar.png",
      topColor: "0F7C10",
      bottomColor: "21AC17",
      points: 30),
  RedeemResponse(
      productId: 2,
      priceDescription: "45",
      logo: "Amazon.png",
      topColor: "4D4D4D",
      bottomColor: "4D4D4D",
      points: 30),
  RedeemResponse(
      productId: 3,
      priceDescription: "10",
      logo: "Apple.png",
      topColor: "787878",
      bottomColor: "D6D6D6",
      points: 30),
  RedeemResponse(
      productId: 4,
      priceDescription: "15",
      logo: "Netflix.png",
      topColor: "0C0C0C",
      bottomColor: "531919",
      points: 30),
  RedeemResponse(
      productId: 5,
      priceDescription: "25",
      logo: "Spotify.png",
      topColor: "434240",
      bottomColor: "110C0C",
      points: 30),
  RedeemResponse(
      productId: 6,
      priceDescription: "60",
      logo: "Xbox.png",
      topColor: "0F7C10",
      bottomColor: "21AC17",
      points: 30),
  RedeemResponse(
      productId: 7,
      priceDescription: "100",
      logo: "Libyana.png",
      topColor: "8A2686",
      bottomColor: "51104F",
      points: 30),
  RedeemResponse(
      productId: 8,
      priceDescription: "999",
      logo: "Apple.png",
      topColor: "787878",
      bottomColor: "D6D6D6",
      points: 30),
  RedeemResponse(
      productId: 9,
      priceDescription: "500",
      logo: "Almadar.png",
      topColor: "0F7C10",
      bottomColor: "21AC17",
      points: 30),
];

class RedeemService {
//********************************************************************************************************/
  static Future<List<RedeemResponse>> get(BuildContext context) async {
    try {
      // Response? response =
      //     await instance.get(api , {}).timeout(const Duration(seconds: 10));
      // if (response!.statusCode != 200) {
      //   GlobalFunctions.errorSnackBar(context, response.body);
      //   return [];
      // }
      // var data = redeemResponseFromJson(response.body);
      return vouchers;
    } catch (ex) {
      GlobalFunctions.serverErrorSnackBar(context);
      return [];
    }
  }
}
