import 'dart:async';
import 'dart:convert';

import 'package:Crootie/models/responseModels/AllProductsResponse.dart';
import 'package:Crootie/models/responseModels/CategoriesResponse.dart';
import 'package:Crootie/models/responseModels/CompaniesResponse.dart';
import 'package:Crootie/models/responseModels/ProductsResponse.dart';
import 'package:Crootie/services/api/Api.dart';
import 'package:Crootie/services/db/Cart.dart';
import 'package:Crootie/services/db/Categories.dart';
import 'package:Crootie/services/db/Companies.dart';
import 'package:Crootie/services/db/Products.dart';
import 'package:Crootie/shared/GlobalFunctions.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';

String api = "/api/Lookup";
Api instance = Api();

class LookupService {
  LookupService._();
  static Future<List<CategoriesResponse>> getCategories(
      BuildContext context) async {
    try {
      Response response = await instance
          .get(api + "/getCategories", {}).timeout(const Duration(seconds: 10));
      if (response.statusCode != 200) {
        GlobalFunctions.errorSnackBar(context, response.body);
        return [];
      }
      var data = categoriesResponseFromJson(response.body);
      CategoriesDatabase.instance.create(data);
      return data;
    } on TimeoutException catch (_) {
      var sqfliteData = await CategoriesDatabase.instance.getAll();
      // if (sqfliteData.length <=0)
      GlobalFunctions.serverErrorSnackBar(context);
      return sqfliteData;
    } catch (ex) {
      GlobalFunctions.serverErrorSnackBar(context);
      return [];
    }
  }

//********************************************************************************************************/
  static Future<List<CompaniesResponse>> getCompanies(
      BuildContext context, int categoryId) async {
    try {
      Response response = await instance.get(
          api + "/getCompanies/${categoryId}",
          {}).timeout(const Duration(seconds: 10));
      if (response.statusCode != 200) {
        GlobalFunctions.errorSnackBar(context, response.body);
        return [];
      }
      var data = companiesResponseFromJson(response.body);
      CompaniesDatabase.instance.create(data, categoryId);
      return data;
    } on TimeoutException catch (_) {
      var sqfliteData = await CompaniesDatabase.instance.getAll(categoryId);
      // if (sqfliteData.length <=0)
      GlobalFunctions.serverErrorSnackBar(context);
      return sqfliteData;
    } catch (ex) {
      GlobalFunctions.serverErrorSnackBar(context);
      return [];
    }
  }

  //********************************************************************************************************/
  static Future<List<ProductsResponse>> getProducts(
      BuildContext context, int companyId) async {
    try {
      Response response = await instance.get(api + "/getProducts/${companyId}",
          {}).timeout(const Duration(seconds: 10));
      if (response.statusCode != 200) {
        GlobalFunctions.errorSnackBar(context, response.body);
        return [];
      }
      var data = productsResponseFromJson(response.body);
      ProductsDatabase.instance.create(data, companyId);
      return data;
    } on TimeoutException catch (_) {
      var sqfliteData = await ProductsDatabase.instance.getAll(companyId);
      // if (sqfliteData.length <=0)
      GlobalFunctions.serverErrorSnackBar(context);
      return sqfliteData;
    } catch (ex) {
      GlobalFunctions.serverErrorSnackBar(context);
      return [];
    }
  }

  //********************************************************************************************************/
  static Future<List<AllProductsResponse>> getAllProducts(
      BuildContext context) async {
    try {
      Response response = await instance.get(
          api + "/getAllProducts", {}).timeout(const Duration(seconds: 10));
      if (response.statusCode != 200) {
        GlobalFunctions.errorSnackBar(context, response.body);
        return [];
      }
      var data = allProductsResponseFromJson(response.body);
      return data;
    } catch (ex) {
      GlobalFunctions.serverErrorSnackBar(context);
      return [];
    }
  }

  //********************************************************************************************************/
  static Future<List<AllProductsResponse>> getCartProducts(
      BuildContext context) async {
    try {
      List<int> productIds = await CartDatabase.instance.getProductIds();
      Response response = await instance
          .post(api + "/getCartProducts", json.encode(productIds))
          .timeout(const Duration(seconds: 10));
      if (response.statusCode != 200) {
        GlobalFunctions.errorSnackBar(context, response.body);
        return [];
      }
      var data = allProductsResponseFromJson(response.body);
      var localCart = await CartDatabase.instance.getAll();
      for (var i = 0; i < localCart.length; i++) {
        int index = data.indexWhere(
            (element) => element.productId == localCart[i].productId);
        data[index].quantity = localCart[i].quantity;
      }

      return data;
    } on TimeoutException catch (_) {
      // if (sqfliteData.length <=0)
      GlobalFunctions.serverErrorSnackBar(context);
      return [];
    } catch (ex) {
      GlobalFunctions.serverErrorSnackBar(context);
      return [];
    }
  }
}
