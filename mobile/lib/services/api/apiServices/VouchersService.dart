import 'dart:async';
import 'package:Crootie/models/responseModels/vouchers/NewVouchersResponse.dart';
import 'package:Crootie/models/responseModels/vouchers/UsedVouchersResponse.dart';
import 'package:Crootie/providers/functions/ContactProvider.dart';
import 'package:Crootie/providers/functions/VouchersProvider.dart';
import 'package:Crootie/services/api/Api.dart';
import 'package:Crootie/shared/GlobalFunctions.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:provider/provider.dart';

String api = "/api/vouchers";
Api instance = Api();

List<NewVouchersResponse> newvouchers = [
  NewVouchersResponse(
    voucherId: 1,
    priceDescription: "25",
    logo: "Almadar.png",
    topColor: "0F7C10",
    bottomColor: "21AC17",
  ),
  NewVouchersResponse(
    voucherId: 2,
    priceDescription: "45",
    logo: "Amazon.png",
    topColor: "4D4D4D",
    bottomColor: "4D4D4D",
  ),
  NewVouchersResponse(
    voucherId: 3,
    priceDescription: "10",
    logo: "Apple.png",
    topColor: "787878",
    bottomColor: "D6D6D6",
  ),
  NewVouchersResponse(
    voucherId: 4,
    priceDescription: "15",
    logo: "Netflix.png",
    topColor: "0C0C0C",
    bottomColor: "531919",
  ),
  NewVouchersResponse(
    voucherId: 5,
    priceDescription: "25",
    logo: "Spotify.png",
    topColor: "434240",
    bottomColor: "110C0C",
  ),
  NewVouchersResponse(
    voucherId: 6,
    priceDescription: "60",
    logo: "Xbox.png",
    topColor: "0F7C10",
    bottomColor: "21AC17",
  ),
  NewVouchersResponse(
    voucherId: 7,
    priceDescription: "100",
    logo: "Libyana.png",
    topColor: "8A2686",
    bottomColor: "51104F",
  ),
  NewVouchersResponse(
    voucherId: 8,
    priceDescription: "999",
    logo: "Apple.png",
    topColor: "787878",
    bottomColor: "D6D6D6",
  ),
  NewVouchersResponse(
    voucherId: 9,
    priceDescription: "500",
    logo: "Almadar.png",
    topColor: "0F7C10",
    bottomColor: "21AC17",
  ),
];
List<UsedVouchersResponse> usedvouchers = [
  UsedVouchersResponse(
    voucherId: 1,
    priceDescription: "25",
    logo: "Almadar.png",
    topColor: "0F7C10",
    serialNo: "20 21 5021 01 541",
    bottomColor: "21AC17",
  ),
  UsedVouchersResponse(
    voucherId: 2,
    priceDescription: "45",
    logo: "Amazon.png",
    topColor: "4D4D4D",
    serialNo: "20 21 5021 01 541",
    bottomColor: "4D4D4D",
  ),
  UsedVouchersResponse(
    voucherId: 3,
    priceDescription: "10",
    logo: "Apple.png",
    topColor: "787878",
    serialNo: "20 21 5021 01 541",
    bottomColor: "D6D6D6",
  ),
  UsedVouchersResponse(
    voucherId: 4,
    priceDescription: "15",
    logo: "Netflix.png",
    topColor: "0C0C0C",
    serialNo: "20 21 5021 01 541",
    bottomColor: "531919",
  ),
  UsedVouchersResponse(
    voucherId: 5,
    priceDescription: "25",
    logo: "Spotify.png",
    topColor: "434240",
    serialNo: "20 21 5021 01 541",
    bottomColor: "110C0C",
  ),
  UsedVouchersResponse(
    voucherId: 6,
    priceDescription: "60",
    logo: "Xbox.png",
    topColor: "0F7C10",
    serialNo: "20 21 5021 01 541",
    bottomColor: "21AC17",
  ),
  UsedVouchersResponse(
    voucherId: 7,
    priceDescription: "100",
    logo: "Libyana.png",
    topColor: "8A2686",
    serialNo: "20 21 5021 01 541",
    bottomColor: "51104F",
  ),
  UsedVouchersResponse(
    voucherId: 8,
    priceDescription: "999",
    logo: "Apple.png",
    topColor: "787878",
    serialNo: "20 21 5021 01 541",
    bottomColor: "D6D6D6",
  ),
  UsedVouchersResponse(
    voucherId: 9,
    priceDescription: "500",
    logo: "Almadar.png",
    topColor: "0F7C10",
    serialNo: "20 21 5021 01 541",
    bottomColor: "21AC17",
  ),
];

class VouchersService {
  VouchersService._();

  //********************************************************************************************************/
  static Future<List<NewVouchersResponse>> getNewVouchers(
      BuildContext context) async {
    try {
      // Response response =
      //     await instance.get(api + "getNewVouchers", {}).timeout(const Duration(seconds: 10));
      // if (response.statusCode != 200) {
      //   GlobalFunctions.errorSnackBar(context, response.body);
      //   return [];
      // }
      // var data = newVouchersResponseFromJson(response.body);
      var vouchers = Provider.of<VouchersProvider>(context);
      vouchers.newVouchers = newvouchers;
      return vouchers.newVouchers;
    } catch (ex) {
      GlobalFunctions.serverErrorSnackBar(context);
      return [];
    }
  }

  //********************************************************************************************************/
  static Future<List<UsedVouchersResponse>> getUsedVouchers(
      BuildContext context) async {
    try {
      var contact = Provider.of<ContactProvider>(context).contactProvider;
      print(contact);

      // Response response =
      //     await instance.get(api + "getUsedVouchers", {}).timeout(const Duration(seconds: 10));
      // if (response.statusCode != 200) {
      //   GlobalFunctions.errorSnackBar(context, response.body);
      //   return [];
      // }
      // var data = usedVouchersResponseFromJson(response.body);
      return usedvouchers;
    } catch (ex) {
      GlobalFunctions.serverErrorSnackBar(context);
      return [];
    }
  }

  //********************************************************************************************************/
  static Future<int> sendPerson(BuildContext context, contact, String verificationNo) async {
    try {
      print(contact);
      // Response response =
      //     await instance.post(api + "sendPerson", {contact,verificationNo}).timeout(const Duration(seconds: 10));
      // if (response.statusCode != 200) {
      //   GlobalFunctions.errorSnackBar(context, response.body);
      //   return 400;
      // }
      // var data = usedVouchersResponseFromJson(response.body);
      GlobalFunctions.succesSnackBar(context, "response.message");
      return 200;
    } catch (ex) {
      GlobalFunctions.serverErrorSnackBar(context);
      return 500;
    }
  }
  //********************************************************************************************************/
  static Future<int> sendHbaibe(BuildContext context, int hbaibeId, String verificationNo) async {
    try {
      print(hbaibeId);
      print(verificationNo);
      // Response response =
      //     await instance.post(api + "sendHbaibe", {hbaibeId,verificationNo}).timeout(const Duration(seconds: 10));
      // if (response.statusCode != 200) {
      //   GlobalFunctions.errorSnackBar(context, response.body);
      //   return 400;
      // }
      // var data = usedVouchersResponseFromJson(response.body);
      GlobalFunctions.succesSnackBar(context, "response.message");
      return 200;
    } catch (ex) {
      GlobalFunctions.serverErrorSnackBar(context);
      return 500;
    }
  }
}
