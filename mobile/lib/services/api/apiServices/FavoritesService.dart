import 'dart:async';
import 'package:Crootie/models/responseModels/AllProductsResponse.dart';
import 'package:Crootie/services/api/Api.dart';
import 'package:Crootie/shared/GlobalFunctions.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';

String api = "/api/Favorites";
Api instance = Api();

class FavoritesService {
  FavoritesService._();
  //********************************************************************************************************/
  static Future<List<AllProductsResponse>> getFavorites(
      BuildContext context) async {
    try {
      Response response =
          await instance.get(api, {}).timeout(const Duration(seconds: 10));
      if (response.statusCode != 200) {
        GlobalFunctions.errorSnackBar(context, response.body);
        return [];
      }
      var data = allProductsResponseFromJson(response.body);
      return data;
    } catch (ex) {
      GlobalFunctions.serverErrorSnackBar(context);
      return [];
    }
  }
  //********************************************************************************************************/
  static Future<int> add(
      BuildContext context, int productId) async {
    try {
      // Response response =
      //     await instance.post(api, productId).timeout(const Duration(seconds: 10));
      // if (response.statusCode != 200) {
      //   GlobalFunctions.errorSnackBar(context, response.body);
      //   return 400;
      // }
      // GlobalFunctions.succesSnackBar(context, response.body);
      return 200;
    } catch (ex) {
      GlobalFunctions.serverErrorSnackBar(context);
      return 500;
    }
  }
  //********************************************************************************************************/
  static Future<int> remove(
      BuildContext context, int productId) async {
    try {
      // Response response =
      //     await instance.delete(api, productId).timeout(const Duration(seconds: 10));
      // if (response.statusCode != 200) {
      //   GlobalFunctions.errorSnackBar(context, response.body);
      //   return 400;
      // }
      // GlobalFunctions.succesSnackBar(context, response.body);
      return 200;
    } catch (ex) {
      GlobalFunctions.serverErrorSnackBar(context);
      return 500;
    }
  }
}
