import 'package:Crootie/services/api/Api.dart';
import 'package:Crootie/shared/GlobalFunctions.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';

String api = "/api/security";
Api instance = new Api();

class LoginService {
  LoginService._();
  static Future<void> login(BuildContext context, Object data) async {
    try {
      Response response = await instance.post(api + '/signup', data);
      if (response.statusCode != 200) {
        // var data = resultDataFromJson2(response.body);
        GlobalFunctions.errorSnackBar(context, "data.message.toString()");
        return;
      }
      // var data = resultDataFromJson(response.body);
      GlobalFunctions.succesSnackBar(context, "data.message.toString()");
      return;
    } catch (ex) {
      GlobalFunctions.serverErrorSnackBar(context);
    }
  }
}
