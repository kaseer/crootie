import 'dart:async';

import 'package:Crootie/models/responseModels/AllProductsResponse.dart';
import 'package:Crootie/models/responseModels/CategoriesResponse.dart';
import 'package:Crootie/models/responseModels/CompaniesResponse.dart';
import 'package:Crootie/models/responseModels/buisness/BuisnessCompaniesResponse.dart';
import 'package:Crootie/models/responseModels/buisness/BuisnessHistoryResponse.dart';
import 'package:Crootie/models/responseModels/buisness/BuisnessProductsResponse.dart';
import 'package:Crootie/services/api/Api.dart';
import 'package:Crootie/services/db/Cart.dart';
import 'package:Crootie/services/db/Companies.dart';
import 'package:Crootie/services/db/Products.dart';
import 'package:Crootie/shared/GlobalFunctions.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';

String api = "/api/buisness";
Api instance = Api();

List<BuisnessCompaniesResponse> buisness = [
  BuisnessCompaniesResponse(
    buisnessId: 1,
    buisnessCompany: "Cortec1",
  ),
  BuisnessCompaniesResponse(
    buisnessId: 2,
    buisnessCompany: "Cortec",
  ),
  BuisnessCompaniesResponse(
    buisnessId: 3,
    buisnessCompany: "Cortec",
  ),
  BuisnessCompaniesResponse(
    buisnessId: 4,
    buisnessCompany: "Cortec",
  ),
  BuisnessCompaniesResponse(
    buisnessId: 5,
    buisnessCompany: "Cortec",
  ),
  BuisnessCompaniesResponse(
    buisnessId: 6,
    buisnessCompany: "Cortec",
  ),
  BuisnessCompaniesResponse(
    buisnessId: 7,
    buisnessCompany: "Cortec",
  ),
  BuisnessCompaniesResponse(
    buisnessId: 8,
    buisnessCompany: "Cortec",
  ),
  BuisnessCompaniesResponse(
    buisnessId: 9,
    buisnessCompany: "Cortec",
  ),
];

//***************************************************************************************************** */
List<BuisnessHistoryResponse> history = [
  BuisnessHistoryResponse(
      buisnessId: 1,
      buisnessCompany: "Cortec1",
      buisnessCard: "Almadar",
      quantity: 30),
  BuisnessHistoryResponse(
      buisnessId: 2,
      buisnessCompany: "Cortec2",
      buisnessCard: "Almadar",
      quantity: 30),
  BuisnessHistoryResponse(
      buisnessId: 3,
      buisnessCompany: "Cortec",
      buisnessCard: "Almadar",
      quantity: 30),
  BuisnessHistoryResponse(
      buisnessId: 4,
      buisnessCompany: "Cortec",
      buisnessCard: "Almadar",
      quantity: 30),
  BuisnessHistoryResponse(
      buisnessId: 5,
      buisnessCompany: "Cortec",
      buisnessCard: "Almadar",
      quantity: 30),
  BuisnessHistoryResponse(
      buisnessId: 6,
      buisnessCompany: "Cortec",
      buisnessCard: "Almadar",
      quantity: 30),
  BuisnessHistoryResponse(
      buisnessId: 7,
      buisnessCompany: "Cortec",
      buisnessCard: "Almadar",
      quantity: 30),
  BuisnessHistoryResponse(
      buisnessId: 8,
      buisnessCompany: "Cortec",
      buisnessCard: "Almadar",
      quantity: 30),
  BuisnessHistoryResponse(
      buisnessId: 9,
      buisnessCompany: "Cortec",
      buisnessCard: "Almadar",
      quantity: 30),
];
//***************************************************************************************************** */
List<BuisnessProductsResponse> products = [
  BuisnessProductsResponse(
      productId: 1,
      priceDescription: "25",
      logo: "Almadar.png",
      topColor: "0F7C10",
      bottomColor: "21AC17",
      price: 30),
  BuisnessProductsResponse(
      productId: 2,
      priceDescription: "45",
      logo: "Amazon.png",
      topColor: "4D4D4D",
      bottomColor: "4D4D4D",
      price: 30),
  BuisnessProductsResponse(
      productId: 3,
      priceDescription: "10",
      logo: "Apple.png",
      topColor: "787878",
      bottomColor: "D6D6D6",
      price: 30),
  BuisnessProductsResponse(
      productId: 4,
      priceDescription: "15",
      logo: "Netflix.png",
      topColor: "0C0C0C",
      bottomColor: "531919",
      price: 30),
  BuisnessProductsResponse(
      productId: 5,
      priceDescription: "25",
      logo: "Spotify.png",
      topColor: "434240",
      bottomColor: "110C0C",
      price: 30),
  BuisnessProductsResponse(
      productId: 6,
      priceDescription: "60",
      logo: "Xbox.png",
      topColor: "0F7C10",
      bottomColor: "21AC17",
      price: 30),
  BuisnessProductsResponse(
      productId: 7,
      priceDescription: "100",
      logo: "Libyana.png",
      topColor: "8A2686",
      bottomColor: "51104F",
      price: 30),
  BuisnessProductsResponse(
      productId: 8,
      priceDescription: "999",
      logo: "Apple.png",
      topColor: "787878",
      bottomColor: "D6D6D6",
      price: 30),
  BuisnessProductsResponse(
      productId: 9,
      priceDescription: "500",
      logo: "Almadar.png",
      topColor: "0F7C10",
      bottomColor: "21AC17",
      price: 30),
];

//***************************************************************************************************** */

class BuisnessService {
  BuisnessService._();
  static Future<List<BuisnessCompaniesResponse>> getBuisnessCompanies(
      BuildContext context) async {
    try {
      // Response? response =
      //     await instance.get(api + "/getBuisnessCompanies", {}).timeout(const Duration(seconds: 10));
      // if (response!.statusCode != 200) {
      //   GlobalFunctions.errorSnackBar(context, response.body);
      //   return [];
      // }
      // var data = buisnessCompaniesResponseFromJson(response.body);
      return buisness;
    } catch (ex) {
      GlobalFunctions.serverErrorSnackBar(context);
      return [];
    }
  }

//********************************************************************************************************/
  static Future<List<BuisnessHistoryResponse>> getBuisnessHistory(
      BuildContext context) async {
    try {
      // Response? response =
      //     await instance.get(api + "/getBuisnessHistory", {}).timeout(const Duration(seconds: 10));
      // if (response!.statusCode != 200) {
      //   GlobalFunctions.errorSnackBar(context, response.body);
      //   return [];
      // }
      // var data = buisnessHistoryResponseFromJson(response.body);
      return history;
    } catch (ex) {
      GlobalFunctions.serverErrorSnackBar(context);
      return [];
    }
  }

  //********************************************************************************************************/
  static Future<List<BuisnessProductsResponse>> getProducts(
      BuildContext context, int companyId) async {
    try {
      // Response response = await instance.get(api + "/getProducts/${companyId}", {})
      //     .timeout(const Duration(seconds: 10));
      // if (response!.statusCode != 200) {
      //   GlobalFunctions.errorSnackBar(context, response.body);
      //   return [];
      // }
      // var data = productsResponseFromJson(response.body);
      return products;
    } catch (ex) {
      GlobalFunctions.serverErrorSnackBar(context);
      return [];
    }
  }
}
