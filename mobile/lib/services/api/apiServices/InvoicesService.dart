import 'dart:async';
import 'dart:convert';

import 'package:Crootie/models/responseModels/invoices/InvoicesResponse.dart';
import 'package:Crootie/models/responseModels/redeem/RedeemResponse.dart';
import 'package:Crootie/providers/PublicProviders.dart';
import 'package:Crootie/services/api/Api.dart';
import 'package:Crootie/shared/GlobalFunctions.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:Crootie/models/responseModels/invoices/InvoiceResponse.dart';

String api = "/api/invoices";
Api instance = Api();

List<InvoicesResponse> invoices = [
  InvoicesResponse(
      intvoiceId: 1,
      invoiceNo: "INV/12/22",
      createdOn: "2020/02/02",
      totalPrice: 30),
  InvoicesResponse(
      intvoiceId: 2,
      invoiceNo: "INV/12/22",
      createdOn: "2020/02/02",
      totalPrice: 30),
  InvoicesResponse(
      intvoiceId: 3,
      invoiceNo: "INV/12/22",
      createdOn: "2020/02/02",
      totalPrice: 30),
  InvoicesResponse(
      intvoiceId: 4,
      invoiceNo: "INV/12/22",
      createdOn: "2020/02/02",
      totalPrice: 30),
  InvoicesResponse(
      intvoiceId: 5,
      invoiceNo: "INV/12/22",
      createdOn: "2020/02/02",
      totalPrice: 30),
  InvoicesResponse(
      intvoiceId: 6,
      invoiceNo: "INV/12/22",
      createdOn: "2020/02/02",
      totalPrice: 30),
  InvoicesResponse(
      intvoiceId: 7,
      invoiceNo: "INV/12/22",
      createdOn: "2020/02/02",
      totalPrice: 30),
  InvoicesResponse(
      intvoiceId: 8,
      invoiceNo: "INV/12/22",
      createdOn: "2020/02/02",
      totalPrice: 30),
  InvoicesResponse(
      intvoiceId: 9,
      invoiceNo: "INV/12/22",
      createdOn: "2020/02/02",
      totalPrice: 30),
];

List<InvoiceResponse> invoice = [
  InvoiceResponse(description: "Almadar 5", price: 5, quantity: 5),
  InvoiceResponse(description: "ltt 5", price: 5, quantity: 1),
  InvoiceResponse(description: "ltt 15", price: 15, quantity: 2),
  InvoiceResponse(description: "PS 50", price: 500, quantity: 3),
  InvoiceResponse(description: "Apple 5", price: 50, quantity: 3),
  InvoiceResponse(description: "neflix 500", price: 1000, quantity: 2),
  InvoiceResponse(description: "neflix 5", price: 5, quantity: 6),
];

class InvoicesService {
//********************************************************************************************************/
  static Future<void> get(BuildContext context, List<DateTime> range) async {
    try {
      print(range);
      // Response? response =
      //     await instance.get(api , {
      //       "start": range[0].toString(),
      //       "end": range[1].toString(),
      //       }).timeout(const Duration(seconds: 10));
      // if (response.statusCode != 200) {
      //   GlobalFunctions.errorSnackBar(context, response.body);
      //   return;
      // }
      // var data = invoicesResponseFromJson(response.body);
      PublicProviders.getInvoices(context, invoices);
      return;
    } catch (ex) {
      GlobalFunctions.serverErrorSnackBar(context);
      return;
    }
  }

  //********************************************************************************************************/
  static Future<List<InvoiceResponse>> getDetails(
      BuildContext context, int invoiceId) async {
    try {
      // Response? response =
      //     await instance.get(api + "getDetails/${invoiceId}" , {}).timeout(const Duration(seconds: 10));
      // if (response.statusCode != 200) {
      //   GlobalFunctions.errorSnackBar(context, response.body);
      //   return [];
      // }
      // var data = invoiceResponseFromJson(response.body);
      return invoice;
    } catch (ex) {
      GlobalFunctions.serverErrorSnackBar(context);
      return [];
    }
  }
}
