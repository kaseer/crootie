import 'dart:async';
import 'package:Crootie/models/requestModels/HbaibeRequest.dart';
import 'package:Crootie/models/responseModels/AllProductsResponse.dart';
import 'package:Crootie/models/responseModels/hbaibe/HbaibeResponse.dart';
import 'package:Crootie/providers/functions/ContactProvider.dart';
import 'package:Crootie/services/api/Api.dart';
import 'package:Crootie/shared/GlobalFunctions.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:provider/provider.dart';

String api = "/api/Hbaibe";
Api instance = Api();

List<HbaibeResponse> hbaibe = [
  HbaibeResponse(
      hbaibeId: 1, memberName: "Cortec5555555", phoneNo: "+218 92 279 2629"),
  HbaibeResponse(
      hbaibeId: 2, memberName: "Cortec", phoneNo: "+218 92 279 2629"),
  HbaibeResponse(
      hbaibeId: 3, memberName: "Cortec", phoneNo: "+218 92 279 2629"),
  HbaibeResponse(
      hbaibeId: 4, memberName: "Cortec", phoneNo: "+218 92 279 2629"),
  HbaibeResponse(
      hbaibeId: 5, memberName: "Cortec", phoneNo: "+218 92 279 2629"),
  HbaibeResponse(
      hbaibeId: 6, memberName: "Cortec", phoneNo: "+218 92 279 2629"),
  HbaibeResponse(
      hbaibeId: 7, memberName: "Cortec", phoneNo: "+218 92 279 2629"),
  HbaibeResponse(
      hbaibeId: 8, memberName: "Cortec", phoneNo: "+218 92 279 2629"),
  HbaibeResponse(
      hbaibeId: 9, memberName: "Cortec", phoneNo: "+218 92 279 2629"),
];

class HbaibeService {
  HbaibeService._();
  //********************************************************************************************************/
  static Future<List<HbaibeResponse>> get(BuildContext context) async {
    try {
      // Response response =
      //     await instance.get(api, {}).timeout(const Duration(seconds: 10));
      // if (response.statusCode != 200) {
      //   GlobalFunctions.errorSnackBar(context, response.body);
      //   return [];
      // }
      // var data = hbaibeResponseFromJson(response.body);
      var prov = Provider.of<ContactProvider>(context);
      prov.contacts = hbaibe;
      return prov.contacts;
    } catch (ex) {
      GlobalFunctions.serverErrorSnackBar(context);
      return [];
    }
  }

  //********************************************************************************************************/
  static Future<HbaibeResponse?> add(
      BuildContext context, HbaibeRequest payload) async {
    try {
      // Response response =
      //     await instance.post(api, payload).timeout(const Duration(seconds: 10));
      // if (response.statusCode != 200) {
      //   GlobalFunctions.errorSnackBar(context, response.body);
      //   return 400;
      // }
      GlobalFunctions.succesSnackBar(context, "response.message");
      HbaibeResponse h = new HbaibeResponse(
        hbaibeId: 0,
        memberName: payload.memberName,
        phoneNo: payload.phoneNo,
      );
      return h;
    } catch (ex) {
      GlobalFunctions.serverErrorSnackBar(context);
      return null;
    }
  }

  //********************************************************************************************************/
  static Future<int> remove(BuildContext context, int productId) async {
    try {
      // Response response =
      //     await instance.delete(api, productId).timeout(const Duration(seconds: 10));
      // if (response.statusCode != 200) {
      //   GlobalFunctions.errorSnackBar(context, response.body);
      //   return 400;
      // }
      // GlobalFunctions.succesSnackBar(context, response.body);
      return 200;
    } catch (ex) {
      GlobalFunctions.serverErrorSnackBar(context);
      return 500;
    }
  }
}
