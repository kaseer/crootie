import 'dart:async';
import 'package:Crootie/models/responseModels/AllProductsResponse.dart';
import 'package:Crootie/services/api/Api.dart';
import 'package:Crootie/shared/GlobalFunctions.dart';
import 'package:flutter/material.dart';

String api = "/api/Gifts";
Api instance = Api();


//***************************************************************************************************** */
List<AllProductsResponse> products = [
  AllProductsResponse(
      productId: 1,
      priceDescription: "25",
      logo: "Almadar.png",
      topColor: "0F7C10",
      bottomColor: "21AC17",
      quantity: 0,
      price: 30),
  AllProductsResponse(
      productId: 2,
      priceDescription: "45",
      logo: "Amazon.png",
      topColor: "4D4D4D",
      bottomColor: "4D4D4D",
      quantity: 0,
      price: 30),
  AllProductsResponse(
      productId: 3,
      priceDescription: "10",
      logo: "Apple.png",
      topColor: "787878",
      bottomColor: "D6D6D6",
      quantity: 0,
      price: 30),
  AllProductsResponse(
      productId: 4,
      priceDescription: "15",
      logo: "Netflix.png",
      topColor: "0C0C0C",
      bottomColor: "531919",
      quantity: 0,
      price: 30),
  AllProductsResponse(
      productId: 5,
      priceDescription: "25",
      logo: "Spotify.png",
      topColor: "434240",
      bottomColor: "110C0C",
      quantity: 0,
      price: 30),
  AllProductsResponse(
      productId: 6,
      priceDescription: "60",
      logo: "Xbox.png",
      topColor: "0F7C10",
      bottomColor: "21AC17",
      quantity: 0,
      price: 30),
  AllProductsResponse(
      productId: 7,
      priceDescription: "100",
      logo: "Libyana.png",
      topColor: "8A2686",
      bottomColor: "51104F",
      quantity: 0,
      price: 30),
  AllProductsResponse(
      productId: 8,
      priceDescription: "999",
      logo: "Apple.png",
      topColor: "787878",
      bottomColor: "D6D6D6",
      quantity: 0,
      price: 30),
  AllProductsResponse(
      productId: 9,
      priceDescription: "500",
      logo: "Almadar.png",
      topColor: "0F7C10",
      bottomColor: "21AC17",
      quantity: 0,
      price: 30),
];

class GiftsService {
  GiftsService._();
  //********************************************************************************************************/
  static Future<List<AllProductsResponse>> getGivenGifts(
      BuildContext context) async {
    try {
      // Response response =
      //     await instance.get(api + "/getGivenGifts", {}).timeout(const Duration(seconds: 10));
      // if (response.statusCode != 200) {
      //   GlobalFunctions.errorSnackBar(context, response.body);
      //   return [];
      // }
      // var data = allProductsResponseFromJson(response.body);
      return products;
    } catch (ex) {
      GlobalFunctions.serverErrorSnackBar(context);
      return [];
    }
  }
  //********************************************************************************************************/
  static Future<List<AllProductsResponse>> getMyGifts(
      BuildContext context) async {
    try {
      // Response response =
      //     await instance.get(api + "/getMyGifts", {}).timeout(const Duration(seconds: 10));
      // if (response.statusCode != 200) {
      //   GlobalFunctions.errorSnackBar(context, response.body);
      //   return [];
      // }
      // var data = allProductsResponseFromJson(response.body);
      return products;
    } catch (ex) {
      GlobalFunctions.serverErrorSnackBar(context);
      return [];
    }
  }
  //********************************************************************************************************/
  static Future<int> collect(
      BuildContext context, int productId) async {
    try {
      // Response response =
      //     await instance.put(api + "/collect", productId).timeout(const Duration(seconds: 10));
      // if (response.statusCode != 200) {
      //   GlobalFunctions.errorSnackBar(context, response.body);
      //   return 400;
      // }
      // GlobalFunctions.succesSnackBar(context, response.body);
      return 200;
    } catch (ex) {
      GlobalFunctions.serverErrorSnackBar(context);
      return 500;
    }
  }
  //********************************************************************************************************/
  static Future<int> returning(
      BuildContext context, int productId) async {
    try {
      // Response response =
      //     await instance.put(api + "/returning", productId).timeout(const Duration(seconds: 10));
      // if (response.statusCode != 200) {
      //   GlobalFunctions.errorSnackBar(context, response.body);
      //   return 400;
      // }
      // GlobalFunctions.succesSnackBar(context, response.body);
      return 200;
    } catch (ex) {
      GlobalFunctions.serverErrorSnackBar(context);
      return 500;
    }
  }
}
