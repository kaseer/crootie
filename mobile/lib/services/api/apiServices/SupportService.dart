import 'dart:async';

import 'package:Crootie/models/requestModels/TicketsRequest.dart';
import 'package:Crootie/models/responseModels/AllProductsResponse.dart';
import 'package:Crootie/models/responseModels/CategoriesResponse.dart';
import 'package:Crootie/models/responseModels/CompaniesResponse.dart';
import 'package:Crootie/models/responseModels/buisness/BuisnessCompaniesResponse.dart';
import 'package:Crootie/models/responseModels/buisness/BuisnessHistoryResponse.dart';
import 'package:Crootie/models/responseModels/buisness/BuisnessProductsResponse.dart';
import 'package:Crootie/models/responseModels/support/TicketsResponse.dart';
import 'package:Crootie/providers/PublicProviders.dart';
import 'package:Crootie/providers/functions/TicketsProvider.dart';
import 'package:Crootie/services/api/Api.dart';
import 'package:Crootie/services/db/Cart.dart';
import 'package:Crootie/services/db/Companies.dart';
import 'package:Crootie/services/db/Products.dart';
import 'package:Crootie/shared/GlobalFunctions.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:provider/provider.dart';

String api = "/api/support";
Api instance = Api();

List<TicketsResponse> tickets = [
  TicketsResponse(
    ticketId: 1,
    ticketNo: "TCK/12/22*******",
    ticketSubject: "Subject: can't add to cart",
    status: 1,
  ),
  TicketsResponse(
    ticketId: 2,
    ticketNo: "TCK/12/22",
    ticketSubject: "Subject: can't add to cart",
    status: 1,
  ),
  TicketsResponse(
    ticketId: 3,
    ticketNo: "TCK/12/22",
    ticketSubject: "Subject: can't add to cart",
    status: 1,
  ),
  TicketsResponse(
    ticketId: 4,
    ticketNo: "TCK/12/22",
    ticketSubject: "Subject: can't add to cart",
    status: 1,
  ),
  TicketsResponse(
    ticketId: 5,
    ticketNo: "TCK/12/22",
    ticketSubject: "Subject: can't add to cart",
    status: 1,
  ),
  TicketsResponse(
    ticketId: 6,
    ticketNo: "TCK/12/22",
    ticketSubject: "Subject: can't add to cart",
    status: 2,
  ),
  TicketsResponse(
    ticketId: 7,
    ticketNo: "TCK/12/22",
    ticketSubject: "Subject: can't add to cart",
    status: 1,
  ),
  TicketsResponse(
    ticketId: 8,
    ticketNo: "TCK/12/22",
    ticketSubject: "Subject: can't add to cart",
    status: 2,
  ),
  TicketsResponse(
    ticketId: 9,
    ticketNo: "TCK/12/22",
    ticketSubject: "Subject: can't add to cart",
    status: 1,
  ),
];

//***************************************************************************************************** */

//***************************************************************************************************** */

//***************************************************************************************************** */

class SupportService {
  SupportService._();
  static Future<List<TicketsResponse>> get(BuildContext context) async {
    try {
      // Response? response =
      //     await instance.get(api , {}).timeout(const Duration(seconds: 10));
      // if (response!.statusCode != 200) {
      //   GlobalFunctions.errorSnackBar(context, response.body);
      //   return [];
      // }
      // var data = ticketsResponseFromJson(response.body);
      var prov = Provider.of<TicketsProvider>(context);
      prov.tickets = tickets;
      return prov.tickets;
    } catch (ex) {
      GlobalFunctions.serverErrorSnackBar(context);
      return [];
    }
  }

//********************************************************************************************************/
  static Future<void> addTicket(
      BuildContext context, TicketsRequest ticket) async {
    try {
      // Response? response =
      //     await instance.post(api , ticket).timeout(const Duration(seconds: 10));
      // if (response!.statusCode != 200) {
      //   GlobalFunctions.errorSnackBar(context, response.body);
      //   return;
      // }
      // var data = ticketResponseFromJson("response.body");
      PublicProviders.addTicket(context, tickets[0]);
      return;
    } catch (ex) {
      GlobalFunctions.serverErrorSnackBar(context);
      return;
    }
  }

  //********************************************************************************************************/
  static Future<List<BuisnessProductsResponse>> getProducts(
      BuildContext context, int companyId) async {
    try {
      // Response response = await instance.get(api + "/getProducts/${companyId}", {})
      //     .timeout(const Duration(seconds: 10));
      // if (response!.statusCode != 200) {
      //   GlobalFunctions.errorSnackBar(context, response.body);
      //   return [];
      // }
      // var data = productsResponseFromJson(response.body);
      return [];
    } catch (ex) {
      GlobalFunctions.serverErrorSnackBar(context);
      return [];
    }
  }
}
