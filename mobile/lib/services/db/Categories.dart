
import 'package:Crootie/models/responseModels/CategoriesResponse.dart';

import 'Database.dart';

class CategoriesDatabase extends CrootieDatabase{
  CategoriesDatabase._init();
  static final CategoriesDatabase instance = CategoriesDatabase._init();

   Future<void> create(List<CategoriesResponse> categories) async {
    final db = await instance.database;
    db.delete('Categories');
    categories.forEach((element) { 
      db.insert('Categories', element.toJson());
    });
    
  }

   Future<CategoriesResponse> get(int categoryId) async {
    final db = await instance.database;

    final maps = await db.query(
      'Categories',
      columns: CategoriesFields.values,
      where: '${CategoriesFields.categoryId} = ?',
      whereArgs: [categoryId],
    );

    if (maps.isNotEmpty) {
      return CategoriesResponse.fromJson(maps.first);
    } else {
      throw Exception('CategoryId $categoryId not found');
    }
  }

   Future<List<CategoriesResponse>> getAll() async {
    final db = await instance.database;

    final orderBy = '${CategoriesFields.categoryId} ASC';
    // final result =
    //     await db.rawQuery('SELECT * FROM $tablecategorys ORDER BY $orderBy');

    final result = await db.query('Categories', orderBy: orderBy);

    return result.map((json) => CategoriesResponse.fromJson(json)).toList();
  }

   Future<int> update(CategoriesResponse category) async {
    final db = await instance.database;

    return db.update(
      'Categories',
      category.toJson(),
      where: '${CategoriesFields.categoryId} = ?',
      whereArgs: [category.categoryId],
    );
  }

   Future<int> delete(int id) async {
    final db = await instance.database;

    return await db.delete(
      'Categories',
      where: '${CategoriesFields.categoryId} = ?',
      whereArgs: [id],
    );
  }

}