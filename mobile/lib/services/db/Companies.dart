import 'package:Crootie/models/responseModels/CompaniesResponse.dart';

import 'Database.dart';

class CompaniesDatabase extends CrootieDatabase {
  CompaniesDatabase._init();
  static final CompaniesDatabase instance = CompaniesDatabase._init();

  Future<void> create(List<CompaniesResponse> companies, int categoryId) async {
    final db = await instance.database;
    CompaniesResponse? checkData = await instance.get(categoryId);
    if(checkData != null) {
      return;
    }
    companies.forEach((element) {
      final json = element.toJson();
      final columns = '''
          ${CompaniesFields.companyId}, ${CompaniesFields.bottomColor}, ${CompaniesFields.topColor},
          ${CompaniesFields.logo}, ${CompaniesFields.categoryId}
        ''';
      final values = '''
          ${element.companyId},'${json["bottomColor"]}','${json["topColor"]}',
          '${json["logo"]}', ${categoryId}
        ''';

      db.rawInsert('INSERT INTO Companies ($columns) VALUES ($values)');
    });
  }

  Future<CompaniesResponse?> get(int categoryId) async {
    final db = await instance.database;

    final maps = await db.query(
      'Companies',
      columns: CompaniesFields.values,
      where: '${CompaniesFields.categoryId} = ?',
      whereArgs: [categoryId],
    );

    if (maps.isNotEmpty) {
      return CompaniesResponse.fromJson(maps.first);
    } else {
      return null;
    }
  }

  Future<List<CompaniesResponse>> getAll(int categoryId) async {
    final db = await instance.database;

    final orderBy = '${CompaniesFields.companyId} ASC';
    // final result =
    //     await db.rawQuery('SELECT * FROM $tableNotes ORDER BY $orderBy');

    final result = await db.query('Companies',
        where: '${CompaniesFields.categoryId} = ${categoryId}',
        orderBy: orderBy);

    return result.map((json) => CompaniesResponse.fromJson(json)).toList();
  }

  Future<int> update(CompaniesResponse company) async {
    final db = await instance.database;

    return db.update(
      'Companies',
      company.toJson(),
      where: '${CompaniesFields.companyId} = ?',
      whereArgs: [company.companyId],
    );
  }

  Future<int> delete(int id) async {
    final db = await instance.database;

    return await db.delete(
      'Companies',
      where: '${CompaniesFields.companyId} = ?',
      whereArgs: [id],
    );
  }
}
