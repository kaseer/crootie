import 'package:Crootie/data/Cart.dart';
import 'package:Crootie/models/responseModels/ProductsResponse.dart';

import 'Database.dart';

class CartDatabase extends CrootieDatabase {
  CartDatabase._init();
  static final CartDatabase instance = CartDatabase._init();
  //********************************************************************************************************/
  Future<void> addToCart(int productId) async {
    final db = await instance.database;
    final data = await db.query('Cart', where: 'productId = ${productId}');
    if (data.length > 0) {
      int quantity = data[0]["quantity"] as int;
      db.rawUpdate('''
          UPDATE Cart 
          SET  quantity = ? 
          WHERE productId = ?
          ''', [quantity + 1, productId]);
    } else {
      db.rawInsert(
          'INSERT INTO Cart (productId, quantity) VALUES ($productId,1)');
    }
    // getAll();
  }

  //********************************************************************************************************/
  Future<void> removeFromCart(int productId) async {
    final db = await instance.database;
    final data = await db.query('Cart', where: 'productId = ${productId}');
    if (data.length > 0) {
      int quantity = data[0]["quantity"] as int;
      if (quantity <= 1) {
        db.delete(
          'Cart',
          where: 'productId = ?',
          whereArgs: [productId],
        );
        return;
      }
      db.rawUpdate('''
          UPDATE Cart 
          SET  quantity = ? 
          WHERE productId = ?
          ''', [quantity - 1, productId]);
    }
    // getAll();
  }

  //********************************************************************************************************/
  Future<List<int>> getProductIds() async {
    final db = await instance.database;
    List<int> productsIds = [];
    final result = await db.query('Cart');
    var data = result.map((json) => CartData.fromJson(json)).toList();
    data.forEach((element) {
      productsIds.add(element.productId);
    });
    return productsIds;
  }

  //********************************************************************************************************/
  Future<List<CartData>> getAll() async {
    final db = await instance.database;
    final result = await db.query('Cart');
    return result.map((json) => CartData.fromJson(json)).toList();
  }

  //********************************************************************************************************/
  Future<int> getCount() async {
    // ignore: unnecessary_null_comparison
    if (instance.database == null) return 0;
    final db = await instance.database;

    final result = await db.query('Cart');
    int count = 0;
    var cart = result.map((json) => CartData.fromJson(json)).toList();
    cart.forEach((element) {
      count += element.quantity;
    });
    return count;
    // cartProvider.cart(count);
  }

  //********************************************************************************************************/
  Future<int> update(ProductsResponse product) async {
    final db = await instance.database;

    return db.update(
      'Products',
      product.toJson(),
      where: '${ProductsFields.companyId} = ?',
      whereArgs: [product.productId],
    );
  }

  //********************************************************************************************************/
  Future<int> emptyCart() async {
    final db = await instance.database;
    db.delete('Cart');
    return 0;
  }
}
