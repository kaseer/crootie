import 'package:Crootie/models/responseModels/CategoriesResponse.dart';
import 'package:Crootie/models/responseModels/CompaniesResponse.dart';
import 'package:Crootie/models/responseModels/ProductsResponse.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class CrootieDatabase {
  static final CrootieDatabase dbinstance = CrootieDatabase();

  static Database? _database;

  Future<Database> get database async =>_database!;

  Future<void> initDB(String filePath) async {
    final dbPath = await getDatabasesPath();
    final path = join(dbPath, filePath);
    bool checkDB = await databaseExists(path);
    if (!checkDB){
      await openDatabase(path, version: 1, onCreate: _createDB);
    }
    await openDatabase(path, version: 1, onOpen: (Database db) async{
      _database = db;
    });
  }

  Future<void> _createDB(Database db, int version) async {
    final idType = 'INTEGER PRIMARY KEY';
    final textType = 'TEXT NOT NULL';
    final boolType = 'BOOLEAN NOT NULL';
    final integerType = 'INTEGER NOT NULL';
    final money = 'FLOAT NOT NULL';

    await db.execute('''
    CREATE TABLE Categories ( 
      ${CategoriesFields.categoryId} $idType, 
      ${CategoriesFields.categoryNameAr} $textType,
      ${CategoriesFields.categoryNameEn} $textType,
      ${CategoriesFields.illistration} $textType,
      ${CategoriesFields.topColor} $textType,
      ${CategoriesFields.bottomColor} $textType
      )
    ''');

    await db.execute('''
    CREATE TABLE Companies ( 
      ${CompaniesFields.companyId} $idType, 
      ${CompaniesFields.categoryId} $integerType, 
      ${CompaniesFields.topColor} $textType,
      ${CompaniesFields.bottomColor} $textType,
      ${CompaniesFields.logo} $textType
      )
    ''');

    await db.execute('''
    CREATE TABLE Products ( 
      ${ProductsFields.productId} $idType, 
      ${ProductsFields.companyId} $integerType, 
      ${ProductsFields.isFav} $boolType,
      ${ProductsFields.priceDescription} $textType,
      ${ProductsFields.price} $money
      )
    ''');

    await db.execute('''
    CREATE TABLE Cart ( 
      productId $integerType, 
      quantity $integerType
      )
    ''');
    _database = db;
  }


  static Future close() async {
    final db = await dbinstance.database;

    db.close();
  }
}
