import 'package:Crootie/models/responseModels/ProductsResponse.dart';

import 'Database.dart';

class ProductsDatabase extends CrootieDatabase {
  ProductsDatabase._init();
  static final ProductsDatabase instance = ProductsDatabase._init();
  //********************************************************************************************************/
  Future<void> create(List<ProductsResponse> products, int companyId) async {
    final db = await instance.database;
    ProductsResponse? checkData = await instance.get(companyId);
    if(checkData != null) {
      return;
    }
    products.forEach((element) {
      var isFav = element.isFav ? 1 : 0;
      final json = element.toJson();
      final columns = '''
            ${ProductsFields.productId}, ${ProductsFields.price}, ${ProductsFields.priceDescription},
            ${ProductsFields.isFav}, ${ProductsFields.companyId}
          ''';
      final values = '''
            ${element.productId}, '${json["price"]}', '${json["priceDescription"]}',
            ${isFav},${companyId}
          ''';

      db.rawInsert('INSERT INTO Products ($columns) VALUES ($values)');
    });
  }
  //********************************************************************************************************/
  Future<List<ProductsResponse>> getAll(int companyId) async {
    final db = await instance.database;

    final orderBy = '${ProductsFields.productId} ASC';

    final result = await db.query('Products',
        where: '${ProductsFields.companyId} = ${companyId}', orderBy: orderBy);

    return result.map((json) => ProductsResponse.localFromJson(json)).toList();
  }
  //********************************************************************************************************/
  Future<ProductsResponse?> get(int companyId) async {
    final db = await instance.database;

    final maps = await db.query(
      'Products',
      columns: ProductsFields.values,
      where: '${ProductsFields.companyId} = ?',
      whereArgs: [companyId],
    );

    if (maps.isNotEmpty) {
      return ProductsResponse.localFromJson(maps.first);
    } else {
      return null;
    }
  }

  //********************************************************************************************************/
  Future<int> update(ProductsResponse product) async {
    final db = await instance.database;

    return db.update(
      'Products',
      product.toJson(),
      where: '${ProductsFields.companyId} = ?',
      whereArgs: [product.productId],
    );
  }
}
