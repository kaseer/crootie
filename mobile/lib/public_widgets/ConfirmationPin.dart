import 'dart:async';
import 'package:Crootie/models/requestModels/HbaibeRequest.dart';
import 'package:Crootie/providers/PublicProviders.dart';
import 'package:Crootie/providers/functions/ContactProvider.dart';
import 'package:Crootie/public_widgets/ListContainer.dart';
import 'package:Crootie/public_widgets/TextButtonWidget.dart';
import 'package:Crootie/services/api/apiServices/VouchersService.dart';
import 'package:Crootie/style/SizeConfig.dart';
import 'package:Crootie/style/Style.dart';
import 'package:flutter/material.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:provider/provider.dart';

class ConfirmationPin extends StatefulWidget {
  final String? title;
  final String serviceType;
  final int? id;

  ConfirmationPin({this.title, required this.serviceType, this.id});

  @override
  _ConfirmationPinState createState() => _ConfirmationPinState();
}

class _ConfirmationPinState extends State<ConfirmationPin> {
  TextEditingController verificationNo = TextEditingController();
  // ..text = "123456";

  // ignore: close_sinks
  StreamController<ErrorAnimationType>? errorController;

  bool hasError = false;
  String currentText = "";
  final formKey = GlobalKey<FormState>();

  @override
  void initState() {
    errorController = StreamController<ErrorAnimationType>();
    super.initState();
  }

  @override
  void dispose() {
    errorController!.close();

    super.dispose();
  }

  Future<int> service() async {
    int response = 0;
    switch (widget.serviceType) {
      case "Buisness":
        response = 0;
        break;
      case "GivenGifts":
        response = 0;
        break;
      case "MyGifts":
        response = 0;
        break;
      case "ResetPassword":
        response = 0;
        break;
      case "ResetPIN":
        response = 0;
        break;
      case "Redeem":
        response = 0;
        break;
      case "SendHbaibe":
        response = await VouchersService.sendHbaibe(
            context, widget.id!, verificationNo.text);
        break;
      case "SendPerson":
        var contact = Provider.of<ContactProvider>(context, listen: false)
            .contactProvider;
        response = await VouchersService.sendPerson(
            context, contact, verificationNo.text);
        break;
      case "Scrap":
        response = 0;
        break;
    }
    return response;
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(20.0))),
      backgroundColor: AppTheme.primary,
      scrollable: true,
      content: Column(
        children: [
          Center(
              child:
                  Text(widget.title!, style: TextStyle(color: Colors.white))),
          ListContainer(
            height: SizeConfig.screenHeight! * 0.3,
            width: SizeConfig.screenWidth! * 0.8,
            child: Container(
              // margin: EdgeInsets.all(20),
              child: Wrap(
                alignment: WrapAlignment.center,
                runSpacing: 20,
                children: [
                  Form(
                    key: formKey,
                    child: PinCodeTextField(
                      appContext: context,
                      textStyle: TextStyle(color: Colors.white),
                      pastedTextStyle: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                      length: 4,
                      blinkWhenObscuring: true,
                      animationType: AnimationType.fade,
                      pinTheme: PinTheme(
                        shape: PinCodeFieldShape.underline,
                        inactiveColor: Colors.white,
                        inactiveFillColor: AppTheme.primary,
                        selectedFillColor: AppTheme.primary,
                        selectedColor: AppTheme.primary,
                        activeFillColor: AppTheme.primary,
                      ),
                      cursorColor: Colors.black,
                      animationDuration: Duration(milliseconds: 300),
                      enableActiveFill: true,
                      errorAnimationController: errorController,
                      controller: verificationNo,
                      keyboardType: TextInputType.number,
                      boxShadows: [
                        BoxShadow(
                          offset: Offset(0, 1),
                          color: Colors.black12,
                          blurRadius: 10,
                        )
                      ],
                      onChanged: (value) {
                        print(value);
                        setState(() {
                          currentText = value;
                        });
                      },
                      // beforeTextPaste: (text) {
                      //   print("Allowing to paste $text");
                      //   //if you return true then it will show the paste confirmation dialog. Otherwise if false, then nothing will happen.
                      //   //but you can show anything you want here, like your pop up saying wrong paste format or etc
                      //   return true;
                      // },
                    ),
                  ),
                  TextButtonWidget(
                    title: "Confirm",
                    fontSize: SizeConfig.screenWidth! * 0.037,
                    color: AppTheme.primary,
                    height: 50,
                    textColor: Colors.white,
                    width: 100,
                    onPressed: () async {
                      formKey.currentState!.validate();
                      // conditions for validating
                      if (currentText.length != 4 || currentText != "1234") {
                        errorController!.add(ErrorAnimationType
                            .shake); // Triggering error shake animation
                        setState(() => hasError = true);
                      } else {
                        int responseCode = await service();
                        if (responseCode == 200) {
                          setState(
                            () {
                              hasError = false;
                              Navigator.pop(context);
                            },
                          );
                        } else {
                          errorController!.add(ErrorAnimationType
                              .shake); // Triggering error shake animation
                          setState(() => hasError = true);
                        }
                      }
                    },
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
