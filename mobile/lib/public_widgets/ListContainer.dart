import 'package:Crootie/style/SizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:Crootie/style/Style.dart';

class ListContainer extends StatelessWidget {
  final Widget? child;
  final Color? color;
  final AlignmentGeometry? alignment;
  final double? width;
  final double? height;
  final double? borderRadius;
  final EdgeInsetsGeometry? padding;
  final EdgeInsetsGeometry? margin;
  final bool showShadow;
  final bool showBorder;
  ListContainer(
      {this.child,
      this.color = AppTheme.primary,
      this.alignment = Alignment.center,
      this.width,
      this.height,
      this.borderRadius = 15,
      this.padding,
      this.margin = SizeConfig.margin,
      this.showShadow =true,
      this.showBorder =false});
  @override
  Widget build(BuildContext context) {
    // height = height == null ? SizeConfig.screenHeight / 10 : height;
    // height = height ?? SizeConfig.screenHeight! / 10 ;
    return Container(
      width: width,
      alignment: alignment,
      height: height,
      margin: margin,
      padding: padding,
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.circular(borderRadius!),
        border: showBorder ? Border.all(
          color: Colors.white,
        ) : null,
        boxShadow: showShadow? [
          BoxShadow(
              color: color!.withOpacity(0.3),
              spreadRadius: 0.1,
              blurRadius: 10)
        ] : [],
      ),
      child: child,
    );
  }
}
