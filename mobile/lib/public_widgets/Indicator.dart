import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:Crootie/style/Style.dart';

class Indicator extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white.withOpacity(0.5),
      child: SpinKitCircle(
        color: AppTheme.secondary,
        // itemBuilder: (_, int index) {
        //   return DecoratedBox(
        //     decoration: BoxDecoration(
        //       shape: BoxShape.circle,
        //       color: index.isEven ? AppTheme.grey : AppTheme.secondary,
        //     ),
        //   );
        // },
        size: 80.0,
      ),
    );
  }
}
