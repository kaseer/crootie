import 'package:Crootie/style/Style.dart';
import 'package:flutter/material.dart';

class CircleButton extends StatelessWidget {
  final Function() onPressed;
  final IconData icon;
  final Color color;
  const CircleButton(
      {required this.onPressed,
      required this.icon,
      this.color = AppTheme.primary,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(5),
      child: IconButton(
        onPressed: onPressed,
        icon: Icon(
          icon,
          color: Colors.white,
        ),
      ),
      // margin: EdgeInsets.fromLTRB(15, 60, 15, 20),
      height: 50,
      width: 50,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: color,
      ),
    );
  }
}
