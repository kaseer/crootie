// import 'package:flutter/material.dart';
// import 'package:easy_localization/easy_localization.dart';

// import 'package:Crootie/public_widgets/ListContainer.dart';
// import 'package:Crootie/style/Style.dart';
// import 'package:skeleton_loader/skeleton_loader.dart';

// class SkeletonList extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return SkeletonLoader(
//       baseColor: AppTheme.secondary,
//       builder: Container(
//         padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
//         child: Row(
//           children: <Widget>[
//             Expanded(
//               child: Column(
//                 children: <Widget>[
//                   ListContainer(
//                     width: double.infinity,
//                     height: 100,
//                     color: Colors.white.withOpacity(0.5),
//                   ),
//                 ],
//               ),
//             ),
//           ],
//         ),
//       ),
//       items: 10,
//       period: Duration(seconds: 2),
//       highlightColor: Colors.lightBlue[300],
//       direction: SkeletonDirection.ltr,
//     );
//   }
// }
