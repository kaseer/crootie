// import 'package:flutter/material.dart';
// import 'package:easy_localization/easy_localization.dart';

// import 'package:skeleton_loader/skeleton_loader.dart';

// class SkeletonGrid extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return SkeletonGridLoader(
//       builder: Card(
//         color: Colors.transparent,
//         child: GridTile(
//           child: Column(
//             mainAxisAlignment: MainAxisAlignment.center,
//             children: <Widget>[
//               Container(
//                 width: 50,
//                 height: 10,
//                 color: Colors.white,
//               ),
//               SizedBox(height: 10),
//               Container(
//                 width: 70,
//                 height: 10,
//                 color: Colors.white,
//               ),
//             ],
//           ),
//         ),
//       ),
//       items: 9,
//       itemsPerRow: 2,
//       period: Duration(seconds: 2),
//       highlightColor: Colors.lightBlue[300],
//       direction: SkeletonDirection.ltr,
//       childAspectRatio: 1,
//     );
//   }
// }
