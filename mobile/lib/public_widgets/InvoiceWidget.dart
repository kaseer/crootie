import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';

import 'package:Crootie/locale/locale_keys.g.dart';
import 'package:Crootie/style/SizeConfig.dart';
import 'package:Crootie/public_widgets/CustomClipPath.dart';
import 'package:Crootie/style/Style.dart';
import 'package:Crootie/models/responseModels/invoices/InvoiceResponse.dart';

class InvoiceWidget extends StatelessWidget {
  final List<InvoiceResponse> list;

  const InvoiceWidget({required this.list, Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(39.0),
      decoration: BoxDecoration(
          // color: AppTheme.primary.withOpacity(0.7),
          boxShadow: [
            BoxShadow(
                color: AppTheme.primary.withOpacity(0.4),
                spreadRadius: 0.1,
                blurRadius: 10)
          ]),
      // margin:SizeConfig.margin,
      child: Stack(
        alignment: Alignment.center,
        children: [
          ClipPath(
            child: Container(
              color: AppTheme.primary.withOpacity(0.7),
              width: SizeConfig.screenWidth,
              height: SizeConfig.screenHeight! * 0.5,
            ),
            clipper: CustomClipPath(),
          ),
          ClipPath(
            child: Container(
              padding: EdgeInsets.all(20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      textContainer(LocaleKeys.invoice_item.tr(),
                          AppTheme.primary, SizeConfig.screenWidth! * 0.028
                          // ThemeData().textTheme.bodyText1!.fontSize!
                          ),
                      textContainer(LocaleKeys.invoice_qty.tr(),
                          AppTheme.primary, SizeConfig.screenWidth! * 0.028
                          // ThemeData().textTheme.bodyText1!.fontSize!
                          ),
                      textContainer(LocaleKeys.total.tr(), AppTheme.primary,
                          SizeConfig.screenWidth! * 0.028
                          // ThemeData().textTheme.bodyText1!.fontSize!
                          ),
                    ],
                  ),
                  Flexible(
                      child: ListView.builder(
                          itemCount: list.length,
                          itemBuilder: (context, index) {
                            return Container(
                              margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      textContainer(
                                          list[index].description,
                                          AppTheme.grey,
                                          SizeConfig.screenWidth! * 0.033),
                                      textContainer(
                                          LocaleKeys.invoice_price.tr() +
                                              " ${list[index].price}" +
                                              LocaleKeys.currency_libyan.tr(),
                                          AppTheme.grey,
                                          SizeConfig.screenWidth! * 0.028)
                                    ],
                                  ),
                                  textContainer(
                                      "${list[index].quantity}",
                                      AppTheme.grey,
                                      SizeConfig.screenWidth! * 0.028),
                                  textContainer(
                                      "${list[index].price * list[index].quantity} " +
                                          LocaleKeys.currency_libyan.tr(),
                                      AppTheme.grey,
                                      SizeConfig.screenWidth! * 0.028)
                                ],
                              ),
                            );
                          })),
                ],
              ),
              height: SizeConfig.screenHeight! * 0.485,
              decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border(
                    left: BorderSide(
                        color: AppTheme.primary.withOpacity(0.7), width: 2),
                    right: BorderSide(
                        color: AppTheme.primary.withOpacity(0.7), width: 2),
                  )),
            ),
            clipper: CustomClipPath(),
          ),
        ],
      ),
    );
  }

//**********************************************************************************
  Widget textContainer(String title, Color color, double fontSize) {
    return Container(
      child: Text(
        title,
        style: TextStyle(color: color, fontSize: fontSize),
      ),
    );
  }
}
