class FormValidator{
  FormValidator._();
  static String? validPhoneNo(String? phoneNo) {
    if (phoneNo!.isEmpty) {
      return 'يرجي ادخال رقم الهاتف';
    }
    if (phoneNo.length > 10 || phoneNo.length < 9) {
      return 'يرجي ادخال رقم الهاتف بشكل صحيح';
    }
    return null;
  }

  static String? validPassword (String? password) {
    if (password!.isEmpty) {
      return 'Please enter password';
    }
    return null;
  }

  static String? validConfirmPassword (String? password) {
    if (password!.isEmpty) {
      return 'Please enter password';
    }
    return null;
  }
}