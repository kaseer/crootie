import 'package:Crootie/style/Style.dart';
import 'package:flutter/material.dart';

class InputForm extends StatelessWidget {
  final TextEditingController? controller;
  final String? label;
  final FormFieldValidator<String>? valid;
  final bool isNumber;
  final bool isPassword;
  final Color labelColor;
  final Icon? icon;
  final Color? iconColor;
  final double radius;
  final double? height;
  final Color borderColor;
  final Color? fillColor;
  final int maxLines;
  final EdgeInsetsGeometry? margin;
  final EdgeInsetsGeometry? padding;
  final bool filled;
  InputForm(
      {this.label,
      required this.controller,
      this.valid,
      this.isNumber = false,
      this.isPassword = false,
      this.labelColor = Colors.white,
      required this.icon,
      this.iconColor,
      this.radius = 10,
      this.height,
      this.borderColor = AppTheme.grey,
      this.fillColor,
      this.maxLines = 1,
      this.margin,
      this.padding,
      this.filled = true});
  @override
  Widget build(BuildContext context) {
    OutlineInputBorder _outlineBorder = OutlineInputBorder(
      borderRadius: BorderRadius.all(Radius.circular(radius)),
      borderSide: BorderSide(style: BorderStyle.solid, color: borderColor),
    );

    return Container(
      margin: margin,
      padding: padding,
      height: height,
      child: TextFormField(
        maxLines: maxLines,
        autocorrect: true,
        controller: controller,
        validator: valid,
        obscureText: isPassword,
        keyboardType: isNumber ? TextInputType.number : null,
        decoration: new InputDecoration(
            border: _outlineBorder,
            focusedBorder: _outlineBorder,
            enabledBorder: _outlineBorder,
            fillColor: fillColor,
            filled: filled,
            contentPadding: EdgeInsets.fromLTRB(10, 0, 10, 0),
            prefixIcon: IconButton(
              color: iconColor,
              icon: icon!,
              onPressed: () {
                controller!.clear();
              },
            ),
            labelText: label,
            labelStyle: TextStyle(color: labelColor)),
      ),
    );
  }
}
