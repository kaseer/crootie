import 'package:flutter/material.dart';

class TestBuilder extends StatefulWidget {
  final Function child;
  final Future<dynamic> future;

  TestBuilder(
      {required this.future,
      required this.child,
      // required this.list,
      Key? key})
      : super(key: key);

  @override
  _TestBuilderState createState() => _TestBuilderState();
}

class _TestBuilderState extends State<TestBuilder> {
  // List<dynamic> list = [];
  @override
  void initState() {
    super.initState();
    test();
    
  }


  Future test() async{
    // PublicProviders.startLoading(context);
    // setState(() {});
    widget.future.then((value) {
      // PublicProviders.stopLoading(context);
      // Future.delayed(
      //         Duration(seconds:2),
      //         () => PublicProviders.stopLoading(context));
      //         setState(() {});
    })
    .catchError((error){
      // PublicProviders.stopLoading(context);
      // setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    // PublicProviders.startLoading(context);
    // LookupService.getCategories(context).then((value) {
    //   PublicProviders.stopLoading(context);
    //   // PublicProviders.getCategories(context, value);
    //   // this.list = value;
    //   return widget.child(value);
    // }).catchError((err) {
    //   print("object");
    //   PublicProviders.stopLoading(context);
    //   return Container();
    // });
    // .whenComplete(() => null);
    return Container();
  }
}
