import 'package:flutter/material.dart';

import 'package:Crootie/public_widgets/topBar/CurvePainter.dart';

class Background extends StatelessWidget {
  final Color? color;
  final double? rightHeightEdge;
  final double? leftHeightEdge;
  final double? widthEdge;
  final double? rigthWidthEdge;
  Background(
      {this.color,
       this.rightHeightEdge,
       this.leftHeightEdge,
        this.widthEdge,
       this.rigthWidthEdge});
  @override
  Widget build(BuildContext context) {
    return Container(
      child: CustomPaint(
        painter: CurvePainter(
            color: color!,
            widthEdge: widthEdge,
            rigthWidthEdge: rigthWidthEdge,
            rightHeightEdge: rightHeightEdge!,
            leftHeightEdge: leftHeightEdge!),
      ),
    );
  }
}
