import 'package:flutter/material.dart';

import 'package:Crootie/style/SizeConfig.dart';
import 'package:Crootie/providers/PublicProviders.dart';
import 'package:Crootie/style/Style.dart';

import '../topBar/Background.dart';
import '../TextButtonWidget.dart';
import 'ImageWidget.dart';
import 'AppBar.dart';

class TopBar extends StatelessWidget {
  final String title;
  final String? logo;
  final String? firstTap;
  final String? secondTap;
  final bool isTap;
  final bool isCenter;
  final bool isTopCenter;
  final Color backgroud;
  final double ratio;
  final double rightHeightEdge;
  final double leftHeightEdge;
  final double widthEdge;
  final double rigthWidthEdge;
  final int? pageNo;
  final int firstPageNo;
  final int secondPageNo;
  final bool isCart;
  final bool showBar;
  final double imageWidth;
  final double imageHeight;
  final Widget? widget;
  final bool showTitle;
  final bool isLine;
  final bool isBottomImage;
  final double? titleWidth;
  final double? titleSize;
  final int flex;
  final double sizedBox;
  TopBar(
      {this.title = "",
      this.logo,
      this.firstTap,
      this.secondTap,
      this.pageNo,
      this.firstPageNo = 1,
      this.secondPageNo = 2,
      this.isTap = true,
      this.isCenter = false,
      this.isTopCenter = false,
      this.backgroud = AppTheme.secondary,
      this.ratio = 0.25,
      this.rightHeightEdge = 0.25,
      this.leftHeightEdge = 0.25,
      this.widthEdge = 4,
      this.rigthWidthEdge = 4,
      this.isCart = false,
      this.showBar = true,
      this.imageWidth = 140,
      this.imageHeight = 140,
      this.widget,
      this.showTitle = true,
      this.isLine = false,
      this.isBottomImage = false,
      this.titleWidth,
      this.titleSize,
      this.flex = 0,
      this.sizedBox = 0});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 20),
      child: Column(
        children: [
          showBar
              ? AppBarWidget(
                  backgroud: backgroud,
                  isCart: isCart,
                )
              : Container(),
          // widget != null ? widget! : Container(),
          Flexible(
            flex: flex,
            child: Stack(
              children: [
                Stack(
                  alignment: isTopCenter
                      ? Alignment.topCenter
                      : isCenter
                          ? Alignment.center
                          : Alignment.bottomCenter,
                  children: [
                    backgroundWidget(context),
                    !isLine
                        ? Column(
                            children: [
                              Container(
                                color: backgroud,
                                child: titleWidget(context),
                                padding: SizeConfig.marginLR,
                              ),
                              !isBottomImage
                                  ? Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                          SizedBox(
                                            width: sizedBox,
                                          ),
                                          imageWidget(firstPageNo.toString()),
                                        ])
                                  : Container(),
                            ],
                          )
                        : Container(
                            height: SizeConfig.screenHeight! * ratio,
                            child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  titleWidget(context),
                                  SizedBox(
                                    width: sizedBox,
                                  ),
                                  imageWidget(pageNo.toString()),
                                ]),
                          ),
                    isBottomImage
                        ? Column(
                            children: [
                              imageWidget(firstPageNo.toString()),
                            ],
                          )
                        : Container(),
                    tap(context, imageWidget(firstPageNo.toString()))
                  ],
                ),
                widget != null ? widget! : Container(),
              ],
            ),
          ),
        ],
      ),
    );
  }

  ////////////////////////////////////// Taps
  Widget tap(context, imagewidget) {
    return isTap
        ? Container(
            height: 37,
            width: SizeConfig.screenWidth! * 0.7,
            decoration: BoxDecoration(
                color: Colors.white,
                shape: BoxShape.rectangle,
                borderRadius: BorderRadius.circular(25.0)),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  TextButtonWidget(
                    title: firstTap!,
                    onPressed: () {
                      PublicProviders.changeTap(context, firstPageNo);
                    },
                    margin: EdgeInsets.all(5),
                    textColor:
                        pageNo != firstPageNo ? AppTheme.grey : Colors.white,
                    width: SizeConfig.screenWidth! * 0.25,
                    color:
                        pageNo == firstPageNo ? AppTheme.primary : Colors.white,
                    borderRadius: 25,
                    fontSize: SizeConfig.screenWidth! * 0.023,
                  ),
                  TextButtonWidget(
                    title: secondTap!,
                    onPressed: () {
                      PublicProviders.changeTap(context, secondPageNo);
                    },
                    margin: EdgeInsets.all(5),
                    textColor:
                        pageNo != secondPageNo ? AppTheme.grey : Colors.white,
                    width: SizeConfig.screenWidth! * 0.25,
                    color: pageNo == secondPageNo
                        ? AppTheme.primary
                        : Colors.white,
                    borderRadius: 25,
                    fontSize: SizeConfig.screenWidth! * 0.023,
                  ),
                ]))
        : Container();
  }

  //////////////////////////////////////// backgroundWidget
  Widget backgroundWidget(context) {
    return Container(
      height: SizeConfig.screenHeight! * ratio,
      width: SizeConfig.screenWidth,
      child: Background(
        color: backgroud,
        widthEdge: widthEdge,
        rightHeightEdge: rightHeightEdge,
        leftHeightEdge: leftHeightEdge,
        rigthWidthEdge: rigthWidthEdge,
      ),
    );
  }

  ////////////////////////// Title
  Widget titleWidget(context) {
    return showTitle
        ? Container(
            color: backgroud,
            width: titleWidth,
            // padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
            child: Text(
              title,
              style: TextStyle(fontSize: titleSize),
            ))
        : Container();
  }

  //////////***************Image */
  Widget imageWidget(key) {
    return Container(
      child: logo != null
          ? AnimatedSwitcher(
              duration: Duration(milliseconds: 300),
              child: ImageWidget(
                key: Key(key),
                logo: logo,
                imageWidth: imageWidth,
                imageHeight: imageHeight,
              ))
          : Container(),
    );
  }
}
