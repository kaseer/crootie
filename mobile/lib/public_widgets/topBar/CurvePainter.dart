import 'package:Crootie/style/Style.dart';
import 'package:flutter/material.dart';

class CurvePainter extends CustomPainter {
  final Color? color;
  final double? rightHeightEdge;
  final double? leftHeightEdge;
  final double? widthEdge;
  final double? rigthWidthEdge;
  CurvePainter(
      {this.color, this.rightHeightEdge, this.leftHeightEdge,
       this.widthEdge,
       this.rigthWidthEdge});
  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint();
    paint.color = color!;
    paint.style = PaintingStyle.fill;
    var path = Path();

    path.moveTo(0, size.height * leftHeightEdge!);
    path.quadraticBezierTo(
        0, size.height, size.width / widthEdge!, size.height);
    path.quadraticBezierTo(
        size.width / 4, size.height, size.width / 2, size.height);
    path.quadraticBezierTo(size.width / 2, size.height,
        size.width - (size.width / rigthWidthEdge!), size.height);

    // path.quadraticBezierTo(size.width - (size.width / 4), size.height,
    //     size.width - (size.width / 55), size.height );
    path.quadraticBezierTo(
        size.width, size.height, size.width, size.height * rightHeightEdge!);
    path.lineTo(size.width, 0);
    path.lineTo(0, 0);
    canvas.drawShadow(path, AppTheme.primary.withOpacity(0.4), 8.8, true);
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
