import 'package:flutter/material.dart';

class ImageWidget extends StatelessWidget {
  final String? logo;
  final double? imageWidth;
  final double? imageHeight;
  const ImageWidget({
    this.logo,
    this.imageWidth,
    this.imageHeight,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print(logo);
    return Container(
      child: logo != null
          ? Image(
              width: imageWidth,
              height: imageHeight,
              // alignment: Alignment.centerRight,
              image: AssetImage(logo!),
              fit: BoxFit.fitHeight,
            )
          : Container(),
    );
  }
}
