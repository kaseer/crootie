import 'package:Crootie/style/SizeConfig.dart';
import 'package:Crootie/providers/functions/CartProvider.dart';
import 'package:Crootie/public_widgets/topBar/MenuIcon.dart';
import 'package:Crootie/services/db/Cart.dart';
import 'package:Crootie/shared/Routes.dart';
import 'package:Crootie/style/AppIcons.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AppBarWidget extends StatelessWidget {
  final Color? backgroud;
  final bool isCart;
  AppBarWidget({
    this.backgroud,
    this.isCart = false,
  });
  @override
  Widget build(BuildContext context) {
    CartProvider cartProvider = Provider.of<CartProvider>(context);
    cartProvider.getCount(context);
    // CartDatabase.instance.getCount(context,cartProvider);
    return Container(
      width: SizeConfig.screenWidth,
      padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
      color: backgroud,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          InkWell(
              onTap: () {
                Routes.openPage(context, "/menu", 0);
              },
              child: MenuIcon()),
          // IconButton(
          //   icon: Icon(
          //     Icons.menu,
          //     color: Colors.white,
          //     size: 30,
          //   ),
          //   onPressed: () {
          //     Routes.openPage(context, "/menu", 0);
          //   },
          // ),
          !isCart
              ? Row(
                  children: [
                    Stack(
                      alignment: Alignment.topCenter,
                      children: <Widget>[
                        IconButton(
                          icon: Icon(
                            AppIcons.notification,
                            color: Colors.white,
                            size: 30,
                          ),
                          onPressed: () {},
                        ),
                        Container(
                          padding: EdgeInsets.all(3),
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: Colors.red,
                          ),
                          child: Text(
                            0.toString(),
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: SizeConfig.screenWidth! * 0.023,
                            ),
                          ),
                        )
                      ],
                    ),
                    Stack(
                      alignment: Alignment.center,
                      children: <Widget>[
                        IconButton(
                          icon: Icon(
                            Icons.shopping_cart_outlined,
                            color: Colors.white,
                            size: 30,
                          ),
                          onPressed: () {
                            Routes.openPage(context, "/cart", 0);
                          },
                        ),
                        Container(
                          padding: EdgeInsets.all(3),
                          margin: EdgeInsets.only(bottom: 20),
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: Colors.red,
                          ),
                          child: Text(
                            cartProvider.cart_count.toString(),
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: SizeConfig.screenWidth! * 0.023,
                            ),
                          ),
                        )
                      ],
                    ),

                    // IconButton(
                    //   icon: Icon(
                    //     Icons.shopping_cart_outlined,
                    //     color: Colors.white,
                    //     size: 30,
                    //   ),
                    //   onPressed: () {
                    //     Routes.openPage(context, "/cart", 0);
                    //   },
                    // )
                  ],
                )
              : IconButton(
                  icon: Icon(
                    Icons.close,
                    color: Colors.white,
                    size: 30,
                  ),
                  onPressed: () {
                    Routes.closePage(context, "/cart");
                  },
                )
        ],
      ),
    );
  }
}
