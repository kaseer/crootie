import 'package:flutter/material.dart';

class TextButtonWidget extends StatelessWidget {
  final double? width;
  final double? height;
  final double borderRadius;
  final Color? color;
  final Color? textColor;
  final String? title;
  final VoidCallback? onPressed;
  final EdgeInsetsGeometry? margin;
  final EdgeInsetsGeometry? padding;
  final double? fontSize;
  final bool isBorder;
  final Color? borderColor;
  TextButtonWidget(
      {this.width,
      this.height,
      this.borderRadius = 20,
      this.color,
      this.textColor,
      @required this.title,
      @required this.onPressed,
      this.margin,
      this.padding,
      this.fontSize,
      this.isBorder = false,
      this.borderColor});
  @override
  Widget build(BuildContext context) {
    return TextButton(
      style: TextButton.styleFrom(
          padding: EdgeInsets.zero,
          minimumSize: Size(50, 30),
          alignment: Alignment.centerLeft),
      onPressed: onPressed,
      child: Container(
          padding: padding,
          margin: margin,
          alignment: Alignment.center,
          width: width,
          height: height,
          decoration: BoxDecoration(
              boxShadow: color != null
                  ? [
                      BoxShadow(
                          color: color!.withOpacity(0.6),
                          spreadRadius: 1.5,
                          blurRadius: 5)
                    ]
                  : [],
              color: color,
              borderRadius: BorderRadius.circular(borderRadius),
              border:
                  isBorder ? Border.all(color: borderColor!, width: 1) : null),
          child: Text(
            title!,
            style: TextStyle(color: textColor, fontSize: fontSize),
          )),
    );
  }
}
