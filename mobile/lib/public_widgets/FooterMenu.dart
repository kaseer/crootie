import 'package:Crootie/style/SizeConfig.dart';
import 'package:Crootie/public_widgets/ListContainer.dart';
import 'package:Crootie/shared/Routes.dart';
import 'package:Crootie/style/AppIcons.dart';
import 'package:flutter/material.dart';

class FooterMenu extends StatelessWidget {
  const FooterMenu({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListContainer(
      showBorder: true,
      width: SizeConfig.screenWidth!,
      height: 50,
      margin: SizeConfig.marginBLR,
      borderRadius: 25,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          IconButton(
            onPressed: () {
              Routes.openPageFromMenu(context, "");
            },
            icon: Icon(AppIcons.home, color: Colors.white, size: 20),
          ),
          IconButton(
            onPressed: () {
              Routes.openPageFromMenu(context, "/vouchers");
            },
            icon: Icon(AppIcons.my_vouchers, color: Colors.white, size: 20),
          ),
          IconButton(
            onPressed: () {
              Routes.openPageFromMenu(context, "/favorites");
            },
            icon: Icon(Icons.favorite_border_outlined, color: Colors.white),
          ),
          IconButton(
            onPressed: () {
              Routes.openPageFromMenu(context, "/gifts");
            },
            icon: Icon(
              AppIcons.gifts,
              color: Colors.white,
              size: 20,
            ),
          ),
        ],
      ),
    );
  }
}
