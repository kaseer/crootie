import 'package:Crootie/style/SizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';

import 'package:Crootie/locale/locale_keys.g.dart';
import 'package:Crootie/shared/Constant.dart';
import 'package:Crootie/style/Style.dart';

class CardView extends StatelessWidget {
  final String? logo;
  final String? description;
  final String? voucherNumber;
  final double? price;
  final bool isPoints;
  final bool isVoucher;
  final bool isFav;
  final bool showFav;
  final Color? topColor;
  final Color? bottomColor;
  final bool isCart;
  final bool showShadow;
  final Function? addQty;
  final Function? removeQty;
  final int? quantity;
  final Function()? favRemove;
  const CardView(
      {@required this.logo,
      @required this.description,
      this.voucherNumber,
      this.price,
      this.isPoints = false,
      this.isVoucher = false,
      this.isFav = false,
      this.showFav = false,
      @required this.topColor,
      @required this.bottomColor,
      this.isCart = false,
      this.showShadow = true,
      this.addQty,
      this.removeQty,
      this.quantity,
      this.favRemove});
  @override
  Widget build(BuildContext context) {
    return Container(
        height: 65,
        // padding: EdgeInsets.fromLTRB(10,0,10,0),
        decoration: BoxDecoration(
            boxShadow: showShadow
                ? [
                    BoxShadow(
                        color: AppTheme.grey.withOpacity(0.3),
                        spreadRadius: 1,
                        blurRadius: 10)
                  ]
                : [],
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [topColor!, bottomColor!]),
            borderRadius: BorderRadius.circular(15)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
                width: SizeConfig.screenWidth! * 0.33,
                child: logoAndDescription()),
            Container(
              margin: EdgeInsets.fromLTRB(2, 0, 2, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  isCart ? cartButtons(context) : Container(),
                  Container(
                    child: isVoucher
                        ? Image(
                            height: 30,
                            width: SizeConfig.screenWidth! * 0.42,
                            // fit: BoxFit.contain,
                            image: AssetImage(Paths.encrypted),
                          )
                        : Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              voucherNo(),
                              favButton(),
                              priceWidget(),
                            ],
                          ),
                  ),
                  SizedBox(width: 10)
                ],
              ),
            ),
          ],
        ));
  }

  //************ Widgets **************/
  //----------------------------- Cart buttons----------------
  Widget button(icon, onPressed) {
    return Container(
      decoration: BoxDecoration(shape: BoxShape.circle, boxShadow: [
        BoxShadow(
          color: Colors.white,
          blurRadius: 2,
          spreadRadius: 1,
        )
      ]),
      child: ClipOval(
        child: Material(
          shadowColor: AppTheme.primary,
          elevation: 2,
          color: Colors.white, // button color
          child: InkWell(
            splashColor: AppTheme.primary, // inkwell color
            child: Container(child: icon),
            onTap: onPressed,
          ),
        ),
      ),
    );
  }

//----------------------------- Logo and description---------------
  Widget logoAndDescription() {
    return Row(
      children: [
        Container(
          margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
          child: Image(
            width: 50,
            image: AssetImage(logo!),
          ),
        ),
        Container(
          // color:Colors.red,
          // margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
          child: Text(
            description!,
            style: TextStyle(
                fontSize: SizeConfig.screenWidth! * 0.06,
                fontWeight: FontWeight.bold),
          ),
        ),
      ],
    );
  }

  //---------------------------- Cart buttons---------
  Widget cartButtons(context) {
    return Container(
        width: SizeConfig.screenWidth! * 0.3,
        padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            button(Icon(Icons.add, size: 18, color: AppTheme.grey), addQty),
            Container(
              margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: Text(
                quantity.toString(),
                style: TextStyle(fontSize: SizeConfig.screenWidth! * 0.028),
              ),
            ),
            button(
                Icon(Icons.remove, size: 17, color: AppTheme.grey), removeQty),
          ],
        ));
  }

  //-----------------Voucher number-------------
  Widget voucherNo() {
    return voucherNumber != null
        ? Container(
            decoration: BoxDecoration(color: AppTheme.grey.withOpacity(0.7)),
            child: Text(
              voucherNumber!,
              style: TextStyle(fontSize: SizeConfig.screenWidth! * 0.04),
            ))
        : Container();
  }

  //---------------Favorite button-------------
  Widget favButton() {
    return showFav
        ? Container(
            child: IconButton(
              icon:
                  Icon(isFav ? Icons.favorite : Icons.favorite_border_outlined),
              onPressed: favRemove,
            ),
          )
        : Container();
  }

  //----------Price widget----------
  Widget priceWidget() {
    return Container(
      child: price != null
          ? Text(
              isPoints
                  ? price!.toInt().toString() + LocaleKeys.currency_points.tr()
                  : price!.toInt().toString() + LocaleKeys.currency_libyan.tr(),
              style: TextStyle(fontSize: SizeConfig.screenWidth! * 0.033),
            )
          : Container(),
    );
  }
}
