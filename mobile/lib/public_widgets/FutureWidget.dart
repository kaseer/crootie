// import 'package:Crootie/models/responseModels/CategoriesResponse.dart';
import 'package:Crootie/public_widgets/Indicator.dart';
import 'package:Crootie/services/db/Categories.dart';
import 'package:Crootie/services/db/Database.dart';
import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart';

// import 'SkeletonGrid.dart';
// import 'SkeletonList.dart';
class FutureWidget extends StatelessWidget {
  final Future<dynamic>? future;
  final Function? child;
  FutureWidget({
    @required this.future,
    this.child,
    // required this.widget
  });
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: future,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          return child!(snapshot.data);
        } else if (snapshot.connectionState == ConnectionState.waiting) {
          return _dialog;
        } else {
          // return FutureBuilder(
          //   future: CategoriesDatabase.instance.readAllNotes(),
          //   builder: (context, snapshot) {
          //     print (snapshot);
          //     return Container();
          //   }
          // );
          // var s = await CrootieDatabase.instance.readAllNotes();
          return child!(snapshot.data);
        }
      },
    );
  }
}

Dialog _dialog = new Dialog(
  elevation: 0,
  backgroundColor: Colors.white.withOpacity(0),
  child: Center(child: Container(height: 100, child: new Indicator())),
);

// Future sqflite() async {
//   var db = await openDatabase('my_db.db');

// }