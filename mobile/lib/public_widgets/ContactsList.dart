import 'package:Crootie/models/requestModels/HbaibeRequest.dart';
import 'package:Crootie/models/responseModels/hbaibe/HbaibeResponse.dart';
import 'package:Crootie/services/api/apiServices/HbaibeService.dart';
import 'package:Crootie/shared/GlobalFunctions.dart';
import 'package:Crootie/style/SizeConfig.dart';
import 'package:contacts_service/contacts_service.dart';
import 'package:flutter/material.dart';
import 'package:Crootie/locale/locale_keys.g.dart';
import 'package:Crootie/providers/PublicProviders.dart';
import 'package:Crootie/public_widgets/form/inputForm.dart';
import 'package:Crootie/style/Style.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:easy_localization/easy_localization.dart';

class ContactsList extends StatefulWidget {
  @override
  _ContactsListState createState() => _ContactsListState();
}

class _ContactsListState extends State<ContactsList> {
  int selected = -1;
  bool loading = true;
  List<Contact> contacts = [];
  List<Contact> contactsFiltered = [];
  Map<String, Color> contactsColorMap = new Map();
  TextEditingController searchController = new TextEditingController();
  @override
  void initState() {
    super.initState();
    getPermissions();
  }

  getPermissions() async {
    if (await Permission.contacts.request().isGranted) {
      getAllContacts().then((value) => {
            setState(() {
              loading = false;
              contacts = value;
            })
          });
      searchController.addListener(() {
        filterContacts();
      });
    }
  }

  String flattenPhoneNumber(String phoneStr) {
    return phoneStr.replaceAllMapped(RegExp(r'^(\+)|\D'), (Match m) {
      return m[0] == "+" ? "+" : "";
    });
  }

  Future<List<Contact>> getAllContacts() async {
    List colors = [Colors.green, Colors.indigo, Colors.yellow, Colors.orange];
    int colorIndex = 0;
    List<Contact> _contacts = (await ContactsService.getContacts()).toList();
    _contacts.removeWhere((element) => element.phones!.length == 0);
    _contacts.forEach((contact) {
      Color baseColor = colors[colorIndex];
      contactsColorMap[contact.displayName!] = baseColor;
      colorIndex++;
      if (colorIndex == colors.length) {
        colorIndex = 0;
      }
    });
    return _contacts;
  }

  filterContacts() {
    List<Contact> _contacts = [];
    _contacts.addAll(contacts);
    if (searchController.text.isNotEmpty) {
      _contacts.retainWhere((contact) {
        String searchTerm = searchController.text.toLowerCase();
        String searchTermFlatten = flattenPhoneNumber(searchTerm);
        String contactName = contact.displayName!.toLowerCase();
        bool nameMatches = contactName.contains(searchTerm);
        if (nameMatches == true) {
          return true;
        }

        if (searchTermFlatten.isEmpty) {
          return false;
        }

        var phone = contact.phones!.firstWhere((phn) {
          String phnFlattened = flattenPhoneNumber(phn.value!);
          return phnFlattened.contains(searchTermFlatten);
        }, orElse: () => null!);

        return phone != null;
      });
    }
    setState(() {
      contactsFiltered = _contacts;
    });
  }

  @override
  Widget build(BuildContext context) {
    bool isSearching = searchController.text.isNotEmpty;
    bool listItemsExist = (contactsFiltered.length > 0 || contacts.length > 0);
    var props = GlobalFunctions.props(context);
    return Column(
      children: [
        Container(
          child: InputForm(
            margin: EdgeInsets.fromLTRB(30, 10, 30, 10),
            controller: searchController,
            label: LocaleKeys.search.tr(),
            labelColor: AppTheme.primary,
            borderColor: AppTheme.primary,
            icon: Icon(Icons.search),
            iconColor: AppTheme.grey,
            filled: false,
            radius: 10,
          ),
        ),
        !listItemsExist && loading
            ? Expanded(child: Center(child: CircularProgressIndicator()))
            : !listItemsExist
                ? Container(
                    padding: EdgeInsets.all(20),
                    child: Text(
                        isSearching
                            ? 'No search results to show'
                            : 'No contacts exist',
                        style: Theme.of(context).textTheme.headline6),
                  )
                : Flexible(
                    child: ListView.builder(
                      // shrinkWrap: true,
                      // physics: NeverScrollableScrollPhysics(),
                      itemCount: isSearching == true
                          ? contactsFiltered.length
                          : contacts.length,
                      itemBuilder: (context, index) {
                        Contact contact = isSearching == true
                            ? contactsFiltered[index]
                            : contacts[index];

                        var baseColor =
                            contactsColorMap[contact.displayName] as dynamic;

                        Color color1 = baseColor[800];
                        Color color2 = baseColor[400];
                        return Container(
                          margin: EdgeInsets.fromLTRB(30, 2, 30, 0),
                          color: AppTheme.primary,
                          child: ExpansionTile(
                            onExpansionChanged: ((newState) {
                              if (newState)
                                setState(() {
                                  Duration(seconds: 20000);
                                  selected = index;
                                });
                              else
                                setState(() {
                                  selected = -1;
                                });
                            }),
                            key: Key(selected.toString()), //attention
                            initiallyExpanded: index == selected, //attention
                            backgroundColor: AppTheme.secondary,
                            trailing: Icon(
                              index != selected
                                  ? Icons.keyboard_arrow_down
                                  : Icons.keyboard_arrow_up,
                              color: Colors.white,
                            ),
                            title: ListTile(
                                title: Text(
                                  contact.displayName!,
                                  style: TextStyle(color: Colors.white),
                                ),
                                leading: (contact.avatar != null &&
                                        contact.avatar!.length > 0)
                                    ? CircleAvatar(
                                        backgroundImage:
                                            MemoryImage(contact.avatar!),
                                      )
                                    : Container(
                                        decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                            gradient: LinearGradient(
                                                colors: [
                                                  color1,
                                                  color2,
                                                ],
                                                begin: Alignment.bottomLeft,
                                                end: Alignment.topRight)),
                                        child: CircleAvatar(
                                            child: Text(contact.initials(),
                                                style: TextStyle(
                                                    color: Colors.white)),
                                            backgroundColor:
                                                Colors.transparent))),
                            children: <Widget>[
                              Container(
                                margin: SizeConfig.marginBLR,
                                height: 2,
                                color: Colors.white,
                              ),
                              Container(
                                // color: AppTheme.secondary.withOpacity(0.5),
                                // margin: SizeConfig.margin,
                                child: ListView.builder(
                                  shrinkWrap: true,
                                  physics: NeverScrollableScrollPhysics(),
                                  itemCount: contact.phones!.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return Container(
                                      margin: SizeConfig.marginBLR,
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Container(
                                              child: Text(
                                                  contact.phones!
                                                      .elementAt(index)
                                                      .value
                                                      .toString(),
                                                  style: TextStyle(
                                                      fontSize: SizeConfig
                                                              .screenWidth! *
                                                          0.028))),
                                          Container(
                                            height: 20,
                                            child: IconButton(
                                              iconSize: 16,
                                              onPressed: () async {
                                                // print(contact.phones!.length);
                                                String? contactPhone =
                                                    contact.phones!.length > 0
                                                        ? contact.phones!
                                                            .elementAt(0)
                                                            .value
                                                        : '';
                                                HbaibeRequest data =
                                                    new HbaibeRequest(
                                                        memberName: contact
                                                            .displayName!,
                                                        phoneNo: contactPhone!);
                                                //-------------------------- if 0 then its add contact for send operations
                                                //-------------------------- if 1 then its add contact for hbaibe list
                                                if (props['id'] == 1) {
                                                  HbaibeResponse? add =
                                                      await HbaibeService.add(
                                                          context, data);
                                                  if (add != null) {
                                                    PublicProviders
                                                        .addHbaibeContact(
                                                            context, add);
                                                    return;
                                                  }
                                                }
                                                PublicProviders.selectContact(
                                                    context,
                                                    data);
                                              },
                                              icon: Icon(
                                                Icons.send,
                                                color: Colors.white,
                                                // size: 14,
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                    );
                                  },
                                ),
                              )
                            ],
                          ),
                          // child: ListTile(
                          //   onTap: (){
                          //     print(contact.phones!.length);
                          //     String? contactPhone = contact.phones!.length > 0
                          //           ? contact.phones!.elementAt(0).value
                          //           : '';
                          //     // PublicProviders.selectContact(context, contactPhone!, contact.displayName!);
                          //   },
                          //     trailing: Icon(
                          //       Icons.arrow_forward_ios_outlined,
                          //       color: Colors.white,
                          //     ),
                          //     title: Text(
                          //       contact.displayName!,
                          //       style: TextStyle(color: Colors.white),
                          //     ),
                          //     subtitle: Text(
                          //       (contact.phones!.length > 0
                          //           ? contact.phones!.elementAt(0).value
                          //           : '')!,
                          //       style: TextStyle(color: Colors.white,fontSize: 14),
                          //     ),
                          //     leading: (contact.avatar != null &&
                          //             contact.avatar!.length > 0)
                          //         ? CircleAvatar(
                          //             backgroundImage: MemoryImage(contact.avatar!),
                          //           )
                          //         : Container(
                          //             decoration: BoxDecoration(
                          //                 shape: BoxShape.circle,
                          //                 gradient: LinearGradient(
                          //                     colors: [
                          //                       color1,
                          //                       color2,
                          //                     ],
                          //                     begin: Alignment.bottomLeft,
                          //                     end: Alignment.topRight)),
                          //             child: CircleAvatar(
                          //                 child: Text(contact.initials(),
                          //                     style: TextStyle(color: Colors.white)),
                          //                 backgroundColor: Colors.transparent))),
                        );
                      },
                    ),
                  ),
        SizedBox(height: SizeConfig.screenHeight! * 0.1)
      ],
    );
  }
}
