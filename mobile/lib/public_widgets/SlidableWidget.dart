import 'package:Crootie/style/SizeConfig.dart';
import 'package:flutter/material.dart';

import 'package:flutter_slidable/flutter_slidable.dart';

import 'CardView.dart';
// enum SlidableAction { add, buy }

class SlidableWidget<T> extends StatelessWidget {
  // final Widget child;
  final VoidCallback? rightFunction;
  final VoidCallback? leftFunction;
  final String? rightDescription;
  final String? leftDescription;
  final Color? slideColor;
  final String? logo;
  final String? description;
  final String? voucherNumber;
  final double? price;
  final bool isPoints;
  final bool isVoucher;
  final bool isFav;
  final bool showFav;
  final bool isLeftSlider;
  final Color? topColor;
  final Color? bottomColor;
  final int index;
  final Function()? favRemove;
  const SlidableWidget({
    // @required this.child,
    @required this.rightFunction,
    this.leftFunction,
    @required this.rightDescription,
    @required this.leftDescription,
    @required this.slideColor,
    @required this.logo,
    @required this.description,
    this.voucherNumber,
    this.price,
    this.isPoints = false,
    this.isVoucher = false,
    this.isFav = false,
    this.showFav = false,
    @required this.topColor,
    @required this.bottomColor,
    this.isLeftSlider = true,
    this.index = 1,
    this.favRemove,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => slidable();

  Widget slidable() {
    return Slidable(
      actionPane: SlidableBehindActionPane(),
      child: CardView(
          description: description,
          logo: logo,
          isFav: isFav,
          isPoints: isPoints,
          isVoucher: isVoucher,
          price: price,
          voucherNumber: voucherNumber,
          showFav: showFav,
          topColor: topColor,
          bottomColor: bottomColor,
          favRemove: favRemove),

      actionExtentRatio: 0.06,

      /// left side
      actions: isLeftSlider
          ? <Widget>[
              Container(
                child: IconSlideAction(
                  closeOnTap: false,
                  color: Color(0xffF6F6F6).withOpacity(0.0),
                  onTap: leftFunction,
                  iconWidget: Container(
                      alignment: Alignment.center,
                      height: double.infinity,
                      child: RotatedBox(
                          quarterTurns: 3,
                          child: Text(
                            leftDescription!,
                            style: TextStyle(fontSize: 10),
                          )),
                      decoration: BoxDecoration(
                          color: slideColor,
                          shape: BoxShape.rectangle,
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(25),
                            bottomLeft: Radius.circular(25),
                          ))),
                ),
              )
            ]
          : [],

      /// right side
      secondaryActions: <Widget>[
        Container(
          child: IconSlideAction(
            color: Color(0xffF6F6F6).withOpacity(0.0),
            onTap: rightFunction,
            iconWidget: Container(
                alignment: Alignment.center,
                height: double.infinity,
                child: RotatedBox(
                    quarterTurns: 3,
                    child: Text(
                      rightDescription!,
                      style:
                          TextStyle(fontSize: SizeConfig.screenWidth! * 0.023),
                    )),
                decoration: BoxDecoration(
                    color: slideColor,
                    shape: BoxShape.rectangle,
                    borderRadius: BorderRadius.only(
                      topRight: Radius.circular(25),
                      bottomRight: Radius.circular(25),
                    ))),
          ),
        ),
      ],
    );
  }
}
