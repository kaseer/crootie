import 'package:Crootie/models/requestModels/HbaibeRequest.dart';
import 'package:Crootie/models/responseModels/hbaibe/HbaibeResponse.dart';
import 'package:Crootie/services/api/apiServices/HbaibeService.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:Crootie/locale/locale_keys.g.dart';

import 'package:Crootie/style/SizeConfig.dart';
import 'package:Crootie/providers/PublicProviders.dart';
import 'package:Crootie/style/Style.dart';
import 'ListContainer.dart';
import 'TextButtonWidget.dart';
import 'form/inputForm.dart';

TextEditingController phoneNo = new TextEditingController();
TextEditingController memberName = new TextEditingController();
void add(context) {
  Navigator.pop(context);
}

class AddContact extends StatelessWidget {
  final String type;
  AddContact({required this.type});
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(20.0))),
      backgroundColor: AppTheme.primary,
      scrollable: true,
      content: ListContainer(
        height: SizeConfig.screenHeight! * 0.25,
        child: Wrap(
          runSpacing: 20,
          alignment: WrapAlignment.center,
          children: [
            InputForm(
              filled: true,
              fillColor: Colors.white,
              controller: memberName,
              icon: Icon(Icons.title),
              iconColor: AppTheme.grey,
              labelColor: AppTheme.grey,
              borderColor: Colors.white,
              radius: 10,
              label: LocaleKeys.contact.tr(),
            ),
            InputForm(
              filled: true,
              fillColor: Colors.white,
              controller: phoneNo,
              icon: Icon(Icons.phone),
              iconColor: AppTheme.grey,
              labelColor: AppTheme.grey,
              borderColor: Colors.white,
              radius: 10,
              label: LocaleKeys.phone.tr(),
              isNumber: true,
            ),
            TextButtonWidget(
              title: LocaleKeys.add.tr(),
              fontSize: SizeConfig.screenWidth! * 0.037,
              onPressed: () async {
                HbaibeRequest data = new HbaibeRequest(
                    memberName: memberName.text, phoneNo: phoneNo.text);
                if (type == "hbaibeContact") {
                  HbaibeResponse? add = await HbaibeService.add(context, data);
                  if (add != null) {
                    PublicProviders.addHbaibeContact(context, add);
                    phoneNo.clear();
                    memberName.clear();
                    return;
                  }
                }
                PublicProviders.selectContact(context, data);
              },
              borderColor: Colors.white,
              borderRadius: 25,
              color: AppTheme.primary,
              height: 50,
              isBorder: true,
              textColor: Colors.white,
              width: 100,
            )
          ],
        ),
      ),
    );
  }
}
