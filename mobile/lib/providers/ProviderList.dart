import 'package:Crootie/providers/functions/SelectCardProvider.dart';
import 'package:Crootie/providers/functions/ContactProvider.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';

import 'functions/CartProvider.dart';
// import 'functions/CategoriesProvider.dart';
import 'functions/ChangePage.dart';
import 'functions/InvoicesProvider.dart';
import 'functions/TicketsProvider.dart';
import 'functions/VouchersProvider.dart';

class ProviderList {
  ProviderList._();
  static List<SingleChildWidget> providers = [
    // ChangeNotifierProvider(create: (_) => CategoriesProvider()),
    ChangeNotifierProvider(create: (_) => ChangePage()),
    ChangeNotifierProvider(create: (_) => SelectCardProvider()),
    ChangeNotifierProvider(create: (_) => CartProvider()),
    ChangeNotifierProvider(create: (_) => ContactProvider()),
    ChangeNotifierProvider(create: (_) => VouchersProvider()),
    ChangeNotifierProvider(create: (_) => InvoicesProvider()),
    ChangeNotifierProvider(create: (_) => TicketsProvider()),
  ];
}
