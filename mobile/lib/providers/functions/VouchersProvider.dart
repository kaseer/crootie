import 'package:Crootie/models/responseModels/vouchers/NewVouchersResponse.dart';
import 'package:flutter/material.dart';

class VouchersProvider extends ChangeNotifier {
  List<NewVouchersResponse> newVouchers = [];

  List<NewVouchersResponse> get new_vouchers => newVouchers;
  void set new_vouchers(List<NewVouchersResponse> list) {
    this.newVouchers = list;
  }

  void removeNewVoucher(List<NewVouchersResponse> list) {
    this.new_vouchers = list;
    notifyListeners();
  }
}
