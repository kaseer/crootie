import 'package:Crootie/models/responseModels/support/TicketsResponse.dart';
import 'package:flutter/material.dart';

class TicketsProvider extends ChangeNotifier {
  List<TicketsResponse> tickets = [];

  void addTicket(BuildContext context, TicketsResponse ticket) {
    tickets.add(ticket);
    Navigator.pop(context, true);
    notifyListeners();
  }
}
