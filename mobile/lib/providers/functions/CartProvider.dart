import 'package:Crootie/models/responseModels/AllProductsResponse.dart';
import 'package:Crootie/services/db/Cart.dart';
import 'package:flutter/material.dart';

class CartProvider extends ChangeNotifier {
  int cartCount = 0;

  int get cart_count => cartCount;
  void set cart_count(int count) {
    this.cartCount = count;
  }

  List<AllProductsResponse> list = [];

  void addToCart() {
    cartCount++;
    notifyListeners();
  }

  void removeFromCart() {
    if (cartCount <= 0) return;
    cartCount--;
    notifyListeners();
  }

  void cart(count) {
    cartCount = count;
    notifyListeners();
  }

  void cartList(List<AllProductsResponse> cartList) {
    list = cartList;
    notifyListeners();
  }

  // void addCartQuantity(int index) {
  //   list[index].quantity++;
  //   cartCount++;
  //   notifyListeners();
  // }

  // void removeCartQuantity(int index) {
  //   if (list[index].quantity == 1) {
  //     list.removeAt(index);
  //   }
  //   else{
  //     list[index].quantity--;
  //   }
  //   cartCount--;
  //   notifyListeners();
  // }
  Future<void> getCount(BuildContext context) async {
    cartCount = await CartDatabase.instance.getCount();
    notifyListeners();
  }

  Future<void> emptyCart(BuildContext context) async {
    cartCount = await CartDatabase.instance.emptyCart();
    notifyListeners();
  }
}
