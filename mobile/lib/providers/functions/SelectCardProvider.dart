import 'package:Crootie/models/responseModels/AllProductsResponse.dart';
import 'package:flutter/material.dart';

class SelectCardProvider extends ChangeNotifier {
  AllProductsResponse product = AllProductsResponse(
      bottomColor: "",
      logo: '',
      price: 0.0,
      priceDescription: '',
      productId: 0,
      quantity: 0,
      topColor: '');
  bool isSelected = false;

  void selectCard(context, AllProductsResponse data) {
    product = data;
    isSelected = true;
    Navigator.pop(context, true);
    notifyListeners();
  }
}
