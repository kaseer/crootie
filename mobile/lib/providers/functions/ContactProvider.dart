import 'package:Crootie/models/requestModels/HbaibeRequest.dart';
import 'package:Crootie/models/responseModels/hbaibe/HbaibeResponse.dart';
import 'package:flutter/material.dart';

class ContactProvider extends ChangeNotifier {
  HbaibeRequest contactProvider = HbaibeRequest(memberName: '', phoneNo:'');
  List<HbaibeResponse> contacts = [];

  void selectContact(BuildContext context, HbaibeRequest contact,) {
    contactProvider = contact;
    Navigator.pop(context, true);
    notifyListeners();
  }
  void addHbaibeContact(BuildContext context, HbaibeResponse contact) {
    contacts.add(contact);
    contactProvider = HbaibeRequest(memberName: '', phoneNo:'');
    Navigator.pop(context, true);
    notifyListeners();
  }
}
