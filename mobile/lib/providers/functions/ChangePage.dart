// import 'dart:ffi';

import 'package:Crootie/data/VoucherModel.dart';

import 'package:flutter/material.dart';

class ChangePage extends ChangeNotifier {
  int pageNo = 0;
  bool isLogined = false;
  VouchersData voucher = VouchersData();
  bool isSelected = false;
  bool disableIntro = false;

  // ignore: unused_element
  // bool get _isSelected => isSelected;

  void changeTap(page) {
    pageNo = page;
    notifyListeners();
  }

  void login() {
    isLogined = true;
    notifyListeners();
  }

  void reset() {
    pageNo = 1;
    voucher = new VouchersData();
    isSelected = false;
    notifyListeners();
  }
}
