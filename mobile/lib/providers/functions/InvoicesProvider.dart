import 'package:Crootie/models/responseModels/invoices/InvoicesResponse.dart';
import 'package:flutter/material.dart';

class InvoicesProvider extends ChangeNotifier {
  List<InvoicesResponse> invoices = [];

  void getInvoices(List<InvoicesResponse> _invoices) {
    invoices = _invoices;
    notifyListeners();
  }
}
