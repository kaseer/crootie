import 'package:Crootie/models/requestModels/HbaibeRequest.dart';
import 'package:Crootie/models/responseModels/AllProductsResponse.dart';
import 'package:Crootie/models/responseModels/hbaibe/HbaibeResponse.dart';
import 'package:Crootie/models/responseModels/invoices/InvoicesResponse.dart';
import 'package:Crootie/models/responseModels/support/TicketsResponse.dart';
import 'package:Crootie/providers/functions/InvoicesProvider.dart';
import 'package:flutter/widgets.dart';
import 'package:Crootie/providers/functions/CartProvider.dart';
import 'package:Crootie/providers/functions/ChangePage.dart';
import 'package:Crootie/providers/functions/ContactProvider.dart';
import 'package:provider/provider.dart';
import 'functions/SelectCardProvider.dart';
import 'functions/TicketsProvider.dart';

class PublicProviders {
  PublicProviders._();
  static void addToCart(BuildContext context) {
    return Provider.of<CartProvider>(context, listen: false).addToCart();
  }

  static void removeFromCart(BuildContext context) {
    return Provider.of<CartProvider>(context, listen: false).removeFromCart();
  }

  static Future<void> emptyCart(BuildContext context) async {
    return Provider.of<CartProvider>(context, listen: false).emptyCart(context);
  }
  // static void addCartQuantity(BuildContext context, int index) {
  //   return Provider.of<CartProvider>(context, listen: false).addCartQuantity(index);
  // }

  // static void removeCartQuantity(BuildContext context, int index) {
  //   return Provider.of<CartProvider>(context, listen: false).removeCartQuantity(index);
  // }

  static void changeTap(BuildContext context, int page) {
    return Provider.of<ChangePage>(context, listen: false).changeTap(page);
  }

  static void login(BuildContext context) {
    return Provider.of<ChangePage>(context, listen: false).login();
  }

  static void reset(BuildContext context) {
    return Provider.of<ChangePage>(context, listen: false).reset();
  }

  static void selectCard(BuildContext context, AllProductsResponse data) {
    return Provider.of<SelectCardProvider>(context, listen: false)
        .selectCard(context, data);
  }

  static void selectContact(BuildContext context, HbaibeRequest contact) {
    return Provider.of<ContactProvider>(context, listen: false)
        .selectContact(context, contact);
  }

  static void addHbaibeContact(BuildContext context, HbaibeResponse contact) {
    return Provider.of<ContactProvider>(context, listen: false)
        .addHbaibeContact(context, contact);
  }

  static void getInvoices(
      BuildContext context, List<InvoicesResponse> invoices) {
    return Provider.of<InvoicesProvider>(context, listen: false)
        .getInvoices(invoices);
  }

  static void addTicket(BuildContext context, TicketsResponse ticket) {
    return Provider.of<TicketsProvider>(context, listen: false)
        .addTicket(context, ticket);
  }
}
