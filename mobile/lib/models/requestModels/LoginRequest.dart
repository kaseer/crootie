class LoginRequest {
  String mobileNo;
  String password;
  LoginRequest({
    required this.mobileNo,
    required this.password,
  });
}