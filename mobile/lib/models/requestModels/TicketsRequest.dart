class TicketsRequest {
  String subject;
  String problem;
  TicketsRequest({
    required this.subject,
    required this.problem,
  });
}
