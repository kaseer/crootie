import 'dart:convert';

List<TicketsResponse> ticketsResponseFromJson(String str) {
  final jsonData = json.decode(str);
  return new List<TicketsResponse>.from(
      jsonData.map((x) => TicketsResponse.fromJson(x)));
}

TicketsResponse ticketResponseFromJson(String str) {
  final jsonData = json.decode(str);
  return jsonData.map((x) => TicketsResponse.fromJson(x));
}

String ticketsResponseToJson(List<TicketsResponse> data) {
  final dyn = new List<dynamic>.from(data.map((x) => x.toJson()));
  return json.encode(dyn);
}

class TicketsResponse {
  int ticketId;
  String ticketNo;
  String ticketSubject;
  int status;

  TicketsResponse({
    required this.ticketId,
    required this.ticketNo,
    required this.ticketSubject,
    required this.status,
  });

  factory TicketsResponse.fromJson(Map<String, dynamic> json) =>
      new TicketsResponse(
        ticketId: json["ticketId"],
        ticketNo: json["ticketNo"],
        ticketSubject: json["ticketSubject"],
        status: json["status"],
      );
  Map<String, dynamic> toJson() => {
        "ticketId": ticketId,
        "ticketNo": ticketNo,
        "ticketSubject": ticketSubject,
        "status": status,
      };
}
