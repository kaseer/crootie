import 'dart:convert';

List<BuisnessProductsResponse> buisnessProductsResponseFromJson(String str) {
  final jsonData = json.decode(str);
  return new List<BuisnessProductsResponse>.from(
      jsonData.map((x) => BuisnessProductsResponse.fromJson(x)));
}

String buisnessProductsResponseToJson(List<BuisnessProductsResponse> data) {
  final dyn = new List<dynamic>.from(data.map((x) => x.toJson()));
  return json.encode(dyn);
}

class BuisnessProductsResponse {
  String logo;
  String topColor;
  String bottomColor;
  int productId;
  String priceDescription;
  double price;

  BuisnessProductsResponse(
      {required this.logo,
      required this.topColor,
      required this.bottomColor,
      required this.productId,
      required this.priceDescription,
      required this.price}) ;

  factory BuisnessProductsResponse.fromJson(Map<String, dynamic> json) =>
      new BuisnessProductsResponse(
        productId: json["productId"],
        priceDescription: json["priceDescription"],
        price: json["price"],
        logo: json["logo"],
        topColor: json["topColor"],
        bottomColor: json["bottomColor"],
      );
  Map<String, dynamic> toJson() => {
        "productId": productId,
        "priceDescription": priceDescription,
        "price": price,
        "logo": logo,
        "topColor": topColor,
        "bottomColor": bottomColor,
      };
}
