import 'dart:convert';

List<BuisnessCompaniesResponse> buisnessCompaniesResponseFromJson(String str) {
  final jsonData = json.decode(str);
  return new List<BuisnessCompaniesResponse>.from(
      jsonData.map((x) => BuisnessCompaniesResponse.fromJson(x)));
}

String buisnessCompaniesResponseToJson(List<BuisnessCompaniesResponse> data) {
  final dyn = new List<dynamic>.from(data.map((x) => x.toJson()));
  return json.encode(dyn);
}



class BuisnessCompaniesResponse {
  int buisnessId;
  String buisnessCompany;

  BuisnessCompaniesResponse({
    required this.buisnessId,
    required this.buisnessCompany,
  });

  factory BuisnessCompaniesResponse.fromJson(Map<String, dynamic> json) =>
      new BuisnessCompaniesResponse(
        buisnessId: json["buisnessId"],
        buisnessCompany: json["buisnessCompany"],
      );
  Map<String, dynamic> toJson() => {
        "buisnessId": buisnessId,
        "buisnessCompany": buisnessCompany,
      };
}
