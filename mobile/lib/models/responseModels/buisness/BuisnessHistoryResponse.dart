import 'dart:convert';

List<BuisnessHistoryResponse> buisnessHistoryResponseFromJson(String str) {
  final jsonData = json.decode(str);
  return new List<BuisnessHistoryResponse>.from(
      jsonData.map((x) => BuisnessHistoryResponse.fromJson(x)));
}

String buisnessHistoryResponseToJson(List<BuisnessHistoryResponse> data) {
  final dyn = new List<dynamic>.from(data.map((x) => x.toJson()));
  return json.encode(dyn);
}



class BuisnessHistoryResponse {
  int buisnessId;
  String buisnessCompany;
  String buisnessCard;
  int quantity;

  BuisnessHistoryResponse({
    required this.buisnessId,
    required this.buisnessCompany,
    required this.buisnessCard,
    required this.quantity,
  });

  factory BuisnessHistoryResponse.fromJson(Map<String, dynamic> json) =>
      new BuisnessHistoryResponse(
        buisnessId: json["buisnessId"],
        buisnessCompany: json["buisnessCompany"],
        buisnessCard: json["buisnessCard"],
        quantity: json["quantity"],
      );
  factory BuisnessHistoryResponse.localFromJson(Map<String, dynamic> json) =>
      new BuisnessHistoryResponse(
        buisnessId: json["buisnessId"],
        buisnessCompany: json["buisnessCompany"],
        buisnessCard: json["buisnessCard"],
        quantity: json["quantity"],
      );
  Map<String, dynamic> toJson() => {
        "buisnessId": buisnessId,
        "buisnessCompany": buisnessCompany,
        "buisnessCard": buisnessCard,
        "quantity": quantity,
      };
}
