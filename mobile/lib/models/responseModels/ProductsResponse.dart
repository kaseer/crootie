import 'dart:convert';

List<ProductsResponse> productsResponseFromJson(String str) {
  final jsonData = json.decode(str);
  return new List<ProductsResponse>.from(
      jsonData.map((x) => ProductsResponse.fromJson(x)));
}

String productsResponseToJson(List<ProductsResponse> data) {
  final dyn = new List<dynamic>.from(data.map((x) => x.toJson()));
  return json.encode(dyn);
}

class ProductsFields {
  static final List<String> values = [
    /// Add all fields
    productId, priceDescription, price, isFav
  ];
  static final String productId = 'productId';
  static final String companyId = 'companyId';
  static final String priceDescription = 'priceDescription';
  static final String price = 'price';
  static final String isFav = 'isFav';
}

class ProductsResponse {
  int productId;
  String priceDescription;
  double price;
  bool isFav;

  ProductsResponse({
    required this.productId,
    required this.priceDescription,
    required this.price,
    required this.isFav,
  });

  factory ProductsResponse.fromJson(Map<String, dynamic> json) =>
      new ProductsResponse(
        productId: json["productId"],
        priceDescription: json["priceDescription"],
        price: json["price"],
        isFav: json["isFav"],
      );
  factory ProductsResponse.localFromJson(Map<String, dynamic> json) =>
      new ProductsResponse(
        productId: json["productId"],
        priceDescription: json["priceDescription"],
        price: json["price"],
        isFav: json["isFav"] == "1" ? true : false,
      );
  Map<String, dynamic> toJson() => {
        "productId": productId,
        "priceDescription": priceDescription,
        "price": price,
        "isFav": isFav,
      };
}
