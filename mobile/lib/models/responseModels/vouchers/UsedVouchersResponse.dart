import 'dart:convert';

List<UsedVouchersResponse> usedVouchersResponseFromJson(String str) {
  final jsonData = json.decode(str);
  return new List<UsedVouchersResponse>.from(
      jsonData.map((x) => UsedVouchersResponse.fromJson(x)));
}

String usedVouchersResponseToJson(List<UsedVouchersResponse> data) {
  final dyn = new List<dynamic>.from(data.map((x) => x.toJson()));
  return json.encode(dyn);
}

class UsedVouchersResponse {
  int voucherId;
  String priceDescription;
  String logo;
  String topColor;
  String bottomColor;
  String serialNo;

  UsedVouchersResponse({
    required this.voucherId,
    required this.priceDescription,
    required this.logo,
    required this.topColor,
    required this.bottomColor,
    required this.serialNo,
  });

  factory UsedVouchersResponse.fromJson(Map<String, dynamic> json) =>
      new UsedVouchersResponse(
        voucherId: json["voucherId"],
        priceDescription: json["priceDescription"],
        logo: json["logo"],
        topColor: json["topColor"],
        bottomColor: json["bottomColor"],
        serialNo: json["serialNo"],
      );
  Map<String, dynamic> toJson() => {
        "voucherId": voucherId,
        "priceDescription": priceDescription,
        "logo": logo,
        "topColor": topColor,
        "bottomColor": bottomColor,
        "serialNo": serialNo,
      };
}
