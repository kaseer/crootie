import 'dart:convert';

List<NewVouchersResponse> newVouchersResponseFromJson(String str) {
  final jsonData = json.decode(str);
  return new List<NewVouchersResponse>.from(
      jsonData.map((x) => NewVouchersResponse.fromJson(x)));
}

String newVouchersResponseToJson(List<NewVouchersResponse> data) {
  final dyn = new List<dynamic>.from(data.map((x) => x.toJson()));
  return json.encode(dyn);
}

class NewVouchersResponse {
  int voucherId;
  String priceDescription;
  String logo;
  String topColor;
  String bottomColor;

  NewVouchersResponse({
    required this.voucherId,
    required this.priceDescription,
    required this.logo,
    required this.topColor,
    required this.bottomColor,
  });

  factory NewVouchersResponse.fromJson(Map<String, dynamic> json) =>
      new NewVouchersResponse(
        voucherId: json["voucherId"],
        priceDescription: json["priceDescription"],
        logo: json["logo"],
        topColor: json["topColor"],
        bottomColor: json["bottomColor"],
      );
  Map<String, dynamic> toJson() => {
        "voucherId": voucherId,
        "priceDescription": priceDescription,
        "logo": logo,
        "topColor": topColor,
        "bottomColor": bottomColor,
      };
}
