import 'dart:convert';

List<InvoiceResponse> invoiceResponseFromJson(String str) {
  final jsonData = json.decode(str);
  return new List<InvoiceResponse>.from(
      jsonData.map((x) => InvoiceResponse.fromJson(x)));
}

String invoiceResponseToJson(List<InvoiceResponse> data) {
  final dyn = new List<dynamic>.from(data.map((x) => x.toJson()));
  return json.encode(dyn);
}

class InvoiceResponse {
  String description;
  double price;
  int quantity;

  InvoiceResponse(
      {required this.description, required this.price, required this.quantity});

  factory InvoiceResponse.fromJson(Map<String, dynamic> json) =>
      new InvoiceResponse(
        description: json["description"],
        price: json["price"],
        quantity: json["quantity"],
      );
  Map<String, dynamic> toJson() => {
        "description": description,
        "price": price,
        "quantity": quantity,
      };
}
