import 'dart:convert';

List<InvoicesResponse> invoicesResponseFromJson(String str) {
  final jsonData = json.decode(str);
  return new List<InvoicesResponse>.from(
      jsonData.map((x) => InvoicesResponse.fromJson(x)));
}

String invoicesResponseToJson(List<InvoicesResponse> data) {
  final dyn = new List<dynamic>.from(data.map((x) => x.toJson()));
  return json.encode(dyn);
}

class InvoicesResponse {
  int intvoiceId;
  String invoiceNo;
  String createdOn;
  double totalPrice;

  InvoicesResponse(
      {required this.intvoiceId,
      required this.invoiceNo,
      required this.createdOn,
      required this.totalPrice});

  factory InvoicesResponse.fromJson(Map<String, dynamic> json) =>
      new InvoicesResponse(
        intvoiceId: json["intvoiceId"],
        invoiceNo: json["invoiceNo"],
        createdOn: json["createdOn"],
        totalPrice: json["totalPrice"],
      );
  Map<String, dynamic> toJson() => {
        "intvoiceId": intvoiceId,
        "invoiceNo": invoiceNo,
        "createdOn": createdOn,
        "totalPrice": totalPrice,
      };
}
