import 'dart:convert';

List<RedeemResponse> redeemResponseFromJson(String str) {
  final jsonData = json.decode(str);
  return new List<RedeemResponse>.from(
      jsonData.map((x) => RedeemResponse.fromJson(x)));
}

String redeemResponseToJson(List<RedeemResponse> data) {
  final dyn = new List<dynamic>.from(data.map((x) => x.toJson()));
  return json.encode(dyn);
}

class RedeemResponse {
  String logo;
  String topColor;
  String bottomColor;
  int productId;
  String priceDescription;
  int points;

  RedeemResponse(
      {required this.logo,
      required this.topColor,
      required this.bottomColor,
      required this.productId,
      required this.priceDescription,
      required this.points});

  factory RedeemResponse.fromJson(Map<String, dynamic> json) =>
      new RedeemResponse(
        productId: json["productId"],
        priceDescription: json["priceDescription"],
        points: json["points"],
        logo: json["logo"],
        topColor: json["topColor"],
        bottomColor: json["bottomColor"],
      );
  Map<String, dynamic> toJson() => {
        "productId": productId,
        "priceDescription": priceDescription,
        "points": points,
        "logo": logo,
        "topColor": topColor,
        "bottomColor": bottomColor,
      };
}
