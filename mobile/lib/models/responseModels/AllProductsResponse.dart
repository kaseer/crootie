import 'dart:convert';

List<AllProductsResponse> allProductsResponseFromJson(String str) {
  final jsonData = json.decode(str);
  return new List<AllProductsResponse>.from(
      jsonData.map((x) => AllProductsResponse.fromJson(x)));
}

String allProductsResponseToJson(List<AllProductsResponse> data) {
  final dyn = new List<dynamic>.from(data.map((x) => x.toJson()));
  return json.encode(dyn);
}

class AllProductsResponse {
  int productId;
  String priceDescription;
  String logo;
  double price;
  int quantity;
  String topColor;
  String bottomColor;

  AllProductsResponse({
    required this.productId,
    required this.priceDescription,
    required this.logo,
    required this.price,
    required this.quantity,
    required this.topColor,
    required this.bottomColor,
  });

  factory AllProductsResponse.fromJson(Map<String, dynamic> json) =>
      new AllProductsResponse(
        productId: json["productId"],
        priceDescription: json["priceDescription"],
        logo: json["logo"],
        price: json["price"],
        quantity: json["quantity"],
        topColor: json["topColor"],
        bottomColor: json["bottomColor"],
      );
  Map<String, dynamic> toJson() => {
        "productId": productId,
        "priceDescription": priceDescription,
        "logo": logo,
        "price": price,
        "quantity": quantity,
        "topColor": topColor,
        "bottomColor": bottomColor,
      };
}
