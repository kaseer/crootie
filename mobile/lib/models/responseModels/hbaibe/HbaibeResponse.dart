import 'dart:convert';

List<HbaibeResponse> hbaibeResponseFromJson(String str) {
  final jsonData = json.decode(str);
  return new List<HbaibeResponse>.from(
      jsonData.map((x) => HbaibeResponse.fromJson(x)));
}

String hbaibeResponseToJson(List<HbaibeResponse> data) {
  final dyn = new List<dynamic>.from(data.map((x) => x.toJson()));
  return json.encode(dyn);
}

class HbaibeResponse {
  int hbaibeId;
  String memberName;
  String phoneNo;

  HbaibeResponse({
    required this.hbaibeId,
    required this.memberName,
    required this.phoneNo,
  });

  factory HbaibeResponse.fromJson(Map<String, dynamic> json) =>
      new HbaibeResponse(
        hbaibeId: json["hbaibeId"],
        memberName: json["memberName"],
        phoneNo: json["phoneNo"],
      );
  Map<String, dynamic> toJson() => {
        "hbaibeId": hbaibeId,
        "memberName": memberName,
        "phoneNo": phoneNo,
      };
}
