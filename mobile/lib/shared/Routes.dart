import 'package:Crootie/screens/profile/mainWidgets/ChangePassword.dart';
import 'package:flutter/material.dart';
import 'package:Crootie/providers/PublicProviders.dart';
import 'package:Crootie/public_widgets/ContactsList.dart';
import 'package:Crootie/screens/RoutePage.dart';
import 'package:Crootie/screens/aboutUs/AboutUs.dart';
import 'package:Crootie/screens/buisness/Buisness.dart';
import 'package:Crootie/screens/buisness/mainWidgets/BuisnessProducts.dart';
import 'package:Crootie/screens/cart/Cart.dart';
import 'package:Crootie/screens/chargeMe/ChargeMe.dart';
import 'package:Crootie/screens/chargeMe/mainWidgets/CardsList.dart';
import 'package:Crootie/screens/faq/Faq.dart';
import 'package:Crootie/screens/favorites/Favorites.dart';
import 'package:Crootie/screens/finalInvoice/FinalInvoice.dart';
import 'package:Crootie/screens/gifts/Gifts.dart';
import 'package:Crootie/screens/hbaibe/Hbaibe.dart';
import 'package:Crootie/screens/home/mainWidgets/Products.dart';
import 'package:Crootie/screens/home/mainWidgets/Companies.dart';
import 'package:Crootie/screens/intro/mainWidgets/FirstIntro.dart';
import 'package:Crootie/screens/intro/mainWidgets/SecondIntro.dart';
import 'package:Crootie/screens/invoices/Invoices.dart';
import 'package:Crootie/screens/invoices/mainWidgets/InvoiceDetails.dart';
import 'package:Crootie/screens/login/Login.dart';
import 'package:Crootie/screens/login/mainWidgets/Signup.dart';
import 'package:Crootie/screens/login/mainWidgets/Verification.dart' as S;
import 'package:Crootie/screens/menu/Menu.dart';
import 'package:Crootie/screens/payments/paymentInfo/PaymentInfo.dart';
import 'package:Crootie/screens/payments/payments/Payments.dart';
import 'package:Crootie/screens/payments/selectedPayment/SelectedPayment.dart';
import 'package:Crootie/screens/payments/verification/Verification.dart';
import 'package:Crootie/screens/profile/Profile.dart';
import 'package:Crootie/screens/redeemPoints/RedeemPoints.dart';
import 'package:Crootie/screens/settings/Settings.dart';
import 'package:Crootie/screens/support/Support.dart';
import 'package:Crootie/screens/support/mainWidgets/NewTicket.dart';
import 'package:Crootie/screens/support/mainWidgets/TicketDetails.dart';
import 'package:Crootie/screens/vouchers/Vouchers.dart';
import 'package:Crootie/screens/vouchers/mainWidgets/sendVoucher/SendVoucher.dart';

class Routes {
  Routes._();
  static Map<String, Widget Function(BuildContext)> routes = {
    '/menu': (context) => Menu(),
    '/companies': (context) => RoutePage(child: Companies()),
    '/products': (context) => RoutePage(child: Products()),
    '/vouchers': (context) => RoutePage(child: Vouchers()),
    '/favorites': (context) => RoutePage(child: Favorites()),
    '/gifts': (context) => RoutePage(child: Gifts()),
    '/redeempoints': (context) => RoutePage(child: RedeemPoints()),
    '/invoices': (context) => RoutePage(child: Invoices()),
    '/invoicedetails': (context) => RoutePage(child: InvoiceDetails()),
    '/buisness': (context) => RoutePage(child: Buisness()),
    '/buisnesscards': (context) => RoutePage(child: BuisnessCards()),
    '/hbaibe': (context) => RoutePage(child: Hbaibe()),
    '/chargeme': (context) => RoutePage(child: ChargeMe()),
    '/cardslist': (context) => RoutePage(child: CardsList()),
    '/settings': (context) => RoutePage(child: Settings()),
    '/support': (context) => RoutePage(child: Support()),
    '/newticket': (context) => RoutePage(child: NewTicket()),
    '/ticketdetails': (context) => RoutePage(child: TicketDetails()),
    '/faq': (context) => RoutePage(child: Faq()),
    '/aboutus': (context) => RoutePage(child: AboutUs()),
    '/payments': (context) => RoutePage(child: Payments()),
    '/paymentinfo': (context) => RoutePage(child: PaymentInfo()),
    '/selectedpayment': (context) => RoutePage(child: SelectedPayment()),
    '/purchassverification': (context) => RoutePage(child: Verification()),
    '/finalinvoice': (context) => RoutePage(child: FinalInvoice()),
    '/cart': (context) => RoutePage(child: Cart()),
    '/firstintro': (context) => FirstIntro(),
    '/secondintro': (context) => SecondIntro(),
    '/signup': (context) => Signup(),
    '/login': (context) => Login(),
    '/verification': (context) => S.Verification(),
    '/sendvoucher': (context) => RoutePage(child: SendVoucher()),
    '/profile': (context) => RoutePage(child: Profile()),
    '/contacts': (context) => RoutePage(child: ContactsList()),
    '/changePassword': (context) => RoutePage(child: ChangePassword())
  };
//*****************************************************************************************************
  static void openPageFromMenu(BuildContext context, String rootName) {
    Navigator.of(context).popUntil(ModalRoute.withName('/'));
    PublicProviders.reset(context);
    if (rootName != "") {
      openPage(context, rootName, 0);
    }
  }
//*****************************************************************************************************
  static void openPage(BuildContext context, String rootName, int id) {
    Navigator.of(context).pushNamed(
      rootName,
      arguments: {'id': id},
    );
  }
//*****************************************************************************************************
  static void closePage(BuildContext context, String rootName) {
    Navigator.of(context).pop(
      rootName,
    );
  }
//*****************************************************************************************************
  static void login(BuildContext context) {
    Navigator.of(context).popUntil(ModalRoute.withName('/'));
    PublicProviders.login(context);
  }
//*****************************************************************************************************
  static void openPageWithArguments(
      BuildContext context, String rootName, Object arguments) {
    Navigator.of(context).pushNamed(
      rootName,
      arguments: arguments,
    );
  }
}
