import 'package:flutter/material.dart';

class Paths {
  Paths._();
  static const String categories = "assets/images/illistrations/categories/";
  static const String offers = "assets/images/illistrations/offers/";
  static const String companies = "assets/images/illistrations/companies/";
  static const String vouchers = "assets/images/illistrations/vouchers/";
  static const String favorites = "assets/images/illistrations/favorites/";
  static const String gifts = "assets/images/illistrations/gifts/";
  static const String invoices = "assets/images/illistrations/invoices/";
  static const String buisness = "assets/images/illistrations/buisness/";
  static const String hbaibe = "assets/images/illistrations/hbaibe/";
  static const String chargeMe = "assets/images/illistrations/chargeMe/";
  static const String settings = "assets/images/illistrations/settings/";
  static const String payments = "assets/images/illistrations/payments/";
  static const String faq = "assets/images/illistrations/faq/";
  static const String cart = "assets/images/illistrations/cart/";
  static const String intro = "assets/images/illistrations/intro/";
  static const String encrypted = "assets/images/encrypted.png";
  static const String logo_primary = "assets/images/Primary_Color.png";
  static const String logo_secondary = "assets/images/logo_secondary.png";
  static const String avatar = "assets/images/avatar.png";
  static const String crootie_black = "assets/images/Crootie_Black.png";
  static const String crootie_white = "assets/images/Crootie_White.png";
  static const String crootie_illistration = "assets/images/crootie_illistration.png";
}

class Constant {
    Constant._();
    // static const Widget footerBox = SizedBox(width:60);
    // static const ScaffoldFeatureController<SnackBar, SnackBarClosedReason> dd = 
}
