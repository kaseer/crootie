import 'package:shared_preferences/shared_preferences.dart';

const String _storageKey = "Crootie";

class LocalStorge {
  Future<SharedPreferences> prefs = SharedPreferences.getInstance();
//*****************************************************************************************************
  static Future<String> getApplicationSavedInformation(String name) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString(_storageKey + name) ?? '';
  }
//*****************************************************************************************************
  static Future<List<String>> getApplicationListSavedInformation(
      String name) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getStringList(_storageKey + name) ?? [];
  }

//*****************************************************************************************************
  static Future<bool> getApplicationSavedBoolInformation(String name) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getBool(_storageKey + name) ?? false;
  }

  /// ----------------------------------------------------------
  /// Generic routine to saves an application preference
  /// ----------------------------------------------------------
//*****************************************************************************************************
  static setApplicationSavedInformation(String name, String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(_storageKey + name, value);
  }
//*****************************************************************************************************
  static setApplicationListSavedInformation(
      String name, List<String> value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setStringList(_storageKey + name, value);
  }
//*****************************************************************************************************
  static setApplicationSavedBoolInformation(String name, bool value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setBool(_storageKey + name, value);
  }
//*****************************************************************************************************
  static reset() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.clear();
  }
}
