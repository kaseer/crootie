import 'package:Crootie/public_widgets/ConfirmationPin.dart';
import 'package:Crootie/shared/LocalStorge.dart';
import 'package:Crootie/style/Style.dart';
import 'package:flutter/material.dart';
import 'package:Crootie/public_widgets/AddContact.dart';
import 'package:permission_handler/permission_handler.dart';

import 'Routes.dart';

class GlobalFunctions {
  GlobalFunctions._();
//*****************************************************************************************************
  static addContactDialog(BuildContext context, String type) {
    return showDialog(
      context: context,
      barrierDismissible: true, // user must tap button!
      builder: (BuildContext c) {
        return AddContact(type: type);
      },
    );
  }

//*****************************************************************************************************
  // static datepickerDialog(BuildContext context) {
  //   // pr.hide();
  //   return showDialog(
  //     context: context,
  //     barrierDismissible: true, // user must tap button!
  //     builder: (BuildContext c) {
  //       return DatePicker(
  //         title: LocaleKeys.Invoices_date.tr(),
  //       );
  //     },
  //   );
  // }

//*****************************************************************************************************
  static confirmationDialog(
      BuildContext context, String title, String serviceType, int id) {
    // pr.hide();
    return showDialog(
      context: context,
      barrierDismissible: true, // user must tap button!
      builder: (BuildContext c) {
        return ConfirmationPin(title: title, serviceType: serviceType, id: id);
      },
    );
  }

//*****************************************************************************************************
  static Future<void> requestPermission(
      Permission permission,
      BuildContext context,
      PermissionStatus permissionStatus,
      int addType) async {
    await permission.request().then((value) {
      if (value.isGranted) {
        Routes.openPage(context, '/contacts', addType);
      }
    });
  }

//*****************************************************************************************************
  static Future<bool> checkShowIntro() async {
    bool check = false;
    // return LocalStorge.getApplicationSavedBoolInformation("disableIntro").then((value) => value) as bool;
    await LocalStorge.getApplicationSavedBoolInformation("disableIntro")
        .then((value) {
      check = value;
    });
    return check;
  }

//*****************************************************************************************************
  static void disableIntro() {
    // return LocalStorge.getApplicationSavedBoolInformation("disableIntro").then((value) => value) as bool;
    LocalStorge.setApplicationSavedBoolInformation("disableIntro", true);
  }

//*****************************************************************************************************
  static ScaffoldFeatureController<SnackBar, SnackBarClosedReason> snackBar(
      BuildContext context, String message, Color? background) {
    return ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(message),
      backgroundColor: background,
    ));
  }

//*****************************************************************************************************
  static ScaffoldFeatureController<SnackBar, SnackBarClosedReason>
      succesSnackBar(BuildContext context, String message) {
    return ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(message),
      backgroundColor: AppTheme.success,
    ));
  }

//*****************************************************************************************************
  static ScaffoldFeatureController<SnackBar, SnackBarClosedReason>
      errorSnackBar(BuildContext context, String message) {
    return ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(message),
      backgroundColor: AppTheme.error,
    ));
  }

//*****************************************************************************************************
  static ScaffoldFeatureController<SnackBar, SnackBarClosedReason>
      serverErrorSnackBar(BuildContext context) {
    return ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text("تعذر الاتصال بالخادم, يرجي المحاولة لاحقا!"),
      backgroundColor: AppTheme.error,
    ));
  }

//*****************************************************************************************************
  static bool validate(BuildContext context, GlobalKey<FormState> formKey) {
    if (!formKey.currentState!.validate()) {
      return false;
    }
    return true;
  }
//*****************************************************************************************************

  static Map<String, dynamic> props(BuildContext context) {
    return ModalRoute.of(context)!.settings.arguments as Map<String, dynamic>;
  }
}
