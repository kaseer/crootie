import 'package:Crootie/shared/GlobalFunctions.dart';
import 'package:Crootie/shared/LocalStorge.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:Crootie/locale/locale_keys.g.dart';

import 'package:Crootie/style/SizeConfig.dart';
import 'package:Crootie/public_widgets/TextButtonWidget.dart';
import 'package:Crootie/public_widgets/topBar/TopBar.dart';
import 'package:Crootie/shared/Constant.dart';
import 'package:Crootie/shared/Routes.dart';
import 'package:Crootie/style/Style.dart';

class Intro extends StatelessWidget {
  final String? logo;
  final bool? isFirst;
  final String? description;
  final VoidCallback? next;
  Intro({
    @required this.logo,
    @required this.isFirst,
    @required this.description,
    @required this.next,
  });
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: Column(
        children: [
          Stack(
            children: [
              Container(
                height: SizeConfig.screenHeight! * 0.63,
                child: Stack(
                  alignment:
                      !isFirst! ? Alignment.bottomCenter : Alignment.topCenter,
                  children: [
                    TopBar(
                      title: "",
                      ratio: 0.6,
                      showBar: false,
                      isTap: false,
                      backgroud: AppTheme.primary,
                      rightHeightEdge: 0.85,
                      leftHeightEdge: 0.85,
                      logo: !isFirst! ? logo : null,
                      widthEdge: 6,
                      rigthWidthEdge: 6,
                      imageWidth: 250,
                      imageHeight: 250,
                    ),
                    !isFirst!
                        ? Row(
                            children: [
                              SizedBox(
                                width: SizeConfig.screenWidth! * 0.52,
                              ),
                              Container(
                                child: Image(
                                  height: 50,
                                  // width:50,
                                  image: AssetImage(Paths.intro + "lock.png"),
                                ),
                              ),
                            ],
                          )
                        : Container(),
                  ],
                ),
              ),
              Wrap(
                children: [
                  logoWidget(),
                  Center(child: avatar()),
                ],
              )
            ],
          ),
          Expanded(
            // flex:4,
            child: Column(
              children: [
                Container(
                    margin: EdgeInsets.fromLTRB(39, 0, 39, 0),
                    height: SizeConfig.screenHeight! * 0.048,
                    width: SizeConfig.screenWidth,
                    child: Text(
                      "Crootie",
                      style: TextStyle(
                          color: AppTheme.grey, fontWeight: FontWeight.bold),
                    )),
                descriptionWidget(),
                Expanded(child: buttons(context))
              ],
            ),
          )
          // Flexible(
          //   flex: 2,
          //   child: Container(
          //               margin: EdgeInsets.fromLTRB(30, 0, 30, 0),
          //               height: SizeConfig.screenHeight! * 0.048,
          //               width: SizeConfig.screenWidth,
          //               child: Text(
          //                 "Crootie",
          //                 style: TextStyle(
          //                     color: AppTheme.grey, fontWeight: FontWeight.bold),
          //               )),
          // ),
          // Flexible(
          //   flex: 1,
          //   child: descriptionWidget(),
          // ),
          // Flexible(
          //   flex: 1,
          //   child: buttons(context)
          // )
        ],
      )),
    );
  }

  //--------------------- Logo widget
  Widget logoWidget() {
    return Container(
      margin: EdgeInsets.fromLTRB(10, 50, 10, 10),
      color: AppTheme.primary,
      width: SizeConfig.screenWidth,
      height: SizeConfig.screenHeight! * 0.099,
      child: Image(
        width: 100,
        alignment: Alignment.bottomCenter,
        image: AssetImage(Paths.intro + "crootie.png"),
      ),
    );
  }

  //--------------------- Logo widget
  Widget avatar() {
    return Container(
      // color: Colors.red,
      height: SizeConfig.screenHeight! * 0.503,

      child: isFirst!
          ? Image(
              image: AssetImage(logo!),
              fit: BoxFit.cover,
            )
          : Container(),
    );
  }

  //--------------------- Logo widget
  Widget descriptionWidget() {
    return Container(
      margin: EdgeInsets.fromLTRB(80, 5, 80, 10),
      // height: SizeConfig.screenHeight * 0.099,
      child: Text(
        description!,
        style: TextStyle(color: AppTheme.grey, fontSize: SizeConfig.screenWidth! * 0.035),
      ),
    );
  }

  //--------------------- Logo widget
  Widget buttons(context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          isFirst!
              ? TextButtonWidget(
                  textColor: AppTheme.grey,
                  title: LocaleKeys.skip.tr(),
                  onPressed: () {
                    GlobalFunctions.disableIntro();
                    Routes.openPageFromMenu(context, "");
                  },
                )
              : IconButton(
                  icon: Icon(
                    Icons.arrow_back_rounded,
                    color: AppTheme.grey,
                  ),
                  onPressed: () {
                    Routes.closePage(context, "/secondintro");
                  }),
          TextButtonWidget(
            textColor: AppTheme.grey,
            title: LocaleKeys.next.tr(),
            onPressed: next,
          )
        ],
      ),
    );
  }
}
