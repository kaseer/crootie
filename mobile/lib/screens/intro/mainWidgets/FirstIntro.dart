import 'package:flutter/material.dart';
import 'package:Crootie/screens/intro/Intro.dart';
import 'package:Crootie/shared/Constant.dart';
import 'package:Crootie/shared/Routes.dart';

class FirstIntro extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Intro(
            logo: Paths.intro + "logo.png",
            isFirst: true,
            description: instruction,
            next: () {
              Routes.openPage(context, "/secondintro", 0);
            }),
      ),
    );
  }
}

String instruction =
    "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna >";
