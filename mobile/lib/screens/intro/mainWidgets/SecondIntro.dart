import 'package:Crootie/shared/GlobalFunctions.dart';
import 'package:flutter/material.dart';
import 'package:Crootie/screens/intro/Intro.dart';
import 'package:Crootie/shared/Constant.dart';
import 'package:Crootie/shared/Routes.dart';

class SecondIntro extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Intro(
            logo: Paths.intro + "payment.png",
            isFirst: false,
            description: instruction,
            next: () {
              GlobalFunctions.disableIntro();
              Routes.openPage(context, "/login",0);
              // GlobalFunctions.checkShowIntro().then((value) => print(value));
            }),
      ),
    );
  }
}

String instruction =
    "Lorem ipsum dolor sit amet, etuerconsectetuerconsectetuerconsectetuerconsectetuerconsectetuerconsectetuercon";
