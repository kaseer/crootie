import 'package:Crootie/models/responseModels/AllProductsResponse.dart';
import 'package:Crootie/public_widgets/FutureWidget.dart';
import 'package:Crootie/screens/gifts/widgets/List.dart';
import 'package:Crootie/services/api/apiServices/GiftsService.dart';
import 'package:Crootie/style/SizeConfig.dart';
import 'package:flutter/material.dart';

import 'package:Crootie/public_widgets/SlidableWidget.dart';
import 'package:Crootie/shared/Constant.dart';
import 'package:Crootie/style/Style.dart';


class GivenGifts extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureWidget(
      future: GiftsService.getGivenGifts(context),
      child: (List<AllProductsResponse> list) => ListWidget(list: list,type: "GivenGifts",),
    ); }
}
