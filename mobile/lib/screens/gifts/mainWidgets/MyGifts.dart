import 'package:Crootie/models/responseModels/AllProductsResponse.dart';
import 'package:Crootie/public_widgets/FutureWidget.dart';
import 'package:Crootie/screens/gifts/widgets/List.dart';
import 'package:Crootie/services/api/apiServices/GiftsService.dart';
import 'package:Crootie/style/SizeConfig.dart';
import 'package:Crootie/shared/GlobalFunctions.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:Crootie/locale/locale_keys.g.dart';

import 'package:Crootie/public_widgets/SlidableWidget.dart';
import 'package:Crootie/shared/Constant.dart';
import 'package:Crootie/style/Style.dart';

class MyGifts extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureWidget(
      future: GiftsService.getMyGifts(context),
      child: (List<AllProductsResponse> list) => ListWidget(list: list,type: "MyGifts",),
    );}
}
