import 'package:Crootie/locale/locale_keys.g.dart';
import 'package:Crootie/models/responseModels/AllProductsResponse.dart';
import 'package:Crootie/public_widgets/GradiantColor.dart';
import 'package:Crootie/public_widgets/SlidableWidget.dart';
import 'package:Crootie/shared/Constant.dart';
import 'package:Crootie/shared/GlobalFunctions.dart';
import 'package:Crootie/style/SizeConfig.dart';
import 'package:Crootie/style/Style.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';

class ListWidget extends StatefulWidget {
  final List<AllProductsResponse> list;
  final String type ;
  const ListWidget({
    required this.list, 
    required this.type,
    Key? key 
  }) : super(key: key);

  @override
  _ListWidgetState createState() => _ListWidgetState();
}

class _ListWidgetState extends State<ListWidget> {
  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: ListView.builder(
        itemCount: widget.list.length,
        itemBuilder: (context, index) {
          if (index+1 == widget.list.length) {
                return SizedBox(height:60);
              }
          return Container(
            margin: SizeConfig.margin,
            decoration: BoxDecoration(
                color: AppTheme.secondary,
                borderRadius: BorderRadius.circular(15)),
            child: SlidableWidget(
              isLeftSlider: false,
              description: widget.list[index].priceDescription,
              leftDescription: "",
              rightDescription: LocaleKeys.gifts_return.tr(),
              logo: Paths.companies + widget.list[index].logo,
              slideColor: AppTheme.secondary,
              isVoucher: false,
              // isFav: true,
              // showFav: true,
              price: widget.list[index].price,
              rightFunction: ()  {
                test() async {};
                if(widget.type == "GivenGifts") 
                  GlobalFunctions.confirmationDialog(context,"Date : 10/10/2010 \nFrom : Mahmoud Salim", "GivenGifts",widget.list[index].productId);
                else 
                  GlobalFunctions.confirmationDialog(context,"Date : 10/10/2010 \nFrom :  Salim", "MyGifts",widget.list[index].productId);
              },
              leftFunction: () => () {},
              topColor: color(widget.list[index].topColor),
              bottomColor: color(widget.list[index].bottomColor),
            ),
          );
        },
      ),
    );
  }
}