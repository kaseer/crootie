import 'package:Crootie/style/SizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:Crootie/locale/locale_keys.g.dart';

import 'package:Crootie/providers/functions/ChangePage.dart';
import 'package:Crootie/public_widgets/topBar/TopBar.dart';
import 'package:Crootie/shared/Constant.dart';
import 'package:provider/provider.dart';

import 'mainWidgets/GivenGifts.dart';
import 'mainWidgets/MyGifts.dart';

class Gifts extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    int page = Provider.of<ChangePage>(context).pageNo;

    return Column(
      children: [
        TopBar(
          title: LocaleKeys.menu_gifts.tr(),
          logo: Paths.gifts + "logo.png",
          isCenter: false,
          pageNo: page,
          ratio: 0.2,
          titleWidth: SizeConfig.screenWidth,
          isTopCenter: false,
          imageHeight: 120,
          firstTap: LocaleKeys.gifts_tab1.tr(),
          secondTap: LocaleKeys.gifts_tab2.tr(),
          // titleWidth: SizeConfig.screenWidth! * 0.2,
        ),
        page == 1 ? GivenGifts() : MyGifts()
      ],
    );
  }
}
