import 'package:flutter/material.dart';
import 'package:Crootie/screens/home/widgets/HomTopBar.dart';
import 'mainWidgets/Categories.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return HomeTopBar(
      widget: Categories(),
    );
  }
}
