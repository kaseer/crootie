import 'package:Crootie/models/responseModels/CompaniesResponse.dart';
import 'package:Crootie/screens/home/widgets/ListCompanies.dart';
import 'package:Crootie/shared/GlobalFunctions.dart';
import 'package:Crootie/services/api/apiServices/LookupService.dart';
import 'package:flutter/material.dart';
import 'package:Crootie/public_widgets/FutureWidget.dart';
import 'package:Crootie/screens/home/widgets/HomTopBar.dart';

class Companies extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final props = GlobalFunctions.props(context);
    return HomeTopBar(
      widget: FutureWidget(
          future: LookupService.getCompanies(context, props['id']),
          child: (List<CompaniesResponse> list) => ListWidget(list:list)),
    );
  }
}

// class CompaniesList {
//   int companyId;
//   String logo;
//   int topColor;
//   int bottomColor;

//   CompaniesList(
//       {required this.companyId,
//       required this.logo,
//       required this.topColor,
//       required this.bottomColor});
// }

// List<CompaniesList> list = [
//   CompaniesList(
//     companyId: 1,
//     logo: "Almadar.png",
//     topColor: 0xff0F7C10,
//     bottomColor: 0xff21AC17,
//   ),
//   CompaniesList(
//     companyId: 2,
//     logo: "Amazon.png",
//     topColor: 0xff4D4D4D,
//     bottomColor: 0xff4D4D4D,
//   ),
//   CompaniesList(
//     companyId: 3,
//     logo: "Apple.png",
//     topColor: 0xff787878,
//     bottomColor: 0xffD6D6D6,
//   ),
//   CompaniesList(
//     companyId: 4,
//     logo: "Netflix.png",
//     topColor: 0xff0C0C0C,
//     bottomColor: 0xff531919,
//   ),
//   CompaniesList(
//     companyId: 5,
//     logo: "Almadar.png",
//     topColor: 0xff0F7C10,
//     bottomColor: 0xff21AC17,
//   ),
//   CompaniesList(
//     companyId: 6,
//     logo: "Amazon.png",
//     topColor: 0xff4D4D4D,
//     bottomColor: 0xff4D4D4D,
//   ),
//   CompaniesList(
//     companyId: 7,
//     logo: "Apple.png",
//     topColor: 0xff787878,
//     bottomColor: 0xffD6D6D6,
//   ),
//   CompaniesList(
//     companyId: 8,
//     logo: "Netflix.png",
//     topColor: 0xff0C0C0C,
//     bottomColor: 0xff531919,
//   )
// ];
