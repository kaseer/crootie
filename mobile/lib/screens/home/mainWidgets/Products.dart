import 'package:Crootie/locale/locale_keys.g.dart';
import 'package:Crootie/models/responseModels/ProductsResponse.dart';
import 'package:Crootie/public_widgets/GradiantColor.dart';
import '../widgets/ListProducts.dart';
import 'package:Crootie/shared/GlobalFunctions.dart';
import 'package:Crootie/style/SizeConfig.dart';
import 'package:Crootie/providers/PublicProviders.dart';
import 'package:Crootie/public_widgets/FutureWidget.dart';
import 'package:Crootie/public_widgets/SlidableWidget.dart';
import 'package:Crootie/public_widgets/topBar/TopBar.dart';
import 'package:Crootie/services/api/apiServices/LookupService.dart';
import 'package:Crootie/services/db/Cart.dart';
import 'package:Crootie/shared/Constant.dart';
import 'package:Crootie/shared/Routes.dart';
import 'package:Crootie/style/Style.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';

class Products extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final props = GlobalFunctions.props(context);
    return Stack(
      children: [
        TopBar(
            logo: Paths.companies + props['logo'],
            pageNo: 0,
            isTap: false,
            isTopCenter: true,
            ratio: 0.45,
            backgroud: AppTheme.primary,
            imageWidth: SizeConfig.screenWidth!,
            imageHeight: 70,
            rightHeightEdge: 0.6,
            leftHeightEdge: 0.6,
            flex: 1),
        FutureWidget(
            future: LookupService.getProducts(context, props['companyId']),
            child: (List<ProductsResponse> list) => ListWidget(
                  list: list,
                  topColor: props['topColor'],
                  bottomColor: props['bottomColor'],
                  logo: props['logo'],
                ))
      ],
    );
  }
}
