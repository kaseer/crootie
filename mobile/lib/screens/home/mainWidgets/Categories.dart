import 'package:Crootie/models/responseModels/CategoriesResponse.dart';
import 'package:Crootie/screens/home/widgets/ListCategories.dart';
import 'package:Crootie/services/api/apiServices/LookupService.dart';
import 'package:flutter/material.dart';
import 'package:Crootie/public_widgets/FutureWidget.dart';

class Categories extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureWidget(
        future: LookupService.getCategories(context),
        child: (List<CategoriesResponse> list) => ListWidget(list: list));
  }
}
