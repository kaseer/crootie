import 'package:Crootie/style/Style.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:Crootie/style/SizeConfig.dart';
import 'package:Crootie/public_widgets/ListContainer.dart';
import 'package:Crootie/shared/Constant.dart';

class OfferData {
  int offerId;
  String image;
  Color color;
  String description;
  OfferData({required this.offerId, required this.image, required this.color, required this.description});
}

List<OfferData> offers = [
  OfferData(
      offerId: 1,
      image: "discount.png",
      color: Color(0xffF8AF61),
      description: "Offers"),
  OfferData(
      offerId: 2,
      image: "xbox.png",
      color: Color(0xff0F7C10),
      description: "Offers"),
  OfferData(
      offerId: 3,
      image: "cart.png",
      color: Color(0xffF8AF61),
      description: "Offers")
];

class Offers extends StatefulWidget {
  @override
  _OffersState createState() => _OffersState();
}

class _OffersState extends State<Offers> {
  int _current = 0;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height: 110,
          // width: SizeConfig.screenWidth,
          child: CarouselSlider(
            options: CarouselOptions(
                aspectRatio: 2.0,
                height: 200,
                viewportFraction: 1.0,
                enlargeCenterPage: false,
                autoPlay: true,
                onPageChanged: (index, reason) {
                  setState(() {
                    _current = index;
                  });
                }),
            items: offers.map((offer) {
              return Builder(
                builder: (BuildContext context) {
                  return GestureDetector(
                    onTap: () {},
                    child: ListContainer(
                      height: 200,
                      color: offer.color,
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Text(offer.description),
                            Image(
                              image: AssetImage(Paths.offers + offer.image),
                              fit: BoxFit.cover,
                            )
                          ]),
                    ),
                  );
                },
              );
            }).toList(),
          ),
        ),
        Container(
          margin: EdgeInsets.only(bottom: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: offers.map((url) {
              int index = offers.indexOf(url);
              return Container(
                width: 8.0,
                height: 8.0,
                margin: EdgeInsets.symmetric( horizontal: 2.0),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: _current == index
                      ? AppTheme.secondary
                      : Colors.white,
                ),
              );
            }).toList(),
          ),
        ),
      ],
    );
  }
}
