import 'package:Crootie/locale/locale_keys.g.dart';
import 'package:Crootie/models/responseModels/ProductsResponse.dart';
import 'package:Crootie/providers/PublicProviders.dart';
import 'package:Crootie/public_widgets/GradiantColor.dart';
import 'package:Crootie/public_widgets/SlidableWidget.dart';
import 'package:Crootie/services/api/apiServices/FavoritesService.dart';
import 'package:Crootie/services/db/Cart.dart';
import 'package:Crootie/shared/Constant.dart';
import 'package:Crootie/shared/Routes.dart';
import 'package:Crootie/style/SizeConfig.dart';
import 'package:Crootie/style/Style.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';

class ListWidget extends StatefulWidget {
  final List<ProductsResponse> list;
  final String logo;
  final String topColor;
  final String bottomColor;
  const ListWidget(
      {required this.list,
      required this.logo,
      required this.topColor,
      required this.bottomColor,
      Key? key})
      : super(key: key);

  @override
  _ListWidgetState createState() => _ListWidgetState();
}

class _ListWidgetState extends State<ListWidget> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(height: SizeConfig.screenWidth! * 0.45),
        Flexible(
          child: ListView.builder(
            itemCount: widget.list.length + 1,
            itemBuilder: (context, index) {
              if (index == widget.list.length) {
                return SizedBox(height: SizeConfig.screenWidth! * 0.2);
              }
              return Container(
                decoration: BoxDecoration(
                    color: AppTheme.secondary,
                    borderRadius: BorderRadius.circular(15)),
                margin: EdgeInsets.fromLTRB(39, 20, 39, 0),
                child: SlidableWidget(
                  index: index,
                  description: widget.list[index].priceDescription,
                  leftDescription: LocaleKeys.addToCart.tr(),
                  rightDescription: LocaleKeys.buy.tr(),
                  logo: Paths.companies + widget.logo,
                  slideColor: AppTheme.secondary,
                  isVoucher: false,
                  isFav: widget.list[index].isFav,
                  showFav: true,
                  price: widget.list[index].price.toDouble(),
                  rightFunction: () => Routes.openPage(
                      context, "/payments", widget.list[index].productId),
                  leftFunction: () {
                    CartDatabase.instance
                        .addToCart(widget.list[index].productId);
                    PublicProviders.addToCart(context);
                  },
                  topColor: color(widget.topColor),
                  bottomColor: color(widget.bottomColor),
                  favRemove: () async{
                    var responseCode;
                    if (!widget.list[index].isFav) {
                      responseCode = await FavoritesService.add(context, widget.list[index].productId);
                    }
                    else {
                      responseCode = await FavoritesService.remove(context, widget.list[index].productId);
                    }
                    if (responseCode == 200){
                      setState(() {
                        widget.list[index].isFav = !widget.list[index].isFav;
                      });
                    }
                  },
                ),
              );
            },
          ),
        ),
      ],
    );
  }
}
