import 'package:Crootie/models/responseModels/CategoriesResponse.dart';
import 'package:Crootie/public_widgets/GradiantColor.dart';
import 'package:Crootie/shared/Constant.dart';
import 'package:Crootie/shared/Routes.dart';
import 'package:Crootie/style/SizeConfig.dart';
import 'package:Crootie/style/Style.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart' as s;

class ListWidget extends StatelessWidget {
  final List<CategoriesResponse> list;
  const ListWidget({required this.list, Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String currnetLanguage = context.locale.toString();
    return Flexible(
      child: ListView.builder(
        itemCount: list.length + 1,
        itemBuilder: (context, index) {
          if (index == list.length) {
            return SizedBox(height: 60);
          }
          return InkWell(
            onTap: (){
              Routes.openPage(context, "/companies", list[index].categoryId);
            },
            child: Container(
              child: Stack(
                alignment: Alignment.bottomCenter,
                children: [
                  Container(
                    height: 100,
                    width: SizeConfig.screenWidth,
                    margin: SizeConfig.margin,
                    decoration: BoxDecoration(
                        gradient: LinearGradient(
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            colors: [
                              color(list[index].topColor),
                              color(list[index].bottomColor)
                            ]),
                        boxShadow: [
                          BoxShadow(
                              color: AppTheme.grey.withOpacity(0.5),
                              spreadRadius: 0.2,
                              blurRadius: 10)
                        ],
                        borderRadius: BorderRadius.circular(10)),
                    child: Row(
                      // mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        SizedBox(
                          width: SizeConfig.screenWidth! * 0.4,
                        ),
                        Container(
                            child: Text(
                          currnetLanguage == 'ar'
                              ? list[index].categoryNameAr
                              : list[index].categoryNameEn,
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: SizeConfig.screenWidth! * 0.044),
                        ))
                      ],
                    ),
                  ),
                  Positioned.directional(
                    textDirection: currnetLanguage == 'ar'
                        ? TextDirection.rtl
                        : TextDirection.ltr,
                    height: 140,
                    start: 40,
                    child: Container(
                      margin: EdgeInsets.fromLTRB(15, 10, 15, 20),
                      child: Image(
                        fit: BoxFit.fill,
                        image: AssetImage(
                            Paths.categories + list[index].illistration),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );

  }
}