import 'package:Crootie/models/responseModels/CompaniesResponse.dart';
import 'package:Crootie/public_widgets/GradiantColor.dart';
import 'package:Crootie/shared/Constant.dart';
import 'package:Crootie/shared/Routes.dart';
import 'package:Crootie/style/SizeConfig.dart';
import 'package:flutter/material.dart';

class ListWidget extends StatelessWidget {
  final List<CompaniesResponse> list;
  const ListWidget({required this.list, Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: Container(
        margin: SizeConfig.marginLR,
        child: GridView.builder(
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2, childAspectRatio: 1.6, mainAxisSpacing: 20),
          itemCount: list.length + 1,
          itemBuilder: (contex, index) {
            if (index == list.length) {
              return SizedBox(height: 60);
            }
            return GestureDetector(
              onTap: () {
                Map<String, dynamic> argument = {
                  "companyId": list[index].companyId,
                  "logo": list[index].logo,
                  "topColor": list[index].topColor,
                  "bottomColor": list[index].bottomColor,
                };
                Routes.openPageWithArguments(context, "/products", argument);
              },
              child: Container(
                margin: EdgeInsets.fromLTRB(8, 12, 8, 0),
                decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                          color: color(list[index].topColor)
                              .withOpacity(0.5),
                          spreadRadius: 0.3,
                          blurRadius: 5),
                    ],
                    gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                          color(list[index].topColor),
                          color(list[index].bottomColor)
                        ]),
                    borderRadius: BorderRadius.circular(10)),
                child: Container(
                  padding: EdgeInsets.all(30),
                  child: Image(
                    image: AssetImage(Paths.companies + list[index].logo),
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  
  }
}