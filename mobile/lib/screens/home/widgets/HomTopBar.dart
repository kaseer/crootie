import 'package:Crootie/shared/Constant.dart';
import 'package:Crootie/style/AppIcons.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:Crootie/locale/locale_keys.g.dart';
import 'package:Crootie/style/SizeConfig.dart';
import 'package:Crootie/public_widgets/topBar/AppBar.dart';
import 'package:Crootie/screens/home/widgets/Offers.dart';
import 'package:Crootie/style/Style.dart';
import 'package:Crootie/public_widgets/topBar/Background.dart';
import 'package:Crootie/public_widgets/form/inputForm.dart';

TextEditingController controller = new TextEditingController();

class HomeTopBar extends StatelessWidget {
  final Widget? widget;
  HomeTopBar({this.widget});
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          height: SizeConfig.screenHeight! * 0.5,
          width: SizeConfig.screenWidth,
          child: Background(
            color: AppTheme.primary,
            rightHeightEdge: 0.8,
            leftHeightEdge: 0.8,
            widthEdge: 4,
            rigthWidthEdge: 4,
          ),
        ),
        Column(
          // runSpacing: 30,
          // spacing: 30,
          children: [
            AppBarWidget(
              backgroud: AppTheme.primary,
            ),
            Container(
                margin: SizeConfig.marginLR,
                width: SizeConfig.screenWidth,
                height: 30,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: new ExactAssetImage(Paths.crootie_illistration),
                        fit: BoxFit.fitWidth))),
            Container(
              height: 35,
              margin: SizeConfig.margin,
              child: InputForm(
                controller: controller,
                isPassword: false,
                label: LocaleKeys.search.tr(),
                isNumber: false,
                valid: null,
                borderColor: Colors.white,
                icon: Icon(
                  AppIcons.search,
                  size: 15,
                ),
                iconColor: Colors.white,
              ),
            ),
            Offers(),
            widget!
          ],
        ),
      ],
    );
  }
}
