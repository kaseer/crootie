import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:Crootie/locale/locale_keys.g.dart';

import 'package:Crootie/style/SizeConfig.dart';
import 'package:Crootie/providers/functions/ChangePage.dart';
import 'package:Crootie/public_widgets/topBar/TopBar.dart';
import 'package:Crootie/screens/chargeMe/mainWidgets/SelectableWidget.dart';
import 'package:Crootie/shared/Constant.dart';
import 'package:Crootie/style/Style.dart';
import 'package:provider/provider.dart';

class ChargeMe extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    int page = Provider.of<ChangePage>(context).pageNo;

    return Column(
      children: [
        TopBar(
          title: LocaleKeys.menu_chargeme.tr(),
          logo: Paths.chargeMe + "logo.png",
          isTap: false,
          isCenter: false,
          pageNo: page,
          backgroud: AppTheme.primary,
          rigthWidthEdge: 18,
          rightHeightEdge: 0.9,
          // isLine: true,
          imageWidth: 200,
          imageHeight: 200,
          titleWidth: SizeConfig.screenWidth,
          ratio: 0.3,
        ),
        SelectableWidget(),
        // SizedBox(height: 80)
      ],
    );
  }
}
