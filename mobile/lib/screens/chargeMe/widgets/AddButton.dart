import 'package:Crootie/public_widgets/CircleButton.dart';
import 'package:Crootie/screens/chargeMe/widgets/HbaibeList.dart';
import 'package:Crootie/style/SizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:Crootie/locale/locale_keys.g.dart';

import 'package:Crootie/shared/GlobalFunctions.dart';
import 'package:Crootie/style/Style.dart';
import 'package:permission_handler/permission_handler.dart';

bool _show = false;
PermissionStatus _permissionStatus = PermissionStatus.denied;

class AddButton extends StatefulWidget {
  @override
  _AddButtonState createState() => _AddButtonState();
}

class _AddButtonState extends State<AddButton> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 200,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            margin: EdgeInsets.all(5),
            // color: Colors.yellow,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                button(LocaleKeys.contacts.tr(), () {
                  GlobalFunctions.requestPermission(
                      Permission.contacts, context, _permissionStatus, 0);
                }),
                Row(
                  children: [
                    button(LocaleKeys.menu_hbaibe.tr(), () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => HbaibeList()),
                      );
                    }),
                    SizedBox(width: 50),
                  ],
                ),
                button(LocaleKeys.manualy.tr(), () {
                  GlobalFunctions.addContactDialog(context, "chargemeContact");
                }),
              ],
            ),
          ),
          CircleButton(
            icon: Icons.add,
            onPressed: () {
              setState(() {
                _show = !_show;
              });
            },
          ),
          Container(
              margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
              child: Text(
                LocaleKeys.chargeme_selectcontact.tr(),
                style: TextStyle(
                    color: AppTheme.grey,
                    fontSize: SizeConfig.screenWidth! * 0.028),
              ))
        ],
      ),
    );
  }

  Widget button(String title, VoidCallback onPressed) {
    return GestureDetector(
      onTap: onPressed,
      child: AnimatedOpacity(
        opacity: _show ? 1.0 : 0.0,
        duration: Duration(milliseconds: 500),
        child: Container(
          alignment: Alignment.center,
          width: 100,
          height: 30,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(25),
              color: AppTheme.secondary),
          child: Text(
            title,
            style: TextStyle(fontSize: SizeConfig.screenWidth! * 0.028),
          ),
        ),
      ),
    );
  }
}
