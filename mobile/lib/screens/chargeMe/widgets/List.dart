import 'package:Crootie/models/responseModels/AllProductsResponse.dart';
import 'package:Crootie/providers/PublicProviders.dart';
import 'package:Crootie/public_widgets/CardView.dart';
import 'package:Crootie/public_widgets/GradiantColor.dart';
import 'package:Crootie/shared/Constant.dart';
import 'package:Crootie/style/SizeConfig.dart';
import 'package:flutter/material.dart';

class ListWidget extends StatelessWidget {
  final List<AllProductsResponse> list;
  const ListWidget({required this.list, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: ListView.builder(
        itemCount: list.length,
        itemBuilder: (context, index) {
          if (index + 1 == list.length) {
            return SizedBox(height: 60);
          }
          return GestureDetector(
            onTap: () {
              PublicProviders.selectCard(context, list[index]);
            },
            child: Container(
              margin: SizeConfig.margin,
              child: CardView(
                description: list[index].priceDescription,
                logo: Paths.companies + list[index].logo,
                isVoucher: false,
                showFav: false,
                price: list[index].price,
                topColor: color(list[index].topColor),
                bottomColor: color(list[index].bottomColor),
              ),
            ),
          );
        },
      ),
    );
  }
}
