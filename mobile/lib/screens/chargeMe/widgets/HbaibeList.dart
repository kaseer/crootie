import 'package:Crootie/models/responseModels/hbaibe/HbaibeResponse.dart';
import 'package:Crootie/public_widgets/FutureWidget.dart';
import 'package:Crootie/public_widgets/ListContainer.dart';
import 'package:Crootie/services/api/apiServices/HbaibeService.dart';
import 'package:Crootie/shared/Constant.dart';
import 'package:Crootie/style/SizeConfig.dart';
import 'package:flutter/material.dart';

class HbaibeList extends StatelessWidget {
  const HbaibeList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // bottomNavigationBar: FooterMenu(),
      body: SafeArea(
          child: FutureWidget(
        future: HbaibeService.get(context),
        child: (List<HbaibeResponse> list) {
          return ListView.builder(
            itemCount: list.length,
            itemBuilder: (context, index) {
              if (index == list.length) {
                return SizedBox(height: 60);
              }
              return GestureDetector(
                  child: ListContainer(
                child: hbaibeWidget(list[index]),
              ));
            },
          );
        },
      )),
    );
  }

  //* ------------------------------------------- Widgets ---------------------------------------------------
  Widget hbaibeWidget(HbaibeResponse data) {
    return Container(
      padding: EdgeInsets.all(10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.fromLTRB(15, 0, 15, 0),
            decoration: BoxDecoration(
              shape: BoxShape.circle,
            ),
            width: 40,
            height: 40,
            child: Image(
              fit: BoxFit.contain,
              image: AssetImage(Paths.avatar),
            ),
          ),
          Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                    child: Text(
                  data.memberName,
                  style: TextStyle(fontSize: SizeConfig.screenWidth! * 0.028),
                )),
                Container(
                    child: Text(
                  data.phoneNo,
                  style: TextStyle(fontSize: SizeConfig.screenWidth! * 0.028),
                )),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
