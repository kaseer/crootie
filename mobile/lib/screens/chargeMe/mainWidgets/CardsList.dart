import 'package:Crootie/models/responseModels/AllProductsResponse.dart';
import 'package:Crootie/public_widgets/FutureWidget.dart';
import 'package:Crootie/screens/chargeMe/widgets/List.dart';
import 'package:Crootie/services/api/apiServices/LookupService.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:Crootie/locale/locale_keys.g.dart';
import 'package:Crootie/style/SizeConfig.dart';
import 'package:Crootie/public_widgets/topBar/TopBar.dart';
import 'package:Crootie/shared/Constant.dart';
import 'package:Crootie/style/Style.dart';

class CardsList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TopBar(
          title: LocaleKeys.menu_chargeme.tr(),
          logo: Paths.chargeMe + "logo.png",
          isTap: false,
          isCenter: false,
          backgroud: AppTheme.primary,
          rigthWidthEdge: 18,
          rightHeightEdge: 0.9,
          // isLine: true,
          imageWidth: 200,
          imageHeight: 200,
          titleWidth: SizeConfig.screenWidth,
          ratio: 0.3,
        ),
        FutureWidget(
            future: LookupService.getAllProducts(context),
            child: (List<AllProductsResponse> list) => ListWidget(list: list))
      ],
    );
  }
}
