import 'package:Crootie/models/responseModels/AllProductsResponse.dart';
import 'package:Crootie/providers/functions/ContactProvider.dart';
import 'package:Crootie/public_widgets/CircleButton.dart';
import 'package:Crootie/public_widgets/GradiantColor.dart';
import 'package:Crootie/style/SizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';

// import 'package:flutter_reaction_button/flutter_reaction_button.dart';
import 'package:Crootie/locale/locale_keys.g.dart';
import 'package:Crootie/providers/functions/SelectCardProvider.dart';
import 'package:Crootie/public_widgets/CardView.dart';
import 'package:Crootie/public_widgets/ListContainer.dart';
import 'package:Crootie/shared/Constant.dart';
import 'package:Crootie/shared/Routes.dart';
import 'package:Crootie/style/Style.dart';
import 'package:provider/provider.dart';

import 'package:Crootie/screens/chargeMe/widgets/AddButton.dart';

class SelectableWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    bool selected = Provider.of<SelectCardProvider>(context).isSelected;
    AllProductsResponse voucher =
        Provider.of<SelectCardProvider>(context).product;
    var contact = Provider.of<ContactProvider>(context).contactProvider;
    return Flexible(
      child: ListView(
        children: [
          GestureDetector(
              onTap: () {
                Routes.openPage(context, "/cardslist", 0);
              },
              child: !selected
                  ? ListContainer(
                      showShadow: false,
                      padding: EdgeInsets.all(18),
                      color: AppTheme.grey.withOpacity(0.2),
                      child: Text(
                        LocaleKeys.chargeme_selectcard.tr(),
                        style: TextStyle(color: AppTheme.grey),
                      ))
                  : Container(
                      margin: SizeConfig.margin,
                      child: CardView(
                        description: voucher.priceDescription,
                        logo: Paths.companies + voucher.logo,
                        isVoucher: false,
                        showFav: false,
                        price: voucher.price,
                        topColor: color(voucher.topColor),
                        bottomColor: color(voucher.bottomColor),
                      ),
                    )),
          ListContainer(
              showShadow: false,
              padding: EdgeInsets.all(18),
              color: AppTheme.grey.withOpacity(0.2),
              child: Text(
                contact.memberName,
                style: TextStyle(color: AppTheme.grey),
              )),
          AddButton(),
          Container(
            margin: EdgeInsets.only(bottom: 80),
            child: CircleButton(
              icon: Icons.send_to_mobile,
              onPressed: () {},
            ),
          )
          // TextButtonWidget(
          //   margin: EdgeInsets.fromLTRB(15, 0, 15, 15),
          //   borderRadius: 20,
          //   color: AppTheme.primary,
          //   title: LocaleKeys.chargeme_charge.tr(),
          //   width: 100,
          //   height: 30,
          //   textColor: Colors.white,
          //   onPressed: () {},
          // ),
        ],
      ),
    );
  }
}
