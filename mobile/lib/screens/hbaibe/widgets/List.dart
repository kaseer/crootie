import 'package:Crootie/models/responseModels/hbaibe/HbaibeResponse.dart';
import 'package:Crootie/public_widgets/ListContainer.dart';
import 'package:Crootie/screens/hbaibe/widgets/AddButton.dart';
import 'package:Crootie/shared/Constant.dart';
import 'package:Crootie/style/SizeConfig.dart';
import 'package:flutter/material.dart';

class ListWidget extends StatelessWidget {
  final List<HbaibeResponse> list;
  const ListWidget({required this.list, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: ListView.builder(
        itemCount: list.length + 1,
        itemBuilder: (context, index) {
          if (index == 0) {
            return Column(
              children: [
                AddButton(),
                GestureDetector(
                    child: ListContainer(
                  // height: 100,
                  child: hbaibeWidget(index),
                ))
              ],
            );
          }
          if (index == list.length) {
            return SizedBox(height: 60);
          }
          return GestureDetector(
            child: ListContainer(
              child: hbaibeWidget(index),
            ));
        },
      ),
    );
  }

  //* ------------------------------------------- Widgets ---------------------------------------------------
  Widget hbaibeWidget(int index) {
    return Container(
      padding: EdgeInsets.all(10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.fromLTRB(15, 0, 15, 0),
            decoration: BoxDecoration(
              shape: BoxShape.circle,
            ),
            width: 40,
            height: 40,
            child: Image(
              fit: BoxFit.contain,
              image: AssetImage(Paths.avatar),
            ),
          ),
          Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                    child: Text(
                  list[index].memberName,
                  style: TextStyle(fontSize: SizeConfig.screenWidth! * 0.028),
                )),
                Container(
                    child: Text(
                  list[index].phoneNo,
                  style: TextStyle(fontSize: SizeConfig.screenWidth! * 0.028),
                )),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
