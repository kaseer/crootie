import 'package:Crootie/style/SizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:Crootie/locale/locale_keys.g.dart';
import 'package:Crootie/shared/GlobalFunctions.dart';
import 'package:Crootie/style/Style.dart';
import 'package:permission_handler/permission_handler.dart';

bool _show = false;
PermissionStatus _permissionStatus = PermissionStatus.denied;

class AddButton extends StatefulWidget {
  @override
  _AddButtonState createState() => _AddButtonState();
}

class _AddButtonState extends State<AddButton> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            // color: Colors.yellow,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                button(LocaleKeys.contacts.tr(), () {
                  GlobalFunctions.requestPermission(Permission.contacts, context, _permissionStatus, 1);
                }),
                Row(
                  children: [
                    
                    button(LocaleKeys.manualy.tr(), () {
                      GlobalFunctions.addContactDialog(context, "hbaibeContact");
                    }),
                    SizedBox(
                      width:30
                    ),
                  ],
                ),
              ],
            ),
          ),
          // : Container(),
          Container(
            margin: EdgeInsets.all(20),
            child: IconButton(
              onPressed: () {
                setState(() {
                  _show = !_show;
                });
              },
              icon: Icon(
                Icons.add,
                color: Colors.white,
              ),
            ),
            // margin: EdgeInsets.fromLTRB(15, 60, 15, 20),
            height: 50,
            width: 50,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: AppTheme.primary,
            ),
          ),
        ],
      ),
    );
  }
  //*-------------------------------------------Widgets---------------------------------------------------
  Widget button(String title, VoidCallback onPressed) {
    return GestureDetector(
      onTap: onPressed,
      child: AnimatedOpacity(
        opacity: _show ? 1.0 : 0.0,
        duration: Duration(milliseconds: 500),
        child: Container(
          alignment: Alignment.center,
          width: 100,
          height: 30,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(25),
              color: AppTheme.secondary),
          child: Text(
            title,
            style: TextStyle(fontSize: SizeConfig.screenWidth! * 0.028),
          ),
        ),
      ),
    );
  }
}
