import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:Crootie/locale/locale_keys.g.dart';

import 'package:Crootie/style/SizeConfig.dart';
import 'package:Crootie/public_widgets/topBar/TopBar.dart';
import 'package:Crootie/screens/hbaibe/mainWidgets/HbaibeList.dart';
import 'package:Crootie/shared/Constant.dart';

class Hbaibe extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // int page = Provider.of<ChangePage>(context).pageNo;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        TopBar(
          title: LocaleKeys.menu_hbaibe.tr(),
          logo: Paths.hbaibe + "logo.png",
          isTap: false,
          isCenter: false,
          ratio: 0.3,
          titleWidth: SizeConfig.screenWidth,
          isTopCenter: false,
          imageWidth: SizeConfig.screenWidth!,
          imageHeight: SizeConfig.screenHeight! * 0.2,
          leftHeightEdge: 0.65,
          rightHeightEdge: 0.65,
        ),
        HbaibeList(),
        // SizedBox(height:60)
      ],
    );
  }
}
