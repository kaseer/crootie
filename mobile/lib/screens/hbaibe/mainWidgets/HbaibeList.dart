import 'package:Crootie/models/responseModels/hbaibe/HbaibeResponse.dart';
import 'package:Crootie/public_widgets/FutureWidget.dart';
import 'package:Crootie/screens/hbaibe/widgets/List.dart';
import 'package:Crootie/services/api/apiServices/HbaibeService.dart';
import 'package:flutter/material.dart';
class HbaibeList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureWidget(
      future: HbaibeService.get(context),
      child: (List<HbaibeResponse> list) => ListWidget(list: list),
    );}

  }



