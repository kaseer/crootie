import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:Crootie/locale/locale_keys.g.dart';

import 'package:Crootie/style/SizeConfig.dart';
import 'package:Crootie/public_widgets/topBar/TopBar.dart';
import 'package:Crootie/screens/faq/mainWidgets/FaqList.dart';
import 'package:Crootie/shared/Constant.dart';

class Faq extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TopBar(
          title: LocaleKeys.menu_faq.tr(),
          logo: Paths.faq + "logo.png",
          isTap: false,
          isCenter: true,
          titleWidth: SizeConfig.screenWidth,
          imageWidth: SizeConfig.screenWidth!,
        ),
        FaqList()
      ],
    );
  }
}
