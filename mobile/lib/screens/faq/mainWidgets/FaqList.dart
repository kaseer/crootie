import 'package:Crootie/style/SizeConfig.dart';
import 'package:flutter/material.dart';

import 'package:Crootie/style/Style.dart';

class FaqList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: ListView.builder(
        itemCount: faq.length,
        itemBuilder: (context, index) {
          if (index+1 == faq.length) {
                return SizedBox(height:60);
              }
          return Container(
            margin: SizeConfig.margin,
            // height:100,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(25),
                color: AppTheme.primary),
            child: ExpansionTile(
              childrenPadding: EdgeInsets.fromLTRB(20, 0, 20, 0),
              
              // collapsedBackgroundColor: AppTheme.primary,
              // backgroundColor: AppTheme.secondary,
              title: Text(faq[index].faqQ,
                  style: TextStyle(
                    color: Colors.white,
                  )),
              children: <Widget>[
                Container(
                  height:2,
                  color: Colors.white,
                ),
                Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      // color: AppTheme.secondary
                    ),
                    padding: EdgeInsets.fromLTRB(5, 10, 5, 40),
                    // color: Colors.grey.withOpacity(0.1),
                    alignment: Alignment.topRight,
                    child: Text(faq[index].faqA,
                        style: TextStyle(
                          fontSize: SizeConfig.screenWidth! * 0.033,
                        )))
              ],
            ),
          );
        },
      ),
    );
  }
}

class FaqData {
  int faqId;
  String faqQ;
  String faqA;
  FaqData({required this.faqId, required this.faqQ, required this.faqA});
}

List<FaqData> faq = [
  FaqData(
      faqId: 1,
      faqQ: "How?",
      faqA:
          "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi."),
  FaqData(
      faqId: 2,
      faqQ: "How?",
      faqA:
          "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi."),
  FaqData(
      faqId: 3,
      faqQ: "How?",
      faqA:
          "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi."),
  FaqData(
      faqId: 4,
      faqQ: "How?",
      faqA:
          "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi."),
];
