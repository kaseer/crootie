import 'package:Crootie/providers/functions/ChangePage.dart';
import 'package:Crootie/public_widgets/FooterMenu.dart';
import 'package:Crootie/public_widgets/Indicator.dart';
import 'package:Crootie/shared/GlobalFunctions.dart';
import 'package:flutter/material.dart';
import 'package:Crootie/style/SizeConfig.dart';
import 'package:Crootie/screens/intro/mainWidgets/FirstIntro.dart';
import 'package:provider/provider.dart';
import 'home/Home.dart';

class AppPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    var prov = Provider.of<ChangePage>(context);
    // var provf = Provider.of<CartProvider>(context);
    // provf.getCount(context);
    return Scaffold(
      body: SafeArea(
        child: Stack(
          alignment: Alignment.bottomCenter,
          children: [
            prov.disableIntro
                ? Home()
                : FutureBuilder(
                    future: GlobalFunctions.checkShowIntro(),
                    builder: (context, snapshot) {
                      if (snapshot.connectionState == ConnectionState.done) {
                        prov.disableIntro = snapshot.data as bool;
                        return !prov.disableIntro ? FirstIntro() : Home();
                      }
                      return Indicator();
                    },
                  ),
            // prov.disableIntro ? Home():
            FutureBuilder(
              future: GlobalFunctions.checkShowIntro(),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  prov.disableIntro = snapshot.data as bool;
                  return !prov.disableIntro ? Container() : FooterMenu();
                }
                return Indicator();
              },
            ),
          ],
        ),
      ),
    );
  }
}
