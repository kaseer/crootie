import 'package:Crootie/models/responseModels/redeem/RedeemResponse.dart';
import 'package:Crootie/public_widgets/FutureWidget.dart';
import 'package:Crootie/screens/redeemPoints/widgets/List.dart';
import 'package:Crootie/services/api/apiServices/RedeemService.dart';
import 'package:flutter/material.dart';

class RedeemList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureWidget(
      future: RedeemService.get(context),
      child: (List<RedeemResponse> list) => ListWidget(list: list),
    );
  }
}
