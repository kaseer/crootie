import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:Crootie/locale/locale_keys.g.dart';

import 'package:Crootie/style/SizeConfig.dart';
import 'package:Crootie/public_widgets/topBar/TopBar.dart';
import 'package:Crootie/screens/redeemPoints/mainWidgets/RedeemList.dart';
import 'package:Crootie/shared/Constant.dart';
import 'package:Crootie/style/Style.dart';

class RedeemPoints extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // int page = Provider.of<ChangePage>(context).pageNo;

    return Column(
      children: [
        Container(
          // color: Colors.red,
          height: SizeConfig.screenHeight! * 0.4,
          child: Stack(
            // alignment: Alignment.bottomCenter,
            children: [
              TopBar(
                  title: LocaleKeys.menu_redeem.tr(),
                  isTap: false,
                  isCenter: true,
                  // pageNo: page,
                  backgroud: AppTheme.secondary,
                  ratio: 0.25,
                  isTopCenter: true,
                  rightHeightEdge: 0.2,
                  leftHeightEdge: 0.2,
                  titleWidth: SizeConfig.screenWidth),
              Positioned(
                bottom: 0,
                right: 50,
                left: 50,
                top: 140,
                child: Container(
                  padding: EdgeInsets.all(5),
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: AppTheme.primary,
                      border:
                          new Border.all(width: 4.0, color: AppTheme.primary),
                      image: DecorationImage(
                          image: new ExactAssetImage(Paths.avatar),
                          fit: BoxFit.contain)),
                ),
              )
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.fromLTRB(0, 20, 0, 10),
          child: Text(
            LocaleKeys.redeem_points.tr() + " 154 Pts",
            style: TextStyle(
                color: AppTheme.grey, fontSize: SizeConfig.screenWidth! * 0.04),
          ),
        ),
        RedeemList(),
        // SizedBox(
        //   height:60
        // )
      ],
    );
  }
}
