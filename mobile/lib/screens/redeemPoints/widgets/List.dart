import 'package:easy_localization/easy_localization.dart';
import 'package:Crootie/locale/locale_keys.g.dart';
import 'package:Crootie/models/responseModels/redeem/RedeemResponse.dart';
import 'package:Crootie/public_widgets/GradiantColor.dart';
import 'package:Crootie/public_widgets/SlidableWidget.dart';
import 'package:Crootie/shared/Constant.dart';
import 'package:Crootie/shared/GlobalFunctions.dart';
import 'package:Crootie/style/SizeConfig.dart';
import 'package:Crootie/style/Style.dart';
import 'package:flutter/material.dart';

class ListWidget extends StatelessWidget {
  final List<RedeemResponse> list;
  const ListWidget({required this.list, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: ListView.builder(
        itemCount: list.length,
        itemBuilder: (context, index) {
          if (index + 1 == list.length) {
            return SizedBox(height: SizeConfig.screenHeight! * 0.1);
          }
          return Container(
            margin: EdgeInsets.fromLTRB(39, 20, 39, 0),
            decoration: BoxDecoration(
                color: AppTheme.secondary,
                borderRadius: BorderRadius.circular(15)),
            child: SlidableWidget(
              description: list[index].priceDescription,
              leftDescription: "",
              rightDescription: LocaleKeys.redeem_button.tr(),
              logo: Paths.companies + list[index].logo,
              slideColor: AppTheme.secondary,
              isVoucher: false,
              isFav: false,
              showFav: false,
              isPoints: true,
              isLeftSlider: false,
              price: list[index].points.toDouble(),
              rightFunction: () {
                GlobalFunctions.confirmationDialog(
                    context, "PIN code", "Redeem", list[index].productId);
              },
              leftFunction: () => () {},
              topColor: color(list[index].topColor),
              bottomColor: color(list[index].bottomColor),
            ),
          );
        },
      ),
    );
  }
}
