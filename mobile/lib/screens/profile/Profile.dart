import 'dart:io';

import 'package:Crootie/shared/Constant.dart';
import 'package:Crootie/shared/GlobalFunctions.dart';
import 'package:Crootie/shared/Routes.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:Crootie/locale/locale_keys.g.dart';
import 'package:Crootie/style/SizeConfig.dart';
import 'package:Crootie/public_widgets/TextButtonWidget.dart';
import 'package:Crootie/public_widgets/topBar/TopBar.dart';
import 'package:Crootie/public_widgets/form/inputForm.dart';
import 'package:Crootie/style/Style.dart';
import 'package:image_picker/image_picker.dart';

TextEditingController controller = new TextEditingController();
File? image;
  final picker = ImagePicker();

  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);

    setState(() {
      if (pickedFile != null) {
        image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

void setState(Null Function() param0) {
}
class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Container(
          height: SizeConfig.screenHeight! * 0.4,
          child: Stack(
            children: [
              //**------------------------- Background ------------------------**/
              TopBar(
                  title: LocaleKeys.profile.tr(),
                  isTap: false,
                  isCenter: true,
                  backgroud: AppTheme.secondary,
                  ratio: 0.25,
                  isTopCenter: true,
                  titleSize: 25,
                  titleWidth: SizeConfig.screenWidth),
              //**------------------------- Profile Pic ------------------------**/
              Positioned(
                bottom: 0,
                right: 50,
                left: 50,
                top: 140,
                child: Stack(
                  alignment: Alignment.bottomCenter,
                  children: [
                    
                    Container(
                      padding: EdgeInsets.all(5),
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: AppTheme.primary,
                          border: new Border.all(
                              width: 8.0,
                              color: AppTheme.grey.withOpacity(0.1)),
                          image: DecorationImage(
                              image: new ExactAssetImage(Paths.avatar),
                              fit: BoxFit.contain)),
                    ),
                    Positioned(
                      bottom: 0,
                      right: 50,
                      left: 160,
                      top: 70,
                      child: Container(
                        decoration: BoxDecoration(
                          // color: AppTheme.grey.withOpacity(0.3),
                          shape: BoxShape.circle,
                        ),
                          child: IconButton(
                          onPressed: (){},
                            icon: Icon(
                        Icons.camera_alt_rounded,
                      ),
                          )),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
        //**------------------------- Profile name ------------------------**/
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              margin: EdgeInsets.only(bottom: 20),
              child: Text(
                "Mahmoud ",
                style: TextStyle(
                    color: Colors.black, fontWeight: FontWeight.bold),
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 20),
              child: Text(
                "Salim",
                style: TextStyle(color: Colors.black),
              ),
            ),
          ],
        ),
        Container(
            margin: SizeConfig.margin,
            child: Wrap(runSpacing: 15,
                // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  //**------------------------- Inputs ------------------------**/
                  input(LocaleKeys.fullname.tr()),
                  input(LocaleKeys.vouchers_send_nickname.tr()),
                  //**------------------------- Buttons ------------------------**/
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      button(LocaleKeys.changepassword.tr(), () {
                        Routes.openPage(context, "/changePassword", 0);
                      }),
                      button(LocaleKeys.resetpassword.tr(), () {
                        GlobalFunctions.confirmationDialog(context,"title", "ResetPassword",0);
                      }),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      button(LocaleKeys.changePIN.tr(), () {
                        Routes.openPage(context, "/verification", 0);
                      }),
                      button(LocaleKeys.resetPIN.tr(), () {
                        GlobalFunctions.confirmationDialog(context,"","ResetPIN",0);
                      }),
                    ],
                  ),
                  TextButtonWidget(
                    title: LocaleKeys.deactivate.tr(),
                    textColor: AppTheme.grey,
                    onPressed: () {
                      Routes.openPage(context, "/verification", 0);
                    },
                  ),
                ])),
        SizedBox(height: 60)
      ],
    );
  }

  Widget input(label) {
    return InputForm(
      filled: true,
      fillColor: AppTheme.grey.withOpacity(0.1),
      controller: controller,
      label: label,
      labelColor: AppTheme.grey,
      radius: 25,
      icon: Icon(Icons.title),
      iconColor: AppTheme.grey,
      borderColor: AppTheme.grey.withOpacity(0.0),
    );
  }

  Widget button(title, onPressed) {
    return TextButtonWidget(
      onPressed: onPressed,
      borderRadius: 20,
      color: AppTheme.primary,
      title: title,
      width: 150,
      height: 40,
      textColor: Colors.white,
      fontSize: SizeConfig.screenWidth! * 0.02,
    );
  }
}
