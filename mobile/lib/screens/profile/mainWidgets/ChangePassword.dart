import 'package:Crootie/locale/locale_keys.g.dart';
import 'package:Crootie/style/SizeConfig.dart';
import 'package:Crootie/public_widgets/TextButtonWidget.dart';
import 'package:Crootie/public_widgets/form/inputForm.dart';
import 'package:Crootie/public_widgets/topBar/TopBar.dart';
import 'package:Crootie/shared/Constant.dart';
import 'package:Crootie/shared/Routes.dart';
import 'package:Crootie/style/Style.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';

TextEditingController oldPassword = new TextEditingController();

TextEditingController newPassword = new TextEditingController();

TextEditingController confirmPassword = new TextEditingController();

class ChangePassword extends StatelessWidget {
  const ChangePassword({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        TopBar(
          logo: Paths.crootie_white,
          pageNo: 0,
          isTap: false,
          isCenter: true,
          ratio: 0.35,
          backgroud: AppTheme.primary,
          imageWidth: 200,
          rightHeightEdge: 0.8,
          leftHeightEdge: 0.8,
          widthEdge: 8,
          rigthWidthEdge: 8,
          showTitle: false,
        ),
        InputForm(
          margin: EdgeInsets.fromLTRB(39, 20, 39, 20),
          borderColor: AppTheme.grey,
          controller: oldPassword,
          label: LocaleKeys.oldPassword.tr(),
          icon: Icon(Icons.lock),
          labelColor: AppTheme.grey,
          isPassword: true,
          filled: false,
          radius: 15,
        ),
        InputForm(
          margin: EdgeInsets.fromLTRB(39, 20, 39, 20),
          borderColor: AppTheme.grey,
          controller: newPassword,
          label: LocaleKeys.newPassword.tr(),
          icon: Icon(Icons.lock),
          labelColor: AppTheme.grey,
          isPassword: true,
          filled: false,
          radius: 15,
        ),
        InputForm(
          margin: EdgeInsets.fromLTRB(39, 20, 39, 20),
          borderColor: AppTheme.grey,
          controller: confirmPassword,
          label: LocaleKeys.confirmPassword.tr(),
          icon: Icon(Icons.lock),
          labelColor: AppTheme.grey,
          isPassword: true,
          filled: false,
          radius: 15,
        ),
        TextButtonWidget(
          margin: EdgeInsets.fromLTRB(39, 20, 39, 20),
          borderRadius: 15,
          color: AppTheme.primary,
          textColor: Colors.white,
          title: LocaleKeys.confirm.tr(),
          width: SizeConfig.screenWidth,
          height: 50,
          onPressed: () {
          },
        ),
        TextButtonWidget(
          title: LocaleKeys.skip.tr(),
          textColor: AppTheme.grey,
          onPressed: () {
            Routes.closePage(context, "/changePassword");
          },
        ),
      ],
    );
  }
}