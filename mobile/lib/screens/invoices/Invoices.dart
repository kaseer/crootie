import 'package:Crootie/screens/invoices/widgets/DatePicker.dart';
import 'package:Crootie/style/Style.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:Crootie/locale/locale_keys.g.dart';

import 'package:Crootie/style/SizeConfig.dart';
import 'package:Crootie/public_widgets/topBar/TopBar.dart';
import 'package:Crootie/shared/Constant.dart';

import 'mainWidgets/InvoicesList.dart';

class Invoices extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height: SizeConfig.screenHeight! * 0.4,
          child: Stack(
            alignment: Alignment.bottomCenter,
            children: [
              TopBar(
                title: LocaleKeys.menu_invoices.tr(),
                logo: Paths.invoices + "logo.png",
                isTap: false,
                ratio: 0.25,
                titleWidth: SizeConfig.screenWidth,
                imageWidth: SizeConfig.screenWidth!,
                imageHeight: 150,
              ),
              // GridView.
              GestureDetector(
                onTap: () {
                  datepickerDialog(context);
                },
                child: Container(
                    height: 50,
                    alignment: Alignment.center,
                    // padding: EdgeInsets.all(10),
                    margin: EdgeInsets.all(10),
                    child: Text(
                      LocaleKeys.Invoices_selectdate.tr(),
                      style: TextStyle(
                        fontSize: SizeConfig.screenWidth! * 0.028,
                      ),
                    ),
                    width: SizeConfig.screenWidth! * 0.6,
                    decoration: BoxDecoration(
                        color: AppTheme.primary,
                        shape: BoxShape.rectangle,
                        borderRadius: BorderRadius.circular(25.0))),
              )
            ],
          ),
        ),
        InvoicesList(),
        // SizedBox(height: 60,)
      ],
    );
  }
}

//*****************************************************************************************************
datepickerDialog(BuildContext context) {
  // pr.hide();
  return showDialog(
    context: context,
    barrierDismissible: true, // user must tap button!
    builder: (BuildContext c) {
      return DatePicker(
        title: LocaleKeys.Invoices_date.tr(),
      );
    },
  );
}
