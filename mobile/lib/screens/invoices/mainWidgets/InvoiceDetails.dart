import 'package:Crootie/public_widgets/FutureWidget.dart';
import 'package:Crootie/screens/invoices/widgets/List.dart';
import 'package:Crootie/services/api/apiServices/InvoicesService.dart';
import 'package:Crootie/shared/GlobalFunctions.dart';
import 'package:flutter/material.dart';
import 'package:Crootie/models/responseModels/invoices/InvoiceResponse.dart';

class InvoiceDetails extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var props = GlobalFunctions.props(context);
    return FutureWidget(
      future: InvoicesService.getDetails(context, props['id']),
      child: (List<InvoiceResponse> list) => ListWidget(list: list),
    );
  }
}
