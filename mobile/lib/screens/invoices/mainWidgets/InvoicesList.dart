import 'package:Crootie/providers/functions/InvoicesProvider.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';

import 'package:Crootie/locale/locale_keys.g.dart';
import 'package:Crootie/style/SizeConfig.dart';
import 'package:Crootie/public_widgets/ListContainer.dart';
import 'package:Crootie/shared/Routes.dart';
import 'package:provider/provider.dart';

class InvoicesList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    InvoicesProvider prov = Provider.of<InvoicesProvider>(context);
    return Flexible(
      child: ListView.builder(
        itemCount: prov.invoices.length,
        itemBuilder: (context, index) {
          if (index + 1 == prov.invoices.length) {
            return SizedBox(height: 60);
          }
          return GestureDetector(
              onTap: () {
                Routes.openPage(context, "/invoicedetails",
                    prov.invoices[index].intvoiceId);
              },
              child: ListContainer(
                showShadow: false,
                // height: 80,
                child: invoiceWidget(index, prov),
              ));
        },
      ),
    );
  }

//********************************************************************************** */
  Widget invoiceWidget(int index, InvoicesProvider prov) {
    return Container(
      padding: EdgeInsets.all(10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                    child: Text(
                  LocaleKeys.Invoices_number.tr() +
                      " : " +
                      prov.invoices[index].invoiceNo,
                  style: TextStyle(fontSize: SizeConfig.screenWidth! * 0.028),
                )),
                Container(
                    child: Text(
                  LocaleKeys.Invoices_date.tr() +
                      prov.invoices[index].createdOn,
                  style: TextStyle(fontSize: SizeConfig.screenWidth! * 0.028),
                )),
              ],
            ),
          ),
          Container(
              child: Text(
            prov.invoices[index].totalPrice.toInt().toString() +
                " " +
                LocaleKeys.currency_libyan.tr(),
            style: TextStyle(fontSize: SizeConfig.screenWidth! * 0.037),
          ))
        ],
      ),
    );
  }
}
