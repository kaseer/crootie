import 'package:Crootie/public_widgets/InvoiceWidget.dart';
import 'package:Crootie/public_widgets/topBar/TopBar.dart';
import 'package:Crootie/style/SizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:Crootie/models/responseModels/invoices/InvoiceResponse.dart';

class ListWidget extends StatelessWidget {
  final List<InvoiceResponse> list;
  const ListWidget({required this.list, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TopBar(
          title: "Invoices",
          isTap: false,
          isTopCenter: true,
          ratio: 0.3,
          titleWidth: SizeConfig.screenWidth,
          widget: Container(
              margin: EdgeInsets.only(top: 30),
              child: InvoiceWidget(list: list)),
        ),
      ],
    );
  }
}
