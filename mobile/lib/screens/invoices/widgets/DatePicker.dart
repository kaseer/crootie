import 'package:Crootie/services/api/apiServices/InvoicesService.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:Crootie/locale/locale_keys.g.dart';

import 'package:Crootie/style/SizeConfig.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';

class DatePicker extends StatefulWidget {
  final String? title;
  DatePicker({Key? key, this.title}) : super(key: key);
  @override
  _DatePickerState createState() => _DatePickerState();
}

class _DatePickerState extends State<DatePicker> {
  void _onSelectionChanged(DateRangePickerSelectionChangedArgs args) {
    print(args.value.endDate);
  }

  DateTime startDate = DateTime.now();
  DateTime endDate = DateTime.now().add(const Duration(days: 5));

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      scrollable: true,
      title: Text(widget.title!),
      content: Container(
        width: SizeConfig.screenWidth! * 0.80,
        height: SizeConfig.screenHeight! * 0.50,
        child: SfDateRangePicker(
          onSelectionChanged: _onSelectionChanged,
          selectionMode: DateRangePickerSelectionMode.range,
        ),
      ),
      // Text('Total is ' + c.total.toString()+' ... aru you sure ?'),
      actions: [
        TextButton(
          child: Text(
            LocaleKeys.confirm.tr(),
          ),
          onPressed: () async {
            InvoicesService.get(context, [startDate, endDate]).then((_) {
              Navigator.pop(context);
            });
          },
        ),
        TextButton(
          child: Text(
            LocaleKeys.cancel.tr(),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ],
    );
  }
}
