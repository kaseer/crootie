import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:Crootie/locale/locale_keys.g.dart';

import 'package:Crootie/style/SizeConfig.dart';
import 'package:Crootie/public_widgets/topBar/TopBar.dart';
import 'package:Crootie/shared/Constant.dart';
import 'package:Crootie/style/Style.dart';

class AboutUs extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TopBar(
          logo: Paths.crootie_white,
          pageNo: 0,
          isTap: false,
          isCenter: true,
          ratio: 0.2,
          backgroud: AppTheme.primary,
          imageWidth: 200,
          imageHeight: 100,
          rightHeightEdge: 0.5,
          leftHeightEdge: 0.5,
          widthEdge: 9,
          rigthWidthEdge: 9,
          showTitle: false,
        ),
        Flexible(
          child: ListView(
          children: [
            Container(
                width: SizeConfig.screenWidth,
                margin: EdgeInsets.all(39),
                child: Text(
                  LocaleKeys.menu_about.tr(),
                  style: TextStyle(color: AppTheme.grey),
                )),
            Container(
              margin: EdgeInsets.fromLTRB(60, 30, 60, 30),
              child: Text(
                aboutUs,
                style: TextStyle(color: AppTheme.grey, fontSize: SizeConfig.screenWidth! * 0.037),
              ),
            ),
            SizedBox(height:60)
          ],
        ))
      ],
    );
  }
}

String aboutUs =
    "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.";
