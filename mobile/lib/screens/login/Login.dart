import 'package:Crootie/models/requestModels/LoginRequest.dart';
import 'package:Crootie/public_widgets/form/FormValidator.dart';
import 'package:Crootie/services/api/apiServices/LoginService.dart';
import 'package:Crootie/shared/GlobalFunctions.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:Crootie/locale/locale_keys.g.dart';

import 'package:Crootie/style/SizeConfig.dart';
import 'package:Crootie/public_widgets/TextButtonWidget.dart';
import 'package:Crootie/public_widgets/form/inputForm.dart';
import 'package:Crootie/shared/Constant.dart';
import 'package:Crootie/shared/Routes.dart';
import 'package:Crootie/style/Style.dart';

TextEditingController mobileNo = new TextEditingController();

TextEditingController password = new TextEditingController();

TextEditingController confirmpassword = new TextEditingController();

final formKey = GlobalKey<FormState>();
class Login extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          margin: SizeConfig.marginTLR,
          child: Form(
            key: formKey,

            child: ListView(
              children: [
                Column(
                  children: [
                    Container(
                      // margin: EdgeInsets.fromLTRB(0, 10, 0, 5),
                      child: Image(
                        image: AssetImage(Paths.logo_primary),
                      ),
                    ),
                    Container(
                      width: 200,
                      child: Image(
                        image: AssetImage(Paths.crootie_black),
                        fit: BoxFit.fill,
                      ),
                    ),
                    InputForm(
                      margin: SizeConfig.marginTB,
                      borderColor: AppTheme.grey,
                      controller: mobileNo,
                      label: LocaleKeys.phone.tr(),
                      isNumber: true,
                      icon: Icon(Icons.phone),
                      labelColor: AppTheme.grey,
                      filled: false,
                      radius: 15,
                      valid: FormValidator.validPhoneNo,
                    ),
                    InputForm(
                      margin: SizeConfig.marginTB,
                      borderColor: AppTheme.grey,
                      controller: password,
                      label: LocaleKeys.password.tr(),
                      icon: Icon(Icons.lock),
                      labelColor: AppTheme.grey,
                      isPassword: true,
                      filled: false,
                      radius: 15,
                      valid: FormValidator.validPassword
                    ),
                    TextButtonWidget(
                      margin: SizeConfig.marginTB,
                      borderRadius: 15,
                      color: AppTheme.primary,
                      textColor: Colors.white,
                      title: LocaleKeys.login.tr(),
                      width: SizeConfig.screenWidth,
                      height: 50,
                      onPressed: () {
                        // bool valid = GlobalFunctions.validate(context,formKey);
                        if (GlobalFunctions.validate(context,formKey)) {
                          LoginRequest data = LoginRequest(
                            mobileNo : mobileNo.text,
                            password : password.text,
                          );
                          LoginService.login(context,data).then((value) => null);
                        }
                      },
                    ),
                    TextButtonWidget(
                      title: LocaleKeys.forget.tr(),
                      textColor: AppTheme.grey,
                      onPressed: () {
                        // Routes.closePage(context, "/login");
                      },
                    ),
                  ],
                ),
                Column(children: [
                  TextButtonWidget(
                    margin: SizeConfig.marginTB,
                    borderRadius: 15,
                    color: AppTheme.primary,
                    textColor: Colors.white,
                    title: LocaleKeys.createaccount.tr(),
                    width: SizeConfig.screenWidth,
                    height: 50,
                    onPressed: () {
                      Routes.openPage(context, "/signup", 0);
                    },
                  ),
                  TextButtonWidget(
                    title: LocaleKeys.skip.tr(),
                    textColor: AppTheme.grey,
                    onPressed: () {
                      Routes.openPageFromMenu(context, "");
                    },
                  ),
                ])
              ],
            ),
          ),
        ),
      ),
    );
  }
}
