import 'package:Crootie/screens/home/Home.dart';
import 'package:Crootie/shared/GlobalFunctions.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:Crootie/locale/locale_keys.g.dart';

import 'package:Crootie/style/SizeConfig.dart';
import 'package:Crootie/public_widgets/TextButtonWidget.dart';
import 'package:Crootie/public_widgets/topBar/TopBar.dart';
import 'package:Crootie/shared/Constant.dart';
import 'package:Crootie/shared/Routes.dart';
import 'package:Crootie/style/Style.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

TextEditingController verificationNo = new TextEditingController();
final GlobalKey<ScaffoldState> _scaffoldkey = GlobalKey<ScaffoldState>();
String? _verificationCode;

class Verification extends StatelessWidget {
//   @override
//   _VerificationState createState() => _VerificationState();
// }

// class _VerificationState extends State<Verification> {
  Future<dynamic> _verifyPhone(BuildContext context, String phoneNo) async {
    phoneNo = "51794362";
    await FirebaseAuth.instance.verifyPhoneNumber(
        phoneNumber: '+216${phoneNo}',
        verificationCompleted: (PhoneAuthCredential credential) async {
          await FirebaseAuth.instance
              .signInWithCredential(credential)
              .then((value) async {
            if (value.user != null) {
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(builder: (context) => Home()),
                  (route) => false);
            }
          });
        },
        verificationFailed: (FirebaseAuthException e) {
          print(e.message);
        },
        codeSent: (String verficationID, int? resendToken) {
          // setState(() {
          _verificationCode = verficationID;
          // });
        },
        codeAutoRetrievalTimeout: (String verificationID) {
          // setState(() {
          _verificationCode = verificationID;
          // });
        },
        timeout: Duration(seconds: 120));
  }

  // @override
  // void initState() {
  //   // TODO: implement initState
  //   super.initState();
  //   _verifyPhone();
  // }
  @override
  Widget build(BuildContext context) {
    var props = GlobalFunctions.props(context);

    return Scaffold(
      key: _scaffoldkey,
      body: SafeArea(
        child: ListView(
          children: [
            TopBar(
              logo: Paths.crootie_white,
              pageNo: 0,
              isTap: false,
              isCenter: true,
              ratio: 0.35,
              backgroud: AppTheme.primary,
              imageWidth: 200,
              rightHeightEdge: 0.8,
              leftHeightEdge: 0.8,
              widthEdge: 8,
              rigthWidthEdge: 8,
              showTitle: false,
            ),
            FutureBuilder(
              future: _verifyPhone(context, props['phoneNo']),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  return Container(
                    margin: EdgeInsets.symmetric(vertical: 20, horizontal: 40),
                    child: PinCodeTextField(
                      appContext: context,
                      textStyle: TextStyle(color: AppTheme.grey),
                      pastedTextStyle: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                      length: 4,
                      blinkWhenObscuring: true,
                      animationType: AnimationType.fade,
                      pinTheme: PinTheme(
                        shape: PinCodeFieldShape.underline,
                        inactiveColor: AppTheme.grey,
                        inactiveFillColor: Color(0xFFF6F6F6),
                        selectedFillColor: Color(0xFFF6F6F6),
                        selectedColor: AppTheme.grey,
                        activeFillColor: Color(0xFFF6F6F6),
                      ),
                      cursorColor: Colors.black,
                      animationDuration: Duration(milliseconds: 300),
                      enableActiveFill: true,
                      // controller: verificationNo,
                      keyboardType: TextInputType.number,
                      onChanged: (pin) async {
                        try {
                          await FirebaseAuth.instance
                              .signInWithCredential(
                                  PhoneAuthProvider.credential(
                                      verificationId: _verificationCode!,
                                      smsCode: pin))
                              .then((value) async {
                            if (value.user != null) {
                              GlobalFunctions.snackBar(
                                  context, "message", AppTheme.success);
                              // Navigator.pushAndRemoveUntil(
                              //     context,
                              //     MaterialPageRoute(builder: (context) => Home()),
                              //     (route) => false);
                            }
                          });
                        } catch (e) {
                          FocusScope.of(context).unfocus();
                          // _scaffoldkey.currentState
                          //     .showSnackBar(SnackBar(content: Text('invalid OTP')));
                        }
                      },
                    ),
                  );
                } else
                  return Container();
              },
            ),
            TextButtonWidget(
              margin: EdgeInsets.all(15),
              borderRadius: 25,
              color: AppTheme.primary,
              textColor: Colors.white,
              title: LocaleKeys.verify.tr(),
              width: SizeConfig.screenWidth,
              height: 50,
              onPressed: () {
                Routes.login(context);
              },
            ),
            TextButtonWidget(
              title: LocaleKeys.skip.tr(),
              textColor: AppTheme.grey,
              onPressed: () {
                Routes.closePage(context, "/verification");
              },
            ),
          ],
        ),
      ),
    );
  }
}
