import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:Crootie/locale/locale_keys.g.dart';

import 'package:Crootie/style/SizeConfig.dart';
import 'package:Crootie/public_widgets/TextButtonWidget.dart';
import 'package:Crootie/public_widgets/topBar/TopBar.dart';
import 'package:Crootie/public_widgets/form/inputForm.dart';
import 'package:Crootie/shared/Constant.dart';
import 'package:Crootie/shared/Routes.dart';
import 'package:Crootie/style/Style.dart';

TextEditingController mobileNo = new TextEditingController();

TextEditingController password = new TextEditingController();

TextEditingController confirmpassword = new TextEditingController();

class Signup extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: ListView(
          children: [
            TopBar(
              logo: Paths.crootie_white,
              pageNo: 0,
              isTap: false,
              isCenter: true,
              ratio: 0.45,
              backgroud: AppTheme.primary,
              imageWidth: 200,
              rightHeightEdge: 0.8,
              leftHeightEdge: 0.8,
              widthEdge: 8,
              rigthWidthEdge: 8,
              showTitle: false,
              showBar: false,
            ),
            InputForm(
              margin: EdgeInsets.fromLTRB(39, 20, 39, 20),
              borderColor: AppTheme.grey,
              controller: mobileNo,
              label: LocaleKeys.phone.tr(),
              isNumber: true,
              icon: Icon(Icons.phone),
              labelColor: AppTheme.grey,
              filled: false,
              radius: 15,
            ),
            InputForm(
              margin: EdgeInsets.fromLTRB(39, 20, 39, 20),
              borderColor: AppTheme.grey,
              controller: mobileNo,
              label: LocaleKeys.password.tr(),
              icon: Icon(Icons.lock),
              labelColor: AppTheme.grey,
              isPassword: true,
              filled: false,
              radius: 15,
            ),
            InputForm(
              margin: EdgeInsets.fromLTRB(39, 20, 39, 20),
              borderColor: AppTheme.grey,
              controller: mobileNo,
              label: LocaleKeys.confirm.tr(),
              icon: Icon(Icons.lock),
              labelColor: AppTheme.grey,
              isPassword: true,
              filled: false,
              radius: 15,
            ),
            TextButtonWidget(
              margin: EdgeInsets.fromLTRB(39, 20, 39, 20),
              borderRadius: 15,
              color: AppTheme.primary,
              textColor: Colors.white,
              title: LocaleKeys.signup.tr(),
              width: SizeConfig.screenWidth,
              height: 50,
              onPressed: () {
                Routes.openPageWithArguments(
                    context, "/verification", {"phoneNo": mobileNo.text});
              },
            ),
            TextButtonWidget(
              title: LocaleKeys.skip.tr(),
              textColor: AppTheme.grey,
              onPressed: () {
                Routes.openPageFromMenu(context, "");
              },
            ),
          ],
        ),
      ),
    );
  }
}
