import 'package:Crootie/models/responseModels/buisness/BuisnessCompaniesResponse.dart';
import 'package:Crootie/public_widgets/FutureWidget.dart';
import 'package:Crootie/services/api/apiServices/BuisnessService.dart';
import 'package:Crootie/style/SizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:Crootie/public_widgets/ListContainer.dart';
import 'package:Crootie/shared/Routes.dart';

class Companies extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureWidget(
      future: BuisnessService.getBuisnessCompanies(context),
      child: (List<BuisnessCompaniesResponse> list) => companies(context,list),
    );
  }
//**------------------------- Widgets ------------------------**/
  Widget companies(BuildContext context, List<BuisnessCompaniesResponse> list){
    return Flexible(
      child: ListView.builder(
        itemCount: list.length,
        itemBuilder: (context, index) {
          if (index+1 == list.length) {
                return SizedBox(height:60);
              }
          return GestureDetector(
              onTap: () {
                Routes.openPageWithArguments(
                    context, "/buisnesscards", {
                      "id": list[index].buisnessId,
                      "companyName": list[index].buisnessCompany,
                    });
                // page.changePage(7.3, context, "Buisness");
              },
              child: ListContainer(
                showShadow: false,
                margin: SizeConfig.listMargin,
                padding: EdgeInsets.all(20),
                child: Text(list[index].buisnessCompany),
              ));
        },
      ),
    );
  }
}
