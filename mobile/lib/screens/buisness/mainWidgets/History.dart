import 'package:Crootie/models/responseModels/buisness/BuisnessHistoryResponse.dart';
import 'package:Crootie/public_widgets/FutureWidget.dart';
import 'package:Crootie/services/api/apiServices/BuisnessService.dart';
import 'package:Crootie/style/SizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:Crootie/locale/locale_keys.g.dart';

import 'package:Crootie/public_widgets/ListContainer.dart';

class History extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureWidget(
      future: BuisnessService.getBuisnessHistory(context),
      child: (List<BuisnessHistoryResponse> list) => history(context, list),
    );
  }

//**------------------------- Widgets ------------------------**/
  Widget history(BuildContext context, List<BuisnessHistoryResponse> list) {
    return Flexible(
      child: ListView.builder(
        itemCount: list.length,
        itemBuilder: (context, index) {
          if (index + 1 == list.length) {
            return SizedBox(height: 60);
          }
          return GestureDetector(
              child: ListContainer(
            showShadow: false,
            padding: EdgeInsets.all(15),
            // margin: SizeConfig.listMargin,
            // height: 100,
            child: Container(
              padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                            child: Text(list[index].buisnessCompany,
                                style: TextStyle(
                                    fontSize:
                                        SizeConfig.screenWidth! * 0.037))),
                        Container(
                            child: Text(
                          list[index].buisnessCard +
                              ", " +
                              LocaleKeys.invoice_qty.tr() +
                              " : " +
                              list[index].quantity.toString(),
                          style: TextStyle(
                              fontSize: SizeConfig.screenWidth! * 0.028),
                        )),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ));
        },
      ),
    );
  }
}
