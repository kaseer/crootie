import 'package:Crootie/models/responseModels/buisness/BuisnessProductsResponse.dart';
import 'package:Crootie/public_widgets/FutureWidget.dart';
import 'package:Crootie/public_widgets/GradiantColor.dart';
import 'package:Crootie/services/api/apiServices/BuisnessService.dart';
import 'package:Crootie/style/SizeConfig.dart';
import 'package:Crootie/shared/GlobalFunctions.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:Crootie/locale/locale_keys.g.dart';

import 'package:Crootie/public_widgets/SlidableWidget.dart';
import 'package:Crootie/public_widgets/topBar/TopBar.dart';
import 'package:Crootie/shared/Constant.dart';
import 'package:Crootie/style/Style.dart';

class BuisnessCards extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var props = GlobalFunctions.props(context);
    return Column(
      children: [
        Container(
          child: TopBar(
            title: LocaleKeys.menu_buisness.tr(),
            logo: Paths.buisness + "logo.png",
            isTap: false,
            isLine: true,
            ratio: 0.2,
          ),
        ),
        FutureWidget(
            future: BuisnessService.getProducts(context, props['id']),
            child: (List<BuisnessProductsResponse> list) =>
                products(list, props['companyName'])),
      ],
    );
  }

//**------------------------- Widgets ------------------------**/
  Widget products(List<BuisnessProductsResponse> list, String name) {
    return Flexible(
      child: ListView.builder(
        itemCount: list.length,
        itemBuilder: (context, index) {
          if (index + 1 == list.length) {
            return SizedBox(height: 60);
          }
          return Column(
            children: [
              index == 0
                  ? Container(
                      height: 40,
                      child: Text(
                        name,
                        style: TextStyle(
                            color: AppTheme.grey, fontWeight: FontWeight.bold),
                      ),
                    )
                  : Container(),
              Container(
                margin: SizeConfig.margin,
                decoration: BoxDecoration(
                    color: AppTheme.secondary,
                    borderRadius: BorderRadius.circular(15)),
                child: SlidableWidget(
                  isLeftSlider: false,
                  description: list[index].priceDescription,
                  leftDescription: "",
                  rightDescription: LocaleKeys.collect.tr(),
                  logo: Paths.companies + list[index].logo,
                  slideColor: AppTheme.secondary,
                  isVoucher: false,
                  topColor: color(list[index].topColor),
                  bottomColor: color(list[index].bottomColor),
                  price: list[index].price,
                  rightFunction: () {
                    // test() async {};
                    GlobalFunctions.confirmationDialog(
                        context,
                        "Date : 10/10/2010 \nFrom : Mahmoud Salim",
                        "Buisness",
                        list[index].productId);
                  },
                  leftFunction: () => () {},
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}
