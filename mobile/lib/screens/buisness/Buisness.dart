import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:Crootie/locale/locale_keys.g.dart';

import 'package:Crootie/providers/functions/ChangePage.dart';
import 'package:Crootie/public_widgets/topBar/TopBar.dart';
import 'package:Crootie/screens/buisness/mainWidgets/History.dart';
import 'package:Crootie/shared/Constant.dart';
import 'package:provider/provider.dart';
import 'mainWidgets/Companies.dart';

class Buisness extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    int page = Provider.of<ChangePage>(context).pageNo;
    return Column(
      children: [
        Container(
          child: TopBar(
            title: LocaleKeys.menu_buisness.tr(),
            logo: Paths.buisness + "logo.png",
            firstTap: LocaleKeys.buisness_tab1.tr(),
            secondTap: LocaleKeys.buisness_tab2.tr(),
            pageNo: page,
            isLine: true,
            ratio: 0.2,
          ),
        ),
        page <= 1 ? Companies() : History(),
      ],
    );
  }
}
