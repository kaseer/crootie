import 'package:Crootie/public_widgets/FooterMenu.dart';
import 'package:flutter/material.dart';
import 'package:Crootie/style/SizeConfig.dart';

class RoutePage extends StatelessWidget {
  final Widget? child;
  RoutePage({this.child});
  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    return Scaffold(
      // bottomNavigationBar: FooterMenu(),
      body: SafeArea(
          child: Stack(
        alignment: Alignment.bottomCenter,
        children: [
          child!,
          FooterMenu(),
        ],
      )),
    );
  }
}
