import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:Crootie/locale/locale_keys.g.dart';

// import 'package:provider/provider.dart';
import 'package:Crootie/style/SizeConfig.dart';
import 'package:Crootie/public_widgets/TextButtonWidget.dart';
import 'package:Crootie/style/Style.dart';

class SettingsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    String currnetLanguage = context.locale.toString();

    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
            // color: Colors.red,
            width: SizeConfig.screenWidth,
            // alignment: Alignment,
            margin: EdgeInsets.all(39),
            child: Text(
              LocaleKeys.settings_language.tr(),
              style: TextStyle(color: AppTheme.grey),
            )),
        Container(
          margin: EdgeInsets.fromLTRB(39, 0, 39, 0),
          padding: EdgeInsets.fromLTRB(0, 8, 0, 8),
          width: SizeConfig.screenWidth,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(25),
              color: AppTheme.secondary),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              selectLanguage(
                context,
                "العربية",
                "ar",
                currnetLanguage,
              ),
              selectLanguage(context, "English", "en", currnetLanguage),
            ],
          ),
        ),
        // TextButtonWidget(
        //   margin: EdgeInsets.all(15),
        //   onPressed: () {},
        //   borderRadius: 25,
        //   height: 50,
        //   width: SizeConfig.screenWidth! * 0.333,
        //   color: AppTheme.primary,
        //   textColor: Colors.white,
        //   title: LocaleKeys.settings_logout.tr(),
        // ),
      ],
    );
  }

//********************************************************************************* */
  Widget selectLanguage(
      BuildContext context, languege, languageCode, currnetLanguage) {
    return GestureDetector(
      onTap: () async {
        await context.setLocale(Locale(languageCode));
      },
      child: Container(
          alignment: Alignment.center,
          margin: EdgeInsets.all(5),
          padding: EdgeInsets.all(5),
          child: Text(
            languege,
            style: TextStyle(
              fontFamily: languageCode == 'ar' ? "Co" : "Futura LT",
              fontSize: SizeConfig.screenWidth! * 0.028,
              color: currnetLanguage == languageCode
                  ? AppTheme.grey
                  : Colors.white,
            ),
          ),
          width: SizeConfig.screenWidth! * 0.25,
          decoration: BoxDecoration(
              color: currnetLanguage == languageCode ? Colors.white : null,
              shape: BoxShape.rectangle,
              borderRadius: BorderRadius.circular(25.0))),
    );
  }
}
