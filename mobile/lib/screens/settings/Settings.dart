import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:Crootie/locale/locale_keys.g.dart';

import 'package:Crootie/style/SizeConfig.dart';
import 'package:Crootie/public_widgets/topBar/TopBar.dart';
import 'package:Crootie/screens/settings/mainWidgets/SettingsPage.dart';
import 'package:Crootie/shared/Constant.dart';
import 'package:Crootie/style/Style.dart';

class Settings extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TopBar(
          title: LocaleKeys.menu_settings.tr(),
          logo: Paths.settings + "logo.png",
          isTap: false,
          isCenter: false,
          backgroud: AppTheme.primary,
          rigthWidthEdge: 18,
          rightHeightEdge: 0.9,
          leftHeightEdge: 0.55,
          titleWidth: SizeConfig.screenWidth,
          ratio: 0.3,
          imageHeight: SizeConfig.screenHeight! * 0.22,
        ),
        SettingsPage()
      ],
    );
  }
}
