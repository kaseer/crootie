import 'package:Crootie/models/responseModels/vouchers/NewVouchersResponse.dart';
import 'package:Crootie/public_widgets/FutureWidget.dart';
import 'package:Crootie/screens/vouchers/widgets/ListNewVouchers.dart';
import 'package:Crootie/services/api/apiServices/VouchersService.dart';
import 'package:flutter/material.dart';
class NewVouchers extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureWidget(
      future: VouchersService.getNewVouchers(context),
      child: (List<NewVouchersResponse> list) => ListWidget(list:list),
    );
  }  
}
