import 'package:Crootie/models/responseModels/vouchers/UsedVouchersResponse.dart';
import 'package:Crootie/public_widgets/FutureWidget.dart';
import 'package:Crootie/screens/vouchers/widgets/ListUsedVouchers.dart';
import 'package:Crootie/services/api/apiServices/VouchersService.dart';
import 'package:flutter/material.dart';
class UsedVoucher extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureWidget(
      future: VouchersService.getUsedVouchers(context),
      child: (List<UsedVouchersResponse> list) => ListWidget(list: list),
    );
  }
}
