import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:Crootie/locale/locale_keys.g.dart';
import 'package:Crootie/style/SizeConfig.dart';
import 'package:Crootie/providers/functions/ChangePage.dart';
import 'package:Crootie/public_widgets/topBar/TopBar.dart';
import 'package:Crootie/shared/Constant.dart';
import 'package:provider/provider.dart';
import 'HbaibeList.dart';
import 'Person.dart';

class SendVoucher extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    int page = Provider.of<ChangePage>(context).pageNo;
    return Column(children: [
      Container(
        child: TopBar(
          title: LocaleKeys.vouchers_send_title.tr(),
          logo: Paths.vouchers + "send-voucher.png",
          isLine: true,
          ratio: 0.2,
          titleWidth: SizeConfig.screenWidth! * 0.3,
          firstPageNo: 1,
          secondPageNo: 2,
          firstTap: LocaleKeys.menu_hbaibe.tr(),
          secondTap: LocaleKeys.vouchers_send_person.tr(),
          pageNo: page,
          imageWidth: 150,
          imageHeight: 100,
        ),
      ),
      page == 1 ? HbaibeList() : Person(),
    ]);
  }
}
