import 'package:Crootie/style/SizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:Crootie/public_widgets/ListContainer.dart';
import 'package:Crootie/shared/Constant.dart';
import 'package:Crootie/shared/GlobalFunctions.dart';

class HbaibeList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: ListView.builder(
        itemCount: hbaibe.length,
        itemBuilder: (context, index) {
          if (index + 1 == hbaibe.length) {
            return SizedBox(height: 60);
          }
          return GestureDetector(
              onTap: () {
                GlobalFunctions.confirmationDialog(context, "", "SendHbaibe",hbaibe[index].hbaibeId);
              },
              child: ListContainer(
                borderRadius: 0,
                margin: EdgeInsets.fromLTRB(39, 1, 39, 1),
                child: hbaibeWidget(index),
              ));
        },
      ),
    );
  }

//-------------------------------------Widgets---------------------------------------------
  Widget hbaibeWidget(int index) {
    return Container(
      padding: EdgeInsets.all(10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                // margin: EdgeInsets.all(15),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                ),
                width: 50,
                height: 50,
                child: Image(
                  fit: BoxFit.contain,
                  image: AssetImage(Paths.avatar),
                ),
              ),
              SizedBox(width: 10),
              Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                        child: Text(
                      hbaibe[index].memberName,
                      style:
                          TextStyle(fontSize: SizeConfig.screenWidth! * 0.028),
                    )),
                    Container(
                        child: Text(
                      hbaibe[index].phoneNo,
                      style:
                          TextStyle(fontSize: SizeConfig.screenWidth! * 0.028),
                    )),
                  ],
                ),
              ),
            ],
          ),
          Icon(
            Icons.arrow_forward_ios_outlined,
            color: Colors.white,
          ),
        ],
      ),
    );
  }
}

class HbaibeData {
  int hbaibeId;
  String memberName;
  String phoneNo;
  HbaibeData({
    required this.hbaibeId,
    required this.memberName,
    required this.phoneNo,
  });
}

List<HbaibeData> hbaibe = [
  HbaibeData(hbaibeId: 1, memberName: "Cortec", phoneNo: "+218 92 279 2629"),
  HbaibeData(hbaibeId: 2, memberName: "Cortec", phoneNo: "+218 92 279 2629"),
  HbaibeData(hbaibeId: 3, memberName: "Cortec", phoneNo: "+218 92 279 2629"),
  HbaibeData(hbaibeId: 4, memberName: "Cortec", phoneNo: "+218 92 279 2629"),
  HbaibeData(hbaibeId: 5, memberName: "Cortec", phoneNo: "+218 92 279 2629"),
  HbaibeData(hbaibeId: 6, memberName: "Cortec", phoneNo: "+218 92 279 2629"),
  HbaibeData(hbaibeId: 7, memberName: "Cortec", phoneNo: "+218 92 279 2629"),
  HbaibeData(hbaibeId: 8, memberName: "Cortec", phoneNo: "+218 92 279 2629"),
  HbaibeData(hbaibeId: 9, memberName: "Cortec", phoneNo: "+218 92 279 2629"),
];
