import 'package:Crootie/models/requestModels/HbaibeRequest.dart';
import 'package:Crootie/services/api/apiServices/VouchersService.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:Crootie/locale/locale_keys.g.dart';
import 'package:Crootie/style/SizeConfig.dart';
import 'package:Crootie/providers/functions/ContactProvider.dart';
import 'package:Crootie/public_widgets/TextButtonWidget.dart';
import 'package:Crootie/shared/GlobalFunctions.dart';
import 'package:Crootie/style/Style.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';

TextEditingController controller = new TextEditingController();
PermissionStatus _permissionStatus = PermissionStatus.denied;



class Person extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var contact = Provider.of<ContactProvider>(context).contactProvider;
    return Flexible(
      child: SingleChildScrollView(
        child: Wrap(
          alignment: WrapAlignment.center,
          runSpacing: 20,
          children: [
            Center(
              child: Column(
                children: [
                  Container(
                    height: 40,
                    margin: EdgeInsets.all(10),
                    child: Text(
                      contact.memberName,
                      style: TextStyle(color: Colors.black),
                    ),
                  ),
                  Container(
                    height: 30,
                    margin: EdgeInsets.all(10),
                    child: Text(
                      contact.phoneNo,
                      style: TextStyle(color: AppTheme.grey, fontSize: SizeConfig.screenWidth! * 0.037),
                    ),
                  ),
                ],
              ),
            ),
            TextButtonWidget(
                fontSize: SizeConfig.screenWidth! * 0.028,
                title: LocaleKeys.phone.tr(),
                textColor: Colors.white,
                color: AppTheme.secondary,
                height: 50,
                borderRadius: 25,
                width: SizeConfig.screenWidth! * 0.6,
                onPressed: () {
                  GlobalFunctions.addContactDialog(context, "voucherContact");
                }),
            TextButtonWidget(
                fontSize: SizeConfig.screenWidth! * 0.028,
                title: LocaleKeys.vouchers_send_contacts.tr(),
                textColor: Colors.white,
                color: AppTheme.secondary,
                height: 50,
                borderRadius: 25,
                width: SizeConfig.screenWidth! * 0.6,
                onPressed: () {
                  GlobalFunctions.requestPermission(Permission.contacts, context,_permissionStatus, 0);
                }),
            TextButtonWidget(
                fontSize: SizeConfig.screenWidth! * 0.028,
                title: LocaleKeys.send.tr(),
                textColor: Colors.white,
                color: AppTheme.primary,
                height: 50,
                borderRadius: 25,
                width: SizeConfig.screenWidth! * 0.6,
                onPressed: () {
                  GlobalFunctions.confirmationDialog(context,"","SendPerson",0);
                }),
            // Container(
            //   alignment: Alignment.center,
            //   width: SizeConfig.screenWidth * 0.6,
            //   child: Text(
            //     LocaleKeys.vouchers_send_notfound.tr(),
            //     style: TextStyle(color: AppTheme.grey, fontSize: 12),
            //   ),
            // ),
            // TextButtonWidget(
            //     borderRadius: 25,
            //     fontSize: 12,
            //     title: LocaleKeys.vouchers_send_invite.tr(),
            //     textColor: AppTheme.grey,
            //     isBorder: true,
            //     borderColor: AppTheme.primary,
            //     height: 50,
            //     width: SizeConfig.screenWidth * 0.6,
            //     onPressed: () {
            //     }),
          ],
        ),
      ),
    );
  }
}
