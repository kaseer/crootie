import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:Crootie/locale/locale_keys.g.dart';

import 'package:Crootie/style/SizeConfig.dart';

import 'package:Crootie/providers/functions/ChangePage.dart';
import 'package:Crootie/public_widgets/topBar/TopBar.dart';
import 'package:Crootie/screens/vouchers/mainWidgets/NewVouchers.dart';
import 'package:Crootie/screens/vouchers/mainWidgets/UsedVoucher.dart';
import 'package:Crootie/shared/Constant.dart';
import 'package:provider/provider.dart';

class Vouchers extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    int page = Provider.of<ChangePage>(context).pageNo;

    return Column(
      children: [
        TopBar(
          title: LocaleKeys.menu_vouchers.tr(),
          logo: page == 1
              ? Paths.vouchers + "new-vouchers.png"
              : Paths.vouchers + "used-vouchers.png",
          firstTap: LocaleKeys.vouchers_tab1.tr(),
          secondTap: LocaleKeys.vouchers_tab2.tr(),
          pageNo: page,
          isLine: true,
          titleSize: 22,
          ratio: 0.2,
          imageWidth: 180,
          titleWidth: SizeConfig.screenWidth! * 0.3,
        ),
        page == 1 ? NewVouchers() : UsedVoucher(),
        // SizedBox(
        //   height:60
        // ),
      ],
    );
  }
}
