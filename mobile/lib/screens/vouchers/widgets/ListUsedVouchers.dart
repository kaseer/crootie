import 'package:Crootie/models/responseModels/vouchers/UsedVouchersResponse.dart';
import 'package:Crootie/public_widgets/CardView.dart';
import 'package:Crootie/public_widgets/GradiantColor.dart';
import 'package:Crootie/shared/Constant.dart';
import 'package:Crootie/style/SizeConfig.dart';
import 'package:flutter/material.dart';

class ListWidget extends StatefulWidget {
  final List<UsedVouchersResponse> list;
  const ListWidget({required this.list, Key? key}) : super(key: key);

  @override
  _ListWidgetState createState() => _ListWidgetState();
}

class _ListWidgetState extends State<ListWidget> {
  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: ListView.builder(
        itemCount: widget.list.length,
        itemBuilder: (context, index) {
          if (index+1 == widget.list.length) {
                return SizedBox(height:SizeConfig.screenHeight! * 0.1);
              }
          return Container(
            margin: EdgeInsets.fromLTRB(39, 20, 39, 0),
            child: CardView(
              topColor: color(widget.list[index].topColor),
              bottomColor: color(widget.list[index].bottomColor),
              description: widget.list[index].priceDescription,
              logo: Paths.companies + widget.list[index].logo,
              isVoucher: false,
              voucherNumber: widget.list[index].serialNo,
            ),
          );
        },
      ),
    );
  }
}
