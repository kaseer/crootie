import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:Crootie/locale/locale_keys.g.dart';

import 'package:Crootie/style/SizeConfig.dart';
import 'package:Crootie/public_widgets/topBar/TopBar.dart';
import 'package:Crootie/screens/payments/verification/mainWidgets/Confirmation.dart';
import 'package:Crootie/shared/Constant.dart';
import 'package:Crootie/style/Style.dart';

class Verification extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Container(
          // color: Colors.red,
          child: Stack(
            alignment: Alignment.bottomCenter,
            children: [
              TopBar(
                title: LocaleKeys.payments_info.tr(),
                logo: Paths.payments + "main.png",
                isTap: false,
                backgroud: AppTheme.primary,
                ratio: 0.25,
                rigthWidthEdge: 18,
                rightHeightEdge: 0.9,
                titleWidth: SizeConfig.screenWidth,
                imageHeight: 150,
                imageWidth: 150,
              ),
              Container(
                // height:80,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      height:40,
                      width:60,
                      padding: EdgeInsets.all(5),
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image:
                                  new ExactAssetImage(Paths.payments + "key.png"),
                              fit: BoxFit.fitHeight)),
                    ),
                    SizedBox(width: 40),
                    Container(
                      height:40,
                      width:60,
                      // padding: EdgeInsets.all(5),
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image:
                                  new ExactAssetImage(Paths.payments + "lock.png"),
                              fit: BoxFit.fitHeight)),
                    ),
                    SizedBox(width: 50),
                  ],
                ),
              )
            ],
          ),
        ),
        Confirmation(),
          SizedBox(
            height:60
          )
      ],
    );
  }
}
