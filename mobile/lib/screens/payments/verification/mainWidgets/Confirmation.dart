import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:Crootie/locale/locale_keys.g.dart';

import 'package:Crootie/style/SizeConfig.dart';
import 'package:Crootie/public_widgets/TextButtonWidget.dart';
import 'package:Crootie/shared/Routes.dart';
import 'package:Crootie/style/Style.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

TextEditingController verificationNo = new TextEditingController();

class Confirmation extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        SizedBox(height: 40),
        Center(
          child: Text(
            LocaleKeys.payments_confirmattion.tr(),
            style: TextStyle(
                color: AppTheme.grey,
                fontSize: SizeConfig.screenWidth! * 0.033),
          ),
        ),
        Center(
          child: Text(
            LocaleKeys.payments_verification.tr(),
            style: TextStyle(
                color: AppTheme.grey,
                fontSize: SizeConfig.screenWidth! * 0.033),
          ),
        ),
        Container(
          margin: EdgeInsets.symmetric(vertical: 20, horizontal: 40),
          child: PinCodeTextField(
            appContext: context,
            textStyle: TextStyle(color: AppTheme.grey),
            pastedTextStyle: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
            ),
            length: 4,
            blinkWhenObscuring: true,
            animationType: AnimationType.fade,
            pinTheme: PinTheme(
              shape: PinCodeFieldShape.underline,
              inactiveColor: AppTheme.grey,
              inactiveFillColor: Color(0xFFF6F6F6),
              selectedFillColor: Color(0xFFF6F6F6),
              selectedColor: AppTheme.grey,
              activeFillColor: Color(0xFFF6F6F6),
            ),
            cursorColor: Colors.black,
            animationDuration: Duration(milliseconds: 300),
            enableActiveFill: true,
            // controller: verificationNo,
            keyboardType: TextInputType.number,
            onChanged: (value) {
              print("object");
            },
            // onCompleted: (v) {
            //   print("Completed");
            // },
            // onTap: () {
            //   print("Pressed");
            // },
            // beforeTextPaste: (text) {
            //   print("Allowing to paste $text");
            //   //if you return true then it will show the paste confirmation dialog. Otherwise if false, then nothing will happen.
            //   //but you can show anything you want here, like your pop up saying wrong paste format or etc
            //   return true;
            // },
          ),

          // child: OTPTextField(
          //   onChanged: (pin) {},
          //   length: 4,
          //   width: SizeConfig.screenWidth! * 0.7,
          //   fieldWidth: 30,
          //   style: TextStyle(fontSize: SizeConfig.screenWidth! * 0.04, color: AppTheme.grey),
          //   textFieldAlignment: MainAxisAlignment.spaceAround,
          //   fieldStyle: FieldStyle.underline,
          //   onCompleted: (pin) {
          //     print("Completed: " + pin);
          //   },
          // ),
        ),
        TextButtonWidget(
          onPressed: () {
            Routes.openPage(context, "/finalinvoice", 0);
          },
          color: AppTheme.primary,
          borderRadius: 15,
          height: 50,
          textColor: Colors.white,
          title: LocaleKeys.payments_purchuss.tr(),
          width: SizeConfig.screenWidth,
          margin: SizeConfig.margin,
        )
      ],
    );
  }
}
