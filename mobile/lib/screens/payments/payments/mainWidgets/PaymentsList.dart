import 'package:Crootie/style/SizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:Crootie/public_widgets/ListContainer.dart';
import 'package:Crootie/shared/Constant.dart';
import 'package:Crootie/shared/Routes.dart';
import 'package:Crootie/style/Style.dart';

class PaymentList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: ListView.builder(
          // physics: NeverScrollableScrollPhysics(),
          // shrinkWrap: true,
          itemCount: payments.length,
          itemBuilder: (context, index) {
            if (index + 1 == payments.length) {
              return SizedBox(height: 60);
            }
            return GestureDetector(
              onTap: () {
                Routes.openPage(
                    context, "/paymentinfo", payments[index].paymentId);
              },
              child: ListContainer(
                height: 60,
                color: AppTheme.secondary,
                child: paymentWidget(index),
              ),
            );
          }),
    );
  }

//***************************************************************** */
  Widget paymentWidget(index) {
    return Container(
      // padding: EdgeInsets.all(10),
      child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        Row(
          children: [
            Container(
              padding: EdgeInsets.all(10),
              child: Image(
                height: 40,
                image: AssetImage(Paths.payments + payments[index].logo),
              ),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
              // width: SizeConfig.screenWidth * 0.15,
              child: Text(
                payments[index].paymentName,
                style: TextStyle(fontSize: SizeConfig.screenWidth! * 0.037),
              ),
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Icon(
              Icons.arrow_forward_ios,
              color: Colors.white,
            ),
          ],
        )
      ]),
    );
  }
}

class PaymentData {
  int paymentId;
  String logo;
  String paymentName;
  PaymentData({
    required this.paymentId,
    required this.logo,
    required this.paymentName,
  });
}

List<PaymentData> payments = [
  PaymentData(
    paymentId: 1,
    logo: "sadad-white.png",
    paymentName: "Sadad",
  ),
  PaymentData(
    paymentId: 2,
    logo: "sadad-white.png",
    paymentName: "Sadad",
  ),
  PaymentData(
    paymentId: 3,
    logo: "raseed.png",
    paymentName: "Sadad",
  ),
  PaymentData(
    paymentId: 4,
    logo: "raseed.png",
    paymentName: "Sadad",
  ),
  PaymentData(
    paymentId: 5,
    logo: "raseed-oil.png",
    paymentName: "Sadad",
  ),
  PaymentData(
    paymentId: 6,
    logo: "raseed-oil.png",
    paymentName: "Sadad",
  ),
  PaymentData(
    paymentId: 7,
    logo: "sadad-white.png",
    paymentName: "Sadad",
  ),
  PaymentData(
    paymentId: 8,
    logo: "sadad-white.png",
    paymentName: "Sadad",
  ),
  PaymentData(
    paymentId: 9,
    logo: "sadad-white.png",
    paymentName: "Sadad",
  ),
];
