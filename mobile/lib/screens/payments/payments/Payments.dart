import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:Crootie/locale/locale_keys.g.dart';

import 'package:Crootie/style/SizeConfig.dart';
import 'package:Crootie/public_widgets/topBar/TopBar.dart';
import 'package:Crootie/screens/payments/payments/mainWidgets/PaymentsList.dart';
import 'package:Crootie/shared/Constant.dart';
import 'package:Crootie/style/Style.dart';

class Payments extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height: SizeConfig.screenHeight! * 0.45,
          child: Stack(
            // alignment: Alignment.bottomCenter,
            children: [
              TopBar(
                title: LocaleKeys.payments_methods.tr(),
                isTopCenter: true,
                // logo: Paths.payments + "logo.png",
                isTap: false,
                backgroud: AppTheme.primary,
                ratio: 0.35,
                titleWidth: SizeConfig.screenWidth,
                imageWidth: SizeConfig.screenWidth!,
                imageHeight: SizeConfig.screenHeight! * 0.3,
                leftHeightEdge: 0.5,
                rightHeightEdge: 0.5,
                titleSize: 15,
              ),
              Positioned(
                bottom: 0,
                right: 50,
                left: 50,
                top: 60,
                child: Container(
                  padding: EdgeInsets.all(5),
                  decoration: BoxDecoration(
                      // shape: BoxShape.circle,
                      // color: AppTheme.primary,
                      // border:
                      //     new Border.all(width: 4.0, color: AppTheme.primary),
                      image: DecorationImage(
                          image: new ExactAssetImage(Paths.payments + "logo.png"),
                          fit: BoxFit.contain)),
                ),
              )
            ],
          ),
        ),
        PaymentList()
      ],
    );
  }
}
