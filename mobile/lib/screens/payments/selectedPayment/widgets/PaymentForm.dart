import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:Crootie/locale/locale_keys.g.dart';

import 'package:Crootie/style/SizeConfig.dart';
import 'package:Crootie/public_widgets/TextButtonWidget.dart';
import 'package:Crootie/public_widgets/form/inputForm.dart';
import 'package:Crootie/shared/Constant.dart';
import 'package:Crootie/shared/Routes.dart';
import 'package:Crootie/style/Style.dart';

TextEditingController controller = new TextEditingController();

class PaymentForm extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top:20),
      child: Column(
        children: [
          Container(
            width: 100,
            height: 100,
            // margin: EdgeInsets.all(10),
            child: Image(
              image: AssetImage(Paths.payments + "sadad.png"),
            ),
          ),
          Container(
            margin: EdgeInsets.all(10),
            child: Text(
              "Sadad payment",
              style: TextStyle(color: AppTheme.grey),
            ),
          ),
          InputForm(
            margin: SizeConfig.margin,
            fillColor: Colors.white,
            controller: controller,
            isPassword: false,
            label: LocaleKeys.phone.tr(),
            isNumber: true,
            valid: null,
            borderColor: AppTheme.primary,
            icon: Icon(Icons.phone),
            iconColor: AppTheme.primary,
            labelColor: AppTheme.grey,
            radius: 15,
          ),
          TextButtonWidget(
            margin: SizeConfig.margin,
            onPressed: () {
              Routes.openPage(context, "/purchassverification", 1);
            },
            color: AppTheme.primary,
            borderRadius: 15,
            height: 50,
            width: SizeConfig.screenWidth,
            textColor: Colors.white,
            title: LocaleKeys.done.tr(),
          )
        ],
      ),
    );
  }
}
