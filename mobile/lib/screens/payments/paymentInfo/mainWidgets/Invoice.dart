import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';

import 'package:Crootie/locale/locale_keys.g.dart';
import 'package:Crootie/style/SizeConfig.dart';
import 'package:Crootie/public_widgets/InvoiceWidget.dart';
import 'package:Crootie/public_widgets/ListContainer.dart';
import 'package:Crootie/shared/Routes.dart';
import 'package:Crootie/style/Style.dart';

class Invoice extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // AppTheme.buildLightTheme().textTheme.apply(bodyColor: AppTheme.primary);
    return Container(
      margin: EdgeInsets.only(top: 30),
      child: Column(
        children: [
          // SizedBox(height:SizeConfig.screenHeight! * 0.1),
          InvoiceWidget(list: []),
          ListContainer(
            // width: SizeConfig.screenWidth! / 1.5,
            color: AppTheme.primary,
            height: SizeConfig.screenHeight! / 5,
            child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Text(
                    LocaleKeys.total.tr() +
                        " 150" +
                        LocaleKeys.currency_libyan.tr(),
                    style: TextStyle(fontSize: SizeConfig.screenWidth! * 0.033),
                  ),
                  Text(
                      LocaleKeys.fees.tr() +
                          " 0" +
                          LocaleKeys.currency_libyan.tr(),
                      style:
                          TextStyle(fontSize: SizeConfig.screenWidth! * 0.033)),
                  TextButton(
                    onPressed: () {
                      Routes.openPage(context, "/selectedpayment", 1);
                    },
                    child: Container(
                        alignment: Alignment.center,
                        width: 100,
                        height: 30,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(20),
                        ),
                        child: Text(
                          LocaleKeys.payments_checkout.tr(),
                        )),
                  ),
                ]),
          ),
          SizedBox(height: 60)
        ],
      ),
    );
  }
}
