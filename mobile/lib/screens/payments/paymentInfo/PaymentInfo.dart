import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:Crootie/locale/locale_keys.g.dart';

import 'package:Crootie/style/SizeConfig.dart';
import 'package:Crootie/public_widgets/topBar/TopBar.dart';
import 'package:Crootie/screens/payments/paymentInfo/mainWidgets/Invoice.dart';
import 'package:Crootie/style/Style.dart';

class PaymentInfo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment:Alignment.bottomCenter,
      children: [
        TopBar(
          title: LocaleKeys.payments_info.tr(),
          isTap: false,
          isTopCenter: true,
          backgroud: AppTheme.secondary,
          ratio: 0.35,
          rightHeightEdge: 0.7,
          leftHeightEdge: 0.7,
          titleWidth: SizeConfig.screenWidth,
          // widget: Invoice(),
        ),
        Container(
          height:SizeConfig.screenHeight! * 0.9,
          child: SingleChildScrollView(child: Invoice())
        ),
      ],
    );
  }
}
