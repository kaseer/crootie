import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';

import 'package:Crootie/locale/locale_keys.g.dart';
import 'package:Crootie/style/SizeConfig.dart';
import 'package:Crootie/public_widgets/InvoiceWidget.dart';
import 'package:Crootie/public_widgets/ListContainer.dart';
import 'package:Crootie/public_widgets/TextButtonWidget.dart';
import 'package:Crootie/shared/Routes.dart';
import 'package:Crootie/style/Style.dart';

class Invoice extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // AppTheme.buildLightTheme().textTheme.apply(bodyColor: AppTheme.primary);
    return Container(
      margin: EdgeInsets.only(top: 30),
      child: Column(
        children: [
          // SizedBox(height:SizeConfig.screenHeight! * 0.1),
          InvoiceWidget(
            list: [],
          ),
          ListContainer(
            padding: EdgeInsets.all(15),
            height: SizeConfig.screenHeight! / 5,
            // width: SizeConfig.screenWidth! / 1.5,
            color: AppTheme.primary,
            child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    LocaleKeys.total.tr() +
                        "150" +
                        LocaleKeys.currency_libyan.tr(),
                    style: TextStyle(fontSize: SizeConfig.screenWidth! * 0.033),
                  ),
                  Text(
                      LocaleKeys.fees.tr() +
                          "0" +
                          LocaleKeys.currency_libyan.tr(),
                      style:
                          TextStyle(fontSize: SizeConfig.screenWidth! * 0.033)),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      TextButtonWidget(
                        onPressed: () {
                          Routes.openPageFromMenu(context, "/");
                        },
                        borderRadius: 20,
                        color: AppTheme.primary,
                        title: LocaleKeys.menu_home.tr(),
                        width: SizeConfig.screenWidth! / 1.5 / 2.5,
                        height: 40,
                        textColor: Colors.white,
                        borderColor: Colors.white,
                        isBorder: true,
                        fontSize: SizeConfig.screenWidth! * 0.028,
                      ),
                      TextButtonWidget(
                        onPressed: () {
                          Routes.openPageFromMenu(context, "/vouchers");
                        },
                        borderRadius: 20,
                        color: AppTheme.primary,
                        title: LocaleKeys.payments_vouchers.tr(),
                        width: SizeConfig.screenWidth! / 1.5 / 2.5,
                        height: 40,
                        textColor: Colors.white,
                        borderColor: Colors.white,
                        isBorder: true,
                        fontSize: SizeConfig.screenWidth! * 0.028,
                      ),
                    ],
                  ),
                ]),
          ),
          SizedBox(height: 60)
        ],
      ),
    );
  }
}
