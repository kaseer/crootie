import 'package:Crootie/models/responseModels/AllProductsResponse.dart';
import 'package:Crootie/public_widgets/FutureWidget.dart';
import 'package:Crootie/screens/favorites/widgets/List.dart';
import 'package:Crootie/services/api/apiServices/FavoritesService.dart';
import 'package:flutter/material.dart';

class FavoritesList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureWidget(
      future: FavoritesService.getFavorites(context),
      child: ((List<AllProductsResponse> list) => ListWidget(
            list: list,
          )),
    );
  }
}
