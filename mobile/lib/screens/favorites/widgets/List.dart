import 'package:Crootie/locale/locale_keys.g.dart';
import 'package:Crootie/models/responseModels/AllProductsResponse.dart';
import 'package:Crootie/public_widgets/GradiantColor.dart';
import 'package:Crootie/public_widgets/SlidableWidget.dart';
import 'package:Crootie/services/api/apiServices/FavoritesService.dart';
import 'package:Crootie/shared/Constant.dart';
import 'package:Crootie/style/SizeConfig.dart';
import 'package:Crootie/style/Style.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';

class ListWidget extends StatefulWidget {
  final List<AllProductsResponse> list;
  const ListWidget({required this.list, Key? key}) : super(key: key);

  @override
  _ListWidgetState createState() => _ListWidgetState();
}

class _ListWidgetState extends State<ListWidget> {
  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: ListView.builder(
        itemCount: widget.list.length,
        itemBuilder: (context, index) {
          if (index + 1 == widget.list.length) {
            return SizedBox(height: 60);
          }
          return Container(
            decoration: BoxDecoration(
                color: AppTheme.secondary,
                borderRadius: BorderRadius.circular(15)),
            margin: SizeConfig.margin,
            child: SlidableWidget(
              description: widget.list[index].priceDescription,
              leftDescription: LocaleKeys.addToCart.tr(),
              rightDescription: LocaleKeys.buy.tr(),
              logo: Paths.companies + widget.list[index].logo,
              slideColor: AppTheme.secondary,
              isVoucher: false,
              isFav: true,
              showFav: true,
              price: widget.list[index].price,
              rightFunction: () => () {},
              leftFunction: () => () {},
              topColor: color(widget.list[index].topColor),
              bottomColor: color(widget.list[index].bottomColor),
              favRemove: () async {
                var responseCode = await FavoritesService.remove(context, widget.list[index].productId);
                if (responseCode == 200){
                  setState(() {
                    widget.list.removeAt(index);
                  });
                }
              },
            ),
          );
        },
      ),
    );
  }
}
