import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:Crootie/locale/locale_keys.g.dart';

import 'package:Crootie/providers/functions/ChangePage.dart';
import 'package:Crootie/public_widgets/topBar/TopBar.dart';
import 'package:Crootie/shared/Constant.dart';
import 'package:provider/provider.dart';

import 'mainWidgets/FavoritesList.dart';

class Favorites extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    int page = Provider.of<ChangePage>(context).pageNo;

    return Column(
      children: [
        Container(
          child: TopBar(
            title: LocaleKeys.menu_favorites.tr(),
            logo: Paths.favorites + "logo.png",
            pageNo: page,
            isTap: false,
            isLine: true,
            ratio: 0.2,
            imageWidth: 200,
            imageHeight: 120,
          ),
        ),
        FavoritesList(),
        // SizedBox(
        //   height:60
        // )
      ],
    );
  }
}
