import 'package:Crootie/models/requestModels/TicketsRequest.dart';
import 'package:Crootie/services/api/apiServices/SupportService.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:Crootie/locale/locale_keys.g.dart';

import 'package:Crootie/style/SizeConfig.dart';
import 'package:Crootie/public_widgets/TextButtonWidget.dart';
import 'package:Crootie/public_widgets/topBar/TopBar.dart';
import 'package:Crootie/public_widgets/form/inputForm.dart';
import 'package:Crootie/style/Style.dart';

TextEditingController subject = new TextEditingController();
TextEditingController problem = new TextEditingController();

class NewTicket extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        TopBar(
          title: LocaleKeys.menu_support.tr(),
          isTap: false,
          isTopCenter: true,
          backgroud: AppTheme.primary,
          rightHeightEdge: 0.99,
          titleWidth: SizeConfig.screenWidth,
          // widget: newTicket(),
        ),
        newTicket(context),
      ],
    );
  }

//************************************************************************* */
  Widget newTicket(BuildContext context) {
    return ListView(
      // mainAxisAlignment: MainAxisAlignment.end,
      children: [
        SizedBox(height: SizeConfig.screenHeight! * 0.1),
        Container(
          margin: EdgeInsets.fromLTRB(20, 100, 20, 20),
          child: InputForm(
            labelColor: AppTheme.grey,
            controller: subject,
            isPassword: false,
            label: LocaleKeys.support_subject.tr(),
            isNumber: false,
            valid: null,
            borderColor: AppTheme.grey,
            fillColor: Colors.white,
            icon: Icon(Icons.subject),
            iconColor: AppTheme.grey,
            radius: 25,
          ),
        ),
        Container(
          margin: EdgeInsets.all(20),
          child: InputForm(
            labelColor: AppTheme.grey,
            controller: problem,
            isPassword: false,
            label: LocaleKeys.support_problem.tr(),
            isNumber: false,
            valid: null,
            maxLines: 8,
            borderColor: AppTheme.primary,
            fillColor: Colors.white,
            icon: Icon(Icons.subject),
            iconColor: AppTheme.grey,
            radius: 25,
          ),
        ),
        Center(
          child: TextButtonWidget(
            onPressed: () {
              TicketsRequest ticket = new TicketsRequest(
                  subject: subject.text, problem: problem.text);
              SupportService.addTicket(context, ticket);
            },
            title: LocaleKeys.send.tr(),
            textColor: Colors.white,
            color: AppTheme.primary,
            height: 50,
            width: SizeConfig.screenWidth! / 2,
          ),
        ),
        SizedBox(height: SizeConfig.screenHeight! * 0.1),
      ],
    );
  }
}
