import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:Crootie/locale/locale_keys.g.dart';

import 'package:Crootie/style/SizeConfig.dart';
import 'package:Crootie/public_widgets/TextButtonWidget.dart';
import 'package:Crootie/public_widgets/topBar/TopBar.dart';
import 'package:Crootie/style/Style.dart';
import 'package:signalr_netcore/hub_connection.dart';
import 'package:signalr_netcore/hub_connection_builder.dart';

class TicketDetails extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TopBar(
          title: LocaleKeys.menu_support.tr(),
          isTap: false,
          isCenter: true,
          // pageNo: page,
          backgroud: AppTheme.primary,
          rigthWidthEdge: 18,
          rightHeightEdge: 0.9,
        ),
        Details()
      ],
    );
  }
}
/*---------------------------------------------------------------------------------*/

class Details extends StatefulWidget {
  const Details({Key? key}) : super(key: key);

  @override
  _DetailsState createState() => _DetailsState();
}

class _DetailsState extends State<Details> {
  final String url = "http://192.168.0.206:6001/requesthub";
  HubConnection? hubConnection;
  @override
  void initState() {
    super.initState();
    initSignalR();
  }

  void initSignalR() {
    hubConnection = HubConnectionBuilder().withUrl(url).build();
    hubConnection!.onclose(({error}) => print("object"));
    hubConnection!.start();
    hubConnection!.on("ReceiveRequest", _handleReceive);
  }

  _handleReceive(List<Object>? args) {
    setState(() {
      tickets.add(
          TicketData(id: 99, isAdmin: false, repliedBy: "test", reply: "test"));
    });
  }

  Widget build(BuildContext context) {
    return Flexible(
      child: ListView(
        children: [
          ListView.builder(
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: tickets.length,
              itemBuilder: (context, index) {
                return Container(
                  child: Column(
                    children: [
                      tickets[index].isAdmin
                          ? Container(
                              alignment: Alignment.center,
                              child: Text(LocaleKeys.support_repliedby.tr() +
                                  "          " +
                                  tickets[index].repliedBy),
                              height: 50,
                              width: SizeConfig.screenWidth,
                              margin: EdgeInsets.fromLTRB(10, 5, 10, 0),
                              decoration: BoxDecoration(
                                  color: AppTheme.secondary,
                                  shape: BoxShape.rectangle,
                                  borderRadius: BorderRadius.circular(25)),
                            )
                          : Container(),
                      Container(
                        child: Text(
                          tickets[index].reply,
                          style: TextStyle(
                              color: AppTheme.primary,
                              fontSize: SizeConfig.screenWidth! * 0.037),
                        ),
                        height: 200,
                        width: SizeConfig.screenWidth,
                        margin: EdgeInsets.fromLTRB(10, 0, 10, 20),
                        padding: EdgeInsets.all(15),
                        decoration: BoxDecoration(
                            color: AppTheme.grey.withOpacity(0.2),
                            shape: BoxShape.rectangle,
                            borderRadius: BorderRadius.circular(25)),
                      ),
                    ],
                  ),
                );
              }),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              TextButtonWidget(
                margin: EdgeInsets.all(15),
                borderRadius: 20,
                color: AppTheme.error,
                title: LocaleKeys.support_close.tr(),
                width: 100,
                height: 50,
                textColor: Colors.white,
                onPressed: () {},
              ),
              TextButtonWidget(
                margin: EdgeInsets.all(15),
                borderRadius: 20,
                color: AppTheme.primary,
                title: LocaleKeys.support_reply.tr(),
                width: 100,
                height: 50,
                textColor: Colors.white,
                onPressed: () async {
                  print("object");
                  var dd = await hubConnection!
                      .invoke("TicketRequest", args: <Object>[22.99, 22.88]);
                  print("12");
                  _handleReceive(<Object>[22.99, 22.88]);
                },
              ),
            ],
          ),
          SizedBox(height: 60)
        ],
      ),
    );
  }
}

/**----------------------------------------------- */
class TicketData {
  int id;
  String reply;
  String repliedBy;
  bool isAdmin;
  TicketData(
      {required this.id,
      required this.reply,
      required this.repliedBy,
      required this.isAdmin});
}

List<TicketData> tickets = [
  TicketData(
      id: 1,
      reply: "This problem is blah blah blah blah blah blah blah",
      repliedBy: "Salim",
      isAdmin: true),
  TicketData(
      id: 2,
      reply: "This problem is blah blah blah blah blah blah blah",
      repliedBy: "Salim",
      isAdmin: true),
  TicketData(
      id: 3,
      reply: "This problem is blah blah blah blah blah blah blah",
      repliedBy: "Salim",
      isAdmin: true),
  TicketData(
      id: 4,
      reply: "This problem is blah blah blah blah blah blah blah",
      repliedBy: "Salim",
      isAdmin: true),
  TicketData(
      id: 5,
      reply: "This problem is blah blah blah blah blah blah blah",
      repliedBy: "Salim",
      isAdmin: true),
];
