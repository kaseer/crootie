import 'package:Crootie/models/responseModels/support/TicketsResponse.dart';
import 'package:Crootie/public_widgets/FutureWidget.dart';
import 'package:Crootie/screens/support/widgets/List.dart';
import 'package:Crootie/services/api/apiServices/SupportService.dart';
import 'package:Crootie/style/SizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:Crootie/shared/Routes.dart';
import 'package:Crootie/style/Style.dart';

class TicketsList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        SizedBox(height: SizeConfig.screenHeight! * 0.1),
        Container(
          margin: SizeConfig.margin,
          child: IconButton(
            onPressed: () {
              Routes.openPage(context, "/newticket", 0);
            },
            icon: Icon(
              Icons.add,
              color: Colors.white,
            ),
          ),
          height: 50,
          width: 50,
          decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: AppTheme.primary,
              border: Border.all(width: 3, color: Colors.white)),
        ),
        FutureWidget(
          future: SupportService.get(context),
          child: (List<TicketsResponse> list) => ListWidget(list: list),
        ),
      ],
    );
  }
}
