import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:Crootie/locale/locale_keys.g.dart';

import 'package:Crootie/style/SizeConfig.dart';
import 'package:Crootie/providers/functions/ChangePage.dart';
import 'package:Crootie/public_widgets/topBar/TopBar.dart';
import 'package:Crootie/screens/support/mainWidgets/TicketsList.dart';
import 'package:Crootie/style/Style.dart';
import 'package:provider/provider.dart';

class Support extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    int page = Provider.of<ChangePage>(context).pageNo;

    return Stack(
      children: [
        TopBar(
          title: LocaleKeys.menu_support.tr(),
          isTap: false,
          isTopCenter: true,
          pageNo: page,
          backgroud: AppTheme.primary,
          rigthWidthEdge: 18,
          rightHeightEdge: 0.9,
          titleWidth: SizeConfig.screenWidth,
          // widget: TicketsList(),
        ),
        TicketsList(),
      ],
    );
  }
}
