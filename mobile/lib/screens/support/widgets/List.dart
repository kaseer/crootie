import 'package:easy_localization/easy_localization.dart';
import 'package:Crootie/locale/locale_keys.g.dart';
import 'package:Crootie/models/responseModels/support/TicketsResponse.dart';
import 'package:Crootie/public_widgets/ListContainer.dart';
import 'package:Crootie/shared/Routes.dart';
import 'package:Crootie/style/SizeConfig.dart';
import 'package:Crootie/style/Style.dart';
import 'package:flutter/material.dart';

class ListWidget extends StatelessWidget {
  final List<TicketsResponse> list;
  const ListWidget({required this.list, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: ListView.builder(
        itemCount: list.length,
        itemBuilder: (context, index) {
          if (index + 1 == list.length) {
            return SizedBox(height: 60);
          }
          return GestureDetector(
              onTap: () {
                Routes.openPage(
                    context, "/ticketdetails", list[index].ticketId);
              },
              child: ListContainer(
                color: Color(0xff4D4D4D),
                child: ticketWidget(list[index]),
              ));
        },
      ),
    );
  }

  //********************************************************************* */
  Widget ticketWidget(TicketsResponse data) {
    return Container(
      padding: EdgeInsets.all(10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                    child: Text(
                  data.ticketNo,
                  style: TextStyle(fontSize: SizeConfig.screenWidth! * 0.028),
                )),
                Container(
                    child: Text(
                  LocaleKeys.support_subject.tr() + data.ticketSubject,
                  style: TextStyle(fontSize: SizeConfig.screenWidth! * 0.023),
                )),
              ],
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              SizedBox(
                width: 60,
                child: Container(
                    child: Text(
                  data.status == 1
                      ? LocaleKeys.support_waiting.tr()
                      : LocaleKeys.support_replied.tr(),
                  style: TextStyle(
                      color:
                          data.status == 2 ? AppTheme.success : AppTheme.error,
                      fontSize: SizeConfig.screenWidth! * 0.028),
                )),
              ),
              SizedBox(
                width: 30,
                child: Container(
                  child: IconButton(
                    color: Colors.white,
                    onPressed: () {},
                    icon: Icon(Icons.arrow_forward_ios_outlined),
                  ),
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
