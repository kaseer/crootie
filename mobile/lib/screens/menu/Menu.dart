import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:Crootie/locale/locale_keys.g.dart';

import 'package:Crootie/style/SizeConfig.dart';
import 'package:Crootie/providers/functions/ChangePage.dart';
import 'package:Crootie/public_widgets/TextButtonWidget.dart';
import 'package:Crootie/shared/Constant.dart';
import 'package:Crootie/shared/Routes.dart';
import 'package:Crootie/style/Style.dart';
import 'package:provider/provider.dart';

import 'widgets/MenuList.dart';

// final menuList = MenuList.menuList;

class Menu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    bool isLogined = Provider.of<ChangePage>(context).isLogined;

    return Scaffold(
      body: SafeArea(
        child: Container(
          child: Column(
            children: [
              Container(
                height: SizeConfig.screenHeight! * 0.1,
                margin: EdgeInsets.fromLTRB(1, 20, 1, 30),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      isLogined ? profile(context) : loginButton(context),
                      closeIcon(context)
                    ]),
              ),
              MenuList()
            ],
          ),
        ),
      ),
    );
  }


  //--------------------------- Profile ------------------------
  Widget profile(context) {
    return GestureDetector(
      onTap: () {
        Routes.openPage(context, "/profile", 0);
      },
      child: Container(
        margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
        child: Row(
          children: [
            ClipRRect(
              borderRadius: const BorderRadius.all(Radius.circular(60.0)),
              child: Image(
                image: AssetImage(Paths.avatar),
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    child: Text(
                      "Mahmoud Salim",
                      style: TextStyle(color: Color(0xff707070), fontSize: SizeConfig.screenWidth! * 0.042),
                    ),
                  ),
                  Text(
                    LocaleKeys.points.tr() + 21.toString(),
                    style: TextStyle(fontSize: SizeConfig.screenWidth! * 0.028, color: Color(0xff707070)),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  //--------------------------- Login button
  Widget loginButton(context) {
    return Expanded(
      child: TextButtonWidget(
        borderRadius: 25,
        margin: EdgeInsets.fromLTRB(20, 10, 20, 10),
        color: AppTheme.primary,
        textColor: Colors.white,
        onPressed: () {
          Routes.openPage(context, "/login", 0);
        },
        title: LocaleKeys.login.tr(),
        // width: ,
      ),
    );
  }

  //------------------------------ Close icon

  Widget closeIcon(context) {
    return Container(
      child: IconButton(
        onPressed: () {
          Routes.closePage(context, "menu");
        },
        icon: Icon(Icons.close),
      ),
    );
  }
}
