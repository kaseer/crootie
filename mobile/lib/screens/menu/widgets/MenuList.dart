import 'package:Crootie/style/SizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:Crootie/locale/locale_keys.g.dart';
import 'package:Crootie/shared/Routes.dart';
import 'package:Crootie/style/AppIcons.dart';
import 'package:easy_localization/easy_localization.dart';

class MenuList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: Container(
        child: ListView(
          children: [
            menuWidget(context, LocaleKeys.menu_home.tr(), '', AppIcons.home,20),
            menuWidget(context, LocaleKeys.menu_vouchers.tr(), '/vouchers',
                AppIcons.my_vouchers,14),
            menuWidget(context, LocaleKeys.menu_favorites.tr(), '/favorites',
                AppIcons.favorite,20),
            menuWidget(
                context, LocaleKeys.menu_gifts.tr(), '/gifts', AppIcons.gifts,20),
            menuWidget(context, LocaleKeys.menu_redeem.tr(), '/redeempoints',
                AppIcons.redeem_point,20),
            menuWidget(context, LocaleKeys.menu_invoices.tr(), '/invoices',
                AppIcons.invoices,20),
            menuWidget(context, LocaleKeys.menu_buisness.tr(), '/buisness',
                AppIcons.buisness,17),
            menuWidget(context, LocaleKeys.menu_hbaibe.tr(), '/hbaibe',
                AppIcons.hbaabe,20),
            menuWidget(context, LocaleKeys.menu_chargeme.tr(), '/chargeme',
                AppIcons.charge_me,20),
            menuWidget(context, LocaleKeys.menu_settings.tr(), '/settings',
                AppIcons.settings,20),
            menuWidget(context, LocaleKeys.menu_support.tr(), '/support',
                AppIcons.help___support,20),
            menuWidget(context, LocaleKeys.menu_faq.tr(), '/faq', AppIcons.faq,20),
            menuWidget(context, LocaleKeys.menu_about.tr(), '/aboutus',
                AppIcons.about_us,20),
          ],
        ),
      ),
    );
  }

  //********************** ********************//

  Widget menuWidget(BuildContext context, String name, String route, icon,double size) {
    return InkWell(
      onTap: () {
        Routes.openPageFromMenu(context, route);
      },
      child: Row(
        children: [
          SizedBox(
            width: 40,
          ),
          Container(
              margin: EdgeInsets.fromLTRB(20, 18, 20, 18),
              child: Icon(
                icon,
                size: size,
              )),
          Text(
            name,
            style: TextStyle(color: Color(0xff707070), fontSize: SizeConfig.screenWidth! * 0.037),
          )
        ],
      ),
    );
  }
}
