import 'package:Crootie/models/responseModels/AllProductsResponse.dart';
import 'package:Crootie/public_widgets/FutureWidget.dart';
import 'package:Crootie/screens/cart/widgets/List.dart';
import 'package:Crootie/services/api/apiServices/LookupService.dart';
import 'package:flutter/material.dart';

class CartList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureWidget(
        future: LookupService.getCartProducts(context),
        child: (List<AllProductsResponse> list) => ListWidget(
              list: list,
            ));
  }
}
