import 'package:Crootie/shared/Routes.dart';
import 'package:Crootie/style/Style.dart';
import 'package:flutter/material.dart';
class Buttons extends StatelessWidget {
  const Buttons({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
          padding: EdgeInsets.all(20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              button(
                  color: AppTheme.primary, icon: Icons.close, onPressed: () {}),
              button(
                  color: AppTheme.primary,
                  icon: Icons.delete,
                  onPressed: () {}),
              button(
                  color: AppTheme.success,
                  icon: Icons.check,
                  onPressed: () {
                    Routes.openPage(context, "/payments", 0);
                  }),
            ],
          ),
        );
  }
  Widget button({icon, onPressed, Color? color}) {
    return ClipOval(
      child: Material(
        color: color, // button color
        child: InkWell(
          splashColor: AppTheme.primary, // inkwell color
          child: SizedBox(
              width: 50, height: 50, child: Icon(icon, color: Colors.white)),
          onTap: onPressed,
        ),
      ),
    );
  }
}