import 'package:Crootie/models/responseModels/AllProductsResponse.dart';
import 'package:Crootie/providers/PublicProviders.dart';
import 'package:Crootie/public_widgets/CardView.dart';
import 'package:Crootie/public_widgets/CircleButton.dart';
import 'package:Crootie/public_widgets/GradiantColor.dart';
import 'package:Crootie/services/db/Cart.dart';
import 'package:Crootie/shared/Constant.dart';
import 'package:Crootie/shared/Routes.dart';
import 'package:Crootie/style/Style.dart';
import 'package:flutter/material.dart';

class ListWidget extends StatefulWidget {
  final List<AllProductsResponse> list;
  ListWidget({required this.list, Key? key}) : super(key: key);

  @override
  _ListWidgetState createState() => _ListWidgetState();
}

class _ListWidgetState extends State<ListWidget> {
  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: ListView.builder(
        itemCount: widget.list.length,
        itemBuilder: (context, index) {
          if (index + 1 == widget.list.length) {
            return Column(
              children: [
                container(context, index),
                SizedBox(height: 20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    CircleButton(
                      icon: Icons.close,
                      onPressed: () {
                        Routes.closePage(context, "/cart");
                      },
                    ),
                    CircleButton(
                      icon: Icons.delete,
                      onPressed: () {
                        widget.list.clear();
                        PublicProviders.emptyCart(context);
                        setState(() {});
                      },
                    ),
                    CircleButton(
                      icon: Icons.check,
                      onPressed: () {
                        Routes.openPage(context, "/payments", 0);
                      },
                      color: AppTheme.success,
                    ),
                  ],
                ),
                SizedBox(height: 80),
              ],
            );
          }
          return container(context, index);
        },
      ),
    );
  }

//**------------------------- Widgets ------------------------**/
  Widget container(BuildContext context, int index) {
    return Container(
      margin: EdgeInsets.fromLTRB(39, 20, 39, 0),
      child: CardView(
          showShadow: false,
          description: widget.list[index].priceDescription,
          logo: Paths.companies + widget.list[index].logo,
          isVoucher: false,
          showFav: false,
          price: widget.list[index].price,
          topColor: color(widget.list[index].topColor),
          bottomColor: color(widget.list[index].bottomColor),
          isCart: true,
          quantity: widget.list[index].quantity,
          addQty: () {
            widget.list[index].quantity++;
            setState(() {});
            CartDatabase.instance.addToCart(widget.list[index].productId);
            PublicProviders.addToCart(context);
          },
          removeQty: () {
            PublicProviders.removeFromCart(context);
            CartDatabase.instance.removeFromCart(widget.list[index].productId);
            if (widget.list[index].quantity <= 1) {
              widget.list[index].quantity--;
              widget.list.removeAt(index);
            } else
              widget.list[index].quantity--;
            setState(() {});
          }),
    );
  }
}
