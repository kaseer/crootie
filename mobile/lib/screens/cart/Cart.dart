import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:Crootie/locale/locale_keys.g.dart';

import 'package:Crootie/style/SizeConfig.dart';
import 'package:Crootie/public_widgets/topBar/TopBar.dart';
import 'package:Crootie/shared/Constant.dart';
import 'package:Crootie/style/Style.dart';

import 'mainWidgets/CartList.dart';

class Cart extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TopBar(
          title: LocaleKeys.cart.tr(),
          logo: Paths.cart + "logo.png",
          isTap: false,
          isCenter: true,
          backgroud: AppTheme.secondary,
          ratio: 0.3,
          titleWidth: SizeConfig.screenWidth,
          rigthWidthEdge: 18,
          rightHeightEdge: 0.9,
          isCart: true,
          imageWidth: SizeConfig.screenWidth!,
          imageHeight: 180,
        ),
        CartList(),
      ],
    );
  }
}
