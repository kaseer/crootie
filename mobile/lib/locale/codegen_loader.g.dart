// DO NOT EDIT. This is code generated via package:easy_localization/generate.dart

// ignore_for_file: prefer_single_quotes

import 'dart:ui';

import 'package:easy_localization/easy_localization.dart' show AssetLoader;

class CodegenLoader extends AssetLoader{
  const CodegenLoader();

  @override
  Future<Map<String, dynamic>> load(String fullPath, Locale locale ) {
    return Future.value(mapLocales[locale.toString()]);
  }

  static const Map<String,dynamic> ar = {
  "menu": {
    "home": "الرئيسية",
    "vouchers": "بطاقاتي",
    "favorites": "المفضلة",
    "gifts": "الهدايا",
    "redeem": "إستبدال النقاط",
    "invoices": "الفواتير",
    "buisness": "الاعمال",
    "hbaibe": "حبايبي",
    "chargeme": "إشحنلي",
    "settings": "الإعدادات",
    "support": "الدعم الفني",
    "faq": "اسئلة شائعة",
    "about": "حول"
  },
  "vouchers": {
    "tab1": "الجديدة",
    "tab2": "المستعملة",
    "scrap": "أكشط",
    "send": {
      "title": "إرسال بطاقة",
      "person": "شخص",
      "contacts": "قائمة الأسماء",
      "nickname": "اللقب",
      "invite": "إرسال و مشاركة",
      "notfound": "لم يتم العثور علي الرقم في التطبيق"
    }
  },
  "gifts": {
    "tab1": "الهدايا المعطاة",
    "tab2": "الهدايا المرسلة",
    "return": "إسترداد"
  },
  "payments": {
    "methods": "وسائل الدفع",
    "info": "معلومات الدفع",
    "checkout": "الدفع",
    "confirmattion": "يرجي التأكيد بإدخال رقم",
    "verification": "سداد السري !",
    "purchuss": "شراء",
    "vouchers": "بطاقاتي"
  },
  "redeem": {
    "points": "نقاطك",
    "button": "إستبدال"
  },
  "Invoices": {
    "selectdate": "إختر التاريخ",
    "number": "رقم الفاتورة",
    "date": "التاريخ "
  },
  "buisness": {
    "tab1": "الشركات",
    "tab2": "هيستوري"
  },
  "chargeme": {
    "selectcard": "إختر البطاقة",
    "selectcontact": "إختر الإسم",
    "charge": "إشحن"
  },
  "settings": {
    "language": "اللغة",
    "logout": "تسجيل الخروج"
  },
  "support": {
    "waiting": "إنتظار",
    "replied": "تم الرد",
    "subject": "العنوان : ",
    "problem": "المشكلة : ",
    "repliedby": "تم الرد بواسطة : ",
    "reply": "رد",
    "close": "إغلاق التيكيت"
  },
  "invoice": {
    "item": "المنتج",
    "qty": "الكمية",
    "price": "السعر"
  },
  "total": "الإجمالي ",
  "fees": "العمولة ",
  "currency": {
    "libyan": " د.ل",
    "points": " نقطة"
  },
  "phone": "رقم الهاتف",
  "password": "الرقم السري",
  "createaccount": "إنشاء حساب",
  "skip": "تخطي",
  "forget": "نسيت كلمة السر",
  "contact": "اسم الكونتاكت",
  "done": "تم",
  "login": "تسجيل الدخول",
  "signup": "تسجيل حساب",
  "profile": "الملف الشخصي",
  "search": "بحث",
  "buy": "شراء",
  "addToCart": "أضف الي السلة",
  "send": "إرسال",
  "collect": "تجميع",
  "cancel": "إلغاء",
  "confirm": "تأكيد",
  "add": "إضافة",
  "next": "التالي",
  "verify": "تأكيد",
  "cart": "السلة",
  "contacts": "الأسماء",
  "manualy": "إضافة",
  "fullname": "الإسم الثلاثي",
  "changepassword": "تغيير كلمة السر",
  "resetpassword": "إعادة تعيين",
  "changePIN": "تغيير PIN",
  "resetPIN": "إعادة تعيين PIN",
  "deactivate": "إغلاق حسابي",
  "oldPassword": "كلمة المرور القديمة",
  "newPassword": "كلمة المرور الجديدة",
  "confirmPassword": "تأكيد كلمة المرور الجديدة",
  "points": "النقاط :"
};
static const Map<String,dynamic> en = {
  "menu": {
    "home": "Home",
    "vouchers": "My vouchers",
    "favorites": "Favorites",
    "gifts": "Gifts",
    "redeem": "Redeem points",
    "invoices": "Invoices",
    "buisness": "Buisness",
    "hbaibe": "Hbaibe",
    "chargeme": "Charge me",
    "settings": "Settings",
    "support": "Help & Support",
    "faq": "FAQ",
    "about": "About us"
  },
  "vouchers": {
    "tab1": "New vouchers",
    "tab2": "Used vouchers",
    "scrap": "Scrap",
    "send": {
      "title": "Send card",
      "person": "Person",
      "contacts": "From Contacts",
      "nickname": "Nickname",
      "invite": "Send & Invite",
      "notfound": "This number not found in the app"
    }
  },
  "gifts": {
    "tab1": "Given gifts",
    "tab2": "My gifts",
    "return": "Return"
  },
  "payments": {
    "methods": "Payment Description",
    "info": "Payment Info",
    "checkout": "Checkout",
    "confirmattion": "Confirm by adding Sadad",
    "verification": "Verification code !",
    "purchuss": "Purchuss",
    "vouchers": "Go to Vouchers"
  },
  "redeem": {
    "points": "Your points is",
    "button": "Redeem"
  },
  "Invoices": {
    "selectdate": "Choose date",
    "number": "Invoice number",
    "date": "Date "
  },
  "buisness": {
    "tab1": "Companies",
    "tab2": "History"
  },
  "chargeme": {
    "selectcard": "Choose card",
    "selectcontact": "Choose contact",
    "charge": "Charge"
  },
  "settings": {
    "language": "Language",
    "logout": "Logout"
  },
  "support": {
    "waiting": "Waiting",
    "replied": "Replied",
    "subject": "Subject : ",
    "problem": "Problem : ",
    "repliedby": "Replied by : ",
    "reply": "Reply",
    "close": "Close ticket"
  },
  "invoice": {
    "item": "Item",
    "qty": "Quantity",
    "price": "Price"
  },
  "total": "Total ",
  "fees": "Fees ",
  "currency": {
    "libyan": " LYD",
    "points": " PT"
  },
  "phone": "Phone number",
  "password": "Password",
  "createaccount": "Create account",
  "skip": "skip",
  "forget": "Forget Password",
  "contact": "Contact name",
  "done": "Done",
  "login": "Login",
  "signup": "Sign up",
  "profile": "Profile",
  "search": "Search",
  "buy": "Buy",
  "addToCart": "Add to cart",
  "send": "Send",
  "collect": "Collect",
  "cancel": "Cancel",
  "confirm": "Confirm",
  "add": "Add",
  "next": "Next",
  "verify": "Verify",
  "cart": "Cart",
  "contacts": "Contacts",
  "manualy": "Manualy",
  "fullname": "Full name",
  "changepassword": "Change password",
  "resetpassword": "Reset password",
  "changePIN": "Change PIN number",
  "resetPIN": "Reset PIN number",
  "deactivate": "Deactivate my account",
  "oldPassword": "Old Password",
  "newPassword": "New Password",
  "confirmPassword": "Confirm New Password",
  "points": "Points :"
};
static const Map<String, Map<String,dynamic>> mapLocales = {"ar": ar, "en": en};
}
