import 'package:flutter/widgets.dart';

class SizeConfig {
  SizeConfig._();
  static MediaQueryData? _mediaQueryData;
  static double? screenWidth;
  static double? screenHeight;
  static double? blockSizeHorizontal;
  static double? blockSizeVertical;

  static double? _safeAreaHorizontal;
  static double? _safeAreaVertical;
  static double? safeBlockHorizontal;
  static double? safeBlockVertical;
  static const EdgeInsetsGeometry margin = EdgeInsets.fromLTRB(39, 10, 39, 20);
  static const EdgeInsetsGeometry listMargin = EdgeInsets.fromLTRB(40, 20, 40, 20);
  static EdgeInsetsGeometry marginLR = EdgeInsets.fromLTRB(39, 0, 39, 0);
  static EdgeInsetsGeometry marginTB = EdgeInsets.fromLTRB(0, 10, 0, 20);
  static EdgeInsetsGeometry marginBLR = EdgeInsets.fromLTRB(39, 0, 39, 20);
  static EdgeInsetsGeometry marginTLR = EdgeInsets.fromLTRB(39, 30, 39, 0);

  static void init(BuildContext context) {
    _mediaQueryData = MediaQuery.of(context);
    screenWidth = _mediaQueryData!.size.width;
    screenHeight = _mediaQueryData!.size.height;
    blockSizeHorizontal = screenWidth! / 100;
    blockSizeVertical = screenHeight! / 100;

    _safeAreaHorizontal =
        _mediaQueryData!.padding.left + _mediaQueryData!.padding.right;
    _safeAreaVertical =
        _mediaQueryData!.padding.top + _mediaQueryData!.padding.bottom;
    safeBlockHorizontal = (screenWidth! - _safeAreaHorizontal!) / 100;
    safeBlockVertical = (screenHeight! - _safeAreaVertical!) / 100;
  }
}
