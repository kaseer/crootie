import 'package:flutter/material.dart';

class AppTheme {
  AppTheme._();

  static const Color primary = Color(0xFF9845FF);
  static const Color secondary = Color(0xFFF8AF61);
  static const Color grey = Color(0xFF707070);
  static const Color success = Color(0xFF21AC17);
  static const Color error = Color(0xFFFC7474);
  static const String fontFamilyAr = 'Co';
  static const String fontFamilyEn = 'Futura LT';
  // static const String fontNameEnBold = 'Futura LT';
  static ThemeData buildLightTheme(String languageCode) {
    print(languageCode);
    final ColorScheme colorScheme = const ColorScheme.light().copyWith(
      primary: primary,
      secondary: secondary,
    );
    return ThemeData(

      bottomNavigationBarTheme: BottomNavigationBarThemeData(),
      cardColor: Color(0xff4D4D4D),
      colorScheme: colorScheme,
      primaryColor: primary,
      buttonColor: primary,
      indicatorColor: Colors.white,
      splashColor: Colors.white24,
      splashFactory: InkRipple.splashFactory,
      accentColor: secondary,
      canvasColor: Colors.white,
      backgroundColor: const Color(0xFFFFFFFF),
      scaffoldBackgroundColor: const Color(0xFFF6F6F6),
      errorColor: const Color(0xFFB00020),
      buttonTheme: ButtonThemeData(
        colorScheme: colorScheme,
        textTheme: ButtonTextTheme.primary,
      ),
      fontFamily: languageCode == 'ar' ? fontFamilyAr : fontFamilyEn,
      platform: TargetPlatform.iOS,
      textTheme: TextTheme(
        bodyText1: TextStyle(),
        bodyText2: TextStyle(fontSize:languageCode == 'ar' ? 20 : 24 ),
      ).apply(
        bodyColor: Colors.white,
        displayColor: Colors.black,
      ),
    );
  }
}
