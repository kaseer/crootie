import 'package:flutter/widgets.dart';

class AppIcons {
  AppIcons._();

  static const _kFontFam = 'AppIcons';
  static const String? _kFontPkg = null;

  static const IconData buisness = IconData(0xe800, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData charge_me = IconData(0xe801, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData faq = IconData(0xe802, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData menu = IconData(0xe803, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData favorite = IconData(0xe804, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData gifts = IconData(0xe805, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData notification = IconData(0xe806, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData search = IconData(0xe807, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData about_us = IconData(0xe808, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData hbaabe = IconData(0xe809, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData menu_white = IconData(0xe80a, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData help___support = IconData(0xe80b, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData my_vouchers = IconData(0xe80c, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData home = IconData(0xe80d, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData redeem_point = IconData(0xe80e, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData invoices = IconData(0xe80f, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData settings = IconData(0xe810, fontFamily: _kFontFam, fontPackage: _kFontPkg);
}
