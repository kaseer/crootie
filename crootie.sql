USE [master]
GO
/****** Object:  Database [CrootieDB]    Script Date: 7/16/2021 1:55:20 PM ******/
CREATE DATABASE [CrootieDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'CrootieDB', FILENAME = N'C:\Users\m.salim\CrootieDB.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'CrootieDB_log', FILENAME = N'C:\Users\m.salim\CrootieDB_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [CrootieDB] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [CrootieDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [CrootieDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [CrootieDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [CrootieDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [CrootieDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [CrootieDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [CrootieDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [CrootieDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [CrootieDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [CrootieDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [CrootieDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [CrootieDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [CrootieDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [CrootieDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [CrootieDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [CrootieDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [CrootieDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [CrootieDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [CrootieDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [CrootieDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [CrootieDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [CrootieDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [CrootieDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [CrootieDB] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [CrootieDB] SET  MULTI_USER 
GO
ALTER DATABASE [CrootieDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [CrootieDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [CrootieDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [CrootieDB] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [CrootieDB] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [CrootieDB] SET QUERY_STORE = OFF
GO
USE [CrootieDB]
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [CrootieDB]
GO
/****** Object:  Table [dbo].[AccountLogs]    Script Date: 7/16/2021 1:55:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountLogs](
	[ActionId] [smallint] NOT NULL,
	[CustomerId] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Actions]    Script Date: 7/16/2021 1:55:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Actions](
	[ActionId] [smallint] NOT NULL,
	[ActionName] [nvarchar](50) NULL,
	[Status] [tinyint] NOT NULL,
 CONSTRAINT [PK_Actions] PRIMARY KEY CLUSTERED 
(
	[ActionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BuisnessCompanies]    Script Date: 7/16/2021 1:55:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BuisnessCompanies](
	[BuisnessCompanyId] [int] IDENTITY(1,1) NOT NULL,
	[CompanyName] [nvarchar](50) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[FoundedOn] [datetime] NULL,
	[Status] [tinyint] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_BuisnessCompanies] PRIMARY KEY CLUSTERED 
(
	[BuisnessCompanyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BuisnessEmployees]    Script Date: 7/16/2021 1:55:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BuisnessEmployees](
	[BuisnessEmployeeId] [int] IDENTITY(1,1) NOT NULL,
	[PhoneNo] [int] NOT NULL,
	[CustomerId] [int] NULL,
	[Status] [tinyint] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_BuisnessEmployees] PRIMARY KEY CLUSTERED 
(
	[BuisnessEmployeeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_BuisnessEmployees] UNIQUE NONCLUSTERED 
(
	[PhoneNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BuisnessEmployeesDetails]    Script Date: 7/16/2021 1:55:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BuisnessEmployeesDetails](
	[BuisnessCompanyId] [int] NOT NULL,
	[BuisnessEmployeeId] [int] NOT NULL,
	[Status] [tinyint] NOT NULL,
 CONSTRAINT [PK_BuisnessEmployeesDetails] PRIMARY KEY CLUSTERED 
(
	[BuisnessCompanyId] ASC,
	[BuisnessEmployeeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BuisnessVouchers]    Script Date: 7/16/2021 1:55:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BuisnessVouchers](
	[BuisnessCompanyId] [int] NOT NULL,
	[BuisnessEmployeeId] [int] NOT NULL,
	[VoucherId] [int] NOT NULL,
	[CollectedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_BuisnessVouchers] PRIMARY KEY CLUSTERED 
(
	[BuisnessCompanyId] ASC,
	[BuisnessEmployeeId] ASC,
	[VoucherId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Categories]    Script Date: 7/16/2021 1:55:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Categories](
	[CategoryId] [smallint] NOT NULL,
	[CategoryNameAr] [nvarchar](20) NOT NULL,
	[CategoryNameEn] [varchar](20) NOT NULL,
	[TopColor] [nchar](6) NOT NULL,
	[BottomColor] [nchar](6) NOT NULL,
	[Illistration] [nvarchar](50) NOT NULL,
	[Status] [tinyint] NOT NULL,
 CONSTRAINT [PK_Categories] PRIMARY KEY CLUSTERED 
(
	[CategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Companies]    Script Date: 7/16/2021 1:55:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Companies](
	[CompanyId] [int] IDENTITY(1,1) NOT NULL,
	[CompanyNameAr] [varchar](20) NOT NULL,
	[CompanyNameEn] [varchar](20) NOT NULL,
	[CategoryId] [smallint] NOT NULL,
	[TopColor] [nchar](6) NOT NULL,
	[BottomColor] [nchar](6) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_Companies] PRIMARY KEY CLUSTERED 
(
	[CompanyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Customers]    Script Date: 7/16/2021 1:55:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customers](
	[CustomerId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerName] [nvarchar](50) NOT NULL,
	[NickName] [nvarchar](15) NULL,
	[PhoneNo] [varchar](10) NOT NULL,
	[PINcode] [nchar](4) NOT NULL,
	[Password] [varchar](100) NOT NULL,
	[VerificationCode] [nchar](4) NULL,
	[CodeGenerationTime] [datetime] NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[DeactivateOn] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_Customers] PRIMARY KEY CLUSTERED 
(
	[CustomerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CustomerVouchers]    Script Date: 7/16/2021 1:55:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomerVouchers](
	[CustomerVoucherId] [int] IDENTITY(1,1) NOT NULL,
	[VoucherId] [int] NOT NULL,
	[InvoiceId] [int] NULL,
	[CustomerId] [int] NOT NULL,
 CONSTRAINT [PK_CustomerVouchers] PRIMARY KEY CLUSTERED 
(
	[CustomerVoucherId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_CustomerVouchers] UNIQUE NONCLUSTERED 
(
	[VoucherId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Faq]    Script Date: 7/16/2021 1:55:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Faq](
	[FaqId] [int] IDENTITY(1,1) NOT NULL,
	[Question] [nvarchar](50) NOT NULL,
	[Answer] [nvarchar](max) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_Faq] PRIMARY KEY CLUSTERED 
(
	[FaqId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Favorites]    Script Date: 7/16/2021 1:55:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Favorites](
	[FavoriteId] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NOT NULL,
	[CustomerId] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_Favorites] PRIMARY KEY CLUSTERED 
(
	[FavoriteId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Features]    Script Date: 7/16/2021 1:55:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Features](
	[FeatureId] [int] NOT NULL,
	[FeatureName] [nvarchar](50) NOT NULL,
	[Status] [tinyint] NOT NULL,
 CONSTRAINT [PK_Features] PRIMARY KEY CLUSTERED 
(
	[FeatureId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Gifts]    Script Date: 7/16/2021 1:55:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Gifts](
	[GiftId] [int] NOT NULL,
	[VoucherId] [int] NOT NULL,
	[SentBy] [int] NOT NULL,
	[SentTo] [int] NOT NULL,
	[SentOn] [datetime] NOT NULL,
	[CollectedOn] [datetime] NULL,
	[HabaibeId] [int] NULL,
	[PhoneNo] [nchar](10) NULL,
	[NickName] [nvarchar](50) NULL,
 CONSTRAINT [PK_Gifts] PRIMARY KEY CLUSTERED 
(
	[GiftId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Habaibe]    Script Date: 7/16/2021 1:55:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Habaibe](
	[HabaibeId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[HabaibeCustomerId] [int] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_Habaibe] PRIMARY KEY CLUSTERED 
(
	[HabaibeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Headlines]    Script Date: 7/16/2021 1:55:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Headlines](
	[FirstIntro] [nvarchar](max) NOT NULL,
	[SecondIntro] [nvarchar](max) NOT NULL,
	[AboutUs] [nvarchar](max) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[InvoiceDetails]    Script Date: 7/16/2021 1:55:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InvoiceDetails](
	[InvoiceDetailsId] [int] IDENTITY(1,1) NOT NULL,
	[InvoiceId] [int] NOT NULL,
	[ProductId] [int] NOT NULL,
	[Quantity] [smallint] NOT NULL,
	[Price] [decimal](5, 0) NOT NULL,
	[Discount] [tinyint] NOT NULL,
 CONSTRAINT [PK_InvoiceDetails] PRIMARY KEY CLUSTERED 
(
	[InvoiceDetailsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Invoices]    Script Date: 7/16/2021 1:55:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Invoices](
	[InvoiceId] [int] IDENTITY(1,1) NOT NULL,
	[InvoiceNo] [varchar](20) NOT NULL,
	[Total] [decimal](18, 0) NOT NULL,
	[PaymentId] [int] NOT NULL,
	[Fees] [decimal](5, 0) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_Invoices] PRIMARY KEY CLUSTERED 
(
	[InvoiceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Offers]    Script Date: 7/16/2021 1:55:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Offers](
	[OfferId] [int] IDENTITY(1,1) NOT NULL,
	[Discount] [tinyint] NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_Offers] PRIMARY KEY CLUSTERED 
(
	[OfferId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OwnedVouchers]    Script Date: 7/16/2021 1:55:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OwnedVouchers](
	[CustomerId] [int] NOT NULL,
	[VoucherId] [int] NOT NULL,
	[UsedOn] [datetime] NULL,
 CONSTRAINT [PK_OwnedVouchers] PRIMARY KEY CLUSTERED 
(
	[CustomerId] ASC,
	[VoucherId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Payments]    Script Date: 7/16/2021 1:55:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Payments](
	[PaymentId] [int] IDENTITY(1,1) NOT NULL,
	[PaymentName] [varchar](20) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_Payments] PRIMARY KEY CLUSTERED 
(
	[PaymentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PermissionRoleDetails]    Script Date: 7/16/2021 1:55:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PermissionRoleDetails](
	[PermissionRoleId] [tinyint] NOT NULL,
	[PermissionId] [int] NOT NULL,
 CONSTRAINT [PK_PermissionRoleDetails] PRIMARY KEY CLUSTERED 
(
	[PermissionRoleId] ASC,
	[PermissionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PermissionRoles]    Script Date: 7/16/2021 1:55:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PermissionRoles](
	[PermissionRoleId] [tinyint] IDENTITY(1,1) NOT NULL,
	[PermissionRoleName] [varchar](20) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_PermissionRoles] PRIMARY KEY CLUSTERED 
(
	[PermissionRoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Permissions]    Script Date: 7/16/2021 1:55:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Permissions](
	[PermissionId] [int] NOT NULL,
	[FeatureId] [int] NOT NULL,
	[PermissionName] [nvarchar](50) NOT NULL,
	[Code] [char](5) NOT NULL,
	[Status] [tinyint] NOT NULL,
 CONSTRAINT [PK_Permissions] PRIMARY KEY CLUSTERED 
(
	[PermissionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PointsOffers]    Script Date: 7/16/2021 1:55:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PointsOffers](
	[PointsOfferId] [int] IDENTITY(1,1) NOT NULL,
	[OfferId] [int] NOT NULL,
	[ProductId] [int] NOT NULL,
	[PointsPrice] [smallint] NOT NULL,
	[Quantity] [smallint] NULL,
	[CurrentQuantity] [smallint] NULL,
 CONSTRAINT [PK_PointsOffers] PRIMARY KEY CLUSTERED 
(
	[PointsOfferId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductOffers]    Script Date: 7/16/2021 1:55:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductOffers](
	[ProductOfferId] [int] IDENTITY(1,1) NOT NULL,
	[OfferId] [int] NOT NULL,
	[ProductId] [int] NOT NULL,
 CONSTRAINT [PK_ProductOffers] PRIMARY KEY CLUSTERED 
(
	[ProductOfferId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Products]    Script Date: 7/16/2021 1:55:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Products](
	[ProductId] [int] IDENTITY(1,1) NOT NULL,
	[CompanyId] [int] NOT NULL,
	[PriceDescription] [nvarchar](10) NOT NULL,
	[Description] [nvarchar](30) NULL,
	[Cost] [decimal](5, 0) NOT NULL,
	[Price] [decimal](5, 0) NOT NULL,
	[Fees] [tinyint] NOT NULL,
	[EarnedPoints] [smallint] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_Vouchers] PRIMARY KEY CLUSTERED 
(
	[ProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RedeemPoints]    Script Date: 7/16/2021 1:55:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RedeemPoints](
	[RedeemPointsId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[PointsOfferId] [int] NOT NULL,
	[PointsAmount] [smallint] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_RedeemPoints] PRIMARY KEY CLUSTERED 
(
	[RedeemPointsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TicketDetails]    Script Date: 7/16/2021 1:55:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TicketDetails](
	[TicketDetailsId] [int] IDENTITY(1,1) NOT NULL,
	[TicketId] [int] NOT NULL,
	[Message] [nvarchar](250) NOT NULL,
	[SentOn] [datetime] NOT NULL,
	[ReplyMessage] [nvarchar](max) NULL,
	[RepliedBy] [int] NULL,
	[RepliedOn] [datetime] NULL,
	[Attachment] [varchar](25) NULL,
 CONSTRAINT [PK_TicketDetails] PRIMARY KEY CLUSTERED 
(
	[TicketDetailsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tickets]    Script Date: 7/16/2021 1:55:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tickets](
	[TicketId] [int] IDENTITY(1,1) NOT NULL,
	[TicketNo] [varchar](20) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[Subject] [nvarchar](50) NULL,
	[Rating] [decimal](5, 0) NULL,
	[Status] [tinyint] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_Tickets] PRIMARY KEY CLUSTERED 
(
	[TicketId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 7/16/2021 1:55:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[LoginName] [varchar](20) NOT NULL,
	[FullName] [nvarchar](50) NOT NULL,
	[PhoneNo] [nchar](10) NOT NULL,
	[Email] [varchar](50) NULL,
	[Password] [varchar](100) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
	[PermissionRoleId] [tinyint] NOT NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[VoucherActions]    Script Date: 7/16/2021 1:55:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VoucherActions](
	[VoucherId] [int] NOT NULL,
	[ActionId] [smallint] NOT NULL,
	[CustomerId] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Vouchers]    Script Date: 7/16/2021 1:55:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Vouchers](
	[VoucherId] [int] IDENTITY(1,1) NOT NULL,
	[SerialNo] [bigint] NOT NULL,
	[ProductId] [int] NOT NULL,
	[VoucherNo] [nvarchar](100) NOT NULL,
	[ExpiredOn] [tinyint] NOT NULL,
	[Status] [tinyint] NOT NULL,
 CONSTRAINT [PK_VouchersWarehouse_1] PRIMARY KEY CLUSTERED 
(
	[VoucherId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_VouchersWarehouse] UNIQUE NONCLUSTERED 
(
	[VoucherNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_VouchersWarehouse_1] UNIQUE NONCLUSTERED 
(
	[SerialNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Actions] ADD  CONSTRAINT [DF_Actions_Status]  DEFAULT ((1)) FOR [Status]
GO
ALTER TABLE [dbo].[BuisnessCompanies] ADD  CONSTRAINT [DF_BuisnessCompanies_Status]  DEFAULT ((1)) FOR [Status]
GO
ALTER TABLE [dbo].[BuisnessEmployees] ADD  CONSTRAINT [DF_BuisnessEmployees_Status]  DEFAULT ((1)) FOR [Status]
GO
ALTER TABLE [dbo].[BuisnessEmployeesDetails] ADD  CONSTRAINT [DF_BuisnessEmployeesDetails_Status]  DEFAULT ((1)) FOR [Status]
GO
ALTER TABLE [dbo].[Categories] ADD  CONSTRAINT [DF_Categories_Status]  DEFAULT ((1)) FOR [Status]
GO
ALTER TABLE [dbo].[Companies] ADD  CONSTRAINT [DF_Companies_Status]  DEFAULT ((1)) FOR [Status]
GO
ALTER TABLE [dbo].[Customers] ADD  CONSTRAINT [DF_Customers_EmailConfirmed]  DEFAULT ((0)) FOR [EmailConfirmed]
GO
ALTER TABLE [dbo].[Faq] ADD  CONSTRAINT [DF_Faq_Status]  DEFAULT ((1)) FOR [Status]
GO
ALTER TABLE [dbo].[Features] ADD  CONSTRAINT [DF_Features_Status]  DEFAULT ((1)) FOR [Status]
GO
ALTER TABLE [dbo].[Habaibe] ADD  CONSTRAINT [DF_Habaibe_Status]  DEFAULT ((1)) FOR [Status]
GO
ALTER TABLE [dbo].[InvoiceDetails] ADD  CONSTRAINT [DF_InvoiceDetails_Discount]  DEFAULT ((0)) FOR [Discount]
GO
ALTER TABLE [dbo].[Invoices] ADD  CONSTRAINT [DF_Invoices_Fees]  DEFAULT ((0)) FOR [Fees]
GO
ALTER TABLE [dbo].[Payments] ADD  CONSTRAINT [DF_Payments_Status]  DEFAULT ((1)) FOR [Status]
GO
ALTER TABLE [dbo].[PermissionRoles] ADD  CONSTRAINT [DF_PermissionRoles_Status]  DEFAULT ((1)) FOR [Status]
GO
ALTER TABLE [dbo].[Permissions] ADD  CONSTRAINT [DF_Permissions_Status]  DEFAULT ((1)) FOR [Status]
GO
ALTER TABLE [dbo].[Products] ADD  CONSTRAINT [DF_Vouchers_Cost]  DEFAULT ((0)) FOR [Cost]
GO
ALTER TABLE [dbo].[Products] ADD  CONSTRAINT [DF_Vouchers_Fees]  DEFAULT ((0)) FOR [Fees]
GO
ALTER TABLE [dbo].[Products] ADD  CONSTRAINT [DF_Vouchers_EarnedPoints]  DEFAULT ((0)) FOR [EarnedPoints]
GO
ALTER TABLE [dbo].[Products] ADD  CONSTRAINT [DF_Vouchers_Status]  DEFAULT ((1)) FOR [Status]
GO
ALTER TABLE [dbo].[Tickets] ADD  CONSTRAINT [DF_Tickets_Status]  DEFAULT ((1)) FOR [Status]
GO
ALTER TABLE [dbo].[Users] ADD  CONSTRAINT [DF_Users_Status]  DEFAULT ((1)) FOR [Status]
GO
ALTER TABLE [dbo].[Vouchers] ADD  CONSTRAINT [DF_VouchersWarehouse_Status1]  DEFAULT ((1)) FOR [ExpiredOn]
GO
ALTER TABLE [dbo].[Vouchers] ADD  CONSTRAINT [DF_VouchersWarehouse_Status]  DEFAULT ((1)) FOR [Status]
GO
ALTER TABLE [dbo].[AccountLogs]  WITH CHECK ADD  CONSTRAINT [FK_AccountLogs_Actions] FOREIGN KEY([ActionId])
REFERENCES [dbo].[Actions] ([ActionId])
GO
ALTER TABLE [dbo].[AccountLogs] CHECK CONSTRAINT [FK_AccountLogs_Actions]
GO
ALTER TABLE [dbo].[AccountLogs]  WITH CHECK ADD  CONSTRAINT [FK_AccountLogs_Customers] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customers] ([CustomerId])
GO
ALTER TABLE [dbo].[AccountLogs] CHECK CONSTRAINT [FK_AccountLogs_Customers]
GO
ALTER TABLE [dbo].[BuisnessCompanies]  WITH CHECK ADD  CONSTRAINT [FK_BuisnessCompanies_Customers] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customers] ([CustomerId])
GO
ALTER TABLE [dbo].[BuisnessCompanies] CHECK CONSTRAINT [FK_BuisnessCompanies_Customers]
GO
ALTER TABLE [dbo].[BuisnessCompanies]  WITH CHECK ADD  CONSTRAINT [FK_BuisnessCompanies_Users] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[BuisnessCompanies] CHECK CONSTRAINT [FK_BuisnessCompanies_Users]
GO
ALTER TABLE [dbo].[BuisnessCompanies]  WITH CHECK ADD  CONSTRAINT [FK_BuisnessCompanies_Users1] FOREIGN KEY([ModifiedBy])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[BuisnessCompanies] CHECK CONSTRAINT [FK_BuisnessCompanies_Users1]
GO
ALTER TABLE [dbo].[BuisnessEmployees]  WITH CHECK ADD  CONSTRAINT [FK_BuisnessEmployees_Customers] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customers] ([CustomerId])
GO
ALTER TABLE [dbo].[BuisnessEmployees] CHECK CONSTRAINT [FK_BuisnessEmployees_Customers]
GO
ALTER TABLE [dbo].[BuisnessEmployees]  WITH CHECK ADD  CONSTRAINT [FK_BuisnessEmployees_Users] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[BuisnessEmployees] CHECK CONSTRAINT [FK_BuisnessEmployees_Users]
GO
ALTER TABLE [dbo].[BuisnessEmployees]  WITH CHECK ADD  CONSTRAINT [FK_BuisnessEmployees_Users1] FOREIGN KEY([ModifiedBy])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[BuisnessEmployees] CHECK CONSTRAINT [FK_BuisnessEmployees_Users1]
GO
ALTER TABLE [dbo].[BuisnessEmployeesDetails]  WITH CHECK ADD  CONSTRAINT [FK_BuisnessEmployeesDetails_BuisnessCompanies] FOREIGN KEY([BuisnessCompanyId])
REFERENCES [dbo].[BuisnessCompanies] ([BuisnessCompanyId])
GO
ALTER TABLE [dbo].[BuisnessEmployeesDetails] CHECK CONSTRAINT [FK_BuisnessEmployeesDetails_BuisnessCompanies]
GO
ALTER TABLE [dbo].[BuisnessEmployeesDetails]  WITH CHECK ADD  CONSTRAINT [FK_BuisnessEmployeesDetails_BuisnessEmployees] FOREIGN KEY([BuisnessEmployeeId])
REFERENCES [dbo].[BuisnessEmployees] ([BuisnessEmployeeId])
GO
ALTER TABLE [dbo].[BuisnessEmployeesDetails] CHECK CONSTRAINT [FK_BuisnessEmployeesDetails_BuisnessEmployees]
GO
ALTER TABLE [dbo].[BuisnessVouchers]  WITH CHECK ADD  CONSTRAINT [FK_BuisnessVouchers_BuisnessCompanies] FOREIGN KEY([BuisnessCompanyId])
REFERENCES [dbo].[BuisnessCompanies] ([BuisnessCompanyId])
GO
ALTER TABLE [dbo].[BuisnessVouchers] CHECK CONSTRAINT [FK_BuisnessVouchers_BuisnessCompanies]
GO
ALTER TABLE [dbo].[BuisnessVouchers]  WITH CHECK ADD  CONSTRAINT [FK_BuisnessVouchers_BuisnessEmployees] FOREIGN KEY([BuisnessEmployeeId])
REFERENCES [dbo].[BuisnessEmployees] ([BuisnessEmployeeId])
GO
ALTER TABLE [dbo].[BuisnessVouchers] CHECK CONSTRAINT [FK_BuisnessVouchers_BuisnessEmployees]
GO
ALTER TABLE [dbo].[BuisnessVouchers]  WITH CHECK ADD  CONSTRAINT [FK_BuisnessVouchers_Vouchers] FOREIGN KEY([VoucherId])
REFERENCES [dbo].[Vouchers] ([VoucherId])
GO
ALTER TABLE [dbo].[BuisnessVouchers] CHECK CONSTRAINT [FK_BuisnessVouchers_Vouchers]
GO
ALTER TABLE [dbo].[Companies]  WITH CHECK ADD  CONSTRAINT [FK_Companies_Categories] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[Categories] ([CategoryId])
GO
ALTER TABLE [dbo].[Companies] CHECK CONSTRAINT [FK_Companies_Categories]
GO
ALTER TABLE [dbo].[Companies]  WITH CHECK ADD  CONSTRAINT [FK_Companies_Users] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[Companies] CHECK CONSTRAINT [FK_Companies_Users]
GO
ALTER TABLE [dbo].[Companies]  WITH CHECK ADD  CONSTRAINT [FK_Companies_Users1] FOREIGN KEY([ModifiedBy])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[Companies] CHECK CONSTRAINT [FK_Companies_Users1]
GO
ALTER TABLE [dbo].[Customers]  WITH CHECK ADD  CONSTRAINT [FK_Customers_Users] FOREIGN KEY([ModifiedBy])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[Customers] CHECK CONSTRAINT [FK_Customers_Users]
GO
ALTER TABLE [dbo].[CustomerVouchers]  WITH CHECK ADD  CONSTRAINT [FK_CustomerVouchers_Customers] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customers] ([CustomerId])
GO
ALTER TABLE [dbo].[CustomerVouchers] CHECK CONSTRAINT [FK_CustomerVouchers_Customers]
GO
ALTER TABLE [dbo].[CustomerVouchers]  WITH CHECK ADD  CONSTRAINT [FK_CustomerVouchers_Invoices] FOREIGN KEY([InvoiceId])
REFERENCES [dbo].[Invoices] ([InvoiceId])
GO
ALTER TABLE [dbo].[CustomerVouchers] CHECK CONSTRAINT [FK_CustomerVouchers_Invoices]
GO
ALTER TABLE [dbo].[CustomerVouchers]  WITH CHECK ADD  CONSTRAINT [FK_CustomerVouchers_VouchersWarehouse] FOREIGN KEY([VoucherId])
REFERENCES [dbo].[Vouchers] ([VoucherId])
GO
ALTER TABLE [dbo].[CustomerVouchers] CHECK CONSTRAINT [FK_CustomerVouchers_VouchersWarehouse]
GO
ALTER TABLE [dbo].[Favorites]  WITH CHECK ADD  CONSTRAINT [FK_Favorites_Customers] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customers] ([CustomerId])
GO
ALTER TABLE [dbo].[Favorites] CHECK CONSTRAINT [FK_Favorites_Customers]
GO
ALTER TABLE [dbo].[Favorites]  WITH CHECK ADD  CONSTRAINT [FK_Favorites_Vouchers] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Products] ([ProductId])
GO
ALTER TABLE [dbo].[Favorites] CHECK CONSTRAINT [FK_Favorites_Vouchers]
GO
ALTER TABLE [dbo].[Gifts]  WITH CHECK ADD  CONSTRAINT [FK_Gifts_Customers] FOREIGN KEY([SentBy])
REFERENCES [dbo].[Customers] ([CustomerId])
GO
ALTER TABLE [dbo].[Gifts] CHECK CONSTRAINT [FK_Gifts_Customers]
GO
ALTER TABLE [dbo].[Gifts]  WITH CHECK ADD  CONSTRAINT [FK_Gifts_Customers1] FOREIGN KEY([SentTo])
REFERENCES [dbo].[Customers] ([CustomerId])
GO
ALTER TABLE [dbo].[Gifts] CHECK CONSTRAINT [FK_Gifts_Customers1]
GO
ALTER TABLE [dbo].[Gifts]  WITH CHECK ADD  CONSTRAINT [FK_Gifts_Habaibe] FOREIGN KEY([HabaibeId])
REFERENCES [dbo].[Habaibe] ([HabaibeId])
GO
ALTER TABLE [dbo].[Gifts] CHECK CONSTRAINT [FK_Gifts_Habaibe]
GO
ALTER TABLE [dbo].[Gifts]  WITH CHECK ADD  CONSTRAINT [FK_Gifts_VouchersWarehouse] FOREIGN KEY([VoucherId])
REFERENCES [dbo].[Vouchers] ([VoucherId])
GO
ALTER TABLE [dbo].[Gifts] CHECK CONSTRAINT [FK_Gifts_VouchersWarehouse]
GO
ALTER TABLE [dbo].[Habaibe]  WITH CHECK ADD  CONSTRAINT [FK_Habaibe_Customers] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customers] ([CustomerId])
GO
ALTER TABLE [dbo].[Habaibe] CHECK CONSTRAINT [FK_Habaibe_Customers]
GO
ALTER TABLE [dbo].[Habaibe]  WITH CHECK ADD  CONSTRAINT [FK_Habaibe_Customers1] FOREIGN KEY([HabaibeCustomerId])
REFERENCES [dbo].[Customers] ([CustomerId])
GO
ALTER TABLE [dbo].[Habaibe] CHECK CONSTRAINT [FK_Habaibe_Customers1]
GO
ALTER TABLE [dbo].[InvoiceDetails]  WITH CHECK ADD  CONSTRAINT [FK_InvoiceDetails_Invoices] FOREIGN KEY([InvoiceId])
REFERENCES [dbo].[Invoices] ([InvoiceId])
GO
ALTER TABLE [dbo].[InvoiceDetails] CHECK CONSTRAINT [FK_InvoiceDetails_Invoices]
GO
ALTER TABLE [dbo].[InvoiceDetails]  WITH CHECK ADD  CONSTRAINT [FK_InvoiceDetails_Vouchers] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Products] ([ProductId])
GO
ALTER TABLE [dbo].[InvoiceDetails] CHECK CONSTRAINT [FK_InvoiceDetails_Vouchers]
GO
ALTER TABLE [dbo].[Invoices]  WITH CHECK ADD  CONSTRAINT [FK_Invoices_Payments] FOREIGN KEY([PaymentId])
REFERENCES [dbo].[Payments] ([PaymentId])
GO
ALTER TABLE [dbo].[Invoices] CHECK CONSTRAINT [FK_Invoices_Payments]
GO
ALTER TABLE [dbo].[Offers]  WITH CHECK ADD  CONSTRAINT [FK_Offers_Users] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[Offers] CHECK CONSTRAINT [FK_Offers_Users]
GO
ALTER TABLE [dbo].[Offers]  WITH CHECK ADD  CONSTRAINT [FK_Offers_Users1] FOREIGN KEY([ModifiedBy])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[Offers] CHECK CONSTRAINT [FK_Offers_Users1]
GO
ALTER TABLE [dbo].[OwnedVouchers]  WITH CHECK ADD  CONSTRAINT [FK_OwnedVouchers_Customers] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customers] ([CustomerId])
GO
ALTER TABLE [dbo].[OwnedVouchers] CHECK CONSTRAINT [FK_OwnedVouchers_Customers]
GO
ALTER TABLE [dbo].[OwnedVouchers]  WITH CHECK ADD  CONSTRAINT [FK_OwnedVouchers_Vouchers] FOREIGN KEY([VoucherId])
REFERENCES [dbo].[Vouchers] ([VoucherId])
GO
ALTER TABLE [dbo].[OwnedVouchers] CHECK CONSTRAINT [FK_OwnedVouchers_Vouchers]
GO
ALTER TABLE [dbo].[Payments]  WITH CHECK ADD  CONSTRAINT [FK_Payments_Users] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[Payments] CHECK CONSTRAINT [FK_Payments_Users]
GO
ALTER TABLE [dbo].[Payments]  WITH CHECK ADD  CONSTRAINT [FK_Payments_Users1] FOREIGN KEY([ModifiedBy])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[Payments] CHECK CONSTRAINT [FK_Payments_Users1]
GO
ALTER TABLE [dbo].[PermissionRoleDetails]  WITH CHECK ADD  CONSTRAINT [FK_PermissionRoleDetails_PermissionRoles] FOREIGN KEY([PermissionRoleId])
REFERENCES [dbo].[PermissionRoles] ([PermissionRoleId])
GO
ALTER TABLE [dbo].[PermissionRoleDetails] CHECK CONSTRAINT [FK_PermissionRoleDetails_PermissionRoles]
GO
ALTER TABLE [dbo].[PermissionRoleDetails]  WITH CHECK ADD  CONSTRAINT [FK_PermissionRoleDetails_Permissions] FOREIGN KEY([PermissionId])
REFERENCES [dbo].[Permissions] ([PermissionId])
GO
ALTER TABLE [dbo].[PermissionRoleDetails] CHECK CONSTRAINT [FK_PermissionRoleDetails_Permissions]
GO
ALTER TABLE [dbo].[PermissionRoles]  WITH CHECK ADD  CONSTRAINT [FK_PermissionRoles_Users] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[PermissionRoles] CHECK CONSTRAINT [FK_PermissionRoles_Users]
GO
ALTER TABLE [dbo].[PermissionRoles]  WITH CHECK ADD  CONSTRAINT [FK_PermissionRoles_Users1] FOREIGN KEY([ModifiedBy])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[PermissionRoles] CHECK CONSTRAINT [FK_PermissionRoles_Users1]
GO
ALTER TABLE [dbo].[Permissions]  WITH CHECK ADD  CONSTRAINT [FK_Permissions_Features] FOREIGN KEY([FeatureId])
REFERENCES [dbo].[Features] ([FeatureId])
GO
ALTER TABLE [dbo].[Permissions] CHECK CONSTRAINT [FK_Permissions_Features]
GO
ALTER TABLE [dbo].[PointsOffers]  WITH CHECK ADD  CONSTRAINT [FK_PointsOffers_Offers] FOREIGN KEY([OfferId])
REFERENCES [dbo].[Offers] ([OfferId])
GO
ALTER TABLE [dbo].[PointsOffers] CHECK CONSTRAINT [FK_PointsOffers_Offers]
GO
ALTER TABLE [dbo].[PointsOffers]  WITH CHECK ADD  CONSTRAINT [FK_PointsOffers_Products] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Products] ([ProductId])
GO
ALTER TABLE [dbo].[PointsOffers] CHECK CONSTRAINT [FK_PointsOffers_Products]
GO
ALTER TABLE [dbo].[ProductOffers]  WITH CHECK ADD  CONSTRAINT [FK_ProductOffers_Offers] FOREIGN KEY([OfferId])
REFERENCES [dbo].[Offers] ([OfferId])
GO
ALTER TABLE [dbo].[ProductOffers] CHECK CONSTRAINT [FK_ProductOffers_Offers]
GO
ALTER TABLE [dbo].[ProductOffers]  WITH CHECK ADD  CONSTRAINT [FK_ProductOffers_Products] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Products] ([ProductId])
GO
ALTER TABLE [dbo].[ProductOffers] CHECK CONSTRAINT [FK_ProductOffers_Products]
GO
ALTER TABLE [dbo].[Products]  WITH CHECK ADD  CONSTRAINT [FK_Products_Users] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[Products] CHECK CONSTRAINT [FK_Products_Users]
GO
ALTER TABLE [dbo].[Products]  WITH CHECK ADD  CONSTRAINT [FK_Products_Users1] FOREIGN KEY([ModifiedBy])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[Products] CHECK CONSTRAINT [FK_Products_Users1]
GO
ALTER TABLE [dbo].[Products]  WITH CHECK ADD  CONSTRAINT [FK_Vouchers_Companies] FOREIGN KEY([CompanyId])
REFERENCES [dbo].[Companies] ([CompanyId])
GO
ALTER TABLE [dbo].[Products] CHECK CONSTRAINT [FK_Vouchers_Companies]
GO
ALTER TABLE [dbo].[RedeemPoints]  WITH CHECK ADD  CONSTRAINT [FK_RedeemPoints_Customers] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customers] ([CustomerId])
GO
ALTER TABLE [dbo].[RedeemPoints] CHECK CONSTRAINT [FK_RedeemPoints_Customers]
GO
ALTER TABLE [dbo].[RedeemPoints]  WITH CHECK ADD  CONSTRAINT [FK_RedeemPoints_PointsOffers] FOREIGN KEY([PointsOfferId])
REFERENCES [dbo].[PointsOffers] ([PointsOfferId])
GO
ALTER TABLE [dbo].[RedeemPoints] CHECK CONSTRAINT [FK_RedeemPoints_PointsOffers]
GO
ALTER TABLE [dbo].[TicketDetails]  WITH CHECK ADD  CONSTRAINT [FK_TicketDetails_Tickets] FOREIGN KEY([TicketId])
REFERENCES [dbo].[Tickets] ([TicketId])
GO
ALTER TABLE [dbo].[TicketDetails] CHECK CONSTRAINT [FK_TicketDetails_Tickets]
GO
ALTER TABLE [dbo].[TicketDetails]  WITH CHECK ADD  CONSTRAINT [FK_TicketDetails_Users] FOREIGN KEY([RepliedBy])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[TicketDetails] CHECK CONSTRAINT [FK_TicketDetails_Users]
GO
ALTER TABLE [dbo].[Tickets]  WITH CHECK ADD  CONSTRAINT [FK_Tickets_Customers] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customers] ([CustomerId])
GO
ALTER TABLE [dbo].[Tickets] CHECK CONSTRAINT [FK_Tickets_Customers]
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK_Users_PermissionRoles] FOREIGN KEY([PermissionRoleId])
REFERENCES [dbo].[PermissionRoles] ([PermissionRoleId])
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK_Users_PermissionRoles]
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK_Users_Users] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK_Users_Users]
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK_Users_Users1] FOREIGN KEY([ModifiedBy])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK_Users_Users1]
GO
ALTER TABLE [dbo].[VoucherActions]  WITH CHECK ADD  CONSTRAINT [FK_VoucherActions_Actions] FOREIGN KEY([ActionId])
REFERENCES [dbo].[Actions] ([ActionId])
GO
ALTER TABLE [dbo].[VoucherActions] CHECK CONSTRAINT [FK_VoucherActions_Actions]
GO
ALTER TABLE [dbo].[VoucherActions]  WITH CHECK ADD  CONSTRAINT [FK_VoucherActions_Customers] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customers] ([CustomerId])
GO
ALTER TABLE [dbo].[VoucherActions] CHECK CONSTRAINT [FK_VoucherActions_Customers]
GO
ALTER TABLE [dbo].[VoucherActions]  WITH CHECK ADD  CONSTRAINT [FK_VoucherActions_VouchersWarehouse] FOREIGN KEY([VoucherId])
REFERENCES [dbo].[Vouchers] ([VoucherId])
GO
ALTER TABLE [dbo].[VoucherActions] CHECK CONSTRAINT [FK_VoucherActions_VouchersWarehouse]
GO
ALTER TABLE [dbo].[Vouchers]  WITH CHECK ADD  CONSTRAINT [FK_VouchersWarehouse_Vouchers] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Products] ([ProductId])
GO
ALTER TABLE [dbo].[Vouchers] CHECK CONSTRAINT [FK_VouchersWarehouse_Vouchers]
GO
USE [master]
GO
ALTER DATABASE [CrootieDB] SET  READ_WRITE 
GO

1	ألعاب	Games	434240	110C0C	Gaming.png	1
2	إنترنت	INTERNET	F58523	FFCB9C	Internet.png	1
3	إتصالات	Telecom	8A2686	51104F	Telelcom.png	1
4	كروت انلاين	Online Cards	0C0C0C	531919	Online_Card.png	1