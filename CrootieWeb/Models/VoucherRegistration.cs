﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models
{
    public partial class VoucherRegistration
    {
        public VoucherRegistration()
        {
            Vouchers = new HashSet<Voucher>();
        }

        public int RegistrationId { get; set; }
        public int ProductId { get; set; }
        public byte[] VouchersList { get; set; }
        public string Description { get; set; }
        public byte Status { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public virtual User CreatedByNavigation { get; set; }
        public virtual User ModifiedByNavigation { get; set; }
        public virtual Product Product { get; set; }
        public virtual ICollection<Voucher> Vouchers { get; set; }
    }
}
