﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models
{
    public partial class PermissionRole
    {
        public PermissionRole()
        {
            BuisnessUsers = new HashSet<BuisnessUser>();
            PermissionRoleDetails = new HashSet<PermissionRoleDetail>();
            Users = new HashSet<User>();
        }

        public byte PermissionRoleId { get; set; }
        public string PermissionRoleName { get; set; }
        public byte Type { get; set; }
        public byte Status { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public virtual User CreatedByNavigation { get; set; }
        public virtual User ModifiedByNavigation { get; set; }
        public virtual ICollection<BuisnessUser> BuisnessUsers { get; set; }
        public virtual ICollection<PermissionRoleDetail> PermissionRoleDetails { get; set; }
        public virtual ICollection<User> Users { get; set; }
    }
}
