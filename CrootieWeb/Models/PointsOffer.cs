﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models
{
    public partial class PointsOffer
    {
        public PointsOffer()
        {
            RedeemPoints = new HashSet<RedeemPoint>();
        }

        public int PointsOfferId { get; set; }
        public int OfferId { get; set; }
        public int ProductId { get; set; }
        public short PointsPrice { get; set; }
        public short Quantity { get; set; }
        public short CurrentQuantity { get; set; }

        public virtual Offer Offer { get; set; }
        public virtual Product Product { get; set; }
        public virtual ICollection<RedeemPoint> RedeemPoints { get; set; }
    }
}
