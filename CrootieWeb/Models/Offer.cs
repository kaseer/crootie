﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models
{
    public partial class Offer
    {
        public Offer()
        {
            PointsOffers = new HashSet<PointsOffer>();
            ProductOffers = new HashSet<ProductOffer>();
        }

        public int OfferId { get; set; }
        public string Title { get; set; }
        public byte OfferType { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public byte Status { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public virtual User CreatedByNavigation { get; set; }
        public virtual User ModifiedByNavigation { get; set; }
        public virtual ICollection<PointsOffer> PointsOffers { get; set; }
        public virtual ICollection<ProductOffer> ProductOffers { get; set; }
    }
}
