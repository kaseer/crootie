﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models
{
    public partial class Habaibe
    {
        public Habaibe()
        {
            Gifts = new HashSet<Gift>();
        }

        public int HabaibeId { get; set; }
        public int CustomerId { get; set; }
        public int HabaibeCustomerId { get; set; }
        public byte Status { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual Customer HabaibeCustomer { get; set; }
        public virtual ICollection<Gift> Gifts { get; set; }
    }
}
