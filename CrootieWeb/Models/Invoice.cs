﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models
{
    public partial class Invoice
    {
        public Invoice()
        {
            CustomerVouchers = new HashSet<CustomerVoucher>();
            InvoiceDetails = new HashSet<InvoiceDetail>();
        }

        public int InvoiceId { get; set; }
        public string InvoiceNo { get; set; }
        public decimal Total { get; set; }
        public int PaymentId { get; set; }
        public decimal Fees { get; set; }
        public DateTime CreatedOn { get; set; }

        public virtual Payment Payment { get; set; }
        public virtual ICollection<CustomerVoucher> CustomerVouchers { get; set; }
        public virtual ICollection<InvoiceDetail> InvoiceDetails { get; set; }
    }
}
