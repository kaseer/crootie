﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models
{
    public partial class Customer
    {
        public Customer()
        {
            BuisnessEmployees = new HashSet<BuisnessEmployee>();
            CustomerVouchers = new HashSet<CustomerVoucher>();
            Favorites = new HashSet<Favorite>();
            GiftSentByNavigations = new HashSet<Gift>();
            GiftSentToNavigations = new HashSet<Gift>();
            HabaibeCustomers = new HashSet<Habaibe>();
            HabaibeHabaibeCustomers = new HashSet<Habaibe>();
            OwnedVouchers = new HashSet<OwnedVoucher>();
            RedeemPoints = new HashSet<RedeemPoint>();
            Tickets = new HashSet<Ticket>();
        }

        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string NickName { get; set; }
        public string PhoneNo { get; set; }
        public string Pincode { get; set; }
        public string Password { get; set; }
        public string VerificationCode { get; set; }
        public DateTime? CodeGenerationTime { get; set; }
        public bool EmailConfirmed { get; set; }
        public byte Status { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? DeactivateOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public virtual User ModifiedByNavigation { get; set; }
        public virtual ICollection<BuisnessEmployee> BuisnessEmployees { get; set; }
        public virtual ICollection<CustomerVoucher> CustomerVouchers { get; set; }
        public virtual ICollection<Favorite> Favorites { get; set; }
        public virtual ICollection<Gift> GiftSentByNavigations { get; set; }
        public virtual ICollection<Gift> GiftSentToNavigations { get; set; }
        public virtual ICollection<Habaibe> HabaibeCustomers { get; set; }
        public virtual ICollection<Habaibe> HabaibeHabaibeCustomers { get; set; }
        public virtual ICollection<OwnedVoucher> OwnedVouchers { get; set; }
        public virtual ICollection<RedeemPoint> RedeemPoints { get; set; }
        public virtual ICollection<Ticket> Tickets { get; set; }
    }
}
