﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models
{
    public partial class Product
    {
        public Product()
        {
            BuisnessInvoiceDetails = new HashSet<BuisnessInvoiceDetail>();
            Favorites = new HashSet<Favorite>();
            InvoiceDetails = new HashSet<InvoiceDetail>();
            PointsOffers = new HashSet<PointsOffer>();
            ProductOffers = new HashSet<ProductOffer>();
            PurchassDetails = new HashSet<PurchassDetail>();
            VoucherRegistrations = new HashSet<VoucherRegistration>();
            Vouchers = new HashSet<Voucher>();
        }

        public int ProductId { get; set; }
        public int CompanyId { get; set; }
        public string PriceDescription { get; set; }
        public string Description { get; set; }
        public decimal Cost { get; set; }
        public decimal Price { get; set; }
        public short Currency { get; set; }
        public byte Status { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public virtual Company Company { get; set; }
        public virtual User CreatedByNavigation { get; set; }
        public virtual User ModifiedByNavigation { get; set; }
        public virtual ICollection<BuisnessInvoiceDetail> BuisnessInvoiceDetails { get; set; }
        public virtual ICollection<Favorite> Favorites { get; set; }
        public virtual ICollection<InvoiceDetail> InvoiceDetails { get; set; }
        public virtual ICollection<PointsOffer> PointsOffers { get; set; }
        public virtual ICollection<ProductOffer> ProductOffers { get; set; }
        public virtual ICollection<PurchassDetail> PurchassDetails { get; set; }
        public virtual ICollection<VoucherRegistration> VoucherRegistrations { get; set; }
        public virtual ICollection<Voucher> Vouchers { get; set; }
    }
}
