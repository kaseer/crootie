﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models
{
    public partial class ProductOffer
    {
        public int ProductOfferId { get; set; }
        public int OfferId { get; set; }
        public int ProductId { get; set; }
        public short Discount { get; set; }
        public byte Status { get; set; }

        public virtual Offer Offer { get; set; }
        public virtual Product Product { get; set; }
    }
}
