﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models
{
    public partial class Ticket
    {
        public Ticket()
        {
            TicketDetails = new HashSet<TicketDetail>();
        }

        public int TicketId { get; set; }
        public string TicketNo { get; set; }
        public int CustomerId { get; set; }
        public string Subject { get; set; }
        public decimal? Rating { get; set; }
        public byte Status { get; set; }
        public DateTime CreatedOn { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual ICollection<TicketDetail> TicketDetails { get; set; }
    }
}
