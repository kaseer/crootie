﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models
{
    public partial class VoucherAction
    {
        public int VoucherId { get; set; }
        public short ActionId { get; set; }
        public int CustomerId { get; set; }
        public DateTime CreatedOn { get; set; }

        public virtual Action Action { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual Voucher Voucher { get; set; }
    }
}
