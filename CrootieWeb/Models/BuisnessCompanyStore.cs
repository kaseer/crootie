﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models
{
    public partial class BuisnessCompanyStore
    {
        public int VoucherId { get; set; }
        public int CompanyId { get; set; }
        public byte Status { get; set; }
        public int? ModifiedByCompanyUser { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public virtual BuisnessCompany Company { get; set; }
        public virtual BuisnessUser ModifiedByCompanyUserNavigation { get; set; }
        public virtual Voucher Voucher { get; set; }
    }
}
