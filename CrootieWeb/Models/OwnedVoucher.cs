﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models
{
    public partial class OwnedVoucher
    {
        public int CustomerId { get; set; }
        public int VoucherId { get; set; }
        public DateTime? UsedOn { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual Voucher Voucher { get; set; }
    }
}
