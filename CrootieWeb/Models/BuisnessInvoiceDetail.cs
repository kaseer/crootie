﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models
{
    public partial class BuisnessInvoiceDetail
    {
        public int InvoiceDetailsId { get; set; }
        public int InvoiceId { get; set; }
        public int ProductId { get; set; }
        public short Quantity { get; set; }
        public decimal Price { get; set; }

        public virtual BiusnessInvoice Invoice { get; set; }
        public virtual Product Product { get; set; }
    }
}
