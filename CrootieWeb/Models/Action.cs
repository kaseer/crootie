﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models
{
    public partial class Action
    {
        public short ActionId { get; set; }
        public string ActionName { get; set; }
        public byte Status { get; set; }
    }
}
