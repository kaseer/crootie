﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models
{
    public partial class PermissionRoleDetail
    {
        public byte PermissionRoleId { get; set; }
        public int PermissionId { get; set; }

        public virtual Permission Permission { get; set; }
        public virtual PermissionRole PermissionRole { get; set; }
    }
}
