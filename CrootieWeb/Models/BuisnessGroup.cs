﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models
{
    public partial class BuisnessGroup
    {
        public BuisnessGroup()
        {
            BuisnessGroupMembers = new HashSet<BuisnessGroupMember>();
        }

        public int GroupId { get; set; }
        public string GroupName { get; set; }
        public int CompanyId { get; set; }
        public byte Status { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public virtual Company Company { get; set; }
        public virtual User CreatedByNavigation { get; set; }
        public virtual User ModifiedByNavigation { get; set; }
        public virtual ICollection<BuisnessGroupMember> BuisnessGroupMembers { get; set; }
    }
}
