﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models
{
    public partial class MoneyTransaction
    {
        public int MoneyTransactionId { get; set; }
        public int ToCompanyId { get; set; }
        public byte Type { get; set; }
        public decimal Amount { get; set; }
        public int ReasonId { get; set; }
        public string Description { get; set; }
        public byte Status { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public virtual User CreatedByNavigation { get; set; }
        public virtual User ModifiedByNavigation { get; set; }
        public virtual MoneyTransactionReason Reason { get; set; }
        public virtual BuisnessCompany ToCompany { get; set; }
    }
}
