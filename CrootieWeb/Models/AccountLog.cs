﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models
{
    public partial class AccountLog
    {
        public short ActionId { get; set; }
        public int CustomerId { get; set; }
        public DateTime CreatedOn { get; set; }

        public virtual Action Action { get; set; }
        public virtual Customer Customer { get; set; }
    }
}
