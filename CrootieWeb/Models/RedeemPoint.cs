﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models
{
    public partial class RedeemPoint
    {
        public int RedeemPointsId { get; set; }
        public int CustomerId { get; set; }
        public int PointsOfferId { get; set; }
        public short PointsAmount { get; set; }
        public DateTime CreatedOn { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual PointsOffer PointsOffer { get; set; }
    }
}
