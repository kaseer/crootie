﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models
{
    public partial class Voucher
    {
        public Voucher()
        {
            BuisnessCompanyStores = new HashSet<BuisnessCompanyStore>();
            BuisnessVouchers = new HashSet<BuisnessVoucher>();
            Gifts = new HashSet<Gift>();
            OwnedVouchers = new HashSet<OwnedVoucher>();
        }

        public int VoucherId { get; set; }
        public int RegistrationId { get; set; }
        public long SerialNo { get; set; }
        public int ProductId { get; set; }
        public string VoucherNo { get; set; }
        public byte ExpiredOn { get; set; }
        public byte Status { get; set; }

        public virtual Product Product { get; set; }
        public virtual VoucherRegistration Registration { get; set; }
        public virtual CustomerVoucher CustomerVoucher { get; set; }
        public virtual ICollection<BuisnessCompanyStore> BuisnessCompanyStores { get; set; }
        public virtual ICollection<BuisnessVoucher> BuisnessVouchers { get; set; }
        public virtual ICollection<Gift> Gifts { get; set; }
        public virtual ICollection<OwnedVoucher> OwnedVouchers { get; set; }
    }
}
