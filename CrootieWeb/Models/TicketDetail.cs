﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models
{
    public partial class TicketDetail
    {
        public int TicketDetailsId { get; set; }
        public int TicketId { get; set; }
        public string Message { get; set; }
        public DateTime SentOn { get; set; }
        public string ReplyMessage { get; set; }
        public int? RepliedBy { get; set; }
        public DateTime? RepliedOn { get; set; }
        public string Attachment { get; set; }

        public virtual User RepliedByNavigation { get; set; }
        public virtual Ticket Ticket { get; set; }
    }
}
