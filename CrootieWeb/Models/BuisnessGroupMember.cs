﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models
{
    public partial class BuisnessGroupMember
    {
        public int BuisnessEmployeeId { get; set; }
        public int GroupId { get; set; }

        public virtual BuisnessEmployee BuisnessEmployee { get; set; }
        public virtual BuisnessGroup Group { get; set; }
    }
}
