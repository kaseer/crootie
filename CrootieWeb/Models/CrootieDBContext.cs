﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace Models
{
    public partial class CrootieDBContext : DbContext
    {
        public CrootieDBContext()
        {
        }

        public CrootieDBContext(DbContextOptions<CrootieDBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AccountLog> AccountLogs { get; set; }
        public virtual DbSet<Action> Actions { get; set; }
        public virtual DbSet<BiusnessInvoice> BiusnessInvoices { get; set; }
        public virtual DbSet<BuisnessCompany> BuisnessCompanies { get; set; }
        public virtual DbSet<BuisnessCompanyStore> BuisnessCompanyStores { get; set; }
        public virtual DbSet<BuisnessEmployee> BuisnessEmployees { get; set; }
        public virtual DbSet<BuisnessEmployeesDetail> BuisnessEmployeesDetails { get; set; }
        public virtual DbSet<BuisnessGroup> BuisnessGroups { get; set; }
        public virtual DbSet<BuisnessGroupMember> BuisnessGroupMembers { get; set; }
        public virtual DbSet<BuisnessInvoiceDetail> BuisnessInvoiceDetails { get; set; }
        public virtual DbSet<BuisnessUser> BuisnessUsers { get; set; }
        public virtual DbSet<BuisnessVoucher> BuisnessVouchers { get; set; }
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<Company> Companies { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<CustomerVoucher> CustomerVouchers { get; set; }
        public virtual DbSet<Faq> Faqs { get; set; }
        public virtual DbSet<Favorite> Favorites { get; set; }
        public virtual DbSet<Feature> Features { get; set; }
        public virtual DbSet<Gift> Gifts { get; set; }
        public virtual DbSet<Habaibe> Habaibes { get; set; }
        public virtual DbSet<Invoice> Invoices { get; set; }
        public virtual DbSet<InvoiceDetail> InvoiceDetails { get; set; }
        public virtual DbSet<MoneyTransaction> MoneyTransactions { get; set; }
        public virtual DbSet<MoneyTransactionReason> MoneyTransactionReasons { get; set; }
        public virtual DbSet<Offer> Offers { get; set; }
        public virtual DbSet<OwnedVoucher> OwnedVouchers { get; set; }
        public virtual DbSet<Payment> Payments { get; set; }
        public virtual DbSet<Permission> Permissions { get; set; }
        public virtual DbSet<PermissionRole> PermissionRoles { get; set; }
        public virtual DbSet<PermissionRoleDetail> PermissionRoleDetails { get; set; }
        public virtual DbSet<PointsOffer> PointsOffers { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<ProductOffer> ProductOffers { get; set; }
        public virtual DbSet<Purchass> Purchasses { get; set; }
        public virtual DbSet<PurchassDetail> PurchassDetails { get; set; }
        public virtual DbSet<RedeemPoint> RedeemPoints { get; set; }
        public virtual DbSet<Ticket> Tickets { get; set; }
        public virtual DbSet<TicketDetail> TicketDetails { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Voucher> Vouchers { get; set; }
        public virtual DbSet<VoucherAction> VoucherActions { get; set; }
        public virtual DbSet<VoucherRegistration> VoucherRegistrations { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=(LocalDB)\\MSSQLLocalDB;Database=CrootieDB;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<AccountLog>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.HasOne(d => d.Action)
                    .WithMany()
                    .HasForeignKey(d => d.ActionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_AccountLogs_Actions");

                entity.HasOne(d => d.Customer)
                    .WithMany()
                    .HasForeignKey(d => d.CustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_AccountLogs_Customers");
            });

            modelBuilder.Entity<Action>(entity =>
            {
                entity.Property(e => e.ActionId).ValueGeneratedNever();

                entity.Property(e => e.ActionName).HasMaxLength(50);

                entity.Property(e => e.Status).HasDefaultValueSql("((1))");
            });

            modelBuilder.Entity<BiusnessInvoice>(entity =>
            {
                entity.HasKey(e => e.InvoiceId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.InvoiceNo)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Total).HasColumnType("decimal(18, 0)");

                entity.HasOne(d => d.CreatedByCompanyUserNavigation)
                    .WithMany(p => p.BiusnessInvoices)
                    .HasForeignKey(d => d.CreatedByCompanyUser)
                    .HasConstraintName("FK_BiusnessInvoices_BuisnessUsers");
            });

            modelBuilder.Entity<BuisnessCompany>(entity =>
            {
                entity.Property(e => e.Balance).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.CompanyName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.ContractValidate).HasColumnType("datetime");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.FoundedOn).HasColumnType("datetime");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Status).HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.BuisnessCompanyCreatedByNavigations)
                    .HasForeignKey(d => d.CreatedBy)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_BuisnessCompanies_Users");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.BuisnessCompanyModifiedByNavigations)
                    .HasForeignKey(d => d.ModifiedBy)
                    .HasConstraintName("FK_BuisnessCompanies_Users1");
            });

            modelBuilder.Entity<BuisnessCompanyStore>(entity =>
            {
                entity.HasKey(e => new { e.VoucherId, e.CompanyId });

                entity.ToTable("BuisnessCompanyStore");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Status).HasDefaultValueSql("((1))");

                entity.HasOne(d => d.Company)
                    .WithMany(p => p.BuisnessCompanyStores)
                    .HasForeignKey(d => d.CompanyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_BuisnessCompanyStore_BuisnessCompanies");

                entity.HasOne(d => d.ModifiedByCompanyUserNavigation)
                    .WithMany(p => p.BuisnessCompanyStores)
                    .HasForeignKey(d => d.ModifiedByCompanyUser)
                    .HasConstraintName("FK_BuisnessCompanyStore_BuisnessUsers");

                entity.HasOne(d => d.Voucher)
                    .WithMany(p => p.BuisnessCompanyStores)
                    .HasForeignKey(d => d.VoucherId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_BuisnessCompanyStore_Vouchers");
            });

            modelBuilder.Entity<BuisnessEmployee>(entity =>
            {
                entity.HasIndex(e => e.PhoneNo, "IX_BuisnessEmployees")
                    .IsUnique();

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Status).HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.BuisnessEmployeeCreatedByNavigations)
                    .HasForeignKey(d => d.CreatedBy)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_BuisnessEmployees_Users");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.BuisnessEmployees)
                    .HasForeignKey(d => d.CustomerId)
                    .HasConstraintName("FK_BuisnessEmployees_Customers");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.BuisnessEmployeeModifiedByNavigations)
                    .HasForeignKey(d => d.ModifiedBy)
                    .HasConstraintName("FK_BuisnessEmployees_Users1");
            });

            modelBuilder.Entity<BuisnessEmployeesDetail>(entity =>
            {
                entity.HasKey(e => new { e.BuisnessCompanyId, e.BuisnessEmployeeId });

                entity.Property(e => e.Status).HasDefaultValueSql("((1))");

                entity.HasOne(d => d.BuisnessCompany)
                    .WithMany(p => p.BuisnessEmployeesDetails)
                    .HasForeignKey(d => d.BuisnessCompanyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_BuisnessEmployeesDetails_BuisnessCompanies");

                entity.HasOne(d => d.BuisnessEmployee)
                    .WithMany(p => p.BuisnessEmployeesDetails)
                    .HasForeignKey(d => d.BuisnessEmployeeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_BuisnessEmployeesDetails_BuisnessEmployees");
            });

            modelBuilder.Entity<BuisnessGroup>(entity =>
            {
                entity.HasKey(e => e.GroupId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.GroupName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Status).HasDefaultValueSql("((1))");

                entity.HasOne(d => d.Company)
                    .WithMany(p => p.BuisnessGroups)
                    .HasForeignKey(d => d.CompanyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_BuisnessGroups_Companies");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.BuisnessGroupCreatedByNavigations)
                    .HasForeignKey(d => d.CreatedBy)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_BuisnessGroups_Users");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.BuisnessGroupModifiedByNavigations)
                    .HasForeignKey(d => d.ModifiedBy)
                    .HasConstraintName("FK_BuisnessGroups_Users1");
            });

            modelBuilder.Entity<BuisnessGroupMember>(entity =>
            {
                entity.HasKey(e => new { e.BuisnessEmployeeId, e.GroupId })
                    .HasName("PK_GroupMembers");

                entity.HasOne(d => d.BuisnessEmployee)
                    .WithMany(p => p.BuisnessGroupMembers)
                    .HasForeignKey(d => d.BuisnessEmployeeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_GroupMembers_BuisnessEmployees");

                entity.HasOne(d => d.Group)
                    .WithMany(p => p.BuisnessGroupMembers)
                    .HasForeignKey(d => d.GroupId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_GroupMembers_GroupMembers");
            });

            modelBuilder.Entity<BuisnessInvoiceDetail>(entity =>
            {
                entity.HasKey(e => e.InvoiceDetailsId);

                entity.Property(e => e.Price).HasColumnType("decimal(5, 0)");

                entity.HasOne(d => d.Invoice)
                    .WithMany(p => p.BuisnessInvoiceDetails)
                    .HasForeignKey(d => d.InvoiceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_BuisnessInvoiceDetails_BiusnessInvoices");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.BuisnessInvoiceDetails)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_BuisnessInvoiceDetails_Products");
            });

            modelBuilder.Entity<BuisnessUser>(entity =>
            {
                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.FullName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Status).HasDefaultValueSql("((1))");

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Company)
                    .WithMany(p => p.BuisnessUsers)
                    .HasForeignKey(d => d.CompanyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_BuisnessUsers_BuisnessUsers");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.BuisnessUserCreatedByNavigations)
                    .HasForeignKey(d => d.CreatedBy)
                    .HasConstraintName("FK_BuisnessUsers_Users");

                entity.HasOne(d => d.CreatedByCompanyUserNavigation)
                    .WithMany(p => p.InverseCreatedByCompanyUserNavigation)
                    .HasForeignKey(d => d.CreatedByCompanyUser)
                    .HasConstraintName("FK_BuisnessUsers_BuisnessUsers1");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.BuisnessUserModifiedByNavigations)
                    .HasForeignKey(d => d.ModifiedBy)
                    .HasConstraintName("FK_BuisnessUsers_Users1");

                entity.HasOne(d => d.PermissionRole)
                    .WithMany(p => p.BuisnessUsers)
                    .HasForeignKey(d => d.PermissionRoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_BuisnessUsers_PermissionRoles");
            });

            modelBuilder.Entity<BuisnessVoucher>(entity =>
            {
                entity.HasKey(e => new { e.BuisnessCompanyId, e.BuisnessEmployeeId, e.VoucherId });

                entity.Property(e => e.CollectedOn).HasColumnType("datetime");

                entity.HasOne(d => d.BuisnessCompany)
                    .WithMany(p => p.BuisnessVouchers)
                    .HasForeignKey(d => d.BuisnessCompanyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_BuisnessVouchers_BuisnessCompanies");

                entity.HasOne(d => d.BuisnessEmployee)
                    .WithMany(p => p.BuisnessVouchers)
                    .HasForeignKey(d => d.BuisnessEmployeeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_BuisnessVouchers_BuisnessEmployees");

                entity.HasOne(d => d.Voucher)
                    .WithMany(p => p.BuisnessVouchers)
                    .HasForeignKey(d => d.VoucherId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_BuisnessVouchers_Vouchers");
            });

            modelBuilder.Entity<Category>(entity =>
            {
                entity.Property(e => e.CategoryId).ValueGeneratedNever();

                entity.Property(e => e.BottomColor)
                    .IsRequired()
                    .HasMaxLength(6)
                    .IsFixedLength(true);

                entity.Property(e => e.CategoryNameAr)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.CategoryNameEn)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Illistration)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Status).HasDefaultValueSql("((1))");

                entity.Property(e => e.TopColor)
                    .IsRequired()
                    .HasMaxLength(6)
                    .IsFixedLength(true);
            });

            modelBuilder.Entity<Company>(entity =>
            {
                entity.Property(e => e.BottomColor)
                    .IsRequired()
                    .HasMaxLength(6)
                    .IsFixedLength(true);

                entity.Property(e => e.CompanyNameAr)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CompanyNameEn)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Logo)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Status).HasDefaultValueSql("((1))");

                entity.Property(e => e.TopColor)
                    .IsRequired()
                    .HasMaxLength(6)
                    .IsFixedLength(true);

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.Companies)
                    .HasForeignKey(d => d.CategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Companies_Categories");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.CompanyCreatedByNavigations)
                    .HasForeignKey(d => d.CreatedBy)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Companies_Users");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.CompanyModifiedByNavigations)
                    .HasForeignKey(d => d.ModifiedBy)
                    .HasConstraintName("FK_Companies_Users1");
            });

            modelBuilder.Entity<Customer>(entity =>
            {
                entity.Property(e => e.CodeGenerationTime).HasColumnType("datetime");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.CustomerName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.DeactivateOn).HasColumnType("datetime");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.NickName).HasMaxLength(15);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneNo)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Pincode)
                    .IsRequired()
                    .HasMaxLength(4)
                    .HasColumnName("PINcode")
                    .IsFixedLength(true);

                entity.Property(e => e.VerificationCode)
                    .HasMaxLength(4)
                    .IsFixedLength(true);

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.Customers)
                    .HasForeignKey(d => d.ModifiedBy)
                    .HasConstraintName("FK_Customers_Users");
            });

            modelBuilder.Entity<CustomerVoucher>(entity =>
            {
                entity.HasIndex(e => e.VoucherId, "IX_CustomerVouchers")
                    .IsUnique();

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.CustomerVouchers)
                    .HasForeignKey(d => d.CustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CustomerVouchers_Customers");

                entity.HasOne(d => d.Invoice)
                    .WithMany(p => p.CustomerVouchers)
                    .HasForeignKey(d => d.InvoiceId)
                    .HasConstraintName("FK_CustomerVouchers_Invoices");

                entity.HasOne(d => d.Voucher)
                    .WithOne(p => p.CustomerVoucher)
                    .HasForeignKey<CustomerVoucher>(d => d.VoucherId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CustomerVouchers_VouchersWarehouse");
            });

            modelBuilder.Entity<Faq>(entity =>
            {
                entity.ToTable("Faq");

                entity.Property(e => e.Answer).IsRequired();

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Question)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Status).HasDefaultValueSql("((1))");
            });

            modelBuilder.Entity<Favorite>(entity =>
            {
                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.Favorites)
                    .HasForeignKey(d => d.CustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Favorites_Customers");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.Favorites)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Favorites_Vouchers");
            });

            modelBuilder.Entity<Feature>(entity =>
            {
                entity.Property(e => e.FeatureId).ValueGeneratedNever();

                entity.Property(e => e.FeatureName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Status).HasDefaultValueSql("((1))");

                entity.Property(e => e.Type).HasDefaultValueSql("((1))");
            });

            modelBuilder.Entity<Gift>(entity =>
            {
                entity.Property(e => e.GiftId).ValueGeneratedNever();

                entity.Property(e => e.CollectedOn).HasColumnType("datetime");

                entity.Property(e => e.NickName).HasMaxLength(50);

                entity.Property(e => e.PhoneNo)
                    .HasMaxLength(10)
                    .IsFixedLength(true);

                entity.Property(e => e.SentOn).HasColumnType("datetime");

                entity.HasOne(d => d.Habaibe)
                    .WithMany(p => p.Gifts)
                    .HasForeignKey(d => d.HabaibeId)
                    .HasConstraintName("FK_Gifts_Habaibe");

                entity.HasOne(d => d.SentByNavigation)
                    .WithMany(p => p.GiftSentByNavigations)
                    .HasForeignKey(d => d.SentBy)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Gifts_Customers");

                entity.HasOne(d => d.SentToNavigation)
                    .WithMany(p => p.GiftSentToNavigations)
                    .HasForeignKey(d => d.SentTo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Gifts_Customers1");

                entity.HasOne(d => d.Voucher)
                    .WithMany(p => p.Gifts)
                    .HasForeignKey(d => d.VoucherId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Gifts_VouchersWarehouse");
            });

            modelBuilder.Entity<Habaibe>(entity =>
            {
                entity.ToTable("Habaibe");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Status).HasDefaultValueSql("((1))");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.HabaibeCustomers)
                    .HasForeignKey(d => d.CustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Habaibe_Customers");

                entity.HasOne(d => d.HabaibeCustomer)
                    .WithMany(p => p.HabaibeHabaibeCustomers)
                    .HasForeignKey(d => d.HabaibeCustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Habaibe_Customers1");
            });

            modelBuilder.Entity<Invoice>(entity =>
            {
                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Fees).HasColumnType("decimal(5, 0)");

                entity.Property(e => e.InvoiceNo)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Total).HasColumnType("decimal(18, 0)");

                entity.HasOne(d => d.Payment)
                    .WithMany(p => p.Invoices)
                    .HasForeignKey(d => d.PaymentId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Invoices_Payments");
            });

            modelBuilder.Entity<InvoiceDetail>(entity =>
            {
                entity.HasKey(e => e.InvoiceDetailsId);

                entity.Property(e => e.Price).HasColumnType("decimal(5, 0)");

                entity.HasOne(d => d.Invoice)
                    .WithMany(p => p.InvoiceDetails)
                    .HasForeignKey(d => d.InvoiceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_InvoiceDetails_Invoices");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.InvoiceDetails)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_InvoiceDetails_Vouchers");
            });

            modelBuilder.Entity<MoneyTransaction>(entity =>
            {
                entity.Property(e => e.Amount).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Description).HasMaxLength(100);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Status).HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.MoneyTransactionCreatedByNavigations)
                    .HasForeignKey(d => d.CreatedBy)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MoneyTransactions_Users");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.MoneyTransactionModifiedByNavigations)
                    .HasForeignKey(d => d.ModifiedBy)
                    .HasConstraintName("FK_MoneyTransactions_Users1");

                entity.HasOne(d => d.Reason)
                    .WithMany(p => p.MoneyTransactions)
                    .HasForeignKey(d => d.ReasonId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MoneyTransactions_MoneyTransactionReasons");

                entity.HasOne(d => d.ToCompany)
                    .WithMany(p => p.MoneyTransactions)
                    .HasForeignKey(d => d.ToCompanyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MoneyTransactions_BuisnessCompanies");
            });

            modelBuilder.Entity<MoneyTransactionReason>(entity =>
            {
                entity.HasKey(e => e.ReasonId);

                entity.Property(e => e.ReasonId).ValueGeneratedNever();

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Offer>(entity =>
            {
                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Description).HasMaxLength(200);

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.Image)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.OfferCreatedByNavigations)
                    .HasForeignKey(d => d.CreatedBy)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Offers_Users");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.OfferModifiedByNavigations)
                    .HasForeignKey(d => d.ModifiedBy)
                    .HasConstraintName("FK_Offers_Users1");
            });

            modelBuilder.Entity<OwnedVoucher>(entity =>
            {
                entity.HasKey(e => new { e.CustomerId, e.VoucherId });

                entity.Property(e => e.UsedOn).HasColumnType("datetime");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.OwnedVouchers)
                    .HasForeignKey(d => d.CustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_OwnedVouchers_Customers");

                entity.HasOne(d => d.Voucher)
                    .WithMany(p => p.OwnedVouchers)
                    .HasForeignKey(d => d.VoucherId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_OwnedVouchers_Vouchers");
            });

            modelBuilder.Entity<Payment>(entity =>
            {
                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.PaymentName)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Status).HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.PaymentCreatedByNavigations)
                    .HasForeignKey(d => d.CreatedBy)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Payments_Users");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.PaymentModifiedByNavigations)
                    .HasForeignKey(d => d.ModifiedBy)
                    .HasConstraintName("FK_Payments_Users1");
            });

            modelBuilder.Entity<Permission>(entity =>
            {
                entity.Property(e => e.PermissionId).ValueGeneratedNever();

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.PermissionName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Status).HasDefaultValueSql("((1))");

                entity.HasOne(d => d.Feature)
                    .WithMany(p => p.Permissions)
                    .HasForeignKey(d => d.FeatureId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Permissions_Features");
            });

            modelBuilder.Entity<PermissionRole>(entity =>
            {
                entity.Property(e => e.PermissionRoleId).ValueGeneratedOnAdd();

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.PermissionRoleName)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Status).HasDefaultValueSql("((1))");

                entity.Property(e => e.Type).HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.PermissionRoleCreatedByNavigations)
                    .HasForeignKey(d => d.CreatedBy)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PermissionRoles_Users");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.PermissionRoleModifiedByNavigations)
                    .HasForeignKey(d => d.ModifiedBy)
                    .HasConstraintName("FK_PermissionRoles_Users1");
            });

            modelBuilder.Entity<PermissionRoleDetail>(entity =>
            {
                entity.HasKey(e => new { e.PermissionRoleId, e.PermissionId });

                entity.HasOne(d => d.Permission)
                    .WithMany(p => p.PermissionRoleDetails)
                    .HasForeignKey(d => d.PermissionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PermissionRoleDetails_Permissions");

                entity.HasOne(d => d.PermissionRole)
                    .WithMany(p => p.PermissionRoleDetails)
                    .HasForeignKey(d => d.PermissionRoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PermissionRoleDetails_PermissionRoles");
            });

            modelBuilder.Entity<PointsOffer>(entity =>
            {
                entity.HasOne(d => d.Offer)
                    .WithMany(p => p.PointsOffers)
                    .HasForeignKey(d => d.OfferId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PointsOffers_Offers");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.PointsOffers)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PointsOffers_Products");
            });

            modelBuilder.Entity<Product>(entity =>
            {
                entity.Property(e => e.Cost).HasColumnType("money");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Currency).HasDefaultValueSql("((1))");

                entity.Property(e => e.Description).HasMaxLength(30);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Price).HasColumnType("money");

                entity.Property(e => e.PriceDescription)
                    .IsRequired()
                    .HasMaxLength(10);

                entity.Property(e => e.Status).HasDefaultValueSql("((1))");

                entity.HasOne(d => d.Company)
                    .WithMany(p => p.Products)
                    .HasForeignKey(d => d.CompanyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Vouchers_Companies");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.ProductCreatedByNavigations)
                    .HasForeignKey(d => d.CreatedBy)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Products_Users");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.ProductModifiedByNavigations)
                    .HasForeignKey(d => d.ModifiedBy)
                    .HasConstraintName("FK_Products_Users1");
            });

            modelBuilder.Entity<ProductOffer>(entity =>
            {
                entity.HasOne(d => d.Offer)
                    .WithMany(p => p.ProductOffers)
                    .HasForeignKey(d => d.OfferId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ProductOffers_Offers");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.ProductOffers)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ProductOffers_Products");
            });

            modelBuilder.Entity<Purchass>(entity =>
            {
                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Description).HasMaxLength(100);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.PurchassDate).HasColumnType("datetime");

                entity.Property(e => e.PurchassNo)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Status).HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.PurchassCreatedByNavigations)
                    .HasForeignKey(d => d.CreatedBy)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Purchasses_Users");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.PurchassModifiedByNavigations)
                    .HasForeignKey(d => d.ModifiedBy)
                    .HasConstraintName("FK_Purchasses_Users1");
            });

            modelBuilder.Entity<PurchassDetail>(entity =>
            {
                entity.HasKey(e => e.PurchassDetailsId);

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.PurchassDetails)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PurchassDetails_Products");

                entity.HasOne(d => d.Purchass)
                    .WithMany(p => p.PurchassDetails)
                    .HasForeignKey(d => d.PurchassId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PurchassDetails_Purchasses");
            });

            modelBuilder.Entity<RedeemPoint>(entity =>
            {
                entity.HasKey(e => e.RedeemPointsId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.RedeemPoints)
                    .HasForeignKey(d => d.CustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_RedeemPoints_Customers");

                entity.HasOne(d => d.PointsOffer)
                    .WithMany(p => p.RedeemPoints)
                    .HasForeignKey(d => d.PointsOfferId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_RedeemPoints_PointsOffers");
            });

            modelBuilder.Entity<Ticket>(entity =>
            {
                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Rating).HasColumnType("decimal(5, 0)");

                entity.Property(e => e.Status).HasDefaultValueSql("((1))");

                entity.Property(e => e.Subject).HasMaxLength(50);

                entity.Property(e => e.TicketNo)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.Tickets)
                    .HasForeignKey(d => d.CustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Tickets_Customers");
            });

            modelBuilder.Entity<TicketDetail>(entity =>
            {
                entity.HasKey(e => e.TicketDetailsId);

                entity.Property(e => e.Attachment)
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.Message)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.Property(e => e.RepliedOn).HasColumnType("datetime");

                entity.Property(e => e.SentOn).HasColumnType("datetime");

                entity.HasOne(d => d.RepliedByNavigation)
                    .WithMany(p => p.TicketDetails)
                    .HasForeignKey(d => d.RepliedBy)
                    .HasConstraintName("FK_TicketDetails_Users");

                entity.HasOne(d => d.Ticket)
                    .WithMany(p => p.TicketDetails)
                    .HasForeignKey(d => d.TicketId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TicketDetails_Tickets");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Email)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FullName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.LoginName)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneNo)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsFixedLength(true);

                entity.Property(e => e.Status).HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.InverseCreatedByNavigation)
                    .HasForeignKey(d => d.CreatedBy)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Users_Users");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.InverseModifiedByNavigation)
                    .HasForeignKey(d => d.ModifiedBy)
                    .HasConstraintName("FK_Users_Users1");

                entity.HasOne(d => d.PermissionRole)
                    .WithMany(p => p.Users)
                    .HasForeignKey(d => d.PermissionRoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Users_PermissionRoles");
            });

            modelBuilder.Entity<Voucher>(entity =>
            {
                entity.HasIndex(e => e.VoucherNo, "IX_VouchersWarehouse")
                    .IsUnique();

                entity.HasIndex(e => e.SerialNo, "IX_VouchersWarehouse_1")
                    .IsUnique();

                entity.Property(e => e.ExpiredOn).HasDefaultValueSql("((1))");

                entity.Property(e => e.Status).HasDefaultValueSql("((1))");

                entity.Property(e => e.VoucherNo)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.Vouchers)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_VouchersWarehouse_Vouchers");

                entity.HasOne(d => d.Registration)
                    .WithMany(p => p.Vouchers)
                    .HasForeignKey(d => d.RegistrationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Vouchers_VoucherRegistrations");
            });

            modelBuilder.Entity<VoucherAction>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.HasOne(d => d.Action)
                    .WithMany()
                    .HasForeignKey(d => d.ActionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_VoucherActions_Actions");

                entity.HasOne(d => d.Customer)
                    .WithMany()
                    .HasForeignKey(d => d.CustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_VoucherActions_Customers");

                entity.HasOne(d => d.Voucher)
                    .WithMany()
                    .HasForeignKey(d => d.VoucherId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_VoucherActions_VouchersWarehouse");
            });

            modelBuilder.Entity<VoucherRegistration>(entity =>
            {
                entity.HasKey(e => e.RegistrationId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Description).HasMaxLength(50);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Status).HasDefaultValueSql("((1))");

                entity.Property(e => e.VouchersList).IsRequired();

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.VoucherRegistrationCreatedByNavigations)
                    .HasForeignKey(d => d.CreatedBy)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_VoucherRegistrations_Users");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.VoucherRegistrationModifiedByNavigations)
                    .HasForeignKey(d => d.ModifiedBy)
                    .HasConstraintName("FK_VoucherRegistrations_Users1");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.VoucherRegistrations)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_VoucherRegistrations_Products");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
