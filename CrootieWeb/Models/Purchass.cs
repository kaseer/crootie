﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models
{
    public partial class Purchass
    {
        public Purchass()
        {
            PurchassDetails = new HashSet<PurchassDetail>();
        }

        public int PurchassId { get; set; }
        public string PurchassNo { get; set; }
        public string Description { get; set; }
        public DateTime PurchassDate { get; set; }
        public byte Status { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public virtual User CreatedByNavigation { get; set; }
        public virtual User ModifiedByNavigation { get; set; }
        public virtual ICollection<PurchassDetail> PurchassDetails { get; set; }
    }
}
