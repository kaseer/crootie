﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models
{
    public partial class Payment
    {
        public Payment()
        {
            Invoices = new HashSet<Invoice>();
        }

        public int PaymentId { get; set; }
        public string PaymentName { get; set; }
        public byte Status { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public virtual User CreatedByNavigation { get; set; }
        public virtual User ModifiedByNavigation { get; set; }
        public virtual ICollection<Invoice> Invoices { get; set; }
    }
}
