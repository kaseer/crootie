﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models
{
    public partial class Favorite
    {
        public int FavoriteId { get; set; }
        public int ProductId { get; set; }
        public int CustomerId { get; set; }
        public DateTime CreatedOn { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual Product Product { get; set; }
    }
}
