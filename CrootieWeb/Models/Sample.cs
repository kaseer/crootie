﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models
{
    public partial class Sample
    {
        public short SampleId { get; set; }
        public string SampleName { get; set; }
        public string Description { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
    }
}
