﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models
{
    public partial class Category
    {
        public Category()
        {
            Companies = new HashSet<Company>();
        }

        public short CategoryId { get; set; }
        public string CategoryNameAr { get; set; }
        public string CategoryNameEn { get; set; }
        public string TopColor { get; set; }
        public string BottomColor { get; set; }
        public string Illistration { get; set; }
        public byte Status { get; set; }

        public virtual ICollection<Company> Companies { get; set; }
    }
}
