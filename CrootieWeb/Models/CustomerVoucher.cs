﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models
{
    public partial class CustomerVoucher
    {
        public int CustomerVoucherId { get; set; }
        public int VoucherId { get; set; }
        public int? InvoiceId { get; set; }
        public int CustomerId { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual Invoice Invoice { get; set; }
        public virtual Voucher Voucher { get; set; }
    }
}
