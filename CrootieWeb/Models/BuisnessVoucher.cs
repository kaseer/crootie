﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models
{
    public partial class BuisnessVoucher
    {
        public int BuisnessCompanyId { get; set; }
        public int BuisnessEmployeeId { get; set; }
        public int VoucherId { get; set; }
        public DateTime CollectedOn { get; set; }

        public virtual BuisnessCompany BuisnessCompany { get; set; }
        public virtual BuisnessEmployee BuisnessEmployee { get; set; }
        public virtual Voucher Voucher { get; set; }
    }
}
