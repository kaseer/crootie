﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models
{
    public partial class MoneyTransactionReason
    {
        public MoneyTransactionReason()
        {
            MoneyTransactions = new HashSet<MoneyTransaction>();
        }

        public int ReasonId { get; set; }
        public string Description { get; set; }

        public virtual ICollection<MoneyTransaction> MoneyTransactions { get; set; }
    }
}
