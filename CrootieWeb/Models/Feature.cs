﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models
{
    public partial class Feature
    {
        public Feature()
        {
            Permissions = new HashSet<Permission>();
        }

        public int FeatureId { get; set; }
        public byte Type { get; set; }
        public string FeatureName { get; set; }
        public byte Status { get; set; }

        public virtual ICollection<Permission> Permissions { get; set; }
    }
}
