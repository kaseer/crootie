﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models
{
    public partial class BuisnessEmployee
    {
        public BuisnessEmployee()
        {
            BuisnessEmployeesDetails = new HashSet<BuisnessEmployeesDetail>();
            BuisnessGroupMembers = new HashSet<BuisnessGroupMember>();
            BuisnessVouchers = new HashSet<BuisnessVoucher>();
        }

        public int BuisnessEmployeeId { get; set; }
        public int PhoneNo { get; set; }
        public int? CustomerId { get; set; }
        public byte Status { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public virtual User CreatedByNavigation { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual User ModifiedByNavigation { get; set; }
        public virtual ICollection<BuisnessEmployeesDetail> BuisnessEmployeesDetails { get; set; }
        public virtual ICollection<BuisnessGroupMember> BuisnessGroupMembers { get; set; }
        public virtual ICollection<BuisnessVoucher> BuisnessVouchers { get; set; }
    }
}
