﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models
{
    public partial class BuisnessCompany
    {
        public BuisnessCompany()
        {
            BuisnessCompanyStores = new HashSet<BuisnessCompanyStore>();
            BuisnessEmployeesDetails = new HashSet<BuisnessEmployeesDetail>();
            BuisnessVouchers = new HashSet<BuisnessVoucher>();
            MoneyTransactions = new HashSet<MoneyTransaction>();
        }

        public int BuisnessCompanyId { get; set; }
        public string CompanyName { get; set; }
        public decimal Balance { get; set; }
        public DateTime? FoundedOn { get; set; }
        public DateTime ContractValidate { get; set; }
        public byte Status { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public virtual User CreatedByNavigation { get; set; }
        public virtual User ModifiedByNavigation { get; set; }
        public virtual ICollection<BuisnessCompanyStore> BuisnessCompanyStores { get; set; }
        public virtual ICollection<BuisnessEmployeesDetail> BuisnessEmployeesDetails { get; set; }
        public virtual ICollection<BuisnessVoucher> BuisnessVouchers { get; set; }
        public virtual ICollection<MoneyTransaction> MoneyTransactions { get; set; }
    }
}
