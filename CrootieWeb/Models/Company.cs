﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models
{
    public partial class Company
    {
        public Company()
        {
            BuisnessGroups = new HashSet<BuisnessGroup>();
            BuisnessUsers = new HashSet<BuisnessUser>();
            Products = new HashSet<Product>();
        }

        public int CompanyId { get; set; }
        public string CompanyNameAr { get; set; }
        public string CompanyNameEn { get; set; }
        public short CategoryId { get; set; }
        public string TopColor { get; set; }
        public string BottomColor { get; set; }
        public byte Status { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public string Logo { get; set; }

        public virtual Category Category { get; set; }
        public virtual User CreatedByNavigation { get; set; }
        public virtual User ModifiedByNavigation { get; set; }
        public virtual ICollection<BuisnessGroup> BuisnessGroups { get; set; }
        public virtual ICollection<BuisnessUser> BuisnessUsers { get; set; }
        public virtual ICollection<Product> Products { get; set; }
    }
}
