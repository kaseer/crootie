﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models
{
    public partial class BiusnessInvoice
    {
        public BiusnessInvoice()
        {
            BuisnessInvoiceDetails = new HashSet<BuisnessInvoiceDetail>();
        }

        public int InvoiceId { get; set; }
        public string InvoiceNo { get; set; }
        public decimal Total { get; set; }
        public byte Status { get; set; }
        public int? CreatedByCompanyUser { get; set; }
        public DateTime CreatedOn { get; set; }

        public virtual BuisnessUser CreatedByCompanyUserNavigation { get; set; }
        public virtual ICollection<BuisnessInvoiceDetail> BuisnessInvoiceDetails { get; set; }
    }
}
