﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models
{
    public partial class Permission
    {
        public Permission()
        {
            PermissionRoleDetails = new HashSet<PermissionRoleDetail>();
        }

        public int PermissionId { get; set; }
        public int FeatureId { get; set; }
        public string PermissionName { get; set; }
        public string Code { get; set; }
        public byte Status { get; set; }

        public virtual Feature Feature { get; set; }
        public virtual ICollection<PermissionRoleDetail> PermissionRoleDetails { get; set; }
    }
}
