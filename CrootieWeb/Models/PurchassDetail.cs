﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models
{
    public partial class PurchassDetail
    {
        public int PurchassDetailsId { get; set; }
        public int PurchassId { get; set; }
        public int ProductId { get; set; }
        public short Quantity { get; set; }

        public virtual Product Product { get; set; }
        public virtual Purchass Purchass { get; set; }
    }
}
