﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models
{
    public partial class BuisnessUser
    {
        public BuisnessUser()
        {
            BiusnessInvoices = new HashSet<BiusnessInvoice>();
            BuisnessCompanyStores = new HashSet<BuisnessCompanyStore>();
            InverseCreatedByCompanyUserNavigation = new HashSet<BuisnessUser>();
        }

        public int BuisnessUserId { get; set; }
        public int CompanyId { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public bool Founder { get; set; }
        public byte PermissionRoleId { get; set; }
        public byte Status { get; set; }
        public int? CreatedByCompanyUser { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public virtual Company Company { get; set; }
        public virtual BuisnessUser CreatedByCompanyUserNavigation { get; set; }
        public virtual User CreatedByNavigation { get; set; }
        public virtual User ModifiedByNavigation { get; set; }
        public virtual PermissionRole PermissionRole { get; set; }
        public virtual ICollection<BiusnessInvoice> BiusnessInvoices { get; set; }
        public virtual ICollection<BuisnessCompanyStore> BuisnessCompanyStores { get; set; }
        public virtual ICollection<BuisnessUser> InverseCreatedByCompanyUserNavigation { get; set; }
    }
}
