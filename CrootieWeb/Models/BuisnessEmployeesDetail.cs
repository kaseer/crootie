﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models
{
    public partial class BuisnessEmployeesDetail
    {
        public int BuisnessCompanyId { get; set; }
        public int BuisnessEmployeeId { get; set; }
        public byte Status { get; set; }

        public virtual BuisnessCompany BuisnessCompany { get; set; }
        public virtual BuisnessEmployee BuisnessEmployee { get; set; }
    }
}
