﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models
{
    public partial class Gift
    {
        public int GiftId { get; set; }
        public int VoucherId { get; set; }
        public int SentBy { get; set; }
        public int SentTo { get; set; }
        public DateTime SentOn { get; set; }
        public DateTime? CollectedOn { get; set; }
        public int? HabaibeId { get; set; }
        public string PhoneNo { get; set; }
        public string NickName { get; set; }

        public virtual Habaibe Habaibe { get; set; }
        public virtual Customer SentByNavigation { get; set; }
        public virtual Customer SentToNavigation { get; set; }
        public virtual Voucher Voucher { get; set; }
    }
}
