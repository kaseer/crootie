﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models
{
    public partial class Headline
    {
        public string FirstIntro { get; set; }
        public string SecondIntro { get; set; }
        public string AboutUs { get; set; }
    }
}
