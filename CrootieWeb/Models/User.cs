﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models
{
    public partial class User
    {
        public User()
        {
            BuisnessCompanyCreatedByNavigations = new HashSet<BuisnessCompany>();
            BuisnessCompanyModifiedByNavigations = new HashSet<BuisnessCompany>();
            BuisnessEmployeeCreatedByNavigations = new HashSet<BuisnessEmployee>();
            BuisnessEmployeeModifiedByNavigations = new HashSet<BuisnessEmployee>();
            BuisnessGroupCreatedByNavigations = new HashSet<BuisnessGroup>();
            BuisnessGroupModifiedByNavigations = new HashSet<BuisnessGroup>();
            BuisnessUserCreatedByNavigations = new HashSet<BuisnessUser>();
            BuisnessUserModifiedByNavigations = new HashSet<BuisnessUser>();
            CompanyCreatedByNavigations = new HashSet<Company>();
            CompanyModifiedByNavigations = new HashSet<Company>();
            Customers = new HashSet<Customer>();
            InverseCreatedByNavigation = new HashSet<User>();
            InverseModifiedByNavigation = new HashSet<User>();
            MoneyTransactionCreatedByNavigations = new HashSet<MoneyTransaction>();
            MoneyTransactionModifiedByNavigations = new HashSet<MoneyTransaction>();
            OfferCreatedByNavigations = new HashSet<Offer>();
            OfferModifiedByNavigations = new HashSet<Offer>();
            PaymentCreatedByNavigations = new HashSet<Payment>();
            PaymentModifiedByNavigations = new HashSet<Payment>();
            PermissionRoleCreatedByNavigations = new HashSet<PermissionRole>();
            PermissionRoleModifiedByNavigations = new HashSet<PermissionRole>();
            ProductCreatedByNavigations = new HashSet<Product>();
            ProductModifiedByNavigations = new HashSet<Product>();
            PurchassCreatedByNavigations = new HashSet<Purchass>();
            PurchassModifiedByNavigations = new HashSet<Purchass>();
            TicketDetails = new HashSet<TicketDetail>();
            VoucherRegistrationCreatedByNavigations = new HashSet<VoucherRegistration>();
            VoucherRegistrationModifiedByNavigations = new HashSet<VoucherRegistration>();
        }

        public int UserId { get; set; }
        public string LoginName { get; set; }
        public string FullName { get; set; }
        public string PhoneNo { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public byte Status { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public byte PermissionRoleId { get; set; }

        public virtual User CreatedByNavigation { get; set; }
        public virtual User ModifiedByNavigation { get; set; }
        public virtual PermissionRole PermissionRole { get; set; }
        public virtual ICollection<BuisnessCompany> BuisnessCompanyCreatedByNavigations { get; set; }
        public virtual ICollection<BuisnessCompany> BuisnessCompanyModifiedByNavigations { get; set; }
        public virtual ICollection<BuisnessEmployee> BuisnessEmployeeCreatedByNavigations { get; set; }
        public virtual ICollection<BuisnessEmployee> BuisnessEmployeeModifiedByNavigations { get; set; }
        public virtual ICollection<BuisnessGroup> BuisnessGroupCreatedByNavigations { get; set; }
        public virtual ICollection<BuisnessGroup> BuisnessGroupModifiedByNavigations { get; set; }
        public virtual ICollection<BuisnessUser> BuisnessUserCreatedByNavigations { get; set; }
        public virtual ICollection<BuisnessUser> BuisnessUserModifiedByNavigations { get; set; }
        public virtual ICollection<Company> CompanyCreatedByNavigations { get; set; }
        public virtual ICollection<Company> CompanyModifiedByNavigations { get; set; }
        public virtual ICollection<Customer> Customers { get; set; }
        public virtual ICollection<User> InverseCreatedByNavigation { get; set; }
        public virtual ICollection<User> InverseModifiedByNavigation { get; set; }
        public virtual ICollection<MoneyTransaction> MoneyTransactionCreatedByNavigations { get; set; }
        public virtual ICollection<MoneyTransaction> MoneyTransactionModifiedByNavigations { get; set; }
        public virtual ICollection<Offer> OfferCreatedByNavigations { get; set; }
        public virtual ICollection<Offer> OfferModifiedByNavigations { get; set; }
        public virtual ICollection<Payment> PaymentCreatedByNavigations { get; set; }
        public virtual ICollection<Payment> PaymentModifiedByNavigations { get; set; }
        public virtual ICollection<PermissionRole> PermissionRoleCreatedByNavigations { get; set; }
        public virtual ICollection<PermissionRole> PermissionRoleModifiedByNavigations { get; set; }
        public virtual ICollection<Product> ProductCreatedByNavigations { get; set; }
        public virtual ICollection<Product> ProductModifiedByNavigations { get; set; }
        public virtual ICollection<Purchass> PurchassCreatedByNavigations { get; set; }
        public virtual ICollection<Purchass> PurchassModifiedByNavigations { get; set; }
        public virtual ICollection<TicketDetail> TicketDetails { get; set; }
        public virtual ICollection<VoucherRegistration> VoucherRegistrationCreatedByNavigations { get; set; }
        public virtual ICollection<VoucherRegistration> VoucherRegistrationModifiedByNavigations { get; set; }
    }
}
