﻿using FluentValidation;
using System;

namespace MobileApi.ViewModels
{
    public class SampleData
    {
        public string SampleName { get; set; }
        public string Description { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
    }

    public class SampleDataValidator : AbstractValidator<SampleData>
    {
        public SampleDataValidator()
        {
            RuleFor(x => x.SampleName).NotEmpty().WithMessage("Error SmapleName").WithErrorCode("111");
            RuleFor(x => x.Description).NotEmpty().WithMessage("Error Description").WithErrorCode("222");

            
        }

    }



}
