﻿using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace MobileApi.Hubs
{
    public class RequestLog: Hub
    {
        //public static void PostToClient(string data)
        //{
        //    try
        //    {
        //        var chat = GlobalHost.ConnectionManager.GetHubContext("Requestlog");
        //        if (chat != null)
        //            chat.Clients.All.postToClient(data);
        //    }
        //    catch
        //    {
        //    }
        //}
        public async Task TicketRequest(float newX, float newY)
        {
            await Clients.Others.SendAsync("ReceiveRequest", "name", "message");
        }
    }
}