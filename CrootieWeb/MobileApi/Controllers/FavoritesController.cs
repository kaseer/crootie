﻿using MobileApi.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Common;

namespace MobileApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FavoritesController : MainController
    {

        public FavoritesController(CrootieDBContext context) : base(context)
        {
        }
        // [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> getAll()
        {
            try
            {
                var favorites = await (from c in db.Favorites
                where c.Product.Status != Status.Deleted
                && c.CustomerId == CustomerId()
                select new {
                    c.Product.Company.TopColor,
                    c.Product.Company.BottomColor,
                    c.Product.Company.Logo,
                    c.Product.ProductId,
                    c.Product.PriceDescription,
                    c.Product.Price,
                    isFave = true,
                    quantity = 0
                }).ToListAsync();
                // return BadRequest("fsdfsdfsd");
                return Ok(favorites);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return StatusCode(500);
            }
        }
    
    }
}
