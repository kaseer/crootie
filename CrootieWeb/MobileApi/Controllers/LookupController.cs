﻿using MobileApi.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Common;

namespace MobileApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LookupController : MainController
    {
        
        public LookupController(CrootieDBContext context) :base(context)
        {
        }
        // [AllowAnonymous]
        [HttpGet("getCategories")]
        public async Task<IActionResult> getCategories()
        {
            try
            {
                var categories = await (from c in db.Categories
                where c.Status != Status.Deleted
                select new {
                    c.CategoryId,
                    c.CategoryNameAr,
                    c.CategoryNameEn,
                    c.Illistration,
                    c.TopColor,
                    c.BottomColor
                }).ToListAsync();
                // return BadRequest("fsdfsdfsd");
                return Ok(categories);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return StatusCode(500);
            }
        }

        [HttpGet("getCompanies/{categoryId}")]
        public async Task<IActionResult> getCompanies(int categoryId)
        {
            try
            {
                var companies = await (from c in db.Companies
                where c.Status != Status.Deleted
                && c.CategoryId == categoryId
                select new {
                    c.CompanyId,
                    c.Logo,
                    c.TopColor,
                    c.BottomColor
                }).ToListAsync();
                return Ok(companies);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return StatusCode(500);
            }
        }
    
        [HttpGet("getProducts/{companyId}")]
        public async Task<IActionResult> getProducts(int companyId)
        {
            try
            {
                var products = await (from c in db.Products
                where c.Status != Status.Deleted
                && c.CompanyId == companyId
                select new {
                    c.ProductId,
                    c.PriceDescription,
                    c.Price,
                    isFav = c.Favorites.Where(x=>x.CustomerId == CustomerId() && c.ProductId == x.ProductId).Any()
                }).ToListAsync();
                return Ok(products);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return StatusCode(500);
            }
        }

        public class ProductIds
        {
            public int [] productIds { get; set; }
        }
        [HttpPost("getCartProducts")]
        public async Task<IActionResult> getCartProducts([FromBody] int[] productIds)
        {
            try
            {
                var products = await (from c in db.Products
                where c.Status != Status.Deleted
                && productIds.Contains(c.ProductId)
                select new {
                    c.Company.TopColor,
                    c.Company.BottomColor,
                    c.ProductId,
                    c.Company.Logo,
                    c.PriceDescription,
                    c.Price,
                    Quantity = 0
                }).ToListAsync();
                return Ok(products);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return StatusCode(500);
            }
        }
        [HttpGet("getAllProducts")]
        public async Task<IActionResult> getAllProducts()
        {
            try
            {
                var products = await (from c in db.Products
                where c.Status != Status.Deleted
                select new {
                    c.Company.TopColor,
                    c.Company.BottomColor,
                    c.Company.Logo,
                    c.ProductId,
                    c.PriceDescription,
                    c.Price,
                    quantity = 0,
                    isFav = false
                }).ToListAsync();
                return Ok(products);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return StatusCode(500);
            }
        }

    }
}
