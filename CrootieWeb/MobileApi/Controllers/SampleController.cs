﻿//using AutoMapper;
//using Common;
//using MobileApi.ViewModels;
//using Microsoft.AspNetCore.Http;
//using Microsoft.AspNetCore.Mvc;
//using Microsoft.EntityFrameworkCore;
//using Models;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;

//namespace MobileApi.Controllers
//{
//    [Route("api/[controller]")]
//    public class SampleController : MainController<Sample>
//    {
//        public SampleController(TestContext db, IMapper mapper) :base(db,mapper)
//        {
//        }
//        [HttpGet]
//        public async Task<IActionResult> Get([FromQuery] Common.Pagination Pagination)
//        {
//            try
//            {
//                var samples = GetByPagination(null,Pagination).Select(c => new {
//                    c.SampleId,
//                    c.SampleName,
//                    c.Description,
//                    c.CreatedBy,
//                    CreatedOn = c.CreatedOn.Value.ToString("yyyy/MM/dd")
//                });
//                var list = await samples.ToListAsync();
//                var totalItems = GetCount(x => x == x);
//                return Ok(new { StatusCode = 1, list, totalItems });
//            }
//            catch (Exception ex)
//            {
//                return StatusCode(500, new { StatusCode = 999999, message = AppMessages.ErrorMessages.ServerError });
//            }

//        }


//        [HttpGet("{id}")]
//        public async Task<IActionResult> GetDetails(int Id)
//        {
//            try
//            {
//                var data = await GetByCondition(c=> c.SampleId == Id).Select(c => c).SingleOrDefaultAsync(); 
//                return Ok(new { StatusCode = 1, data });
//            }
//            catch (Exception ex)
//            {
//                return StatusCode(500, new { StatusCode = 999999, message = AppMessages.ErrorMessages.ServerError });
//            }
//        }

//        [HttpGet("{id}/getForEdit")]
//        public async Task<IActionResult> GetForEdit(int Id)
//        {
//            try
//            {
//                var data = await GetByCondition(c => c.SampleId == Id).Select(c => new
//                {
//                    c.SampleName,
//                    c.Description,
//                    c.CreatedBy,
//                    c.CreatedOn
//                }).SingleOrDefaultAsync();
//                return Ok(new { StatusCode = 1, data });
//            }
//            catch (Exception ex)
//            {
//                return StatusCode(500, new { StatusCode = 999999, message = AppMessages.ErrorMessages.ServerError });
//            }
//        }

//        [HttpPut("{id}")]
//        public async Task<IActionResult> Edit(short Id, [FromBody] SampleData dataVM)
//        {
//            try
//            {
//                //var edit = await GetByCondition(c => c.SampleId == Id).Select(c => c).SingleOrDefaultAsync();

//                var edit = mapper.Map<Sample>(dataVM);
//                edit.SampleId = Id;
//                Update(edit);
//                await db.SaveChangesAsync();
//                var data = await GetByCondition(c => c.SampleId == Id).Select(c => new
//                {
//                    c.SampleId,
//                    c.SampleName,
//                    c.Description,
//                    c.CreatedBy,
//                    CreatedOn = c.CreatedOn.Value.ToString("yyyy/MM/dd")
//                }).SingleOrDefaultAsync();

//                return Ok(new { StatusCode = 1, data, message = AppMessages.SuccessMessages.EditedSuccessfuly });
//            }
//            catch (Exception ex)
//            {
//                return StatusCode(500, new { StatusCode = 999999, message = AppMessages.ErrorMessages.ServerError });
//            }
//        }

//        [HttpPost]
//        public async Task<IActionResult> Add([FromBody] SampleData dataVM)
//        {
//            try
//            {
//                var newsample = mapper.Map<Sample>(dataVM);
//                Add(newsample);
//                await db.SaveChangesAsync();
//                var addedItem = await GetByCondition(c => c.SampleId == newsample.SampleId)
//                    .Select(c => new {
//                        c.SampleId,
//                        c.SampleName,
//                        c.Description,
//                        c.CreatedBy,
//                        CreatedOn = c.CreatedOn.Value.ToString("yyyy/MM/dd")
//                    }).SingleOrDefaultAsync();

//                return Ok(new { StatusCode = 1, message = AppMessages.SuccessMessages.AddedSuccessfuly, addedItem });
//            }
//            catch (Exception ex)
//            {
//                return StatusCode(500, new { StatusCode = 999999, message = AppMessages.ErrorMessages.ServerError });
//            }
//        }

//        [HttpDelete("{id}")]
//        public async Task<IActionResult> Delete(int Id)
//        {
//            try
//            {
//                var delete = await GetByCondition(c => c.SampleId == Id).Select(c => c).SingleOrDefaultAsync();
//                delete.SampleName = Status.Deleted.ToString();
//                await db.SaveChangesAsync();
//                return Ok(new { StatusCode = 1, message = AppMessages.SuccessMessages.DeletedSuccessfuly });
//            }
//            catch (Exception ex)
//            {
//                return StatusCode(500, new { StatusCode = 999999, message = AppMessages.ErrorMessages.ServerError });
//            }
//        }
//    }
//}
