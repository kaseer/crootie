// v2.0
// import 'font-awesome/css/font-awesome.min.css' // Ensure you are using css-loader
// import colors from 'vuetify/lib/util/colors'

// Your own translation file
//import sv from './i18n/vuetify/sv'
// import ar from 'vuetify/src/locale/ar'
// import en from 'vuetify/src/locale/en'

const opts = {
    // breakpoint: {
    //     scrollBarWidth: 16,
    //     thresholds: {
    //         xs: 600,
    //         sm: 960,
    //         md: 1280,
    //         lg: 1920,
    //     },
    // },
    icons: {
        iconfont: 'mdiSvg',
        //values: {},
    },
    lang: {
        current: 'ar',
        //locales: { en },
        //t: undefined as any,
    },
    rtl: true,
    theme: {
        dark: false,
        default: 'light',
        disable: false,
        // options: {
        //     cspNonce: undefined,
        //     customProperties: undefined,
        //     minifyTheme: undefined,
        //     themeCache: {
        //         get: key => localStorage.getItem(key),
        //         set: (key, value) => localStorage.setItem(key, value),
        //     },
        // },
        themes: {
            light: {
                primary: '#9845ff',
                secondary: '#767676',
                accent: '#82B1FF',
                error: '#e20202',
                info: '#2196F3',
                success: '#4CAF50',
                warning: '#FB8C00',
            },
            dark: {
                primary: '#f8af61',
                secondary: '#ffffff',
                accent: '#FF4081',
                error: '#e20202',
                info: '#2196F3',
                success: '#4CAF50',
                warning: '#FB8C00',
            },
        },
        // application: {
        //     bar: 200,
        //     bottom: 200,
        //     footer: 200,
        //     insetFooter: 200,
        //     left: 200,
        //     right: 200,
        //     top: 200,
        // }
    },
}
export default opts