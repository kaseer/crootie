import Vue from 'vue'
import Vuex from 'vuex'
import samples from './modules/sample'
import companies from './modules/companies'
import loading from './modules/ui/loading'
import notify from './modules/ui/notify'

Vue.use(Vuex)

export const store = new Vuex.Store({

    modules: {
        // ui:{
        loading,
        notify,
        // },
        samples,
        companies
    }
})
