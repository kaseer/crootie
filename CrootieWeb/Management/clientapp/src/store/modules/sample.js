﻿import api from '@/api/index';
import helper from '@/shared/helper';

// import {eventBus} from '@/main.js'
// import sample from '@/api/controllers/sample'
const state = {
    samples: [],
    sample: {},
    sampleForEdit: {},
    totalItems: 0,
}
const mutations = {
    setSamples(state, payload) {
        state.samples = payload
    },
    setSample(state, payload) {
        state.sample = payload
    },
    setSampleForEdit(state, payload) {
        state.sampleForEdit = payload
    },
    add(state, payload) {
        state.samples.unshift(payload);
    },
    edit(state, { index, payload }) {
        console.log(index, payload)
        state.samples[index] = payload;
    },
    delete(state, index) {
        state.samples.splice(index, 1);
    },
    totalItems(state, payload) {
        state.totalItems = payload
    },
}
const getters = {
    index(id) {
        return state.samples.findIndex(x => x.sampleId == id);
    }
}
const actions = {
    async getAll({ commit }, params) {
        var query = JSON.stringify(params)
            .replace(/{/g, "").replace(/}/g, "")
            .replace(/"/g, "").replace(/:/g, "=")
            .replace(/,/g, "&");
        try {
            helper.startLoading();
            const res = await api.sample.getAll(query);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let list = res.data.list;
            let totalItems = res.data.totalItems;
            commit('setSamples', list);
            commit('totalItems', totalItems);
        }
        catch (err) {
            helper.catch(err)
        }
    },
    async getDetails({ commit }, id) {

        try {
            helper.startLoading();
            const res = await api.sample.getDetails(id);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let data = res.data.data;
            commit('setSample', data)
        }
        catch (err) {
            helper.catch(err)
        }
    },
    async getForEdit(_, id) {
        try {
            helper.startLoading();
            const res = await api.sample.getForEdit(id);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let data = res.data.data;
            return Promise.resolve(data);
        }
        catch (err) {
            helper.catch(err)
            return new Promise.reject(err);
        }
    },
    async add({ commit }, payload) {

        try {
            helper.startLoading();
            const res = await api.sample.add(payload);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let data = res.data.addedItem;
            let message = res.data.message;
            commit('add', data)
            helper.startNotify(message, "success");
        }
        catch (err) {
            helper.catch(err)
            return new Promise.reject(err);
        }

    },
    async edit({ commit }, { id, payload }) {
        try {
            helper.startLoading();
            const res = await api.sample.edit(id, payload);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            payload = res.data.data;
            let message = res.data.message;
            let index = getters.index(id);
            commit('edit', { index, payload})
            helper.startNotify(message, "success");
            //return new Promise.resolve();
        }
        catch (err) {
            helper.catch(err)
            //return new Promise.reject(err);
        }
    },
    async delete({ commit }, id) {
        try {
            helper.startLoading();
            const res = await api.sample.delete(id);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let message = res.data.message;
            let index = getters.index(id);
            commit('delete', index)
            helper.startNotify(message, "success");
        }
        catch (err) {
            helper.catch(err)
        }
    },
}
export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};