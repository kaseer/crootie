﻿import api from '@/api/index';
import helper from '@/shared/helper';

// import {eventBus} from '@/main.js'
// import company from '@/api/controllers/company'
const state = {
    companies: [
        {
            companyId:1,
            companyName:"test",
            category:"test",
            status:"",
            createdBy:"",
            createdOn:"",
        }
    ],
    company: {},
    companyForEdit: {},
    totalItems: 0,
}
const mutations = {
    setCompanies(state, payload) {
        state.companies = payload
    },
    setCompany(state, payload) {
        state.company = payload
    },
    setCompanyForEdit(state, payload) {
        state.companyForEdit = payload
    },
    add(state, payload) {
        state.companies.unshift(payload);
    },
    edit(state, { index, payload }) {
        console.log(index, payload)
        state.companies[index] = payload;
    },
    lock(state, index) {
        state.companies[index].status = 2;
    },
    unlock(state, index) {
        state.companies[index].status = 1;
    },
    delete(state, index) {
        state.companies.splice(index, 1);
    },
    totalItems(state, payload) {
        state.totalItems = payload
    },
}
const getters = {
    index(id) {
        return state.companies.findIndex(x => x.companyId == id);
    }
}
const actions = {
    async getAll({ commit }, params) {
        var query = JSON.stringify(params)
            .replace(/{/g, "").replace(/}/g, "")
            .replace(/"/g, "").replace(/:/g, "=")
            .replace(/,/g, "&");
        try {
            helper.startLoading();
            const res = await api.companies.getAll(query);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let list = res.data.list;
            let totalItems = res.data.totalItems;
            commit('setCompanies', list);
            commit('totalItems', totalItems);
        }
        catch (err) {
            helper.catch(err)
        }
    },
    async getDetails({ commit }, id) {

        try {
            helper.startLoading();
            const res = await api.companies.getDetails(id);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let data = res.data.data;
            commit('setCompany', data)
        }
        catch (err) {
            helper.catch(err)
        }
    },
    async getForEdit(_, id) {
        try {
            helper.startLoading();
            const res = await api.companies.getForEdit(id);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let data = res.data.data;
            return Promise.resolve(data);
        }
        catch (err) {
            helper.catch(err)
            return new Promise.reject(err);
        }
    },
    async add({ commit }, payload) {

        try {
            helper.startLoading();
            const res = await api.companies.add(payload);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let data = res.data.addedItem;
            let message = res.data.message;
            commit('add', data)
            helper.startNotify(message, "success");
        }
        catch (err) {
            helper.catch(err)
            return new Promise.reject(err);
        }

    },
    async edit({ commit }, { id, payload }) {
        try {
            helper.startLoading();
            const res = await api.companies.edit(id, payload);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            payload = res.data.data;
            let message = res.data.message;
            let index = getters.index(id);
            commit('edit', { index, payload})
            helper.startNotify(message, "success");
            //return new Promise.resolve();
        }
        catch (err) {
            helper.catch(err)
            //return new Promise.reject(err);
        }
    },
    async lock({ commit }, id) {
        try {
            helper.startLoading();
            const res = await api.companies.lock(id);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let message = res.data.message;
            let index = getters.index(id);
            commit('lock', index)
            helper.startNotify(message, "success");
        }
        catch (err) {
            console.log("sdfdsfsdfsd")
            helper.catch(err)
        }
    },
    async unlock({ commit }, id) {
        try {
            helper.startLoading();
            const res = await api.companies.unlock(id);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let message = res.data.message;
            let index = getters.index(id);
            commit('unlock', index)
            helper.startNotify(message, "success");
        }
        catch (err) {
            helper.catch(err)
        }
    },
    async delete({ commit }, id) {
        try {
            helper.startLoading();
            const res = await api.companies.delete(id);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let message = res.data.message;
            let index = getters.index(id);
            commit('delete', index)
            helper.startNotify(message, "success");
        }
        catch (err) {
            helper.catch(err)
        }
    },
}
export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};