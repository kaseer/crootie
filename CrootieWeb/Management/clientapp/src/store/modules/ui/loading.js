const state = {
    loading: false,
}

const getters = {
    loading: state => state.loading
}
const mutations = {
    setLoading(state, payload) {
        state.loading = payload
    },
}
const actions = {
    startLoading({ commit }) {
        console.log("load")
        commit('setLoading', true)
    },
    stopLoading({ commit }) {
        commit('setLoading', false)
    },
}

export default {
    namespaced: true,
    state,
    actions,
    mutations,
    getters
};