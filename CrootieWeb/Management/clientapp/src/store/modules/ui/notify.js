const state = {
    snackbar: false,
    alertDialog: false,
    message: "",
    color: ""
}

const mutations = {
    setSnackBar(state, payload) {
        state.snackbar = payload
    },
    setMessage(state, payload) {
        state.message = payload
    },
    setColor(state, payload) {
        state.color = payload
    },
    setAlertDialog(state, payload) {
        state.alertDialog = payload
    },
}
const actions = {
    openNotify({ commit }, { snackbar, message, color }) {
        commit('setSnackBar', snackbar);
        commit('setMessage', message);
        commit('setColor', color);
        setTimeout(() => commit('setSnackBar', false), 2000);
    },
    openAlert({ commit }, { dialog, message }) {
        console.log(message)
        commit('setAlertDialog', dialog);
        commit('setMessage', message);
    },
    closeAlert({ commit }) {
        commit('setAlertDialog', false);
        commit('setMessage', '');
    },
}

export default {
    namespaced: true,
    state,
    actions,
    mutations,
};