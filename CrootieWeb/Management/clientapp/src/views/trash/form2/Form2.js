import {  ValidationObserver, ValidationProvider } from 'vee-validate'

export default {
    created() {
        
    },
    components: {
        ValidationProvider,
        ValidationObserver,
    },
    data: ()=>({
        phoneNumber: '',
        name: '',
        email: '',
        select: null,
        checkbox: false,
        items: [
            'Item 1',
            'Item 2',
            'Item 3',
            'Item 4',
        ],
    }),
    methods: {
        clear() {
            this.name = ''
            this.phoneNumber = ''
            this.email = ''
            this.select = null
            this.checkbox = null
            this.$refs.observer.reset()
        },
    },
}