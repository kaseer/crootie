import Vue from 'vue'
import VueRouter from 'vue-router'
// import Table from '../views/table/Table.vue'
import Users from '../views/users/Index.vue'

Vue.use(VueRouter)

const routes = [
  
  {
    path: '/',
    name: 'لوحة البيانات',
    icon: 'mdi-view-dashboard',
    component:  () => import('../views/dashboard/Dashboard.vue')
  },
  {
    path: '/Sample',
    name: 'عينة',
    icon: 'mdi-cellphone-wireless',
    component: () => import('../views/sample/Index.vue')
  },
  {
    path: '/Companies',
    name: 'الشركات',
    icon: 'mdi-domain',
    component: () => import('../views/companies/Index.vue')
  },
  {
    path: '/Products',
    name: 'المنتجات',
    icon: 'mdi-watermark',
    component: () => import('../views/products/Index.vue')
  },
  {
    path: '/Vouchers',
    name: 'بطاقات الدفع المسجلة',
    icon: 'mdi-credit-card',
    component: () => import('../views/vouchers/Index.vue')
  },
  {
    path: '/Purchases',
    name: 'المشتريات',
    icon: 'mdi-bag-suitcase-outline',
    component: () => import('../views/purchases/Index.vue')
  },
  {
    path: '/Offers',
    name: 'العروض',
    icon: 'mdi-offer',
    component: () => import('../views/offers/Index.vue')
  },
  {
    path: '/Buisnesscompanies',
    name: 'الشركات الخاصة',
    icon: 'mdi-office-building',
    component: () => import('../views/buisnessCompanies/Index.vue')
  },
  
  {
    name: 'المستخدمين',
    path: '',
    icon: 'mdi-account-group',
    component: Users,
    
    children: [
      {
        path: '/Users',
        name: 'مستخدمي النظام',
        icon: 'mdi-account',
        component: () => import('../views/users/Index.vue')
      },
      {
        path: '/Buisnessusers',
        name: 'الشركات',
        icon: 'mdi-badge-account',
        component: () => import('../views/buisnessUsers/Index.vue')
      },
      
      {
        path: '/Customers',
        name: 'الزبائن',
        icon: 'mdi-account-box',
        component: () => import('../views/customers/Index.vue')
      },
    ]
  },
  
  {
    path: '/Support',
    name: 'الدعم',
    icon: 'mdi-face-agent',
    component: () => import('../views/support/Index.vue')
  },
  // {
  //   path: '/Paymentmethods',
  //   name: 'طرق الدفع',
  //   icon: 'mdi-calendar',
  //   component: () => import('../views/sample/Index.vue')
  // },
  
  
  {
    name: 'تقارير',
    path: '',
    icon: 'mdi-chart-box',
    component: null,
    children: [
      {
        path: '/specieltable',
        name: 'specieltable-Nested',
        icon: 'mdi-calendar',
        component: () => import('../views/specielTable/SpecielTable.vue')
      }
    ]
  },
  {
    name: 'الاعدادات',
    path: '',
    icon: 'mdi-cog',
    component: null,
    children: [
      {
        path: '/faq',
        name: 'معلومات التطبيق',
        icon: 'mdi-information-outline',
        component: () => import('../views/faq/Index.vue')
      },
      {
        path: '/roles',
        name: 'الصلاحيات',
        icon: 'mdi-water',
        component: () => import('../views/roles/Index.vue')
      }
    ]
  },
  {
    path: '/CurrencySettings',
    name: 'اعدادات العملة',
    icon: 'mdi-currency-usd',
    component: () => import('../views/currencySettings/Index.vue')
  },
  {
    path: '/Salestransactions',
    name: 'حركة المبيعات',
    icon: 'mdi-swap-horizontal',
    component: () => import('../views/salesTransactions/Index.vue')
  },
  // {
  //   path: '/Table',
  //   name: 'جداول',
  //   icon: 'mdi-calendar',
  //   component: Table
  // },
  // {
  //   path: '/Grid',
  //   name: 'Grid',
  //   icon: 'mdi-calendar',
  //   component: () => import('../views/grid/Grid.vue')
  // },
  // {
  //   path: '/Form',
  //   name: 'فورم 1',
  //   icon: 'mdi-calendar',
  //   component: () => import('../views/form/Form.vue')
  // },
  // {
  //   path: '/Form2',
  //   name: 'فورم 2',
  //   icon: 'mdi-calendar',
  //   component: () => import('../views/form2/Form2.vue')
  // },
  // {
  //   path: '/Form3',
  //   name: 'قورم 3',
  //   icon: 'mdi-calendar',
  //   component: () => import('../views/form3/Form3.vue')
  // },
  // {
  //   path: '/Icons',
  //   name: 'ايقونات',
  //   icon: 'mdi-calendar',
  //   component: () => import('../views/icons/Icons.vue')
  // },
  // {
  //   path: '/specieltable',
  //   name: 'جدول خاص',
  //   icon: 'mdi-calendar',
  //   component: () => import('../views/specielTable/SpecielTable.vue')
  // },
  // {
  //   path: '/Calendar',
  //   name: 'تقويم',
  //   icon: 'mdi-calendar',
  //   component: () => import('../views/calendar/Calendar.vue')
  // },
  // {
  //   name: 'Nested',
  //   path: '',
  //   icon: 'mdi-calendar',
  //   component: null,
  //   children: [
  //     {
  //       path: '/specieltable',
  //       name: 'specieltable-Nested',
  //       icon: 'mdi-calendar',
  //       component: () => import('../views/specielTable/SpecielTable.vue')
  //     }
  //   ]
  // }
]

const router = new VueRouter({
  routes
})

export default router
