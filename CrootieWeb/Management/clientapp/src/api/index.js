import sample from './controllers/sample.js'
import companies from './controllers/companies.js'

//axios.defaults.headers.common['X-CSRF-TOKEN'] = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

export default {
    sample,
    companies,
}