import axios from 'axios';

const api = "api/Sample"
export default {
    async getAll(params) {
        return axios.get(`${api}?${params}`);
    },
    async add(data) {
        return axios.post(`${api}`, data);
    },
    async getDetails(id) {
        return axios.get(`${api}/${id}`);
    },
    async getForEdit(id) {
        return axios.get(`${api}/${id}/getForEdit`);
    },
    async edit(id,data) {
        return axios.put(`${api}/${id}`, data);
    },
    // lock(id) {
    //     return axios.put(`${api}/${id}/lock`);
    // },
    // unLock(id) {
    //     return axios.put(`${api}/${id}/unLock`);
    // },
    async delete(id) {
        return axios.delete(`${api}/${id}`);
    },
}