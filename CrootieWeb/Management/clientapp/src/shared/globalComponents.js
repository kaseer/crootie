import Vue from 'vue'
import { ValidationObserver } from "vee-validate";
import { ValidationProvider } from 'vee-validate/dist/vee-validate.full.esm';
Vue.component('Dialog',
    () => import('../components/Dialog.vue')
);
Vue.component('AlertDialog',
    () => import('../components/AlertDialog.vue')
);
Vue.component('Notify',
    () => import('../components/Notify.vue')
);
Vue.component("ValidationProvider", ValidationProvider);
Vue.component("ValidationObserver", ValidationObserver);
