﻿import { store }  from '@/store'

export default {
    startNotify(message,color) {
        store.dispatch('notify/openNotify',{
            snackbar:true,
            message: message,
            color: color
        });
    },
    startLoading() {
        store.dispatch('loading/startLoading');
    },
    stopLoading() {
        store.dispatch('loading/stopLoading');
    },
    openAlert(message) {
        store.dispatch('notify/openAlert',{
            dialog:true,
            message: message,
        });
    },
    catch(err) {
        this.stopLoading();
        console.log(err.response.data.statusCode)
        if (!err.response.data.statusCode || err.response.data.statusCode == undefined) {
            this.openAlert('تعذر الاتصال بالخادم , يرجي المحاولة لاحقا');
            return;
        }
        let message = err.response.data.message + "  [" + err.response.data.statusCode + "]";
        this.openAlert(message);
    }
}
