import Vue from 'vue'
import vuetify from '@/plugins/vuetify'
import Layout from './layout/Layout.vue'
import router from './router'
import {store} from './store'
import './shared/globalComponents'
import './shared/globalValiditors'
import './styles/style.scss'
// export const eventBus = new Vue();
Vue.config.productionTip = false
// Vue.prototype.$loading = {status : false}

new Vue({
  router,
  store,
  vuetify,
  render: h => h(Layout)
}).$mount('#app')
