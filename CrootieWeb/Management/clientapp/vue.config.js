module.exports = {
  transpileDependencies: [
    'vuetify'
  ],
  outputDir: "../wwwroot",
    filenameHashing: false,
    devServer:{
        port:8080,
        https:false,
         proxy:{
             '^/api':{
                 target:"http://localhost:5001",
             }
         },
        
    },
    chainWebpack: config => {
        config.module
          .rule('images')
            .use('url-loader')
              .loader('url-loader')
              .tap(options => Object.assign(options, { limit: 100240 }))
    }
}
