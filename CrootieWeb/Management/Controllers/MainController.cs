﻿using AutoMapper;
using Common;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Management.Controllers
{
    [Route("api/[controller]")]
    public abstract class MainController<T> : ControllerBase  where T : class
    {
        public readonly TestContext db;
        public readonly IMapper mapper;

        public MainController(TestContext context, IMapper mapper)
        {
            db = context;
            this.mapper = mapper;
        }

        public IQueryable<T> GetAll()
        {
            return db.Set<T>().AsNoTracking();
        }

        public int GetCount(Expression<Func<T, bool>> expression)
        {
            return db.Set<T>().Where(expression).Count();
        }
        public IQueryable<T> GetByCondition(Expression<Func<T, bool>> expression)
        {
            return db.Set<T>().Where(expression).AsNoTracking();
        }

        public IQueryable<T> GetByPagination(Expression<Func<T, bool>> expression, Pagination Pagination)
        {
            var dd = db.Set<T>().AsNoTracking();
            if (expression is not null)
            {
                dd = dd.Where(expression);
            }
            return dd.Skip((Pagination.PageNo - 1) * Pagination.PageSize).Take(Pagination.PageSize);
        }

        public void Add(T entity)
        {
            db.Set<T>().Add(entity);
        }

        public void Update(T entity)
        {
            db.Set<T>().Update(entity);
            //db.SaveChanges();
        }

        public void Delete(T entity)
        {
            db.Set<T>().Remove(entity);
        }
    }
}
