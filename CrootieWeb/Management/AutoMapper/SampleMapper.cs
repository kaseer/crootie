﻿using AutoMapper;
using Management.ViewModels;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Management.AutoMapper
{
    public class SampleMapper : Profile
    {
        public SampleMapper()
        {
            CreateMap<SampleData, Sample>().ForSourceMember(x => x.SampleId , opt => opt.DoNotValidate());
        }
    }
}
