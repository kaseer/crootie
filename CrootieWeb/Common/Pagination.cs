﻿using System;

namespace Common
{
    public class Pagination
    {
        public short PageNo { get; set; }
        public short PageSize { get; set; }
    }
}
