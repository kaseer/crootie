﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class AppMessages
    {
        public class ErrorMessages
        {
            public static string ServerError = "حدث خطأ, يرجي التأكد من الإتصال بالخادم أو التحقق من الدعم الفني";
            public static string NotFoundError = "لم يتم العثور علي محتوي";
        }
        public class SuccessMessages
        {
            public static string AddedSuccessfuly = "تمت عملية الاضافة بنجاح";
            public static string EditedSuccessfuly = "تمت عملية التعديل بنجاح";
            public static string DeletedSuccessfuly = "تمت عملية الحذف بنجاح";
        }
    }
}
